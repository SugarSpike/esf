/* File : ErfCoreBase.i */

%define InitializeErfCore(interfacesDir,headerDir,headerDirRelativeToWrapper)

%include "std_string.i"

%rename(Equals) operator==;

#ifdef ESFCORE_EXPORTS
    #define ESFCORE_API __declspec(dllexport)
#else
    #define ESFCORE_API __declspec(dllimport)
#endif


//Windows interface hack for preprocessor macros
%include <windows.i>

//swig support for std -> std_list.i not included in base SWIG installation:
//Included in patch: http://sourceforge.net/p/swig/patches/134/
//It is far from perfect and that is why we ignore 302 warn
#pragma SWIG nowarn=302 //Warning(302): Identifier 'list' redefined (ignored),

%include "typemaps.i" 

//Unfortunetly exposing internal constructors as public is the only viable way of allowing inheritance of Erf classes
//from different namespaces
#define SWIG_SHARED_PTR_TYPEMAPS(CONST, TYPE...) SWIG_SHARED_PTR_TYPEMAPS_IMPLEMENTATION(public, public, CONST, TYPE)
//SWIG_CSBODY_PROXY(public, public, SWIGTYPE)
%include "std_shared_ptr.i"

%include "std_map.i" 
%include "std_list.i" 
%include "std_vector.i"
%include "exception.i"  

%rename(BaseEquals) operator=;

%typemap(ctype) time_t "time_t"
%typemap(imtype) time_t "long"
%typemap(cstype) time_t "long"
%typemap(out) time_t { 
	$result = (long)$1; 
}
%typemap(in) time_t {
	$1 = (time_t)$input;
}

%typemap(csin) time_t "$csinput"
%typemap(csout, excode=SWIGEXCODE) time_t {
    long ret = $imcall;$excode
    return ret;
  }

%ignore IsAsynchSupported;
%ignore HandleEmotionVectorAsynch; 
%ignore HandleEventAsynch;
%ignore UpdatePersonalityAsynch;
%ignore GetRaw();
%ignore Variant( boost::any* );
%ignore Variant(BoostAny* raw);
%ignore erf::BasicObject::operator==; 
%ignore SetGameConfiguration(GameConfigurationSP );
%ignore erf::EecEvent::operator <<; 
%ignore ertutils::PriorityList::remove;
%ignore erfutils::PriorityList::remove;
%ignore erfutils::PriorityList::removeByClassName;
%ignore erfutils::PriorityList::InsertIntoPriorityList;
%ignore erfutils::PriorityList::insert;
%ignore erfutils::PriorityList::removeByObjectName;
%ignore erfutils::PriorityList::contains(const PriorityRecord<T>&);
%ignore erfutils::PriorityRecord::compare;
%ignore erf::ErfLogger::GetLogger;

%exception {
    try {
        $action
    } catch(erf::ErfException erfEx) {
        SWIG_exception(SWIG_SystemError, erfEx.Message().c_str());
    } 
}



%typemap(cscode) erf::BasicObject %{
  public override bool Equals(Object obj) {
    bool equal = false;
    if (obj is $csclassname)
      equal = (obj as $csclassname).GetUniqueName().Equals(this.GetUniqueName());
    return equal;
  }
  
  public override int GetHashCode() {
     return this.swigCPtr.Handle.ToInt32();
  }
%}

%include "interfacesDir\ErfCoreCharacterData.i"
%include "interfacesDir\ErfCoreCharacterModel.i"
%include "interfacesDir\ErfCoreVariantCollection.i"


//Base
%shared_ptr(erf::BasicObject)
%shared_ptr(erf::Variant)
%shared_ptr(erf::VariantCollection)
%shared_ptr(erf::Personality)
%shared_ptr(erf::EecEvent)
%shared_ptr(erf::CharacterData)
%shared_ptr(erf::GameConfiguration)
%shared_ptr(erf::ErfContext)

//Sensors & Experts
%shared_ptr(erf::ExternalComponent)
%shared_ptr(erf::ExternalComponentConfiguration)
%shared_ptr(erf::ExternalExpert)

%shared_ptr(erf::EmotionExpert)
%shared_ptr(erf::MoodExpert)
%shared_ptr(erf::PsychologicalExpert)
%shared_ptr(erf::EmotionSensorReading)
%shared_ptr(erf::EmotionSensor)
%shared_ptr(erf::ExternalComponentLibrary)

//Emotions
%shared_ptr(erf::Emotion)

//Vectors
%shared_ptr(erf::EmotionVector)
%shared_ptr(erf::MoodVector)


//Models
%shared_ptr(erf::EmotionVectorFilter)
%shared_ptr(erf::EmotionModel)
%shared_ptr(erf::MoodModel)
%shared_ptr(erf::PsychologicalModel)
%shared_ptr(erf::CharacterModel)

//Filters
%shared_ptr(erf::ExternalFilter)


%typemap(csbody) erf::BasicObject %{
  private HandleRef swigCPtr;
  private bool swigCMemOwnBase;
  public String CSharpUniqueName { get; set; }

  public $csclassname(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwnBase = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
	CSharpUniqueName = this.GetUniqueName();
  }

  public static HandleRef getCPtr($csclassname obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }
%}

/*
%typemap(csbody) erf::BasicObject %{
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;
  public String CSharpUniqueName { get; set; }

  public BasicObject(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
	CSharpUniqueName = this.GetUniqueName();
  }

  public static HandleRef getCPtr(BasicObject obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }
%}
*/

%inline %{
	typedef unsigned __int8 uint8_t;
	#include "headerDirRelativeToWrapper\Common.h"
	using namespace std;

	//#include "boost/shared_ptr.hpp"
	//Enums
	#include "headerDirRelativeToWrapper\GameEnums.h"
	#include "headerDirRelativeToWrapper\EmotionEnums.h"
	#include "headerDirRelativeToWrapper\EmotionConstants.h"
	
	//Utils
	#include "headerDirRelativeToWrapper\PriorityList.h"
	#include "headerDirRelativeToWrapper\BasicObject.h"
	#include "headerDirRelativeToWrapper\Variant.h"
	#include "headerDirRelativeToWrapper\VariantCollection.h"
	#include "headerDirRelativeToWrapper\Personality.h"
	#include "headerDirRelativeToWrapper\GameConfiguration.h"
	#include "headerDirRelativeToWrapper\ErfException.h"
	#include "headerDirRelativeToWrapper\ErfContext.h"
	#include "headerDirRelativeToWrapper\ErfLogger.h"
	
	//Sensors
	#include "headerDirRelativeToWrapper\ExternalComponentEnums.h"
	#include "headerDirRelativeToWrapper\ExternalComponentConfiguration.h"
	#include "headerDirRelativeToWrapper\ErfSensorReadException.h"
	#include "headerDirRelativeToWrapper\ErfExternalComponentException.h"
	#include "headerDirRelativeToWrapper\EmotionSensorReading.h"
	#include "headerDirRelativeToWrapper\EmotionSensor.h"
	#include "headerDirRelativeToWrapper\ExternalComponentLibrary.h"
	
	#include "headerDirRelativeToWrapper\EecEvent.h"
	#include "headerDirRelativeToWrapper\EmotionVector.h"
	#include "headerDirRelativeToWrapper\MoodVector.h"
	#include "headerDirRelativeToWrapper\CharacterData.h"
	#include "headerDirRelativeToWrapper\EmotionVectorFilter.h"
	#include "headerDirRelativeToWrapper\EmotionModel.h"
	#include "headerDirRelativeToWrapper\MoodModel.h"
	#include "headerDirRelativeToWrapper\PsychologicalModel.h"
	#include "headerDirRelativeToWrapper\CharacterModel.h"

	//Experts
	#include "headerDirRelativeToWrapper\ExternalExpert.h"
	#include "headerDirRelativeToWrapper\EmotionExpert.h"
	#include "headerDirRelativeToWrapper\MoodExpert.h"
	#include "headerDirRelativeToWrapper\PsychologicalExpert.h"
	
	//Filters
	#include "headerDirRelativeToWrapper\ExternalFilter.h"

%}

//enums
%include "headerDir\GameEnums.h"
%include "headerDir\EmotionEnums.h"
%include "headerDir\CharacterModelEnums.h"
//%include "headerDir\EmotionConstants.h"

%include "headerDir\BasicObject.h"
%include "headerDir\Variant.h"
%include "headerDir\VariantCollection.h"
%include "headerDir\GameConfiguration.h"
%include "headerDir\PriorityList.h"
%include "headerDir\Personality.h"
%include "headerDir\ErfException.h"
%include "headerDir\ErfLogger.h"
//Sensors
%include "headerDir\ExternalComponentEnums.h"
%include "headerDir\ExternalComponent.h"
%include "headerDir\ExternalComponentConfiguration.h"
%include "headerDir\ErfSensorReadException.h"
%include "headerDir\ErfExternalComponentException.h"
%include "headerDir\EmotionSensorReading.h"
%include "headerDir\EmotionSensor.h"
%include "headerDir\ExternalComponentLibrary.h"

%include "headerDir\ErfContext.h"

%include "headerDir\EecEvent.h"
%include "headerDir\EmotionVector.h"
%include "headerDir\MoodVector.h"
%include "headerDir\CharacterData.h"
%include "headerDir\EmotionVectorFilter.h"
%include "headerDir\EmotionModel.h"
%include "headerDir\MoodModel.h"
%include "headerDir\PsychologicalModel.h"
%include "headerDir\CharacterModel.h"

//Experts
%include "headerDir\ExternalExpert.h"
//%include "headerDir\EmotionExpert.h"
//%include "headerDir\MoodExpert.h"
//%include "headerDir\PsychologicalExpert.h"


%include "headerDir\ExternalFilter.h"

%include "interfacesDir\ErfCoreVariant.i"

SWIG_STD_LIST_SPECIALIZE(EmotionVectorFilter, erf::EmotionVectorFilter* )
//std templates
namespace std {
   %template(IntegerVector) vector<int>;
   %template(FloatVector) vector<float>;
   %template(StringVector) vector<std::string >;
   %template(DoubleVector) vector<double >;
   %template(LongVector) vector<long >;
   
   //%template(EmotionFilterPriorityRecord) erfutils::PriorityRecord<erf::EmotionVectorFilter* >;
   %template(EmotionFilterPriorityList) erfutils::PriorityList<erf::EmotionVectorFilter* >;
   
   %template(EmotionModelVector) vector<erf::EmotionModel* >;
   %template(MoodModelVector) vector<erf::MoodModel* >;
   %template(PsychologicalModelVector) vector<erf::PsychologicalModel* >;
   %template(EmotionVectorFilterList) list<erf::EmotionVectorFilter* >;

   %template(StringToStringMap) map<string,string >;
   %template(StringToVariantMap) map<string ,erf::Variant* > ;
   %template(CharateristicsVector) vector< erf::GameCharateristics >;
   %template(ChallengesVector) vector< erf::GameChallenges >;
   %template(ComponentTypeVector) vector< erf::ComponentType >;
   %template(ExternalComponentVector) vector< erf::ExternalComponent* >;
   %template(EmotionSensorVector) vector< erf::EmotionSensor* >;
   %template(ExternalFilterVector) vector< erf::ExternalFilter* >;
   %template(ExternalExpertVector) vector< erf::ExternalExpert* >;
   %template(ExternalComponentLibraryVector) vector< erf::ExternalComponentLibrary* >;
   %template(EmotionSensorReadingVector) vector< erf::EmotionSensorReading* >;
   %template(EmotionVectorVector) vector<erf::EmotionVector* >;
   %template(MoodVectorVector) vector<erf::MoodVector* >;
}

%enddef