/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace Erf {

using System;
using System.Runtime.InteropServices;

public class EmotionFilterPriorityList : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal EmotionFilterPriorityList(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(EmotionFilterPriorityList obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~EmotionFilterPriorityList() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          erfCorePINVOKE.delete_EmotionFilterPriorityList(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public bool contains(int priority, EmotionVectorFilter val) {
    bool ret = erfCorePINVOKE.EmotionFilterPriorityList_contains(swigCPtr, priority, EmotionVectorFilter.getCPtr(val));
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public EmotionVectorFilterList getPrioritizedList() {
    EmotionVectorFilterList ret = new EmotionVectorFilterList(erfCorePINVOKE.EmotionFilterPriorityList_getPrioritizedList(swigCPtr), true);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public EmotionFilterPriorityList() : this(erfCorePINVOKE.new_EmotionFilterPriorityList(), true) {
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

}

}
