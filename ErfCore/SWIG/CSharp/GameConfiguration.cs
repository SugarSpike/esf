/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace Erf {

using System;
using System.Runtime.InteropServices;

public class GameConfiguration : BasicObject {
  private HandleRef swigCPtr;
  private bool swigCMemOwnDerived;

  public GameConfiguration(IntPtr cPtr, bool cMemoryOwn) : base(erfCorePINVOKE.GameConfiguration_SWIGSmartPtrUpcast(cPtr), true) {
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  public static HandleRef getCPtr(GameConfiguration obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~GameConfiguration() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwnDerived) {
          swigCMemOwnDerived = false;
          erfCorePINVOKE.delete_GameConfiguration(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public GameConfiguration() : this(erfCorePINVOKE.new_GameConfiguration__SWIG_0(), true) {
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public GameConfiguration(GameConfiguration other) : this(erfCorePINVOKE.new_GameConfiguration__SWIG_1(GameConfiguration.getCPtr(other)), true) {
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public GameConfiguration(string name, GameType gameType) : this(erfCorePINVOKE.new_GameConfiguration__SWIG_2(name, (int)gameType), true) {
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public GameType GetGameType() {
    GameType ret = (GameType)erfCorePINVOKE.GameConfiguration_GetGameType(swigCPtr);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void SetGameType(GameType gameType) {
    erfCorePINVOKE.GameConfiguration_SetGameType(swigCPtr, (int)gameType);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public void AddGameCharacteristic(GameCharateristics gameCharacteristic) {
    erfCorePINVOKE.GameConfiguration_AddGameCharacteristic(swigCPtr, (int)gameCharacteristic);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public void RemoveGameCharacteristic(GameCharateristics gameCharacteristic) {
    erfCorePINVOKE.GameConfiguration_RemoveGameCharacteristic(swigCPtr, (int)gameCharacteristic);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public CharateristicsVector GetGameCharacteristics() {
    CharateristicsVector ret = new CharateristicsVector(erfCorePINVOKE.GameConfiguration_GetGameCharacteristics(swigCPtr), false);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void AddGameChallenge(GameChallenges gameChallenge) {
    erfCorePINVOKE.GameConfiguration_AddGameChallenge(swigCPtr, (int)gameChallenge);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public void RemoveGameChallenge(GameChallenges gameChallenge) {
    erfCorePINVOKE.GameConfiguration_RemoveGameChallenge(swigCPtr, (int)gameChallenge);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public ChallengesVector GetGameChallenges() {
    ChallengesVector ret = new ChallengesVector(erfCorePINVOKE.GameConfiguration_GetGameChallenges(swigCPtr), false);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void SetGameName(string name) {
    erfCorePINVOKE.GameConfiguration_SetGameName(swigCPtr, name);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public string GetGameName() {
    string ret = erfCorePINVOKE.GameConfiguration_GetGameName(swigCPtr);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public GameConfiguration BaseEquals(GameConfiguration rhs) {
    GameConfiguration ret = new GameConfiguration(erfCorePINVOKE.GameConfiguration_BaseEquals(swigCPtr, GameConfiguration.getCPtr(rhs)), true);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

}

}
