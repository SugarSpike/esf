/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace Erf {

public enum SensorType {
  STD_UNDEF_SENSOR = 0,
  STD_HEART_RATE,
  STD_BVP,
  STD_SKIN_CONDUCTANCE,
  STD_EMG,
  EXT_SENSOR
}

}
