/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace Erf {

using System;
using System.Runtime.InteropServices;

public class MoodVector : EmotionVector {
  private HandleRef swigCPtr;
  private bool swigCMemOwnDerived;

  public MoodVector(IntPtr cPtr, bool cMemoryOwn) : base(erfCorePINVOKE.MoodVector_SWIGSmartPtrUpcast(cPtr), true) {
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  public static HandleRef getCPtr(MoodVector obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~MoodVector() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwnDerived) {
          swigCMemOwnDerived = false;
          erfCorePINVOKE.delete_MoodVector(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public MoodVector(int moodModelType) : this(erfCorePINVOKE.new_MoodVector__SWIG_0(moodModelType), true) {
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public MoodVector(int emotionModelType, long timeOfEvaluation) : this(erfCorePINVOKE.new_MoodVector__SWIG_1(emotionModelType, timeOfEvaluation), true) {
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public MoodVector(IntegerVector emotionModelTypes, long timeOfEvaluation) : this(erfCorePINVOKE.new_MoodVector__SWIG_2(IntegerVector.getCPtr(emotionModelTypes), timeOfEvaluation), true) {
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
  }

  public new MoodVector Clone() {
    IntPtr cPtr = erfCorePINVOKE.MoodVector_Clone(swigCPtr);
    MoodVector ret = (cPtr == IntPtr.Zero) ? null : new MoodVector(cPtr, true);
    if (erfCorePINVOKE.SWIGPendingException.Pending) throw erfCorePINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

}

}
