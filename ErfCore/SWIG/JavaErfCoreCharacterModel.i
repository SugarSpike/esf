/* File : ErfCoreCharacterModel.i */
%typemap(javaimports) erf::CharacterModel
%{
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator; 
%}

%extend erf::CharacterModel {

	void erf::CharacterModel::SetGameConfiguration(erf::GameConfiguration* gameConfiguration) {
		self->GetCharacterData().SetGameConfiguration(gameConfiguration,false);
	}

	void erf::CharacterModel::SetPersonality(erf::Personality* personality) {
		self->GetCharacterData().SetPersonality(personality,false);
	}
}

%ignore erf::CharacterModel::SetGameConfiguration;
%ignore erf::CharacterModel::SetPersonality;


%typemap(javacode) erf::CharacterModel %{
  // Ensure that the GC doesn't collect any element set from C#
  // as the underlying C++ class stores a shallow copy
  private Set<BasicObject> registeredElementsReferences;
  private void registerElementReference(BasicObject registerElement) {
	if(registeredElementsReferences == null)
		registeredElementsReferences = new HashSet<BasicObject>();
	registeredElementsReferences.add(registerElement);
  }
  
  private void unregisterElementReference(BasicObject unregisterElement) {
	if(registeredElementsReferences == null)
		return;
	registeredElementsReferences.remove(unregisterElement);
  }
  
  private void unregisterElementReferenceByName(String basicObjectName){
      if (registeredElementsReferences == null)
          return;
      Iterator<BasicObject> it = registeredElementsReferences.iterator();
      while(it.hasNext()) {
    	  BasicObject current = it.next();
    	  if(current.GetName().equals(basicObjectName))
    		  it.remove();
      }
  }
  
  // Ensure that the GC doesn't collect any Element set from C#
  // after the variable goes out of scope on the C# side
  private pl.gda.pg.isi.erf.Personality personalityReference;
  private void registerNewPersonality(Personality personality) {
    personalityReference = personality;
  }
  
  // Ensure that the GC doesn't collect any Element set from C#
  // after the variable goes out of scope on the C# side
  private pl.gda.pg.isi.erf.GameConfiguration gameConfReference;
  private void registerNewConfiguration(GameConfiguration conf) {
    gameConfReference = conf;
  }
  
  
%}
//
// GAME CONFIGURATION AND PERSONALITY FUNCTIONS
//

%typemap(javaout) void erf::CharacterModel::SetPersonality {
  $jnicall;
  registerNewPersonality(personality);
}

%typemap(javaout) void erf::CharacterModel::SetGameConfiguration {
  $jnicall;
  registerNewConfiguration(gameConfiguration);
}



//
// REGISTER FUNCTIONS
//
%typemap(javaout) bool erf::CharacterModel::RegisterEmotionModel {
    boolean ret = $jnicall;
	if(ret) registerElementReference(emotionModel);
    return ret;
  }
 
  
%typemap(javaout) bool erf::CharacterModel::RegisterPsychologicalModel {
    boolean ret = $jnicall;
	if(ret) registerElementReference(psychologicalModel);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::RegisterMoodModel {
    boolean ret = $jnicall;
	if(ret) registerElementReference(moodModel);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::RegisterEmotionVectorFilter {
    boolean ret = $jnicall;
	if(ret) registerElementReference(filter);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::RegisterEmotionSensor {
    boolean ret = $jnicall;
	if(ret) registerElementReference(emotionSensor);
    return ret;
  }

  
%typemap(javaout) bool erf::CharacterModel::RegisterExpert {
    boolean ret = $jnicall;
	if(ret) registerElementReference(expert);
    return ret;
  }

%typemap(javaout) bool erf::CharacterModel::RegisterExternalFilter {
    boolean ret = $jnicall;
	if(ret) registerElementReference(filter);
    return ret;
  }
  
//
// UNREGISTER BY REFERENCE FUNCTIONS
//
  
%typemap(javaout) bool erf::CharacterModel::UnregisterEmotionModel {
    boolean ret = $jnicall;
	if(ret) unregisterElementReference(emotionModel);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterEmotionModel {
    boolean ret = $jnicall;
	if(ret) unregisterElementReference(emotionModel);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterMoodModel {
    boolean ret = $jnicall;
	if(ret) unregisterElementReference(moodModel);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterPsychologicalModel {
    boolean ret = $jnicall;
	if(ret) unregisterElementReference(psychologicalModel);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterEmotionSensor {
    boolean ret = $jnicall;
	if(ret) unregisterElementReference(emotionSensor);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterExpert {
    boolean ret = $jnicall;
	if(ret) unregisterElementReference(expert);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterEmotionVectorFilter {
    boolean ret = $jnicall;
	if(ret) unregisterElementReference(emotionVectorFilter);
    return ret;
  }  

%typemap(javaout) bool erf::CharacterModel::UnregisterExternalFilter {
    boolean ret = $jnicall;
	if(ret) unregisterElementReference(filter);
    return ret;
  }  
  
//
// UNREGISTER BY NAME FUNCTIONS
//  
  
%typemap(javaout) bool erf::CharacterModel::UnregisterEmotionModelByName {
    boolean ret = $jnicall;
	if(ret) unregisterElementReferenceByName(emotionModelName);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterMoodModelByName {
    boolean ret = $jnicall;
	if(ret) unregisterElementReferenceByName(moodModelName);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterPsychologicalModelByName {
    boolean ret = $jnicall;
	if(ret) unregisterElementReferenceByName(psychologicalModelName);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterEmotionSensorByName {
    boolean ret = $jnicall;
	if(ret) unregisterElementReferenceByName(sensorName);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterExpertByName {
    boolean ret = $jnicall;
	if(ret) unregisterElementReferenceByName(expertName);
    return ret;
  }
  
%typemap(javaout) bool erf::CharacterModel::UnregisterEmotionVectorFilterByName {
    boolean ret = $jnicall;
	if(ret) unregisterElementReferenceByName(filterName);
    return ret;
  }    
  
%typemap(javaout) bool erf::CharacterModel::UnregisteExternalFilterByName {
    boolean ret = $jnicall;
	if(ret) unregisterElementReferenceByName(filterName);
    return ret;
  }    
