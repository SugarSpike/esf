%typemap(javacode) erf::Variant %{

  // Ensure that the GC doesn't collect any element set from C#
  // as the underlying C++ class stores a shallow copy
  private Object variantValue;
  protected void SetVariantValue(Object value) {
	this.variantValue = value;
  }
  protected Object GetVariantValue() {
	return this.variantValue;
  }

  @Override
  public boolean equals(Object obj) {
    boolean equal = false;
    if (obj != null && obj instanceof $javaclassname)
      equal = (($javaclassname)obj).swigCPtr == this.swigCPtr;
    return equal;
  }
  
  @Override
  public int hashCode() {
     return (int)this.swigCPtr;
  }
%}
%define VariantCollectionSupport(type, namespace)

%typemap(javaimports) namespace ## :: ## type
%{
import java.util.Map;
import java.util.HashMap;
%}


%typemap(javacode) namespace ## :: ## type %{
  // Ensure that the GC doesn't collect any element set from C#
  // as the underlying C++ class stores a shallow copy
  private Map<String,Variant> variants;
  private void registerVariant(String name,Variant variant) {
	if(variants == null)
		variants = new HashMap<String, Variant>();
	variants.put(name,variant);
  }
  
  /*private void unregisterVariant(String name) {
	if(variants == null)
		return;
	variants.remove(name);
  }*/

  // Ensure that the GC doesn't collect any element set from C#
  // as the underlying C++ class stores a shallow copy
  private Map<Long,Variant> variantsByIndex;
  private void registerVariant(Long index,Variant variant) {
	if(variantsByIndex == null)
		variantsByIndex = new HashMap<Long, Variant>();
	variantsByIndex.put(index,variant);
  }
  
  private void unregisterVariant(Long index) {
	if(variantsByIndex == null)
		return;
	variantsByIndex.remove(index);
  }
  
%}

%typemap(javaout) void namespace ## :: ## type ## ::AddValue {
    $jnicall;
	registerVariant(name,data);
  }

%typemap(javaout) erf::Variant* namespace ## :: ## type ## ::GetValue {
    long cPtr = $jnicall;
    $javaclassname ret = (cPtr == 0) ? null : new $javaclassname(cPtr, $owner);
	registerVariant(name,ret);
    return ret;
  }

%typemap(javaout) erf::Variant* namespace ## :: ## type ## ::GetValueByIndex {
    long cPtr = $jnicall;
    $javaclassname ret = (cPtr == 0) ? null : new $javaclassname(cPtr, $owner);
	registerVariant(index,ret);
    return ret;
  }
  
%enddef

%define VariantCollectionSupportErf(type)
	VariantCollectionSupport(type, erf)
%enddef

VariantCollectionSupportErf(VariantCollection)
VariantCollectionSupportErf(EmotionVector)
//VariantCollectionSupportErf(MoodVector)
//VariantCollectionSupportErf(Personality)
//VariantCollectionSupportErf(EmotionSensorReading)
//VariantCollectionSupportErf(EecEvent)
//VariantCollectionSupportErf(CharacterData)