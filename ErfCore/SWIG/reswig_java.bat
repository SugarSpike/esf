echo Invoking SWIG...
echo In order to function correctly, please ensure the following environment variables are correctly set
echo PATH: Contains SWIG exe in path
echo on 
ECHO Cleaning...
cd ..
rmdir /Q /S .\SWIG\Java\pl\gda\pg\isi\erf
mkdir .\SWIG\Java\pl\gda\pg\isi\erf
rmdir /Q /S .\SWIG\JavaWrappers
mkdir .\SWIG\JavaWrappers
ECHO Compiling...
swig.exe -v -debug-classes -Wall -c++ -outdir .\SWIG\Java\pl\gda\pg\isi\erf -java -package pl.gda.pg.isi.erf ./SWIG/JavaErfCore.i
ECHO Copying necessary headers...
cd .\SWIG
for %%x in (*_wrap.cxx) do (
copy "%%x" .\JavaWrappers\
del "%%x"
)
Echo SWIG DONE