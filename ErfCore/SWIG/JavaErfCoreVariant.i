
%define ExternalVariantSupport(variantnamespace, variantclass, namespace, type)
%typemap(javaout) erf::Variant* variantnamespace ## :: ## variantclass ## ::Create ## type   {
	long cPtr = $jnicall;
	$javaclassname ret = (cPtr == 0) ? null : new $javaclassname(cPtr, true);
	((variantclass)ret).SetVariantValue(arg);
	return ret;
}

%extend variantnamespace ## :: ## variantclass {
	static erf::Variant* Create ## type ( namespace ## :: ## type* arg) {
        return variantnamespace ## :: ## variantclass ## ::Create< namespace ## :: ## type >(arg,false);
    } 

	bool Is ## type () {
        return self->Is< namespace ## :: ## type >();
    } 

	namespace ## :: ## type* As ## type () {
        return self->As< namespace ## :: ## type >();
    } 

}

%enddef

%define VariantSupport(type, namespace)

%typemap(javaout) erf::Variant* erf::Variant::Create ## type   {
	long cPtr = $jnicall;
	$javaclassname ret = (cPtr == 0) ? null : new $javaclassname(cPtr, true);
	ret.SetVariantValue(arg);
	return ret;
}

	static erf::Variant* Create ## type ( namespace ## :: ## type* arg) {
        return erf::Variant::Create< namespace ## :: ## type >(arg,false);
    } 

	%template(Is ## type) Is< ## namespace ## :: ## type >;
	%template(As ## type) As< ## namespace ## :: ## type >;


%enddef

%define VariantSupportErf(type)
	VariantSupport(type,erf)
%enddef

// Specialized create functions 

%extend erf::Variant { 
	VariantSupportErf(EmotionVector)
	VariantSupportErf(MoodVector)
	VariantSupportErf(Personality)
	VariantSupportErf(EmotionSensorReading)
}


