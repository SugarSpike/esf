%typemap(cscode) erf::Variant %{

  // Ensure that the GC doesn't collect any element set from C#
  // as the underlying C++ class stores a shallow copy
  private Object variantValue;
  protected void SetVariantValue(Object value) {
	this.variantValue = value;
  }

  public override bool Equals(Object obj) {
    bool equal = false;
    if (obj is $csclassname)
      equal = (obj as $csclassname).swigCPtr.Handle.ToInt32() == this.swigCPtr.Handle.ToInt32();
    return equal;
  }
  
  public override int GetHashCode() {
     return this.swigCPtr.Handle.ToInt32();
  }
%}

%define VariantCollectionSupport(type, namespace)

%typemap(csimports) namespace ## :: ## type
%{
using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
%}


%typemap(cscode) namespace ## :: ## type %{
  // Ensure that the GC doesn't collect any element set from C#
  // as the underlying C++ class stores a shallow copy
  private Dictionary<string,Variant> variants;
  private void registerVariant(string name,Variant variant) {
	if(variants == null)
		variants = new Dictionary<string, Variant>();
	variants[name] = variant;
  }
  
  private void unregisterVariant(string name) {
	if(variants == null)
		return;
	variants.Remove(name);
  }

  // Ensure that the GC doesn't collect any element set from C#
  // as the underlying C++ class stores a shallow copy
  private Dictionary<uint,Variant> variantsByIndex;
  private void registerVariant(uint index,Variant variant) {
	if(variantsByIndex == null)
		variantsByIndex = new Dictionary<uint, Variant>();
	variantsByIndex[index] = variant;
  }
  
  private void unregisterVariant(uint index) {
	if(variantsByIndex == null)
		return;
	variantsByIndex.Remove(index);
  }
  
%}

%typemap(csout, excode=SWIGEXCODE) void namespace ## :: ## type ## ::AddValue {
    $imcall;$excode
	registerVariant(name,data);
  }

%typemap(csout, excode=SWIGEXCODE) erf::Variant* namespace ## :: ## type ## ::GetValue {
    IntPtr cPtr = $imcall;
    $csclassname ret = (cPtr == IntPtr.Zero) ? null : new $csclassname(cPtr, $owner);$excode
	registerVariant(name,ret);
    return ret;
  }

%typemap(csout, excode=SWIGEXCODE) erf::Variant* namespace ## :: ## type ## ::GetValueByIndex {
    IntPtr cPtr = $imcall;
    $csclassname ret = (cPtr == IntPtr.Zero) ? null : new $csclassname(cPtr, $owner);$excode
	registerVariant(index,ret);
    return ret;
  }

%enddef

%define VariantCollectionSupportErf(type)
	VariantCollectionSupport(type, erf)
%enddef

VariantCollectionSupportErf(VariantCollection)
VariantCollectionSupportErf(EmotionVector)
//VariantCollectionSupportErf(MoodVector)
//VariantCollectionSupportErf(Personality)
//VariantCollectionSupportErf(EmotionSensorReading)
//VariantCollectionSupportErf(EecEvent)
//VariantCollectionSupportErf(CharacterData)