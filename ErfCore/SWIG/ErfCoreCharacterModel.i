/* File : ErfCoreCharacterModel.i */
%typemap(csimports) erf::CharacterModel
%{
using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
%}

%extend erf::CharacterModel {

	void erf::CharacterModel::SetGameConfiguration(erf::GameConfiguration* gameConfiguration) {
		self->GetCharacterData().SetGameConfiguration(gameConfiguration,false);
	}

	void erf::CharacterModel::SetPersonality(erf::Personality* personality) {
		self->GetCharacterData().SetPersonality(personality,false);
	}
}

%ignore erf::CharacterModel::SetGameConfiguration;
%ignore erf::CharacterModel::SetPersonality;


%typemap(cscode) erf::CharacterModel %{
  // Ensure that the GC doesn't collect any element set from C#
  // as the underlying C++ class stores a shallow copy
  private HashSet<BasicObject> registeredElementsReferences;
  private void registerElementReference(BasicObject registerElement) {
	if(registeredElementsReferences == null)
		registeredElementsReferences = new HashSet<BasicObject>();
	registeredElementsReferences.Add(registerElement);
  }
  
  private void unregisterElementReference(BasicObject unregisterElement) {
	if(registeredElementsReferences == null)
		return;
	registeredElementsReferences.Remove(unregisterElement);
  }
  
  private void unregisterElementReferenceByName(string basicObjectName){
      if (registeredElementsReferences == null)
          return;
      registeredElementsReferences.RemoveWhere(obj => obj.GetName().Equals(basicObjectName));
  }
  
  // Ensure that the GC doesn't collect any Element set from C#
  // after the variable goes out of scope on the C# side
  private Personality personalityReference;
  private void registerNewPersonality(Personality personality) {
    personalityReference = personality;
  }
  
  // Ensure that the GC doesn't collect any Element set from C#
  // after the variable goes out of scope on the C# side
  private GameConfiguration gameConfReference;
  private void registerNewConfiguration(GameConfiguration conf) {
    gameConfReference = conf;
  }
  
  
%}
//
// GAME CONFIGURATION AND PERSONALITY FUNCTIONS
//

%typemap(csout, excode=SWIGEXCODE) void erf::CharacterModel::SetPersonality {
    $imcall;$excode
	registerNewPersonality(personality);
  }
  
%typemap(csout, excode=SWIGEXCODE) void erf::CharacterModel::SetGameConfiguration {
  $imcall;$excode
  registerNewConfiguration(gameConfiguration);
}


//
// REGISTER FUNCTIONS
//

%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::RegisterEmotionModel {
    bool ret = $imcall;$excode
	if(ret) registerElementReference(emotionModel);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::RegisterPsychologicalModel {
    bool ret = $imcall;$excode
	if(ret) registerElementReference(psychologicalModel);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::RegisterMoodModel {
    bool ret = $imcall;$excode
	if(ret) registerElementReference(moodModel);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::RegisterEmotionVectorFilter {
    bool ret = $imcall;$excode
	if(ret) registerElementReference(filter);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::RegisterEmotionSensor {
    bool ret = $imcall;$excode
	if(ret) registerElementReference(emotionSensor);
    return ret;
  }

  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::RegisterExpert {
    bool ret = $imcall;$excode
	if(ret) registerElementReference(expert);
    return ret;
  }

%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::RegisterExternalFilter {
    bool ret = $imcall;$excode
	if(ret) registerElementReference(filter);
    return ret;
  }
  
//
// UNREGISTER BY REFERENCE FUNCTIONS
//
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterEmotionModel {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReference(emotionModel);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterEmotionModel {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReference(emotionModel);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterMoodModel {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReference(moodModel);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterPsychologicalModel {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReference(psychologicalModel);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterEmotionSensor {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReference(emotionSensor);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterExpert {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReference(expert);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterEmotionVectorFilter {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReference(emotionVectorFilter);
    return ret;
  }  

%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterExternalFilter {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReference(filter);
    return ret;
  }  
  
//
// UNREGISTER BY NAME FUNCTIONS
//  
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterEmotionModelByName {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReferenceByName(emotionModelName);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterMoodModelByName {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReferenceByName(moodModelName);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterPsychologicalModelByName {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReferenceByName(psychologicalModelName);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterEmotionSensorByName {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReferenceByName(sensorName);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterExpertByName {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReferenceByName(expertName);
    return ret;
  }
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisterEmotionVectorFilterByName {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReferenceByName(filterName);
    return ret;
  }    
  
%typemap(csout, excode=SWIGEXCODE) bool erf::CharacterModel::UnregisteExternalFilterByName {
    bool ret = $imcall;$excode
	if(ret) unregisterElementReferenceByName(filterName);
    return ret;
  }    
