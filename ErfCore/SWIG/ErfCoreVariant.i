
%define ExternalVariantSupport(variantnamespace, variantclass, namespace, type)
%typemap(csout, excode=SWIGEXCODE) erf::Variant* variantnamespace ## :: ## variantclass ## ::Create ## type   {
	IntPtr cPtr = $imcall;
	$csclassname ret = (cPtr == IntPtr.Zero) ? null : new $csclassname(cPtr, $owner);$excode
	(ret as variantclass).SetVariantValue(arg);
	return ret;
}

%extend variantnamespace ## :: ## variantclass {
	static erf::Variant* Create ## type ( namespace ## :: ## type* arg) {
        return variantnamespace ## :: ## variantclass ## ::Create< namespace ## :: ## type >(arg,false);
    } 

	bool Is ## type () {
        return self->Is< namespace ## :: ## type >();
    } 

	namespace ## :: ## type* As ## type () {
        return self->As< namespace ## :: ## type >();
    } 

}

%enddef

%define VariantSupport(type, namespace)

%typemap(csout, excode=SWIGEXCODE) erf::Variant* erf::Variant::Create ## type   {
	IntPtr cPtr = $imcall;
	$csclassname ret = (cPtr == IntPtr.Zero) ? null : new $csclassname(cPtr, $owner);$excode
	ret.SetVariantValue(arg);
	return ret;
}

	static erf::Variant* Create ## type ( namespace ## :: ## type* arg) {
        return erf::Variant::Create< namespace ## :: ## type >(arg,false);
    } 

	%template(Is ## type) Is< ## namespace ## :: ## type >;
	%template(As ## type) As< ## namespace ## :: ## type >;


%enddef

%define VariantSupportErf(type)
	VariantSupport(type,erf)
%enddef

// Specialized create functions 

%extend erf::Variant { 
	VariantSupportErf(EmotionVector)
	VariantSupportErf(MoodVector)
	VariantSupportErf(Personality)
	VariantSupportErf(EmotionSensorReading)
}


