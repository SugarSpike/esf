echo Invoking SWIG...
echo In order to function correctly, please ensure the following environment variables are correctly set
echo PATH: Contains SWIG exe in path
echo on 
ECHO Cleaning...
cd ..
rmdir /Q /S .\SWIG\CSharp
mkdir .\SWIG\CSharp
rmdir /Q /S .\SWIG\Wrappers
mkdir .\SWIG\Wrappers
ECHO Compiling...
swig.exe -v -debug-classes -Wall -c++ -outdir .\SWIG\CSharp -namespace Erf -csharp ./SWIG/ErfCoreCSharp.i
ECHO Copying necessary headers...
cd .\SWIG
for %%x in (*_wrap.cxx) do (
copy "%%x" .\Wrappers\
del "%%x"
)
Echo SWIG DONE