/* File : ErfCoreCharacterData.i */

%extend erf::CharacterData {

	void erf::CharacterData::SetGameConfiguration(erf::GameConfiguration* gameConfiguration) {
		self->SetGameConfiguration(gameConfiguration,false);
	}

	void erf::CharacterData::SetPersonality(erf::Personality* personality) {
		self->SetPersonality(personality,false);
	}
}

%ignore erf::CharacterData::SetGameConfiguration;
%ignore erf::CharacterData::SetPersonality;