/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pl.gda.pg.isi.erf;

public final class GameChallenges {
  public final static GameChallenges STD_BOSS_FIGHT = new GameChallenges("STD_BOSS_FIGHT", erfCoreJavaJNI.STD_BOSS_FIGHT_get());
  public final static GameChallenges STD_DONT_DIE = new GameChallenges("STD_DONT_DIE");
  public final static GameChallenges STD_RACE_WITH_TIME = new GameChallenges("STD_RACE_WITH_TIME");
  public final static GameChallenges STD_LOGIC_RIDDLE = new GameChallenges("STD_LOGIC_RIDDLE");
  public final static GameChallenges STD_TREASURE_FINDING = new GameChallenges("STD_TREASURE_FINDING");
  public final static GameChallenges STD_MOVE_PRECISION = new GameChallenges("STD_MOVE_PRECISION");
  public final static GameChallenges STD_REACTION_TIME = new GameChallenges("STD_REACTION_TIME");
  public final static GameChallenges STD_REACH_DIFFICULT_LOCATION = new GameChallenges("STD_REACH_DIFFICULT_LOCATION");
  public final static GameChallenges EXT_GAME_CHALLENGE = new GameChallenges("EXT_GAME_CHALLENGE");

  public final int swigValue() {
    return swigValue;
  }

  public String toString() {
    return swigName;
  }

  public static GameChallenges swigToEnum(int swigValue) {
    if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
      return swigValues[swigValue];
    for (int i = 0; i < swigValues.length; i++)
      if (swigValues[i].swigValue == swigValue)
        return swigValues[i];
    throw new IllegalArgumentException("No enum " + GameChallenges.class + " with value " + swigValue);
  }

  private GameChallenges(String swigName) {
    this.swigName = swigName;
    this.swigValue = swigNext++;
  }

  private GameChallenges(String swigName, int swigValue) {
    this.swigName = swigName;
    this.swigValue = swigValue;
    swigNext = swigValue+1;
  }

  private GameChallenges(String swigName, GameChallenges swigEnum) {
    this.swigName = swigName;
    this.swigValue = swigEnum.swigValue;
    swigNext = this.swigValue+1;
  }

  private static GameChallenges[] swigValues = { STD_BOSS_FIGHT, STD_DONT_DIE, STD_RACE_WITH_TIME, STD_LOGIC_RIDDLE, STD_TREASURE_FINDING, STD_MOVE_PRECISION, STD_REACTION_TIME, STD_REACH_DIFFICULT_LOCATION, EXT_GAME_CHALLENGE };
  private static int swigNext = 0;
  private final int swigValue;
  private final String swigName;
}

