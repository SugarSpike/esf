/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pl.gda.pg.isi.erf;

public class EmotionVectorFilterList {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected EmotionVectorFilterList(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(EmotionVectorFilterList obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        erfCoreJavaJNI.delete_EmotionVectorFilterList(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public EmotionVectorFilterList() {
    this(erfCoreJavaJNI.new_EmotionVectorFilterList(), true);
  }

  public long size() {
    return erfCoreJavaJNI.EmotionVectorFilterList_size(swigCPtr, this);
  }

  public boolean isEmpty() {
    return erfCoreJavaJNI.EmotionVectorFilterList_isEmpty(swigCPtr, this);
  }

  public void clear() {
    erfCoreJavaJNI.EmotionVectorFilterList_clear(swigCPtr, this);
  }

  public void add(EmotionVectorFilter x) {
    erfCoreJavaJNI.EmotionVectorFilterList_add(swigCPtr, this, EmotionVectorFilter.getCPtr(x), x);
  }

  public EmotionVectorFilter get(int i) {
    long cPtr = erfCoreJavaJNI.EmotionVectorFilterList_get(swigCPtr, this, i);
    return (cPtr == 0) ? null : new EmotionVectorFilter(cPtr, true);
  }

}
