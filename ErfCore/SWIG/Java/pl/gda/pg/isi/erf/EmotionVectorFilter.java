/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pl.gda.pg.isi.erf;

public class EmotionVectorFilter extends BasicObject {
  private long swigCPtr;
  private boolean swigCMemOwnDerived;

  public EmotionVectorFilter(long cPtr, boolean cMemoryOwn) {
    super(erfCoreJavaJNI.EmotionVectorFilter_SWIGSmartPtrUpcast(cPtr), true);
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = cPtr;
  }

  public static long getCPtr(EmotionVectorFilter obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwnDerived) {
        swigCMemOwnDerived = false;
        erfCoreJavaJNI.delete_EmotionVectorFilter(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public void DoFilter(EmotionVector emotionVector, CharacterData characterData) {
    erfCoreJavaJNI.EmotionVectorFilter_DoFilter(swigCPtr, this, EmotionVector.getCPtr(emotionVector), emotionVector, CharacterData.getCPtr(characterData), characterData);
  }

  public boolean IsEmotionVectorSupported(EmotionVector emotionVector) {
    return erfCoreJavaJNI.EmotionVectorFilter_IsEmotionVectorSupported(swigCPtr, this, EmotionVector.getCPtr(emotionVector), emotionVector);
  }

}
