/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pl.gda.pg.isi.erf;

public class PsychologicalModel extends BasicObject {
  private long swigCPtr;
  private boolean swigCMemOwnDerived;

  public PsychologicalModel(long cPtr, boolean cMemoryOwn) {
    super(erfCoreJavaJNI.PsychologicalModel_SWIGSmartPtrUpcast(cPtr), true);
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = cPtr;
  }

  public static long getCPtr(PsychologicalModel obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwnDerived) {
        swigCMemOwnDerived = false;
        erfCoreJavaJNI.delete_PsychologicalModel(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public float GetModelRank() {
    return erfCoreJavaJNI.PsychologicalModel_GetModelRank(swigCPtr, this);
  }

  public void InitializeForCharacter(CharacterData characterData) {
    erfCoreJavaJNI.PsychologicalModel_InitializeForCharacter(swigCPtr, this, CharacterData.getCPtr(characterData), characterData);
  }

  public void UpdatePersonality(EecEvent event, CharacterData characterData) {
    erfCoreJavaJNI.PsychologicalModel_UpdatePersonality(swigCPtr, this, EecEvent.getCPtr(event), event, CharacterData.getCPtr(characterData), characterData);
  }

}
