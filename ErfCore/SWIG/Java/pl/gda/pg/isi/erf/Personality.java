/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pl.gda.pg.isi.erf;

public class Personality extends VariantCollection {
  private long swigCPtr;
  private boolean swigCMemOwnDerived;

  public Personality(long cPtr, boolean cMemoryOwn) {
    super(erfCoreJavaJNI.Personality_SWIGSmartPtrUpcast(cPtr), true);
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = cPtr;
  }

  public static long getCPtr(Personality obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwnDerived) {
        swigCMemOwnDerived = false;
        erfCoreJavaJNI.delete_Personality(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public Personality() {
    this(erfCoreJavaJNI.new_Personality(), true);
  }

  public long GetTimeOfEvaluation() {
    long ret = erfCoreJavaJNI.Personality_GetTimeOfEvaluation(swigCPtr, this);
    return ret;
  }

  public void SetTimeOfEvaluation(long newTime) {
    erfCoreJavaJNI.Personality_SetTimeOfEvaluation(swigCPtr, this, newTime);
  }

  public Personality Clone() {
    long cPtr = erfCoreJavaJNI.Personality_Clone(swigCPtr, this);
    return (cPtr == 0) ? null : new Personality(cPtr, true);
  }

}
