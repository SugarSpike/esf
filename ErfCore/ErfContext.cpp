#pragma once
#include "ErfContext.h"
#include "ExternalComponentConfiguration.h"
#include "ExternalComponentLibrary.h"
#include "EmotionSensor.h"
#include "ExternalExpert.h"
#include "ExternalFilter.h"
#include "ErfUtils.h"
#include "thirdparty\pugixml\pugixml.hpp"
#include "Shlwapi.h"
#include "ErfException.h"
#include "ErfLogging.h"
#include "boost\filesystem\path.hpp"
#include "ErfForEach.h"

#ifdef _DEBUG
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif


using namespace pugi;

namespace erf {

	ErfContext::ErfContext() : BasicObject("ErfContext") { 
	}

	ErfContext::~ErfContext() { 
		try {
			while (!this->registeredExternalComponentLibrary.empty())
			{
				this->UnloadComponentLibrary(this->registeredExternalComponentLibrary.front());
			}
		} catch(...) {
			LOG(error) << "ErfContext::~ErfContext - error occured during library unload";
		}
	}

	vector<ExternalFilter*>& ErfContext::GetRegisteredFilters() {
		return this->registeredFilters;
	}

	vector<EmotionSensor*>& ErfContext::GetRegisteredSensors() {
		return this->registeredSensors;
	}

	vector<ExternalExpert*>& ErfContext::GetRegisteredExperts() {
		return this->registeredExperts;
	}

	vector<ExternalComponentLibrary*>& ErfContext::GetRegisteredLibraries() {
		return this->registeredExternalComponentLibrary;
	}

	void ErfContext::LoadConfiguration()
	{
		LoadConfiguration(erfutils::GetCurrentExecutingDirectory() + "\\erf-config.xml");
	}

	ExternalExpert* ErfContext::FindExpert(string expertName) {
		for_each_erf(ExternalExpert* object, this->registeredExperts)
		{
			if(object->GetName() == expertName)
				return object;
		}
		return null;
	}
	EmotionSensor*ErfContext:: FindSensor(string sensorName) {
		for_each_erf(EmotionSensor* object, this->registeredSensors)
		{
			if(object->GetName() == sensorName)
				return object;
		}
		return null;
	}
	ExternalComponentLibrary* ErfContext::FindLibrary(string libraryName){ 
		for_each_erf(ExternalComponentLibrary* object, this->registeredExternalComponentLibrary)
		{
			if(object->GetName() == libraryName)
				return object;
		}
		return null;
	}
	ExternalFilter* ErfContext::FindFilter(string filterName){ 
		for_each_erf(ExternalFilter* object, this->registeredFilters)
		{
			if(object->GetName() == filterName)
				return object;
		}
		return null;
	}

	void ErfContext::LoadConfiguration(string fileName)
	{
		LOG(debug) << "ErfContext::LoadConfiguration - perform " << GetUniqueName();
		LOG(debug) << "ErfContext::LoadConfiguration - load file: " << fileName;

		if(!erfutils::FileExists(fileName))
		{
			LOG(warning) << "ErfContext::LoadConfiguration - supplied file does not exists: " << fileName;
			return;
		}

		pugi::xml_document doc;

	    pugi::xml_parse_result result = doc.load_file(fileName.c_str());
		if (result)
			LOG(info) << "ErfContext::LoadConfiguration [" << fileName << "] parsed without errors";
		else
		{
			LOG(error) << "ErfContext::LoadConfiguration [" << fileName << "] parsed with errors";
			LOG(error) << "Error description: " << result.description();
			LOG(error) << "Error offset: " << result.offset;
			return;
		}

		xml_node configuration = doc.child("erfConfiguration");

		for (xml_node sensor = configuration.child("externalLibrary"); sensor; sensor = sensor.next_sibling("externalLibrary"))
		{
			xml_node dllPath = sensor.child("dllPath");
			if(!dllPath)
			{
				LOG(error) << "ErfContext::LoadConfiguration - one of the configured " <<
					"sensors is missing the dllPath tag (sensor offset:" << sensor.offset_debug() << ")";
				continue;
			}

			string pathToDll = dllPath.first_child().value();
			LoadComponentLibrary(pathToDll);
		}

	}

	
	ExternalComponentLibrary* ErfContext::LoadComponentLibrary(string pathToDll){

		boost::filesystem::path path(pathToDll);
		if(path.is_relative())
			pathToDll = erfutils::GetCurrentExecutingDirectory() + "\\" + pathToDll;

		if(!erfutils::FileExists(pathToDll))
		{
			LOG(error) << "ErfContext::LoadConfiguration - could not find the file '" << pathToDll << "'";
			return NULL;
		}

		boost::filesystem::path normalizedPath(pathToDll);
		normalizedPath = normalizedPath.normalize();

		for_each_erf(ExternalComponentLibrary* lib, this->registeredExternalComponentLibrary)
		{
			string dllName = normalizedPath.filename().string();
			if(lib->GetConfiguration().componentDllName == dllName)
			{
				LOG(info) << "ErfContext::LoadConfiguration - library already loaded '" << dllName << "' returning previous instance";
				return lib;
			}
		}

		ExternalComponentConfiguration* configuration = new ExternalComponentConfiguration();
		configuration->componentDllPath= normalizedPath.parent_path().string() + "\\";
		configuration->componentDllName= normalizedPath.filename().string();

		ExternalComponentLibrary* library = NULL;
		try {
			library = new ExternalComponentLibrary(configuration);
			library->CreateLibrary();

			for_each_erf(ExternalComponent* component, configuration->components)
			{
				if(component->GetComponentType() == ComponentType::SENSOR)
				{
					EmotionSensor* sensor = dynamic_cast<EmotionSensor*>(component);
					if(sensor == NULL)
					{
						LOG(error) << GetUniqueName() << " -found registered sensor not extending EmotionSensor class in '" 
							<< configuration->componentDllName << "'";
						continue;
					}
					if(!erfutils::Contains(this->registeredSensors, sensor))
					{
						this->registeredSensors.push_back(sensor);
					}
				}
				else if(component->GetComponentType() == ComponentType::EXPERT)
				{
					ExternalExpert* expert = dynamic_cast<ExternalExpert*>(component);
					if(expert == NULL)
					{
						LOG(error) << GetUniqueName() << " -found registered expert not extending ExternalExpert class in '" 
							<< configuration->componentDllName << "'";
						continue;
					}
					if(!erfutils::Contains(this->registeredExperts, expert))
					{
						this->registeredExperts.push_back(expert);
					}
				}
				else if(component->GetComponentType() == ComponentType::FILTER)
				{
					ExternalFilter* filter = dynamic_cast<ExternalFilter*>(component);
					if(filter == NULL)
					{
						LOG(error) << GetUniqueName() << " -found registered filter not extending ExternalFilter class in '" 
							<< configuration->componentDllName << "'";
						continue;
					}
					if(!erfutils::Contains(this->registeredFilters, filter))
					{
						this->registeredFilters.push_back(filter);
					}
				}
			}
			this->registeredExternalComponentLibrary.push_back(library);

		} catch(ErfException e)
		{
				
			LOG(error) << "ErfContext::LoadConfiguration - an error occured during the initialization of '" << pathToDll << "'";
			LOG(error) << "ErfContext::LoadConfiguration - message: " << e.Message();
				
			if(library != NULL) {
				try {
					delete library;
					library = NULL;
				} catch(ErfException e)
				{
					LOG(error) << "ErfContext::LoadConfiguration - an error occured during the deletion of '" << pathToDll << "'";
					LOG(error) << "ErfContext::LoadConfiguration - message: " << e.Message();
				}
			}
			return null;
		}
		LOG(info) << "ErfContext::LoadConfiguration - library loaded: '" <<library->GetConfiguration().componentDllName << "'";

		return library;
	}

	
	void ErfContext::UnloadComponentLibrary(string pathToDll)
	{
		boost::filesystem::path path(pathToDll);
		if(path.is_relative())
			pathToDll = erfutils::GetCurrentExecutingDirectory() + "\\" + pathToDll;

		if(!erfutils::FileExists(pathToDll))
		{
			LOG(error) << "ErfContext::UnloadComponentLibrary - could not find the file '" << pathToDll << "'";
			return;
		}

		boost::filesystem::path normalizedPath(pathToDll);
		normalizedPath = normalizedPath.normalize();

		for_each_erf(ExternalComponentLibrary* lib, this->registeredExternalComponentLibrary)
		{
			string dllName = normalizedPath.filename().string();
			if(lib->GetConfiguration().componentDllName == dllName)
			{
				LOG(info) << "ErfContext::UnloadComponentLibrary - library already loaded '" << dllName << "' returning previous instance";
				UnloadComponentLibrary(lib);
				return;
			}
		}
		LOG(error) << "ErfContext::UnloadComponentLibrary - could not find the libary in loaded libraries '" << pathToDll << "'";
		return;
		
	}

	void ErfContext::UnloadComponentLibrary(ExternalComponentLibrary* toDelete) {

		if(toDelete == null)
		{
			LOG(error) << "ErfContext::UnloadComponentLibrary - supplied null pointer for deletion";
			return;
		}
		string dllName = toDelete->GetConfiguration().componentDllName;

		for_each_erf(ExternalComponent* component, toDelete->GetConfiguration().GetComponents())
		{
			if(ComponentType::SENSOR == component->GetComponentType())
				erfutils::Remove(this->GetRegisteredSensors(),component);
			else if(ComponentType::EXPERT == component->GetComponentType())
				erfutils::Remove(this->GetRegisteredExperts(),component);

		}

		erfutils::Remove(this->GetRegisteredLibraries(),toDelete);
		try {
			delete toDelete;
		} catch(ErfException e) {
			LOG(error) << "ErfContext::LoadConfiguration - an error occured during the deletion of '" << dllName << "'";
			LOG(error) << "ErfContext::LoadConfiguration - message: " << e.Message();
		}
		catch(...) {
			LOG(error) << "ErfContext::LoadConfiguration - an unknown error occured during the deletion of '" << dllName << "'";
		}
		LOG(info) << "ErfContext::LoadConfiguration - library unloaded: '" << dllName << "'";
	}

	bool ErfContext::StartMemLeakLogging() {
#ifdef _DEBUG
		int newFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
		newFlag = newFlag | _CRTDBG_ALLOC_MEM_DF | _CRTDBG_CHECK_ALWAYS_DF | _CRTDBG_LEAK_CHECK_DF;
		_CrtSetDbgFlag(newFlag);

		HANDLE hLogFile;
		
		_CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_FILE );
		_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_FILE );
		_CrtSetReportMode( _CRT_ASSERT, _CRTDBG_MODE_FILE );

		hLogFile = CreateFile("D:\\log.txt", GENERIC_WRITE, 
			FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, 
			FILE_ATTRIBUTE_NORMAL, NULL);
		if( INVALID_HANDLE_VALUE == hLogFile)
		{
			string s = erfutils::GetLastErrorStdString();
			LOG(error) << s;
			_CrtSetReportFile( _CRT_WARN, _CRTDBG_FILE_STDOUT );
			_CrtSetReportFile( _CRT_ERROR, _CRTDBG_FILE_STDOUT );
			_CrtSetReportFile( _CRT_ASSERT, _CRTDBG_FILE_STDOUT );
			return true;
		}
		
		_CrtSetReportFile( _CRT_WARN, hLogFile );
		_CrtSetReportFile( _CRT_WARN, hLogFile );
		_CrtSetReportFile( _CRT_WARN, hLogFile );
		return true;
#else
		LOG(warning) << "ErfContext::StartMemLeakLogging - memory and performance logging available only for DEBUG configuration";
		return false;
#endif
	}

	bool ErfContext::StopMemLeakLoggingAndLog() {
#ifdef _DEBUG
		//_CrtSetReportMode(_CRT_ERROR | _CRT_WARN | _CRT_ASSERT, _CRTDBG_MODE_DEBUG | _CRTDBG_MODE_WNDW);
        int i = _CrtDumpMemoryLeaks();
		_flushall();
		LOG(warning) << "ErfContext::StopMemLeakLoggingAndLog - log done, status: " << i;
		return true;
#else
		LOG(warning) << "ErfContext::StopMemLeakLoggingAndLog - memory and performance logging available only for DEBUG configuration";
		return false;
#endif
	}

}