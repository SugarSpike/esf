#pragma once
#include "BasicObject.h"
#include <vector>
#include "ExternalComponentEnums.h"

/*! \file ExternalComponentConfiguration.h
	\brief ExternalComponentConfiguration header file
*/


//=================================
// forward declared dependencies
namespace erf
{
	class ExternalComponent;
	class ExternalComponentLibrary;
}

namespace erf
{
   /*!	\brief The configuration for a library of ExternalComponent%s
	*	
	*	The class is created in pair with a ExternalComponentLibrary.
	*	It is a way to tie ExternalComponents together for easy access and maintenance. 
	*	When an ExternalComponentConfiguration is destroyed so are all ExternalComponent%s tied to it.
	*/
	class ESFCORE_API ExternalComponentConfiguration : public BasicObject {

		friend class ExternalComponent;	///< ExternalComponent require direct acces to the list of components (remove on deletion)
		friend class ExternalComponentLibrary; ///< ExternalComponentLibrary require direct acces to the list of components (remove all on deletion)
		friend class ErfContext; ///< ExternalComponentLibrary instances and ExternalComponentConfiguration should be created by using ErfContext
	public:
		string componentDllPath;	///< Absolute path to the library DLL
		string componentDllName;	///< Name of the library DLL

		/*!	\brief Base ExternalComponentConfiguration constructor		
		*	
		*	Base ExternalComponentConfiguration constructor
		*/
		ExternalComponentConfiguration();
		virtual ~ExternalComponentConfiguration();

		/*!	\brief Returns all ExternalComponent%s registered in the configuration		
		*	
		*	 Returns all ExternalComponent%s tied to this configuration. Including the ExternalComponentLibrary.
		*	\return All ExternalComponent%s registered in the configuration
		*/
		vector<ExternalComponent*>& GetComponents();
		/*!	\brief Returns the library component for this configuration		
		*	
		*	\return The library component
		*/
		ExternalComponentLibrary* GetLibraryComponent();

	private:
		ExternalComponentLibrary* libraryComponent; ///< The library instance to which the configuration is bound
		vector<ExternalComponent*> components;		///< All components in the given ExternalComponentLibrary
	};

}