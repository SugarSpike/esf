#pragma once
#include "BasicObject.h"

/*! \file ErfLogger.h
	\brief ErfLogger header file - contains logging wrappers and logging control for Erf framework 
		   does not require boost
*/

///Forward declaration of boost logging namespaces
#ifndef ERFLOGGING_INCLUDED
namespace boost {

	namespace log {

		namespace sources {
			/*! \brief	Forward declaration of a boost trivial severity logger
			 *			
			 *	Forward declaration as to seperate the boost logging framework from the ErfCore.
			 *	Thanks to this extending projects will not to have boost included;
			 */
			template< typename LevelT = int >
			class severity_logger;
		}
	}
}

#endif

namespace erf {

	/*! \brief	Copy of boost::log::trivial::severity_level
	*			
	*	Enumerations cannot be forward declared and as to seperate from boost logging framework this enum clone was created
	*/
	enum severity_level
	{
		trace,
		debug,
		info,
		warning,
		error,
		fatal
	};


	/*! \brief	The singleton logger for Erf
	 *			
	 *	The singleton logger for Erf. Acts as a wrapper for boost::logging framework.
	 *	Also constrols logging in general for Erf (enable/disable).
	 *	At this moment the logger uses the boost logging framework directly and as such cannot be used by external libraries
	 *	without including boost framework;
	 */
	class ESFCORE_API ErfLogger
    {
	private:
		boost::log::sources::severity_logger<erf::severity_level>* lg; ///< Wrapped boost severity logger

		/*! \brief The default constructor of ErfLogger
		 *	
		 *	The default constructor of ErfLogger - the class is a singleton and as such the constructor is private
		 *
		 */
		ErfLogger();
		/*! \brief The default copy constructor of ErfLogger
		 *	
		 *	The copy constructor of ErfLogger - the class is a singleton and as such the constructor is private
		 *
		 */
		ErfLogger(ErfLogger const&) {}
		/*! \brief The assign operator of ErfLogger
		 *	
		 *	The assign operator of ErfLogger - the class is a singleton and as such the operator is private
		 *
		 */
		void operator=(ErfLogger const&) {}

	public:
		virtual ~ErfLogger();

		/*! \brief Sets the logging enabled flag for Erf
		 *	
		 *	Sets the logging enabled flag for Erf - if set to false no logging will be done by the framework.
		 *	\param enable The new flag value
		 */
		static void SetLoggingEnabled(bool enable);
		/*! \brief Gets the current logging enabled flag for Erf
		 *	
		 *	Gets the current logging enabled flag for Erf
		 *	\return Current value of logging enabled flag
		 */
		static bool GetLoggingEnabled();

		/*! \brief Sets the logging file for boost::logging framework
		 *	
		 *	\param filePath Path to the logging file
		 */
		static void SetLoggingFile(string filePath);

		/*! \brief Redirects both stdout and stderr to the specified file
		 *	
		 *	\param filePath Path to the logging file
		 */
		static void SetAllStreamsToLoggingFile(string filePath);

		

		/*! \brief Returns the current instance of ErfLogger
		 *	
		 *	Returns the current instance of ErfLogger
		 *	\return Current ErfLogger
		 */
		static ErfLogger& GetErfLogger()
        {
            static ErfLogger instance;
            return instance;
        }

		/*! \brief Returns the boost logger wrapped by ErfLogger
		 *	
		 *	Returns the boost logger wrapped by ErfLogger
		 *	\return Wrapped boost logger
		 */
		static boost::log::sources::severity_logger<severity_level>* GetLogger()
		{
			return ErfLogger::GetErfLogger().lg;
		}
	};

}