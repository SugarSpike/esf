#pragma once
#include "MoodExpert.h"
#include "ErfException.h"

namespace erf {

	MoodExpert::MoodExpert(ExternalComponentConfiguration* pConfiguration): 
		BasicObject("MoodExpert"),ExternalExpert(pConfiguration, ExpertType::STD_MOOD)
	{
	}

	MoodExpert::MoodExpert(const MoodExpert &other): 
		BasicObject(),ExternalExpert(other.configuration, ExpertType::STD_MOOD)
	{
		throw new ErfException("MoodExpert::MoodExpert(const MoodExpert &other) called");
	}

	MoodExpert::~MoodExpert() { }

}