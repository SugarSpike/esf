/* File : EsfCoreCSharp.i */
%module EsfCoreCSharp

%rename(Equals) operator==;

#ifdef ESFCORE_EXPORTS
    #define ESFCORE_API __declspec(dllexport)
#else
    #define ESFCORE_API __declspec(dllimport)
#endif
//Windows interface hack for preprocessor macros
%include <windows.i>

//swig support for std -> std_list.i not included in base SWIG installation:
//Included in patch: http://sourceforge.net/p/swig/patches/134/
//It is far from perfect and that is why we ignore 302 warn
#pragma SWIG nowarn=302 //Warning(302): Identifier 'list' redefined (ignored),

%include "boost_shared_ptr.i"
%include "typemaps.i" 
%include "std_string.i" 
%include "std_map.i" 
%include "std_list.i" 
%include "std_vector.i"

%shared_ptr(esf::BasicObject)
%shared_ptr(esf::Personality)
%shared_ptr(esf::SensorData)
%shared_ptr(esf::EecEvent)
%shared_ptr(esf::Emotion)
%shared_ptr(esf::EmotionSensor)
%shared_ptr(esf::AffectVector)
%shared_ptr(esf::MoodVector)
%shared_ptr(esf::UserData)

%inline %{
	#include "Common.h"
	#include "boost/shared_ptr.hpp"
	#include "BasicObject.h"
%}

%include "BasicObject.h"
%include "Personality.h"
%include "SensorData.h"
%include "EecEvent.h"
%include "Emotion.h"
%include "EmotionSensor.h"
%include "AffectVector.h"
//%include "MoodVector.h"
//%include "UserData.h"

//std templates
namespace std {
   //%template(IntVector) vector<int>;
  // %template(DoubleVector) vector<double>;
   %template(Traits) map<string,boost::shared_ptr<void> >;
   %template(BaseVector) vector<boost::shared_ptr<esf::BasicObject> >;
   %template(EmotionsVector) vector<boost::shared_ptr<esf::Emotion> >;
  // %template(PersonalityVector) vector<boost::shared_ptr<Personality> >;
}



