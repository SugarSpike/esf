#pragma once
#include "BasicObject.h"

/*! \file VariantCollection.h
	\brief VariantCollection header file
*/

//Forward declaration
namespace erf {
	class Variant;
}

namespace erf
{
	/*! \brief	A collection of Variant%s
	 *			
	 *	The VariantCollection is a container that can hold Variant data.
	 *	It is a multi-type, multi-value generic container. 
	 */
    class ESFCORE_API VariantCollection : public virtual BasicObject
    {

	protected:

		vector<Variant*> values; ///< Variant data this collection holds
		map<string,int> nameToIndex; ///< ParameterName to Index map
		map<int, string> indexToName; ///< Index to ParameterName map
		map<string,Variant*> nameToValue; ///< ParameterName to Variant value map

		/*! \brief	Copies Variant data from this variant collection into another
		 *	
		 *	Copies Variant data from this variant collection into another
		 *	The data are parial deep-cloned. See Variant class for more details about partial-deep cloning.
		 *
		 *	\param other The collection into which to copy the data held by the VariantCollection
		 */
		void CopyInto(VariantCollection& other);

	public:
		/*! \brief Base VariantCollection constructor
		*	
		*	Base constructor for VariantCollection
		*/
		VariantCollection();

		/*! \brief Base VariantCollection copy constructor
		*	
		*	Base copy constructor for VariantCollection
		*/
		VariantCollection(const VariantCollection &other);

		virtual ~VariantCollection();

		/*! \brief Returns the number of Variant values held by collection
		*	
		*	Returns the number of Variant values held by collection
		*	\return The number of Variant values held by collection
		*/
		int ValueCount();

		/*! \brief Adds the Variant data to the vector
		*	
		*	Adds the Variant data under the specified name
		*
		*	\param name Name of the parameter under which the data will be accessible
		*	\param data Variant data to save in this collection
		*/
		virtual void AddValue(string name, Variant* data);

		/*! \brief Returns the Variant data by index
		*	
		*	Returns the Variant data by index
		*	\param index The index of data
		*	\return Variant under specified index, null if index exceedes bounds
		*/
		Variant* GetValueByIndex(unsigned int index);

		/*! \brief Returns the Variant data by parameter name
		*	
		*	Returns the Variant data by parameter name
		*	\param name The name of the parameter
		*	\return Variant under specified name, null if none found
		*/
		Variant* GetValue(string name);

		/*! \brief Returns all Variant data held in this collection
		*	
		*	Returns all Variant data held in this collection
		*	\return Map of parameter name to Variant data held by this collection
		*/
		map<string,Variant*>& GetValues();

		
		/*! \brief Clears all variants
		*	
		*	Clears all variants and calling deletion on them.
		*/
		void Clear();

		/*! \brief Clears all variants
		*	
		*	\param removeAndDelete Whenever to call deletion on Variant elements
		*/
		void Clear(bool removeAndDelete);

		/*! \brief Prints the collection to a string
		*	
		*	Prints the collection to a string
		*	\return Printed collection
		*/
		string Print();

	};
}