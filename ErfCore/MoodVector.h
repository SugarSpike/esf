#pragma once
#include "EmotionVector.h"

/*! \file MoodVector.h
	\brief MoodVector header file
*/


namespace erf
{
	/*! \brief	A vector containing evaluated mood emotion values by the MoodModel%s 
	 *			
	 *	The MoodModel is a container for evaluated mood emotions values by the framework (specificly the MoodModel%s).
	 *	It is a multi-type, multi-value generic container. The values that it contains depends on the EmotionModelType
	 *	of the MoodModel that produced the vector. For example a PAD vector would contain three values: 'pleasure',
	 *	'dominance', 'arousal'. While an OCC vector could contain any subset of the 24 emotions modeled by it eg.: 'Fear',
	 *	'Happy-For', %sad'. In general the MoodVector does not differs much from a EmotionVEctor. but the class was created
	 *	for clarity of the framework flow.
	 */
    class ESFCORE_API MoodVector: public EmotionVector
    {
		public:
			/*! \brief Base MoodVector constructor
			 *	
			 *	Base constructor for MoodVector
			 *	\param moodModelType the emotion model type that this vector represents
			 */
			MoodVector(int moodModelType);

			/*! \brief Base MoodVector constructor
			 *	
			 *	Base constructor for MoodVector
			 *	\param emotionModelType the emotion model type that this vector represents
			 *	\param timeOfEvaluation time of evaluation of the vector
			 */
			MoodVector(int emotionModelType,time_t timeOfEvaluation);

			/*! \brief Base MoodVector constructor
			 *	
			 *	Base constructor for MoodVector
			 *	\param emotionModelTypes the emotion model types that this vector represents
			 *	\param timeOfEvaluation time of evaluation of the vector
			 */
			MoodVector(vector<int>& emotionModelTypes,time_t timeOfEvaluation);

			/*! \brief Clones the vector
			 *
			 *  Returns a parial deep-clone of the vector. See Variant class for more details about partial-deep cloning.
			 *
			 *	\return Clone of vector
			 */
			MoodVector* Clone();
	};

}

