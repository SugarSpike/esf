#pragma once
#include "BasicObject.h"
#include "CharacterModel.h"

/*! \file VectorEvaluator.h
	\brief VectorEvaluator header file

*/

//=================================
// forward declared dependencies
namespace erf
{
	class EecEvent;
}

namespace erf
{
	/*! \brief	Evaluates target emotion vector based on experts models output
	 *			
	 *	The evaluator computes target emotion vector based on experts models output (both mood and emotion)
	 *
	 */
	class ESFCORE_API VectorEvaluator : public virtual BasicObject
    {

	protected:

		CharacterModel& model; ///< Parent model, expose all data needed by the evaluator 
		
		/*! \brief Sets the new current unfiltered emotion vector
		 *
		 *	Sets new current unfiltered emotion vector before EmotionFilterType::After filters were applied.
		 *  Not all VectorEvaluators sets this value
		 *	\param vector new vector to be set
		 */
		void SetUnfilteredEmotionVector(EmotionVector* vector);

		/*! \brief Sets new current emotion vector
		 *
		 *	 Sets new current emotion vector
		 *	\param vector new vector to be set
		 */
		void SetCurrentEmotionVector(EmotionVector* vector);

		/*! \brief Sets new current mood vector
		 *	
		 *	Sets new current mood vector
		 *	\param vector new vector to be set
		 */
		void SetMoodVector(MoodVector* vector);

	public:
		/*! \brief Base VectorEvaluator constructor
		 *	
		 *	Base constructor for VectorEvaluator
		 *	\param model The parent model for this evaluator, the model is the owner of evaluator
		 */
		VectorEvaluator(CharacterModel& model);

		/*! \brief Evaluates emotion vector
		 *	
		 *	Evaluates target emotion vector based on experts models output
		 *	\param eecEvent The event for which the new emotion state is computed
		 *	\return if the evaluator was able to compute the vector, false otherwise
		 */
		virtual bool Evaluate(EecEvent& eecEvent) = 0;

	};

}