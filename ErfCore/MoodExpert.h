#pragma once
#include "MoodModel.h"
#include "ExternalExpert.h"

/*! \file MoodExpert.h
	\brief MoodExpert header file

*/

namespace erf
{
	/*! \brief	Represents an external expert that acts as an MoodModel
	 *			
	 *	This class is a specialization of ExternalExpert.
	 *	Represents an external expert that was dynamically loaded into the library and acts as a MoodModel.
	 *	See ExternalComponent about more information about component states and parameters.
	 *	
	 */
    class ESFCORE_API MoodExpert : public ExternalExpert, public MoodModel
    {
		private:
		public:
			/*!	\brief The base constructor for MoodExpert
			 *	
			 *	The base constructor for MoodExpert. It will register itself inside supplied configuration and unregister upon
			 *	destruction. The component type will be set to ComponentType::EXPERT and the expert type will be set to STD_MOOD.
			 *	The expert will be recognized as an MoodModel in the framework.
			 *	\param configuration The configuration of this external component and the library it comes from
			 */
			MoodExpert(ExternalComponentConfiguration* configuration);

			/*!	\brief The base copy constructor for MoodExpert
			 *	
			 *	The base copy constructor for MoodExpert
			 *	\param other The original MoodExpert
			 */
			MoodExpert(const MoodExpert &other);
			virtual ~MoodExpert();
	};
}