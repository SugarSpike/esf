#include "WeightedMeanFloatVectorEvaluator.h"
#include "EmotionVector.h"
#include "ErfForEach.h"
#include "Variant.h"


namespace erf {

	WeightedMeanFloatVectorEvaluator::WeightedMeanFloatVectorEvaluator(CharacterModel& model) :
		BasicObject("WeightedMeanFloatVectorEvaluator"), WeightedMeanVectorEvaluator(model)
	{

	}

	void WeightedMeanFloatVectorEvaluator::ValidateVectors(vector<EmotionVector*>& vectors,vector<int>& highestRankModelType, bool highestIsUniform) { }

	EmotionVector* WeightedMeanFloatVectorEvaluator::AggregateVectors(vector<EmotionVector*>& vectors) {

		EmotionVector* aggregatedVector = new EmotionVector(vectors[0]->GetEmotionModelTypes());
		map<string,Variant*>& parametersMapOfFirst =  vectors[0]->GetValues();
		for (auto parameterIterator = parametersMapOfFirst.begin() ; parameterIterator != parametersMapOfFirst.end(); ++parameterIterator)
		{
			string emotionName = parameterIterator->first;
			float averageDivisor = 0.0f;
			float averageDividend = 0.0f;
			float temporary = 0.0f;

			for_each_erf(EmotionVector* vector, vectors)
			{
				temporary = (vector->GetCertainty(emotionName)+vector->GetModelRank());
				averageDividend += temporary * vector->GetValue(emotionName)->AsFloat();
				averageDivisor += temporary;
			}

			aggregatedVector->AddValue(parameterIterator->first,Variant::Create(averageDividend/averageDivisor));
		}
		return aggregatedVector;
	}

}