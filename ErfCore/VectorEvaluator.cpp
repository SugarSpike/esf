#include "VectorEvaluator.h"

namespace erf {

	
	VectorEvaluator::VectorEvaluator(CharacterModel& model) :
		BasicObject("VectorEvaluator"), model(model)
	{
	}
	void VectorEvaluator::SetUnfilteredEmotionVector(EmotionVector* vector) {
		this->model.GetCharacterData().SetCurrentUnfilteredEmotionVector(vector);
	}
	void VectorEvaluator::SetCurrentEmotionVector(EmotionVector* vector) {
		this->model.GetCharacterData().SetCurrentEmotionVector(vector);
	}

	void VectorEvaluator::SetMoodVector(MoodVector* vector){
		this->model.GetCharacterData().SetCurrentMoodVector(vector);
	}

}