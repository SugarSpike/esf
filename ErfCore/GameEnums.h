#pragma once


/*! \file GameEnums.h
	\brief Contains enum declarations for Game Information Module
*/

namespace erf
{
	/*! \brief	Type/genre of the game		
	 *	
	 *	Contains definitions for game genres
	 */
	enum GameType {
		STD_NONE = 0, ///< No genre specified
		STD_FPS , ///< First person shooter
		STD_TPS, ///< Third person shooter
		STD_RTS, ///< Real time strategy
		STD_RPG, ///< Role playing game
		STD_TFG, ///< Traditional fighting game
		STD_PLATFORM, ///< Platform game genre
		STD_MMOFPS, ///< Massively multiplayer online first person shooter
		STD_MMORPG, ///< Massively multiplayer online role playing game
		STD_MMORTS, ///< Massively multiplayer online real time strategy
		STD_RAIL, ///< Rail genre
		EXT_GAME_TYPE ///< Use to mark the end of standard supported genres, can be use to expand the enum
	};

	/*! \brief	Characteristics of the game	
	 *	
	 *	Characteristics/keywords that can be associated with the game
	 */
	enum GameCharateristics {
		STD_GOTHIC = 0, ///< Gothic
		STD_DARK, ///< Dark
		STD_GORE, ///< Gore
		STD_ACTION, ///< Action
		STD_SHOOTER, ///< Shooter
		STD_ACTION_ADVENTURE, ///< Action adventure
		STD_STRATEGY, ///< Strategy
		STD_RACING, ///< Racing
		STD_ADULT, ///< Adult
		STD_EDUCATIONAL, ///< Educational
		STD_CASUAL, ///< Casual
		EXT_GAME_CHARACTERISTICS ///< Use to mark the end of standard supported characteristics, can be use to expand the enum
	};

	/*! \brief Challenges that can be found in the game	
	 *	
	 *	Contains definitions for game challenges
	 */
	enum GameChallenges {
		STD_BOSS_FIGHT = 0, ///< Boss fight
		STD_DONT_DIE, ///< Do not die
		STD_RACE_WITH_TIME, ///< Race with time
		STD_LOGIC_RIDDLE, ///< Logic riddle
		STD_TREASURE_FINDING, ///< Treasure find
		STD_MOVE_PRECISION, ///< Move precision
		STD_REACTION_TIME, ///< Reation time
		STD_REACH_DIFFICULT_LOCATION, ///< Difficult location
		EXT_GAME_CHALLENGE ///< Use to mark the end of standard supported challenges, can be use to expand the enum
	};

	/*! \brief Game events that can be send by the game
	 *	
	 *	The game event ids that are send by the game. Used to define type of EecEvent.
	 */
	enum GameEvent {
		STD_TEST_EVENT = 0, ///< A test event
		STD_PLAYER_EMOTIONAL_STATE_CHANGE, ///< The emotional state of player has changed
		STD_ACTOR_EMOTIONAL_STATE_CHANGE, ///< The emotional state of an actor has changed
		STD_TIME_ELAPSED, ///< The passing of time, most common of events. Sent at defined intervals by the game
		STD_LEVEL_LOADED, ///< The event is send upon the load of a game level
		STD_PLAYER_KILLED, ///< The event is sent upon player death
		STD_ACTOR_KILLED, ///< The event is sent upon actor death
		STD_ACTOR_ATTACKED, ///< The event is sent when an actor is attacked
		STD_ACTOR_THREATENED, ///< The event is sent when an actor is threatened
		STD_OBSTACLES_CLOSE, ///< The event is sent when the player is getting closed to a game obstace
		STD_SKILL_USED, ///< The event is sent when the player/actor uses a skill
		EXT_GAME_EVENT ///< Use to mark the end of standard supported game events, can be use to expand the enum
	};
}