/*! \brief	Defines the for each macro for Erf
*	
*	Defines the for each macro for Erf - at the moment Erf uses boost for each (BOOST_FOREACH)
*/
#define for_each_erf BOOST_FOREACH
#include <boost\foreach.hpp>

/*! \file ErfForEach.h
	\brief ErfForEach header file - contains for each macro
*/