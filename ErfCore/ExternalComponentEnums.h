#pragma once

/*! \file ExternalComponentEnums.h
	\brief Contains enum declarations for External Components Module
*/

namespace erf
{
	/*! \brief	Types of supported ExternalComponents
	 *			
	 *	Types of supported ExternalComponents
	 */
	enum ComponentType {
		LIBRARY, ///< Type for external library, represents the loaded dll (ExternalComponentLibrary)
		SENSOR,	///< Type for external sensor (EmotionSensor)
		EXPERT,	///< Type for external expert (ExternalExpert)
		FILTER,	///< Type for external filter (ExternalFilter)
		EXT_COMPONENT ///< Use to mark the end of standard supported types, can be use to expand the enum
	};

	/*! \brief	States of a ExternalComponent
	 *			
	 *	States of a ExternalComponent
	 */
	enum ComponentState {
		UNINITIALIZED,	///< Unitialize component
		INITIALIZED,	///< Initialized component
		REINITIALIZED,	///< Reinitialized component
		DEINITIALIZED,	///< Deinitialized component
		COMPONENT_ERROR,///< Component is in error state
		UNDEFINED		///< The component is in unidentified state
	};

	/*! \brief	Types of EmotionSensor
	 *			
	 *	Represents the types of data the EmotionSensor provides (eg. heart rate, blood volume pulse)
	 */
	enum SensorType {
		STD_UNDEF_SENSOR = 0,	///< Undefined output
		STD_HEART_RATE,			///< Provides heart rate / pulse
		STD_BVP,				///< Provides blood volume pulse
		STD_SKIN_CONDUCTANCE,	///< Provide skin conductance
		STD_EMG,				///< Provide Electromyography data
		STD_AU,					///< Provide Animation Unit data
		STD_FPP,				///< Provide Feature Point Positions data
		EXT_SENSOR				///< Use to mark the end of standard supported sensor types, can be use to expand the enum
	};

	/*! \brief	Types of ExternalExpert
	 *			
	 *	Defines the type of an ExternalExpert. Base on the value the expert fills different roles in the system.
	 *	Eg. an STD_EMOTION expert will be seen by the framework not only as an ExternalExpert but a EmotionModel as well.
	 */
	enum ExpertType {
		STD_UNDEF_EXPERT = 0,	///< The type is undefined
		STD_EMOTION,			///< The expert is an EmotionModel
		STD_MOOD,				///< The expert is a MoodModel
		STD_PSYCHOLOGICAL,		///< The expert is a PsychologicalModel
		EXT_EXPERT				///< Use to mark the end of standard supported expert types, can be use to expand the enum
	};
}