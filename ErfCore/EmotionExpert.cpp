#pragma once
#include "EmotionExpert.h"
#include "ErfException.h"

namespace erf {
	
	EmotionExpert::EmotionExpert(ExternalComponentConfiguration* pConfiguration): 
		BasicObject("EmotionExpert"),ExternalExpert(pConfiguration, ExpertType::STD_EMOTION)
	{
	}

	EmotionExpert::EmotionExpert(const EmotionExpert &other): 
		BasicObject(other.objectName),ExternalExpert(other.configuration, ExpertType::STD_EMOTION)
	{
		throw new ErfException("EmotionExpert::EmotionExpert(const EmotionExpert &other) called");
	}

	EmotionExpert::~EmotionExpert() { }

}