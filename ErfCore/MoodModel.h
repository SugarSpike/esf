#pragma once
#include "BasicObject.h"

/*! \file MoodModel.h
	\brief MoodModel header file
*/

//=================================
// forward declared dependencies
namespace erf
{
	class MoodVector;
	class EmotionVector;
	class CharacterData;
	class EecEvent;
}
using namespace std;

namespace erf
{
	/*! \brief	Mood model computes new MoodVector%s based on the data supplied by Erf Framework
	 *			
	 *	The MoodModel is one of the core components of the framework. It is used to compute new MoodVector%s based on the data supplied by Erf Framework.
	 *	The data includes (but is not limited to):
	 *		- Current and previous EmotionVector%s
	 *		- Information from the game itself (passed throu EecEvent)
	 *		- Sensory data supplied by external sensors (passed throu CharacterData)
	 *		- Information about the personality of the character
	 *		- Other user data passed by CharacterData
	 *
	 *	The overall input into the evaluated vectors (what the vector actually contains) depends on the VectorEvaluator choosen by a CharacterModel.
	 *
	 */
    class ESFCORE_API MoodModel : public virtual BasicObject
    {
	public:
		/*! \brief	Returns the model rank
		 *			
		 *	Return the model rank. The rank is an arbitrary number of range <0.0f,1.0f> supplied by the creator of the MoodModel.
		 *	The rank should reflect the overall correctivnes of the algoritm for computing mood emotions that this model represents.
		 *	It is used by VectorEvaluator%s to weight the outputs of all mood models when computing the final MoodVector.
		 *
		 *	\return the model rank
		 */
		virtual float GetModelRank() = 0;

		/*! \brief	Initialize the model for given CharacterModel/CharacterData
		 *			
		 *	The method is called upon registering the model to a certain CharacterModel. All character dependend initialization
		 *	should be done in this function (like setting mood model initial data in CharacteData).
		 *
		 *	\param characterData the character data for initialization
		 */
		virtual void InitializeForCharacter(CharacterData& characterData) = 0;

		/*! \brief	Whenever the model supports an EmotionVector of specified type
		 *			
		 *	Whenever the model supports an EmotionVector of specified type. It does not necessarily means that the output of the model
		 *	will not be used by VectorEvaluator%s when the function return false. As the behaviour is dependend on concrete VectorEvaluator
		 *	implementation.
		 *
		 *	\param emotionVector to check
		 *	\return whenever the model supports specified emotion vector type
		 */
		virtual bool IsEmotionVectorSupported(EmotionVector& emotionVector) = 0;

		/*! \brief	Generates new MoodVector in response to a new EmotionVector
		 *			
		 *	Generates new MoodVector in response to EmotionVector and based on passed data (throu CharacterData)
		 *
		 *	\param event Emotion eliciting condition event in response to which the new emotion vector is computed
		 *	\param characterData Holds information about the character (see CharacterData for more information)
		 *	\return new computed MoodVector, null if computation fails
		 */
		virtual MoodVector* HandleEmotionVector(EecEvent& event, CharacterData& characterData) = 0;

	};
}