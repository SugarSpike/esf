#pragma once
#include "Common.h"
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <cctype>

/*! \file ErfUtils.h
	\brief ErParameterfUtils header file - contains utility functions for ErfFramework for parameter parsing
*/

namespace erfutils
{
	
	int ParseInt(const std::string& parameterValue) {
		std::stringstream ss(parameterValue);
		int i;
		if ((ss >> i).fail() || !(ss >> std::ws).eof())
			throw std::bad_cast();
		return i;
	}

	float ParseFloat(const std::string& parameterValue) {
		double temp = ::atof(parameterValue.c_str());
		return (float)temp;
	}

	double ParseDouble(const std::string& parameterValue) {
		double temp = ::atof(parameterValue.c_str());
		return (float)temp;
	}

	bool ParseBoolean(std::string parameterValue) {
		std::transform(parameterValue.begin(), parameterValue.end(), parameterValue.begin(), ::tolower);
		std::istringstream is(parameterValue);
		bool b;
		is >> std::boolalpha >> b;
		return b;
	}

	int ParseIntFromBool(std::string parameterValue) {
		bool result = ParseBoolean(parameterValue);
		if(result)
			return 1;

		return 0;
	}

}