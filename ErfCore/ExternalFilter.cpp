#pragma once
#include "ExternalFilter.h"
#include "ErfLogging.h"
#include "ErfException.h"
#include "EmotionVectorFilter.h"
#include "ExternalComponentConfiguration.h"
#include "ExternalComponentLibrary.h"

namespace erf {

	ExternalFilter::ExternalFilter(ExternalComponentConfiguration* pConfiguration): 
			BasicObject("ExternalFilter"), ExternalComponent(pConfiguration, ComponentType::FILTER)
	{
	}

	ExternalFilter::ExternalFilter(const ExternalFilter &other): 
		BasicObject(other.objectName), ExternalComponent( other.configuration, ComponentType::EXPERT)
	{
		throw new ErfException("ExternalFilter::ExternalFilter(const ExternalFilter &other) called");
	}

	ExternalFilter* ExternalFilter::CloneFilter()
	{
		return this->configuration->GetLibraryComponent()->CreateFilterForName(this->GetName());
	}

	ExternalFilter::~ExternalFilter() { }

	EmotionVectorFilter* ExternalFilter::CastToEmotionVectorFilter(ExternalFilter* base) {
				return dynamic_cast<EmotionVectorFilter*>(base);
	}

}