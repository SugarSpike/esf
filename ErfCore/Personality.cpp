#include "Personality.h"
#include "Variant.h"
#include <ctime>

namespace erf {

	Personality::Personality() : BasicObject("Personality")
	{
		this->timeOfEvaluation = time(0);
	}
	
	time_t Personality::GetTimeOfEvaluation() { return this->timeOfEvaluation; }

	void Personality::SetTimeOfEvaluation(time_t newTime) { this->timeOfEvaluation = newTime; }

	Personality* Personality::Clone() {
		Personality* clone = new Personality();
		clone->timeOfEvaluation = this->timeOfEvaluation;
		this->CopyInto(*clone);
		return clone;
	}

}