#pragma once
#include "Common.h"
#include <Windows.h>

/*! \file ErfUtils.h
	\brief ErfUtils header file - contains utility functions for ErfFramework
*/

namespace erfutils
{
	/*! \brief	Returns current working directory path
	*
	*	Returns current working directory path
	*	\return Current working directory path
	*/
	string GetCurrentExecutingDirectory();

	/*! \brief	Converts std::string to LPCWSTR
	*
	*	Converts std::string to LPCWSTR
	*	\param str String to convert
	*	\return Converted string
	*/
	LPCWSTR GetWStr(string& str);

	/*! \brief	Checks if file under specified path exists
	*	
	*	Checks if file under specified path exists
	*	\param name Path to the file
	*	\return Whenever the file exists
	*/
	bool FileExists (const std::string& name);

	/*! \brief	Returns the last error message from underlying OS
	*	
	*	Returns the last error message from underlying OS
	*	\return Last error message
	*/
	string GetLastErrorStdString();

	/*! \brief	Converts std::string to std::wstring
	*	
	*	Converts std::string to std::wstring
	*	\param s String to convert
	*	\return Converted string
	*/
	std::wstring GeWCStr(const std::string& s);
	

	/*! \brief	Check if specified element can be found in specified vector
	*	
	*	Check if specified element can be found in specified vector
	*	\param vec The vector to check
	*	\param el The element to check
	*	\return Whenever the vector contains the element
	*/
	template <typename T> 
	bool Contains(vector<T>& vec, T el)
	{
		return std::find(vec.begin(), vec.end(), el) != vec.end();
	}

	/*! \brief	Inserts the specified element into the vector if it is not already present
	*	
	*	Inserts the specified element into the vector if it is not already present
	*	\param vec The vector to insert to
	*	\param el The element to insert
	*	\return Whenever the insertion was successful
	*/
	template <typename T> 
	bool InsertIfNotContains(vector<T>& vec, T el)
	{
		bool contains = erfutils::Contains(vec,el);
		if(!contains)
			vec.push_back(el);

		return !contains;
	}

	/*! \brief	Removes the specified element from a vector
	*	
	*	Removes the specified element from a vector
	*	\param vec The vector to remove from
	*	\param el The element to remove
	*	\return Whenever the remove of element was successful
	*/
	template <typename T,typename Y> 
	bool Remove(vector<T>& vec, Y el)
	{
		T cast = dynamic_cast<T>(el);
		if(cast == null)
			return false;

		vector<T>::iterator position =  std::find(vec.begin(), vec.end(), cast);
		if (position != vec.end())
		{
			vec.erase(position);
			return true;
		}
		return false;
	}


	/*! \brief	Removes the specified element by BasicObject name from a vector
	*	
	*	Removes the specified element by BasicObject name from a vector. The vector must contain only BasicObject elements
	*	\param vec The vector to remove from
	*	\param name NAme of the element to remove
	*	\return Whenever the remove of element was successful
	*/
	template <typename T> 
	bool RemoveByName(vector<T>& vec, string name)
	{
		for ( auto it = vec.begin(); it != vec.end(); it++ ) {
			erf::BasicObject* cast = ((erf::BasicObject*) *it);
			if(cast->GetName() == name)
			{
				vec.erase(it);
				return true;
			}
		}
		return false;
	}

	/*! \brief	Removes the specified element from a vector
	*	
	*	Removes the specified element from a vector
	*	\param vec The vector to remove from
	*	\param el The element to remove
	*	\return Whenever the remove of element was successful
	*/
	template <typename T> 
	bool Remove(vector<T>& vec, T el)
	{
		vector<T>::iterator position =  std::find(vec.begin(), vec.end(), el);
		if (position != vec.end())
		{
			vec.erase(position);
			return true;
		}
		return false;
	}
	
	/*! \brief	Prints a vector
	*	
	*	Prints a vector
	*	\param vec The vector to print 
	*	\return The vector to print
	*/
	template <typename T> 
	string PrintVector(vector<T>& vec)
	{
		std::ostringstream oss;

	  if (!vec.empty())
	  {
		// Convert all but the last element to avoid a trailing ","
		std::copy(vec.begin(), vec.end()-1,
			std::ostream_iterator<T>(oss, ","));

		// Now add the last element with no delimiter
		oss << vec.back();
	  }
	  return oss.str();
	}

}