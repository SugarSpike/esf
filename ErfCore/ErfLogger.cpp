#include "ErfLogging.h"
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/severity_logger.hpp>



namespace erf {

	ErfLogger::ErfLogger()
	{
		lg = new boost::log::sources::severity_logger<erf::severity_level>();
	}

	void ErfLogger::SetAllStreamsToLoggingFile(string filePath)
	{
		freopen(filePath.c_str(), "a", stdout);
	}

	void ErfLogger::SetLoggingFile(string filePath)
	{
		freopen(filePath.c_str(), "a", stdout);
	}

	void ErfLogger::SetLoggingEnabled(bool enable){
		boost::log::core::get()->set_logging_enabled(enable);
	}

	bool ErfLogger::GetLoggingEnabled(){
		return boost::log::core::get()->get_logging_enabled();
	}

	ErfLogger::~ErfLogger() {
		delete lg;
	}

}