#pragma once
#include "WeightedMeanVectorEvaluator.h"

/*! \file WeightedMeanDoubleVectorEvaluator.h
	\brief WeightedMeanDoubleVectorEvaluator header file

*/

namespace erf
{
	/*! \brief	Computes target emotion vector as an weighted average of all expert vectors
	 *			
	 *	The evaluator computes target emotion vector based on experts models output (both mood and emotion).
	 *	The evaluator does no validity checks for sake of performance. It assumes that:
	 *  -All intermediated vectors will be of the same emotion model type.
	 *	-All vector values must be of type double. 
	 *  -All vectors must be uniform.
	 *	
	 *	If the above assumptions are not satisfied then the result is undefined;
	 *
	 *	The EmotionVectorFilter%s of EmotionVectorFilterType::Before will be applied to all intermediate vectors.
	 *	The EmotionVectorFilter%s of EmotionVectorFilterType::After will be applied to the target vector.
	 */
	class ESFCORE_API WeightedMeanDoubleVectorEvaluator : public WeightedMeanVectorEvaluator
    {
	protected:
		
		/*! \brief This implementation does no validation on the vectors
		 *  
		 *  This implementation does no validation on the vectors. The function body is empty.
		 *	\param[in,out] vectors The vectors to be validated, the function will remove invalid vectors from this vector
		 *	\param[in] highestRankModelType All emotion model types supported by the expert with highest rank
		 *	\param[in] highestIsUniform The uniform flag of the expert with highest rank
		 */
		virtual void ValidateVectors(vector<EmotionVector*>& vectors,vector<int>& highestRankModelType, bool highestIsUniform);
		
		virtual EmotionVector* AggregateVectors(vector<EmotionVector*>& vectors);

	public:
		
		/*! \brief	Base constructor for WeightedMeanDoubleVectorEvaluator
		 *  
		 *	Base constructor for WeightedMeanDoubleVectorEvaluator
		 *	\param model The parent CharacterModel
		 */
		WeightedMeanDoubleVectorEvaluator(CharacterModel& model);

	};

}