#pragma once
#include "Common.h"
/*! \file BasicObject.h
	\brief BasicObject header file
*/

namespace boost 
{
	namespace uuids 
	{
		/*! \brief Forward declaration of boost::uuids::uuid
		 *
		 */
		struct uuid;
	}
}

namespace erf  
{
	/*! \brief	The base class for most components of the framework 
	 *			
	 *	The BasicObject is the base clss for most components of the framework. Contains the most base of information about 
	 *	a framework components like the name and id of the object. It also supplies equaility operators for object of the framework.
	 */
    class ESFCORE_API BasicObject
    {
	private:
		/*! \brief	Initialize the BasicObject 
		 *			
		 *	Initialize the BasicObject. Should be called by all constructors.
		 *
		 */
		 void InitializeBasicObject();

	protected:
		 boost::uuids::uuid& objectId;	///< Unique uuid
		 std::string objectIdStr;	///< The unique uuid cast to string (saved for performance)
		 std::string objectName;	///< The name of the component, not necessarily unique accross application 

	public:
	
		/*! \brief	Default constructor for BasicObject
		 *			
		 *	Default constructor for BasicObject
		 *	\param objectName name of the component
		 */
		BasicObject(string objectName);
		/*! \brief	Copy constructor for BasicObject
		 *			
		 *	Copy constructor for BasicObject
		 *	
		 */
		BasicObject(const BasicObject &other);
		/*! \brief	Default constructor for BasicObject
		 *			
		 *	Default constructor for BasicObject
		 */
		BasicObject();

		/*! \brief Return the component name
		 *	Return the component name. The name is not necessarily unique accross application .
		 *	
		 *	\return name of the component
		 */
		string GetName() { return this->objectName; }
		
		/*! \brief Sets a new name for a component
		 *	Sets a new name for a component. The name does not to be unique accross application .
		 *	
		 *	\return name of the component
		 */
		void SetName(string newName) { this->objectName = newName; }

		/*! \brief Retruns the unique uuid of the component.
		 *	
		 *	Returns the unique uuid of the component.
		 *	\return unique uuid
		 */
		string GetId() { return this->objectIdStr; }

		/*! \brief Retruns the unique name of the component.
		 *	
		 *	Returns the unique name of the component. The name is a concatenation of component name and uuid (joined by '::')
		 *	\return unique name
		 */
		string GetUniqueName() { return this->objectName + "::" + this->objectIdStr; }

		virtual ~BasicObject();

		/*! \brief Assignment operator
		 *
		 *	Assignment operator
		 */
		BasicObject& operator= (const BasicObject &rhs);

		/*! \brief Equality operator
		 *
		 * Equality operator
		 */
		bool operator==(BasicObject const& rhs) const;
		

	};
}