#pragma once
#include "VariantCollection.h"

/*! \file Personality.h
	\brief Personality header file
*/

namespace erf
{

	/*! \brief	A container holding personality traits evaluated by the PsychologicalModel%s 
	 *			
	 *	The Personality is a container for evaluated personality traits by the framework (specificly the PsychologicalModel%s).
	 *	It is a multi-type, multi-value generic container. The values that it contains depends on the PsychologicalModel implementation
	 */
    class ESFCORE_API Personality : public VariantCollection
    {
	private:
	public:
		private:
			time_t timeOfEvaluation; ///< Time of last evaluation of the Personality (as number of miliseconds since start of UNIX epoch)

		public:
			/*! \brief Base Personality constructor
			 *	
			 *	Base constructor for Personality
			 */
			Personality();

			/*! \brief Gets the last evaluation time
			 *
			 *  Returns the time of the evaluation of (as number of miliseconds since start of UNIX epoch)
			 *
			 *	\return time of evaluation
			 */
			time_t GetTimeOfEvaluation();

			/*! \brief Updates the evaluation time of this Personality
			 *
			 *  Updates the time of the evaluation of this Personality (as number of miliseconds since start of UNIX epoch)
			 *
			 *	\param newTime updated time of evaluation
			 */
			void SetTimeOfEvaluation(time_t newTime);

			/*! \brief Clones the personality
			 *
			 *  Returns a parial deep-clone of the personality. See Variant class for more details about partial-deep cloning.
			 *
			 *	\return clone of personality
			 */
			virtual Personality* Clone();
	};
}