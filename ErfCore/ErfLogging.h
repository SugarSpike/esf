#pragma once
/*! \file ErfLogger.h
	\brief ErfLogger header file - contains logging wrappers and logging control for Erf framework 
		   REQUIRES boost unlsess ERFCORE_DISABLE_LOGGING flag is enabled when compiling ErfCore
*/

#ifndef ERFCORE_DISABLE_LOGGING
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/severity_logger.hpp>

#define ERFLOGGING_INCLUDED
#include "ErfLogger.h"



/*! \brief The logging macro definition
*	
*	Used to log information, example of usage:
*	LOG(debug) << "This is a debug message
*	LOG(error) << "This is an error message";
*/
#define LOG(severity) BOOST_LOG_SEV((*erf::ErfLogger::GetLogger()),erf::severity_level::severity)

#else

#include "ErfLogger.h"
#define LOG(severity) null;

#endif