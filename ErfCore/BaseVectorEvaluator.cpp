#include "BaseVectorEvaluator.h"
#include "EmotionModel.h"
#include "ErfLogging.h"
#include "ErfForEach.h"
#include "EmotionVectorFilter.h"
#include "EmotionVector.h"
#include "EecEvent.h"

namespace erf {

	BaseVectorEvaluator::BaseVectorEvaluator(CharacterModel& model) :
		BasicObject("BaseVectorEvaluator"), VectorEvaluator(model)
	{

	}
		
	bool BaseVectorEvaluator::Evaluate(EecEvent& eecEvent) {
		EmotionVector* newEmotionVector = null;
		EmotionVector* afterFilterVector = null;

		for_each_erf(EmotionModel* model,this->model.GetRegisteredEmotionModels())
		{
			if(model->IsEventSupported(eecEvent))
			{
				LOG(debug) << "BaseVectorEvaluator::Evaluate - Delegate to EmotionModel: " << model->GetUniqueName();
				newEmotionVector = model->HandleEvent(eecEvent,this->model.GetCharacterData());
				break;
			}
		}

		if(newEmotionVector==null)
		{
			LOG(error) << "BaseVectorEvaluator::Evaluate - No EmotionModel found that handle event: " << eecEvent;
			return false;
		}

		afterFilterVector = newEmotionVector;
		if(this->model.GetRegisteredAfterFilters().size() > 0)
		{
			afterFilterVector = newEmotionVector->Clone();
			for_each_erf(EmotionVectorFilter* filter,this->model.GetRegisteredAfterFilters().getPrioritizedList())
			{
				if(filter->IsEmotionVectorSupported(*afterFilterVector))
				{
					LOG(debug) << "BaseVectorEvaluator::Evaluate - After Filter to Emotion Vector applied: " << filter->GetUniqueName();
					filter->DoFilter(*afterFilterVector,this->model.GetCharacterData());
				}
			}
		}
		this->SetCurrentEmotionVector(afterFilterVector);
		this->SetUnfilteredEmotionVector(newEmotionVector);
		return true;
	}
}