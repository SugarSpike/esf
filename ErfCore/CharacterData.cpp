#pragma once
#include "CharacterData.h"
#include "EmotionSensor.h"
#include "EmotionVector.h"
#include "MoodVector.h"
#include "ErfLogging.h"
#include "ErfUtils.h"
#include "ErfForEach.h"
#include "ErfException.h"
#include "Personality.h"
#include "GameConfiguration.h"
#include <boost/circular_buffer.hpp>

namespace erf
{
	CharacterData::CharacterData() :
			BasicObject("CharacterData"),
			moodVectorBufferSize(10),
			unfilteredEmotionVectorBufferSize(10),
			emotionVectorBufferSize(10),
			currentEmotionVector(null),
			currentUnfilteredEmotionVector(null),
			currentMoodVector(null){ 


		this->previousEmotionVectors = 
			new boost::circular_buffer<std::shared_ptr<EmotionVector> >(this->emotionVectorBufferSize);

		this->previousUnfilteredEmotionVectors = 
			new boost::circular_buffer<std::shared_ptr<EmotionVector> >(this->unfilteredEmotionVectorBufferSize);

		this->previousMoodVectors = 
			new boost::circular_buffer<std::shared_ptr<MoodVector> >(this->moodVectorBufferSize);

		this->personality = new Personality();
		this->gameConfiguration = new GameConfiguration();
	}

	CharacterData::~CharacterData() {
		delete this->previousEmotionVectors;
		delete this->previousUnfilteredEmotionVectors;
		delete this->previousMoodVectors;

		if(this->currentEmotionVector != this->currentUnfilteredEmotionVector)
			delete this->currentUnfilteredEmotionVector;

		delete this->currentEmotionVector;
		delete this->currentMoodVector;

		if(this->isGameConfigurationOwner)
			delete this->gameConfiguration;

		if(this->isPersonalityOwner)
			delete this->personality;
	}

	//Configuration
	unsigned int CharacterData::GetEmotionVectorBufferSize() {
		return this->emotionVectorBufferSize;
	}
	unsigned int CharacterData::GetUnfilteredEmotionVectorBufferSize(){
		return this->unfilteredEmotionVectorBufferSize;
	}
	unsigned int CharacterData::GetMoodVectorBufferSize(){
		return this->moodVectorBufferSize;
	}

	void CharacterData::SetEmotionVectorBufferSize(unsigned int newSize){
		this->emotionVectorBufferSize =  newSize;
		if(newSize == 0)
		{
			this->previousEmotionVectors->set_capacity(1);
			return;
		}
		this->previousEmotionVectors->set_capacity(newSize);
	}
	void CharacterData::SetUnfilteredEmotionVectorBufferSize(unsigned int newSize){
		this->unfilteredEmotionVectorBufferSize =  newSize;
		if(newSize == 0)
		{
			this->previousUnfilteredEmotionVectors->set_capacity(1);
			return;
		}
		this->previousUnfilteredEmotionVectors->set_capacity(newSize);

	}
	void CharacterData::SetMoodVectorBufferSize(unsigned int newSize){
		this->moodVectorBufferSize =  newSize;
		if(newSize == 0)
		{
			this->previousMoodVectors->set_capacity(1);
			return;
		}
		this->previousMoodVectors->set_capacity(newSize);
	}

	//Emotion and Mood Vectors
	void CharacterData::SetCurrentEmotionVector(EmotionVector* vector) 
	{
		if(this->currentEmotionVector != null)
		{
			if(this->currentEmotionVector == this->currentUnfilteredEmotionVector)
			{
				std::shared_ptr<EmotionVector> shared(this->currentEmotionVector);
				//cout << "CharacterData::SetCurrentEmotionVector: push shared" << endl;
				this->previousEmotionVectors->push_back(shared);
				//std::shared_ptr<EmotionVector> otherShared = shared;
				this->previousUnfilteredEmotionVectors->push_back(shared);
				this->currentUnfilteredEmotionVector = null;
			}
			else
			{
				//cout << "CharacterData::SetCurrentEmotionVector: push current" << endl;
				this->previousEmotionVectors->push_back
					(std::shared_ptr<EmotionVector>(this->currentEmotionVector));
			}
		}
		this->currentEmotionVector = vector;
	}

	void CharacterData::SetCurrentUnfilteredEmotionVector(EmotionVector* vector)
	{
		if(this->currentUnfilteredEmotionVector != null)
		{
			if(this->currentEmotionVector == this->currentUnfilteredEmotionVector)
			{
				std::shared_ptr<EmotionVector> shared(this->currentEmotionVector);
				this->previousEmotionVectors->push_back(shared);
				//std::shared_ptr<EmotionVector> otherShared = shared;
				this->previousUnfilteredEmotionVectors->push_back(shared);
				this->currentEmotionVector = null;
			}
			else
			{
				this->previousUnfilteredEmotionVectors->push_back
					(std::shared_ptr<EmotionVector>(this->currentUnfilteredEmotionVector));
			}
		}
		this->currentUnfilteredEmotionVector = vector;
	}
		
	void CharacterData::SetCurrentMoodVector(MoodVector* vector) {
		if(this->currentMoodVector != null)
		{
			this->previousMoodVectors->push_back
				(std::shared_ptr<MoodVector>(this->currentMoodVector));
		}
		this->currentMoodVector = vector;
	}

	EmotionVector* CharacterData::GetCurrentEmotionVector() {
		return this->currentEmotionVector;
	}

	EmotionVector* CharacterData::GetPreviousEmotionVector()
	{
		//cout << "CharacterData::GetPreviousEmotionVector: " << this->previousEmotionVectors->size()  << endl;;
		if(this->previousEmotionVectors->size() == 0)
			return null;

		EmotionVector* previous = this->previousEmotionVectors->at(this->previousEmotionVectors->size()-1).get();
		//cout << "CharacterData::GetPreviousEmotionVector: returning " << (int) previous << endl;
		return previous;
	}


	void CharacterData::GetPreviousEmotionVectors(vector<EmotionVector* >& destination, int previousVectorsCount)
	{
		int retrievedData = 0;
		for( int i = this->previousEmotionVectors->size()-1 ; i >= 0 ; i--)
		{
			if(retrievedData == previousVectorsCount)
				return;
			destination.push_back(this->previousEmotionVectors->at(i).get());
		}
	}

	EmotionVector* CharacterData::GetCurrentUnfilteredEmotionVector() {
		return this->currentUnfilteredEmotionVector;
	}

	EmotionVector* CharacterData::GetPreviousUnfilteredEmotionVector()
	{
		if(this->previousUnfilteredEmotionVectors->size() == 0)
			return null;

		return this->previousUnfilteredEmotionVectors->at(this->previousUnfilteredEmotionVectors->size()-1).get();
	}

	void CharacterData::GetPreviousUnfilteredEmotionVectors(vector<EmotionVector* >& destination,int previousVectorsCount) {
		int retrievedData = 0;
		for( int i = this->previousUnfilteredEmotionVectors->size()-1 ; i >= 0 ; i--)
		{
			if(retrievedData == previousVectorsCount)
				return;
			destination.push_back(this->previousUnfilteredEmotionVectors->at(i).get());
		}
	}

	MoodVector* CharacterData::GetCurrentMoodVector() {
		return this->currentMoodVector;
	}

	MoodVector* CharacterData::GetPreviousMoodVector()
	{
		if(this->previousMoodVectors->size() == 0)
			return null;

		return this->previousMoodVectors->at(this->previousMoodVectors->size()-1).get();
	}

	void CharacterData::GetPreviousMoodVectors(vector<MoodVector* >& destination,int previousVectorsCount){
		int retrievedData = 0;
		for( int i = this->previousMoodVectors->size()-1 ; i >= 0 ; i--)
		{
			if(retrievedData == previousVectorsCount)
				return;
			destination.push_back(this->previousMoodVectors->at(i).get());
		}
	}


	//Personality

	Personality& CharacterData::GetPersonality() {
		return *this->personality;
	}

	void CharacterData::SetPersonality(Personality* personality){
		if(personality == null)
			throw ErfException("Cannot set Personality to null");

		if(this->isPersonalityOwner)
			delete this->personality;

		this->personality = personality;
		this->isPersonalityOwner = true;
	}

	void CharacterData::SetPersonality(Personality* personality,bool isMemoryOwner){
		if(personality == null)
			throw ErfException("Cannot set Personality to null");

		if(this->isPersonalityOwner)
			delete this->personality;

		this->personality = personality;
		this->isPersonalityOwner = isMemoryOwner;
	}

	//Game Configuration

	GameConfiguration& CharacterData::GetGameConfiguration() {
		return *this->gameConfiguration;
	}

	void CharacterData::SetGameConfiguration(GameConfiguration* configuration) {
		if(this->isGameConfigurationOwner)
			delete this->gameConfiguration;

		this->gameConfiguration = configuration;
		this->isGameConfigurationOwner = true;
	}

	void CharacterData::SetGameConfiguration(GameConfiguration* configuration,bool isMemoryOwner) {
		if(this->isGameConfigurationOwner)
			delete this->gameConfiguration;

		this->gameConfiguration = configuration;
		this->isGameConfigurationOwner = isMemoryOwner;
	}

	//Sensors

	bool CharacterData::RegisterSensor(EmotionSensor* sensor) {
		if(erfutils::Contains(this->sensors,sensor))
			return false;

		this->sensors.push_back(sensor);
		return true;
	}

	bool CharacterData::UnregisterSensor(EmotionSensor* sensor){
		return erfutils::Remove(this->sensors,sensor);
	}

	bool CharacterData::UnregisterSensorByName(string sensorName){
		return erfutils::RemoveByName(this->sensors,sensorName);
	}

	const vector<EmotionSensor*>& CharacterData::GetSensors() { return sensors; }

	void CharacterData::GetSensorsOfType(vector<EmotionSensor*>& destination, int sensorType) {
		for_each_erf(EmotionSensor* sensor ,this->sensors)
		{
			if(erfutils::Contains(sensor->GetSensorTypes(), sensorType))
				destination.push_back(sensor);
		}
	}

	EmotionSensor*  CharacterData::GetFirstSensorOfType(int sensorType) {
		for_each_erf(EmotionSensor* sensor ,this->sensors)
		{
			if(erfutils::Contains(sensor->GetSensorTypes(), sensorType))
				return sensor;
		}
		return null;
	}

	EmotionSensor*  CharacterData::GetSensorByName(string sensorName) {
		for_each_erf(EmotionSensor* sensor ,this->sensors)
		{
			if(sensor->GetName() == sensorName)
				return sensor;
		}
		return null;
	}

}