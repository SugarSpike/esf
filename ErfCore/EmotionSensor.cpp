#pragma once
#include "EmotionSensor.h"
#include "ExternalComponentConfiguration.h"
#include "ExternalComponentLibrary.h"

namespace erf {
	EmotionSensor::EmotionSensor(ExternalComponentConfiguration* pConfiguration, SensorType sensorType): 
			BasicObject("EmotionSensor"),ExternalComponent(pConfiguration, ComponentType::SENSOR)
	{
		this->sensorTypes.push_back(sensorType);
	}

	EmotionSensor::EmotionSensor(const EmotionSensor &other): 
		BasicObject("EmotionSensor"),ExternalComponent(other.configuration, ComponentType::SENSOR),
		sensorTypes(other.sensorTypes)
	{
	}

	EmotionSensor* EmotionSensor::CloneSensor()
	{
		return this->configuration->GetLibraryComponent()->CreateSensorForName(this->GetName());
	}

	EmotionSensor::~EmotionSensor() { }
}