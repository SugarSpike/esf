#include "VariantCollection.h"
#include "Variant.h"
#include "ErfLogging.h"
//#include "boost\any.hpp"
#include <sstream> 
#include "boost\spirit\home\support\detail\hold_any.hpp"

namespace erf {

	VariantCollection::VariantCollection() :BasicObject("VariantCollection") { }
	
	VariantCollection::VariantCollection(const VariantCollection &other) {
		LOG(debug) << "VariantCollection::CopyCnstr - this "<< this ;
		LOG(debug) << "VariantCollection::CopyCnstr - other "<< &other ;
		for (auto variant = other.nameToValue.begin(); 
			variant != other.nameToValue.end(); 
			++variant)
		{
			Variant* copy = variant->second != null ? variant->second->Clone() : null;
			this->AddValue(variant->first,variant->second);
			
		}
	}

	VariantCollection::~VariantCollection() {
		for(unsigned int i = 0 ; i < this->values.size() ; i++)
			delete this->values[i];

	}

	int VariantCollection::ValueCount() {
		return this->values.size();
	}

	map<string, Variant*>& VariantCollection::GetValues() { return this->nameToValue; }

	void VariantCollection::AddValue(string name, Variant* data) {
		auto it = this->nameToValue.find(name);
		if(it != this->nameToValue.end()) {
		   this->values[this->nameToIndex[name]] = data;
		   delete it->second;
		   this->nameToValue[name] = data;
		}
		else {
			this->values.push_back(data);
			this->nameToIndex[name] = this->values.size()-1;
			this->indexToName[this->values.size()-1] = name;
			this->nameToValue[name] = data;
		}
	}

	
	Variant* VariantCollection::GetValueByIndex(unsigned int index) {
		if(this->values.size() >= index)
		{
			return this->values[index];
		}
		LOG(error) << "Could not retrieve index: "<< index << ", current size is: " << this->values.size();
		return null;
	}

	Variant* VariantCollection::GetValue(string name) {
		if(this->nameToValue.count(name) == 0)
			return null;

		return this->nameToValue[name];
	}
	
	void VariantCollection::CopyInto(VariantCollection& other) {
		for(unsigned int i = 0 ; i < this->values.size() ; i++)
		{
			Variant* copy = this->values[i] != null ? this->values[i]->Clone() : null;
			other.AddValue(this->indexToName[i], copy);
		}
	}

	
	string VariantCollection::Print() {
		std::stringstream ss;
		ss << "VariantCollection [" << endl;
		for (auto variant = this->nameToValue.begin(); 
			variant != this->nameToValue.end(); 
			++variant)
		{
			ss << "\t[name: " << variant->first << 
				", val: " << (variant->second != null ? variant->second->Print() : "null") 
				<< " ]" << endl;
		}
		ss << "]" << endl;
		return ss.str();
	}

	
	void VariantCollection::Clear(bool removeAndDelete) {
		if(removeAndDelete)
			for(unsigned int i = 0 ; i < this->values.size() ; i++)
				delete this->values[i];

		this->values.clear();
		this->nameToIndex.clear();
		this->indexToName.clear();
		this->nameToValue.clear();
	}

	void VariantCollection::Clear() {
		this->Clear(true);
	}

}