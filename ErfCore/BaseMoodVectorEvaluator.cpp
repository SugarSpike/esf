#include "BaseMoodVectorEvaluator.h"
#include "MoodModel.h"
#include "ErfLogging.h"
#include "ErfForEach.h"
#include "EmotionVector.h"
#include "EecEvent.h"

namespace erf {

	BaseMoodVectorEvaluator::BaseMoodVectorEvaluator(CharacterModel& model) :
		BasicObject("BaseMoodVectorEvaluator"), VectorEvaluator(model)
	{

	}
		
	bool BaseMoodVectorEvaluator::Evaluate(EecEvent& eecEvent) {
		if(this->model.GetRegisteredMoodModels().size() == 0)
			return false;

		
		EmotionVector* currentEmotionVector = this->model.GetEmotionVector();
		if(currentEmotionVector == null)
			return false;

		MoodVector* newMoodVector = null;
		for_each_erf(MoodModel* model,this->model.GetRegisteredMoodModels())
		{
			if(model->IsEmotionVectorSupported(*currentEmotionVector))
			{
				LOG(debug) << "BaseMoodVectorEvaluator::Evaluate - Delegate to MoodModel: " << model->GetUniqueName();
				newMoodVector = model->HandleEmotionVector(eecEvent,this->model.GetCharacterData());
				if(newMoodVector == null)
				{
					LOG(debug) << "BaseMoodVectorEvaluator::Evaluate - MoodModel: " << model->GetUniqueName()
						<< " returned null for EecEvent" << eecEvent;
					return false;
				}
				this->SetMoodVector(newMoodVector);
				return true;
			}
		}
		return false;
	}
}