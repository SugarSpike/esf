#pragma once
#include "VariantCollection.h"

/*! \file EmotionVector.h
	\brief EmotionVector header file
*/


namespace erf
{
	/*! \brief	A vector containing evaluated emotion values by the EmotionModel%s 
	 *			
	 *	The EmotionVector is a container for evaluated emotion values by the framework (specificly the EmotionModel%s).
	 *	It is a multi-type, multi-value generic container. The values that it contains depends on the EmotionModelType
	 *	of the EmotionModel that produced the vector. For example a PAD vector would contain three values: 'pleasure',
	 *	'dominance', 'arousal'. While an OCC vector could contain any subset of the 24 emotions modeled by it eg.: 'Fear',
	 *	'Happy-For', %sad'.
	 */
    class ESFCORE_API EmotionVector: public VariantCollection
    {
		protected:
			vector<int> emotionModelTypes; ///< List of emotion model types (eg. PAD, OCC) that this vector supports
			time_t timeOfEvaluation; ///< The time of the evaluation of this emotion vector (as number of miliseconds since start of UNIX epoch)
			bool uniformType; ///< Whenever all elements of the vector are of the same type (eg. float, double), used by VectorEvaluator%s to optimize evaluation
			float modelRank; ///< Rank of model that evaluated the vector, should be the same as EmotionModel::GetModelRank, duplicated for easy acces by VectorEvaluators
			vector<float> emotionCertainties; ///< The certainties of the specific emotion as evaluated by an EmotionModel, used mostly by VectorEvaluator%s during evaluation
		public:
			/*! \brief Base EmotionVector constructor
			 *	
			 *	Base constructor for EmotionVector
			 *	\param emotionModelType the emotion model type that this vector represents
			 */
			EmotionVector(int emotionModelType);

			/*! \brief Base EmotionVector constructor
			 *	
			 *	Base constructor for EmotionVector
			 *	\param emotionModelType the emotion model type that this vector represents
			 *	\param timeOfEvaluation time of evaluation of the vector
			 */
			EmotionVector(int emotionModelType,time_t timeOfEvaluation);

			/*! \brief Base EmotionVector constructor
			 *	
			 *	Base constructor for EmotionVector
			 *	\param emotionModelType the emotion model types that this vector represents
			 *	\param timeOfEvaluation time of evaluation of the vector
			 */
			EmotionVector(vector<int>& emotionModelType,time_t timeOfEvaluation);

			/*! \brief Base EmotionVector constructor
			 *	
			 *	Base constructor for EmotionVector
			 *	\param emotionModelType the emotion model types that this vector represents
			 */
			EmotionVector(vector<int>& emotionModelType);

			/*! \brief Adds specified EmotionModelType to the list of supported models of this vector
			 *	
			 *	Adds specified EmotionModelType to the list of supported models of this vector
			 *	\param type The model type to support
			 */
			void AddEmotionModelType(int type);

			/*! \brief Adds the Variant data to the vector
			 *	
			 *	Adds the Variant data (should represent emotion data) under the given name, the certainty of the emotion is set to zero
			 *
			 *	\param name name of the parameter under which the data will be accessible
			 *	\param data emotion data to save in this vector
			 */
			virtual void AddValue(string name, Variant* data);

			/*! \brief Adds the Variant data to the vector
			 *	
			 *	Adds the Variant data (should represent emotion data) under the given name
			 *
			 *	\param name name of the parameter under which the data will be accessible
			 *	\param data emotion data to save in this vector
			 *	\param certainty certainty of the emotion data by the model
			 */
			void AddValue(string name, Variant* data, float certainty);

			/*! \brief Gets the certainty for the data
			 *
			 *	Gets the certainty for the data
			 *	\param name name of the emotion data for which to get the model certainty of it
			 *	\return certainty of the emotion data by the model
			 */
			float GetCertainty(string name);

			/*! \brief Gets the certainty for the data
			 *
			 *	Gets the certainty for the data
			 *	\param index index of the emotion data for which to get the model certainty of it
			 *	\return certainty of the emotion data by the model
			 */
			float GetCetainity(int index);

			/*! \brief Gets the certainties
			 *
			 *	Gets the certainties
			 *	\return certainties of the emotion data by the model
			 */
			vector<float>& GetCertainties();

			/*! \brief Gets the EmotionModelTypes this vector represents
			 *
			 *	Gets the EmotionModelTypes this vector represents
			 *	\return supported EmotionModelTypes
			 */
			vector<int>& GetEmotionModelTypes();

			/*! \brief Checks if the vector represents specified EmotionModelType
			 *
			 *	Checks if the vector represents specified EmotionModelType
			 *	\return true if supports, false otherwise
			 */
			bool IsOfEmotionModelType(int modelType);

			/*! \brief Gets the evaluation time of this vector
			 *
			 *  Returns the time of the evaluation of this emotion vector (as number of miliseconds since start of UNIX epoch)
			 *
			 *	\return time of evaluation
			 */
			time_t GetTimeOfEvaluation();

			/*! \brief Updates the evaluation time of this vector
			 *
			 *  Updates the time of the evaluation of this emotion vector (as number of miliseconds since start of UNIX epoch)
			 *
			 *	\param newTime updated time of evaluation
			 */
			void SetTimeOfEvaluation(time_t newTime);

			/*! \brief Sets the uniform type flag
			 *
			 *	The flags states whenever all elements of the vector are of the same type (eg. float, double), 
			 *	used by VectorEvaluator%s to optimize evaluation
			 *
			 *	\param newFlag new value of the uniform flag
			 */
			void SetUniformType(bool newFlag);

			/*! \brief Gets the uniform type flag
			 *
			 *	The flags states whenever all elements of the vector are of the same type (eg. float, double), 
			 *	used by VectorEvaluator%s to optimize evaluation
			 *
			 *	\return whenever the vector is uniform
			 */
			bool GetUniformType();

			
			/*! \brief Sets the model rank
			 *
			 *	Sets the model rank
			 *	\param rank new value of the uniform flag
			 */
			void SetModelRank(float rank);

			/*! \brief Gets the model rank
			 *
			 *
			 *	\return models rank
			 */
			float GetModelRank();

			/*! \brief Clones the vector
			 *
			 *  Returns a parial deep-clone of the vector. See Variant class for more details about partial-deep cloning.
			 *
			 *	\return clone of vector
			 */
			EmotionVector* Clone();
	};
}  

