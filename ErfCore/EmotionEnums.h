#pragma once

/*! \file EmotionEnums.h
	\brief Contains enum declarations for Emotion Model Module
*/

namespace erf
{
	/*! \brief	Types of supported emotion models by the framework
	 *			
	 *	Each EmotionVector and MoodVectors represents emotions from a certain mathematical emotion model.
	 *	This enum acts as a helper for identification of such models in the framework.
	 */
	enum EmotionModelType {
		STD_PAD,	///< Pleasure, Arousal and Dominance model of emotions
		STD_OCC,	///< Ortony, Clarc & Collins model of emotions
		EXT_OTHER	///< Use to mark the end of standard supported models, can be use to expand the enum
	};

	/*! \brief	Characteristics of the EmotionVector%s produced by EmotionModel%s
	 *			
	 *	The characteristics are used by CharacterModel and VectorEvaluatorFactory in order to choose optimal VectorEvaluator%s.
	 *	The enum is a bitmap.
	 */
	enum EmotionVectorCharacteristics {
		STD_NO_CHARACTERISTICS = 0, ///< The produced vectors have no special characteristics, default value
		STD_SKIP_VALIDATION = 1, ///< The produced vectors have no special characteristics, default value
		STD_VECTOR_OF_FLOATS = 2, ///< The produced vectors consists only of floats values
		STD_VECTOR_OF_DOUBLES = 4, ///< The produced vectors consists only of double values
		STD_VECTOR_OF_LONGS = 8, ///< The produced vectors consists only of long values
		EXT_EMOTION_VECTOR_CHARACTERISTICS = 16 ///< Use to mark the end of standard supported characteristics, can be use to expand the enum
	};

}