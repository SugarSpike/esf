#pragma once
#include "BasicObject.h"

/*! \file EmotionModel.h
	\brief EmotionModel header file
*/


//=================================
// forward declared dependencies
namespace erf
{
	class CharacterData;
	class EecEvent;
	class EmotionVector;
}

namespace erf
{
	/*! \brief	Emotion model computes new EmotionVector%s based on the data supplied by Erf Framework
	 *			
	 *	The EmotionModel is one of the core components of the framework. It is used to compute new EmotionVector%s based on the data supplied by Erf Framework.
	 *	The data includes (but is not limited to):
	 *		- Information from the game itself (passed throu EecEvent)
	 *		- Sensory data supplied by external sensors (passed throu CharacterData)
	 *		- Information about the personality of the character
	 *		- Other user data passed by CharacterData
	 *
	 *	The overall input into the evaluated vectors (what the vector actually contains) depends on the VectorEvaluator choosen by a CharacterModel.
	 *
	 */
    class ESFCORE_API EmotionModel : public virtual BasicObject
    {
	public:
		/*! \brief	Returns the model rank
		 *			
		 *	Return the model rank. The rank is an arbitrary number of range <0.0f,1.0f> supplied by the creator of the EmotionModel.
		 *	The rank should reflect the overall correctivnes of the algoritm for computing emotions that this model represents.
		 *	It is used by VectorEvaluator%s to weight the outputs of all emotion models when computing the final EmotionVector.
		 *
		 *	\return the model rank
		 */
		virtual float GetModelRank() = 0;

		/*! \brief	Initialize the model for given CharacterModel/CharacterData
		 *			
		 *	The method is called upon registering the model to a certain CharacterModel. All character dependend initialization
		 *	should be done in this function (like setting emotion model initial data in CharacteData).
		 *
		 *	\param characterData the character data for initialization
		 */
		virtual void InitializeForCharacter(CharacterData& characterData) = 0;

		/*! \brief	Whenever the model supports an EecEvent of specified type
		 *			
		 *	Whenever the model supports an EecEvent of specified type. It does not necessarily means that the output of the model
		 *	will not be used by VectorEvaluator%s when the function return false. As the behaviour is dependend on concrete VectorEvaluator
		 *	implementation.
		 *
		 *	\param event to check
		 *	\return whenever the model supports specified event
		 */
		virtual bool IsEventSupported(EecEvent& event) = 0;

		/*! \brief	Generates new EmotionVector in response to EecEvent
		 *			
		 *	Generates new EmotionVector in response to EecEvent and based on passed data (throu CharacterData)
		 *
		 *	\param event emotion eliciting condition event in response to which the new emotion vector is computed
		 *	\param characterData holds information about the character (see CharacterData for more information)
		 *	\return new computed EmotionVector, null if computation fails
		 */
		virtual EmotionVector* HandleEvent(EecEvent& event, CharacterData& characterData) = 0;

	};
}