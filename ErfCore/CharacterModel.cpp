#pragma once
#include "CharacterModel.h"
#include "ErfUtils.h"
#include "MoodVector.h"
#include "Personality.h"
#include "CharacterData.h"
#include "EecEvent.h"
#include "EmotionVector.h"
#include "EmotionSensor.h"
#include "GameConfiguration.h"

#include "Personality.h"
#include "GameConfiguration.h"
#include "EmotionModel.h"
#include "MoodModel.h"
#include "PsychologicalModel.h"
#include "ExternalExpert.h"
#include "ExternalFilter.h"
#include "ErfLogging.h"
#include "ErfException.h"
#include "ErfForEach.h"
#include "VectorEvaluator.h"
#include "EmotionEnums.h"

namespace erf {

	CharacterModel::CharacterModel() : BasicObject("CharacterModel") , 
		emotionEvaluator(null), 
		moodEvaluator(null),
		emotionModelEvaluationCharacteristics(EmotionVectorCharacteristics::STD_NO_CHARACTERISTICS),
		moodModelEvaluationCharacteristics(EmotionVectorCharacteristics::STD_NO_CHARACTERISTICS),
		moodModelEvaluation( ModelsEvaluationMode::STD_BASIC),
		emotionModelEvaluation( ModelsEvaluationMode::STD_BASIC)
	{ 
		LOG(debug) << "CharacterModel::CharacterModel - perform " << GetUniqueName();
		this->emotionEvaluator = vectorEvaluatorFactory.BuildEmotionEvaluator(*this,ModelsEvaluationMode::STD_BASIC);
		this->moodEvaluator = vectorEvaluatorFactory.BuildMoodEvaluator(*this,ModelsEvaluationMode::STD_BASIC);

	}
	
	CharacterModel::CharacterModel(const CharacterModel& model) {
		throw ErfException("CharacterModel::CharacterModel - copy constr not supported");
	}

	CharacterModel::~CharacterModel() { 
		LOG(debug) << "CharacterModel::~CharacterModel - perform " << this->GetUniqueName();
		delete this->emotionEvaluator;
		delete this->moodEvaluator;
	}

	CharacterData& CharacterModel::GetCharacterData() {
		return this->characterData;
	}

	EmotionVector* CharacterModel::GetUnfilteredEmotionVector() { 
		return this->characterData.GetCurrentUnfilteredEmotionVector(); 
	}
		
	EmotionVector* CharacterModel::GetEmotionVector() { 
		return this->characterData.GetCurrentEmotionVector(); 
	}
		
	MoodVector* CharacterModel::GetMoodVector() { 
		return this->characterData.GetCurrentMoodVector(); 
	}
		
	Personality& CharacterModel::GetPersonality() { return this->characterData.GetPersonality(); }

	void CharacterModel::SetPersonality(Personality* personality) {
		characterData.SetPersonality(personality,true);
	}

	void CharacterModel::SetPersonality(Personality* personality, bool isMemoryOwner) {
		characterData.SetPersonality(personality,isMemoryOwner);
	}

	erf::GameConfiguration& CharacterModel::GetGameConfiguration() { return this->characterData.GetGameConfiguration(); }

	void CharacterModel::SetGameConfiguration(erf::GameConfiguration* gameConfiguration) {

		this->characterData.SetGameConfiguration(gameConfiguration,true);
	}

	void CharacterModel::SetGameConfiguration(erf::GameConfiguration* gameConfiguration, bool isMemoryOwner) {

		this->characterData.SetGameConfiguration(gameConfiguration,isMemoryOwner);
	}

	void CharacterModel::SetEmotionModelEvaluationMode(ModelsEvaluationMode mode) {
		if(this->emotionModelEvaluation == mode)
			return;
		VectorEvaluator* previous = this->emotionEvaluator;
		this->emotionEvaluator = vectorEvaluatorFactory.BuildEmotionEvaluator(*this,mode);
		delete previous;
		this->emotionModelEvaluation = mode;
	}
		
	ModelsEvaluationMode CharacterModel::GetEmotionModelEvaluationMode(){
		return this->emotionModelEvaluation;
	}
		
	void CharacterModel::SetMoodModelEvaluationMode(ModelsEvaluationMode mode){
		if(this->moodModelEvaluation == mode)
			return;
		cout << " CREATING mood evaluator";
		VectorEvaluator* previous = this->moodEvaluator;
		this->moodEvaluator = vectorEvaluatorFactory.BuildMoodEvaluator(*this,mode);
		delete previous;
		this->moodModelEvaluation = mode;
		cout << " CREATED mood evaluator: " << previous;
	}
		
	ModelsEvaluationMode CharacterModel::GetMoodModelEvaluationMode() {
		return this->moodModelEvaluation;
	}

	int CharacterModel::GetEmotionModelEvaluationCharacteristics() 
	{
		return this->emotionModelEvaluationCharacteristics;
	}

	void CharacterModel::SetEmotionModelEvaluationCharacteristics(int newVal) {
		if(this->emotionModelEvaluationCharacteristics == newVal)
			return;
		this->emotionModelEvaluationCharacteristics = newVal;
		VectorEvaluator* previous = this->emotionEvaluator;
		this->emotionEvaluator = vectorEvaluatorFactory.BuildEmotionEvaluator(*this,this->emotionModelEvaluation);
		delete previous;
	}

	int CharacterModel::GetMoodModelEvaluationCharacteristics() {
		return this->moodModelEvaluationCharacteristics;
	}

	void CharacterModel::SetMoodModelEvaluationCharacteristics(int newVal) {
		if(this->moodModelEvaluationCharacteristics == newVal)
			return;

		this->moodModelEvaluationCharacteristics = newVal;
		VectorEvaluator* previous = this->moodEvaluator;
		this->moodEvaluator = vectorEvaluatorFactory.BuildMoodEvaluator(*this,this->emotionModelEvaluation);
		delete previous;
	}

// EMOTION MODELS

	bool CharacterModel::RegisterEmotionModel(EmotionModel* emotionModel) {
		LOG(debug) << "CharacterModel::EmotionModels - Registering model of type: " << emotionModel->GetUniqueName() ;
		bool inserted = erfutils::InsertIfNotContains(this->emotionModels,emotionModel);
		if(inserted) emotionModel->InitializeForCharacter(this->characterData);
		return inserted;
	}

	bool CharacterModel::UnregisterEmotionModel(EmotionModel* affectionModel){
		LOG(debug) << "CharacterModel::EmotionModel - Unregistering model: " << affectionModel->GetUniqueName();
		return erfutils::Remove(this->emotionModels,affectionModel);
	}

	bool CharacterModel::UnregisterEmotionModelByName(string emotionModelName){
		LOG(debug) << "CharacterModel::EmotionModel - Unregistering model with name: " << emotionModelName;
		return erfutils::RemoveByName(this->emotionModels,emotionModelName);
	}

//MOOD MODELS


	bool CharacterModel::RegisterMoodModel(MoodModel* moodModel){
		LOG(debug) << "CharacterModel::MoodModels - Registering model of type: " << moodModel->GetUniqueName();
		bool inserted = erfutils::InsertIfNotContains(this->moodModels,moodModel);
		if(inserted) moodModel->InitializeForCharacter(this->characterData);
		return inserted;
	}

	bool CharacterModel::UnregisterMoodModel(MoodModel* moodModel) {
		LOG(debug) << "CharacterModel::MoodModel - Unregistering model: " << moodModel->GetUniqueName();
		return erfutils::Remove(this->moodModels,moodModel);
	}

	bool CharacterModel::UnregisterMoodModelByName(string moodModelName){
		LOG(debug) << "CharacterModel::MoodModel - Unregistering model with name: " << moodModelName;
		return erfutils::RemoveByName(this->moodModels,moodModelName);
	}

//EMOTION FILTERS

	
	bool CharacterModel::RegisterEmotionVectorFilter(EmotionVectorFilter* filter, EmotionVectorFilterType filterType){
		return this->RegisterEmotionVectorFilter(filter,filterType,0);
	}
	bool CharacterModel::RegisterEmotionVectorFilter(EmotionVectorFilter* filter, EmotionVectorFilterType filterType, int priority){
		LOG(debug) << "CharacterModel::AffectionFilters::"<< filterType <<" - Registering filter of type: " << filter->GetUniqueName() << " for priority: " << priority;
		if(erf::After == filterType)
			return this->afterEmotionVectorFilters.InsertIntoPriorityList(filter,priority);
		else
			return this->beforeEmotionVectorFilters.InsertIntoPriorityList(filter,priority);
	}


	
	bool CharacterModel::RegisterExternalFilter(ExternalFilter* filter, EmotionVectorFilterType filterType) {
		return this->RegisterEmotionVectorFilter(filter,filterType);
	}

	bool CharacterModel::RegisterExternalFilter(ExternalFilter* filter, EmotionVectorFilterType filterType, int priority) {
		return this->RegisterEmotionVectorFilter(filter,filterType,priority);
	}

	bool CharacterModel::UnregisterEmotionVectorFilter(EmotionVectorFilter* affectionVectorFilter){
		UnregisterEmotionVectorFilter(affectionVectorFilter,erf::After);
		UnregisterEmotionVectorFilter(affectionVectorFilter,erf::Before);
		return true;
	}

	bool CharacterModel::UnregisterEmotionVectorFilterByName(string filterName){
		UnregisterEmotionVectorFilterByName(filterName,erf::After);
		UnregisterEmotionVectorFilterByName(filterName,erf::Before);
		return true;
	}

	bool CharacterModel::UnregisterEmotionVectorFilter(EmotionVectorFilter* affectionVectorFilter, EmotionVectorFilterType filterType) {
		LOG(debug) << "CharacterModel::EmotionVectorFilter::"<< filterType <<" - Unregistering filter: " << affectionVectorFilter->GetUniqueName();
		if(erf::After == filterType)
			this->afterEmotionVectorFilters.remove(affectionVectorFilter);
		else
			this->beforeEmotionVectorFilters.remove(affectionVectorFilter);
		return true;
	}

	bool CharacterModel::UnregisterEmotionVectorFilterByName(string filterName, EmotionVectorFilterType filterType){
		LOG(debug) << "CharacterModel::EmotionVectorFilter::"<< filterType <<" - Unregistering filter with name: " <<filterName;
		if(erf::After == filterType)
			this->afterEmotionVectorFilters.removeByObjectName(filterName);
		else
			this->beforeEmotionVectorFilters.removeByObjectName(filterName);
		return true;
	}

	bool CharacterModel::UnregisterExternalFilter(ExternalFilter* filter) {
		return this->UnregisterEmotionVectorFilter(filter);
	}
	bool CharacterModel::UnregisteExternalFilterByName(string filterName) {
		return this->UnregisterEmotionVectorFilterByName(filterName);
	}
	bool CharacterModel::UnregisterExternalFilter(ExternalFilter* filter, EmotionVectorFilterType filterType){
		return this->UnregisterEmotionVectorFilter(filter,filterType);
	}

//PSYCHOLOGICAL

	bool CharacterModel::RegisterPsychologicalModel(PsychologicalModel* psychologicalModel) {
		LOG(debug) << "CharacterModel::PsychologicalModels - Registering model of type: " << psychologicalModel->GetUniqueName();	
		bool inserted = erfutils::InsertIfNotContains(this->psychologicalModels,psychologicalModel);
		if(inserted) psychologicalModel->InitializeForCharacter(this->characterData);
		return inserted;
	}

	bool CharacterModel::UnregisterPsychologicalModel(PsychologicalModel* psychologicalModel) {
		LOG(debug) << "CharacterModel::PsychologicalModel - Unregistering model: " << psychologicalModel->GetUniqueName();
		return erfutils::Remove(this->psychologicalModels,psychologicalModel);
	}

	bool CharacterModel::UnregisterPsychologicalModelByName(string modelName){
		LOG(debug) << "CharacterModel::PsychologicalModel - Unregistering model witg name: " << modelName;
		return erfutils::RemoveByName(this->psychologicalModels,modelName);
	}

// SENSORS

	bool CharacterModel::RegisterEmotionSensor(EmotionSensor* emotionSensor){
		LOG(debug) << "CharacterModel::EmotionSensors - Registering sensor of type: " << emotionSensor->GetUniqueName();
		return this->characterData.RegisterSensor(emotionSensor);
	}

	bool CharacterModel::UnregisterEmotionSensor(EmotionSensor* emotionSensor) {
		LOG(debug) << "CharacterModel::PsychologicalModel - Unregistering sensor: " << emotionSensor->GetUniqueName();
		return this->characterData.UnregisterSensor(emotionSensor);
	}

	bool CharacterModel::UnregisterEmotionSensorByName(string sensorName){
		LOG(debug) << "CharacterModel::EmotionSensor - Unregistering sensor with name: " << sensorName;
		return this->characterData.UnregisterSensorByName(sensorName);
	}

// EXPERTS

	bool CharacterModel::RegisterExpert(ExternalExpert* expert) { 
		LOG(debug) << "CharacterModel::RegisterExpert - Registering expert of type: " << expert->GetUniqueName();
		bool expertInserted = erfutils::InsertIfNotContains(this->experts,expert);
		if(!expertInserted)
			return false;

		if(ExpertType::STD_EMOTION == expert->GetExpertType())
			this->RegisterEmotionModel(dynamic_cast<EmotionModel*>(expert));
		else if(ExpertType::STD_MOOD == expert->GetExpertType())
			this->RegisterMoodModel(dynamic_cast<MoodModel*>(expert));
		else if(ExpertType::STD_PSYCHOLOGICAL == expert->GetExpertType())
			this->RegisterPsychologicalModel(dynamic_cast<PsychologicalModel*>(expert));
		
		return true;
	}

	bool CharacterModel::UnregisterExpert(ExternalExpert* expert) {
		LOG(debug) << "CharacterModel::UnregisterExpert - Unregistering expert: " << expert->GetUniqueName();

		if(ExpertType::STD_EMOTION == expert->GetExpertType())
			this->UnregisterEmotionModel(dynamic_cast<EmotionModel*>(expert));
		else if(ExpertType::STD_MOOD == expert->GetExpertType())
			this->UnregisterMoodModel(dynamic_cast<MoodModel*>(expert));
		else if(ExpertType::STD_PSYCHOLOGICAL == expert->GetExpertType())
			this->UnregisterPsychologicalModel(dynamic_cast<PsychologicalModel*>(expert));

		return erfutils::Remove(this->experts,expert);;
	}

	bool CharacterModel::UnregisterExpertByName(string expertName){
		LOG(debug) << "CharacterModel::EmotionSensor - Unregistering expert with name: " << expertName;
		ExternalExpert* expert = this->FindExpert(expertName);
		if(expert == null)
			return false;

		return this->UnregisterExpert(expert);
	}

	EmotionModelContainer CharacterModel::GetRegisteredEmotionModels() {
		return this->emotionModels;
	}

	MoodModelContainer CharacterModel::GetRegisteredMoodModels() { 
		return this->moodModels;
	}

	FilterContainer CharacterModel::GetRegisteredBeforeFilters() { 
		return this->beforeEmotionVectorFilters;
	}

	FilterContainer CharacterModel::GetRegisteredAfterFilters() { 
		return this->afterEmotionVectorFilters;
	}

	PsychologicalModelContainer CharacterModel::GetRegisteredPsychologicalModels() { 
		return this->psychologicalModels;
	}

	ExternalExpert* CharacterModel::FindExpert(string expertName) {
		for_each_erf(ExternalExpert* object, this->experts)
		{
			if(object->GetName() == expertName)
				return object;
		}
		return null;
	}

//EVENT HANDLING FOR SYNCHRONOUS WORK

	bool CharacterModel::HandleEvent(EecEvent& eecEvent) {
		LOG(debug) << "CharacterModel::HandleEvent - Event handling: " << eecEvent;

		bool evaluated = this->emotionEvaluator->Evaluate(eecEvent);
		if(evaluated == false)
			return false;

		this->moodEvaluator->Evaluate(eecEvent);
		
		Personality* newPersonality = null;
		if(this->psychologicalModels.size() >0)
			for_each_erf(PsychologicalModel* model,this->psychologicalModels)
			{
				LOG(debug) << "CharacterModel::HandleEvent - Delegate to PM model: " << typeid(*model).name();
				model->UpdatePersonality(eecEvent,this->characterData);
			}

		return true;
	}

}