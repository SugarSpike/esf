#include "VectorEvaluatorFactory.h"
#include "BaseMoodVectorEvaluator.h"
#include "BaseVectorEvaluator.h"
#include "WeightedMeanVectorEvaluator.h"
#include "WeightedMeanFloatVectorEvaluator.h"
#include "ErfLogging.h"
#include "ErfException.h"
#include "EmotionEnums.h"
#include <sstream> 

namespace erf {

	VectorEvaluatorFactory::VectorEvaluatorFactory() : BasicObject("VectorEvaluatorFactory") { }

	VectorEvaluator* VectorEvaluatorFactory::BuildEmotionEvaluator(CharacterModel& model, ModelsEvaluationMode type) {
		int emotionVectorsCharacteristics = model.GetEmotionModelEvaluationCharacteristics();
		VectorEvaluator* createdInstance = null;
		switch (type) {
		case ModelsEvaluationMode::STD_BASIC:
			createdInstance = new BaseVectorEvaluator(model);
			break;
		case ModelsEvaluationMode::STD_WEIGHTED_AVERAGE:
			if((emotionVectorsCharacteristics & EmotionVectorCharacteristics::STD_VECTOR_OF_FLOATS)
				== EmotionVectorCharacteristics::STD_VECTOR_OF_FLOATS)
				//Optimize for floats
				createdInstance = new WeightedMeanFloatVectorEvaluator(model);
			else if((emotionVectorsCharacteristics & EmotionVectorCharacteristics::STD_VECTOR_OF_FLOATS)
				== EmotionVectorCharacteristics::STD_VECTOR_OF_DOUBLES)
				//Optimize for floats
				createdInstance = new WeightedMeanFloatVectorEvaluator(model);
			else
				createdInstance = new WeightedMeanVectorEvaluator(model);

			break;
		default:
			std::ostringstream oss;
			oss << "VectorEvaluatorFactory::BuildEmotionEvaluator - type not supported: " << type;
			throw ErfException(oss.str());
		}
		LOG(debug) << "VectorEvaluatorFactory::BuildEmotionEvaluator - evaluator constructed: " << createdInstance->GetUniqueName();
		return createdInstance;
	}

	VectorEvaluator* VectorEvaluatorFactory::BuildMoodEvaluator(CharacterModel& model,ModelsEvaluationMode type) {
		VectorEvaluator* createdInstance = null;
		switch (type) {
		case ModelsEvaluationMode::STD_BASIC:
			createdInstance = new BaseMoodVectorEvaluator(model);
			break;
		default:
			std::ostringstream oss;
			oss << "VectorEvaluatorFactory::BuildMoodEvaluator - type not supported: " << type;
			throw ErfException(oss.str());
		}
		LOG(debug) << "VectorEvaluatorFactory::BuildMoodEvaluator - evaluator constructed: " << createdInstance->GetUniqueName();
		return createdInstance;
	}

}