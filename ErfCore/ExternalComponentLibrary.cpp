#pragma once
#include "ExternalComponentLibrary.h"
#include "ErfExternalComponentException.h"
#include "ExternalComponentConfiguration.h"
#include "ErfUtils.h"
#include "ErfLogging.h"
#include "ErfForEach.h"
#include "EmotionSensor.h"
#include "ExternalExpert.h"
#include "ExternalFilter.h"
#include "ErfContext.h"

namespace erf {
	
	ExternalComponentLibrary::ExternalComponentLibrary(ExternalComponentConfiguration* pConfiguration)
		: BasicObject("ExternalComponentLibrary"), ExternalComponent(pConfiguration, ComponentType::LIBRARY),
		getSupportsComponentTypeFunction(null),
		getSupportedParametersAddress(null),
		setParameterAddress(null),
		getParameterAddress(null),
		registerComponentsAddress(null),
		createExpertForNameAddress(null),
		createSensorForNameAddress(null),
		createFilterForNameAddress(null)
	{
		this->sharedLibraryHandle = ExternalSharedLibrary::Load(pConfiguration->componentDllPath,pConfiguration->componentDllName);
		try {
			this->getSupportsComponentTypeFunction = 
				ExternalSharedLibrary::GetFunctionPointer<SupportsComponentTypeFunction>(
					this->sharedLibraryHandle,"SupportsComponentType");

			this->setParameterAddress =
				ExternalSharedLibrary::GetFunctionPointer<SetParameterFunction>(
					this->sharedLibraryHandle,"SetParameter");

			this->getParameterAddress =
				ExternalSharedLibrary::GetFunctionPointer<GetParameterFunction>(
					this->sharedLibraryHandle,"GetParameter");

			this->getSupportedParametersAddress =
				ExternalSharedLibrary::GetFunctionPointer<GetSupportedParametersFunction>(
					this->sharedLibraryHandle,"GetSupportedParameters");

			this->getParametersAddress =
				ExternalSharedLibrary::GetFunctionPointer<GetParametersFunction>(
					this->sharedLibraryHandle,"GetParameters");

			this->registerComponentsAddress =
				ExternalSharedLibrary::GetFunctionPointer<RegisterComponentsFunction>(
					this->sharedLibraryHandle,"RegisterComponents");

			try {
				this->createExpertForNameAddress = ExternalSharedLibrary::GetFunctionPointer<CreateExpertForNameFunction>(
					this->sharedLibraryHandle,"CreateExpertForName");
			} catch(...) { }

			try {
				this->createFilterForNameAddress = ExternalSharedLibrary::GetFunctionPointer<CreateFilterForNameFunction>(
					this->sharedLibraryHandle,"CreateFilterForName");
			} catch(...) { }

			try {
				this->createSensorForNameAddress = ExternalSharedLibrary::GetFunctionPointer<CreateSensorForNameFunction>(
					this->sharedLibraryHandle,"CreateSensorForName");
			} catch(...) { }

		}
		catch(...) {
		  ExternalSharedLibrary::Unload(this->sharedLibraryHandle);
		  throw;
		}

		pConfiguration->libraryComponent = this;
		this->componentState = ComponentState::INITIALIZED;
	}

	ExternalComponentLibrary::~ExternalComponentLibrary() {
		LOG(debug) << GetUniqueName() << " - deleting component library for '" << this->configuration->componentDllName << "'";
		vector<ExternalComponent*>& components = (*this->configuration).GetComponents();
		for(vector<ExternalComponent*>::iterator component = components.begin(); 
			component != components.end(); 
			) {
			try {
				if((*component) == this)
				{
					++component;
					continue;
				}
				ExternalComponent* toDelete = (*component);
				component = components.erase(component);
				delete toDelete;
			} catch(...) {}
		}
		ExternalSharedLibrary::Unload(this->sharedLibraryHandle);
	}

	vector<ComponentType>& ExternalComponentLibrary::GetSupportedComponentTypes() { return this->supportedComponentTypes; }
	vector<int>& ExternalComponentLibrary::GetSupportedSensorTypes() { return this->supportedSensorTypes; }

	void ExternalComponentLibrary::CreateLibrary()
	{
		ExternalComponentConfiguration& configuration = this->GetConfiguration();
		this->registerComponentsAddress(*this);
		for_each_erf(ExternalComponent* component, configuration.components)
		{
			ComponentType componentType = component->GetComponentType();
			if(!erfutils::Contains(this->supportedComponentTypes, component->GetComponentType()))
			{
				this->supportedComponentTypes.push_back(componentType);
			}
			if(componentType == ComponentType::SENSOR)
			{
				EmotionSensor* sensor = dynamic_cast<EmotionSensor*>(component);
				if(sensor == NULL)
				{
					LOG(error) << GetUniqueName() << " -found registered sensor not extending EmotionSensor class in '" 
						<< this->configuration->componentDllName << "'";
					continue;
				}
				for_each_erf(int& sensorDataType, sensor->GetSensorTypes())
				{
					if(!erfutils::Contains(this->supportedSensorTypes, sensorDataType))
					{
						this->supportedSensorTypes.push_back(sensorDataType);
					}
				}
			}
		}

	}

	ExternalExpert* ExternalComponentLibrary::CreateExpertForName(string expertName) {
		if(this->createExpertForNameAddress == null)
			throw ErfException("Creating new experts is not supported by the library: " + this->GetConfiguration().componentDllName);
		
		ExternalExpert* newExpert =  this->createExpertForNameAddress(*this,expertName);
		if(newExpert == null)
			return null;

		if(!erfutils::Contains(this->supportedComponentTypes, newExpert->GetComponentType()))
		{
			this->supportedComponentTypes.push_back(newExpert->GetComponentType());
		}

		ErfContext::GetInstance().GetRegisteredExperts().push_back(newExpert);
		return newExpert;
	}

	EmotionSensor* ExternalComponentLibrary::CreateSensorForName(string sensorName) {
		if(this->createSensorForNameAddress == null)
			throw ErfException("Creating new sensors is not supported by the library: " + this->GetConfiguration().componentDllName);

		EmotionSensor* newSensor = this->createSensorForNameAddress(*this,sensorName);
		if(newSensor == null)
			return null;

		if(!erfutils::Contains(this->supportedComponentTypes, newSensor->GetComponentType()))
		{
			this->supportedComponentTypes.push_back(newSensor->GetComponentType());
		}
		
		ErfContext::GetInstance().GetRegisteredSensors().push_back(newSensor);
		return newSensor;
	}

	ExternalFilter* ExternalComponentLibrary::CreateFilterForName(string filterName) {
		if(this->createFilterForNameAddress == null)
			throw ErfException("Creating new filters is not supported by the library: " + this->GetConfiguration().componentDllName);

		ExternalFilter* newFilter = this->createFilterForNameAddress(*this,filterName);
		if(newFilter == null)
			return null;

		if(!erfutils::Contains(this->supportedComponentTypes, newFilter->GetComponentType()))
		{
			this->supportedComponentTypes.push_back(newFilter->GetComponentType());
		}
		
		ErfContext::GetInstance().GetRegisteredFilters().push_back(newFilter);
		return newFilter;
	}

	string ExternalComponentLibrary::GetComponentLastError(){
		return GetParameter("COMPONENT_LAST_ERROR");
	}

	void ExternalComponentLibrary::GetParameters(map<string,string>& paramHolder) {
		return this->getParametersAddress(paramHolder);
	}

	void ExternalComponentLibrary::GetSupportedParameters(vector<string>& paramHolder){
		this->getSupportedParametersAddress(paramHolder);
	}
	
	const string ExternalComponentLibrary::GetParameter(string parameterName){
		return this->getParameterAddress(parameterName);
	}

	bool ExternalComponentLibrary::SetParameter(string parameterName,string parameterValue){
		return this->setParameterAddress(parameterName,parameterValue);
	}
	
}