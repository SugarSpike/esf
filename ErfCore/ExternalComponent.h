#pragma once
#include "BasicObject.h"
#include "ExternalComponentEnums.h"

/*! \file ExternalComponent.h
	\brief ExternalComponent header file

*/
//=================================
// forward declared dependencies
namespace erf
{
	class ExternalComponentConfiguration;
}

namespace erf
{
	/*! \brief	Represents an external component that was dynamically loaded into the library
	 *			
	 *	External components are components that were loaded through externall DLL libraries with the use of
	 *	ErfContext. External components and its specializations fill a wide array of roles inside the framework including:
	 *	- External models eg. EmotionModel%s, MoodModel%s, PsychologicalModel%s - and are represented by ExternalExpert class
	 *	- External EmotionVectorFilter - represented by ExternalFilter
	 *	- External data sensors - represented by EmotionSensor%s
	 *	- The data about the library and the handle to it are wrapper inside ExternalComponentLibrary
	 *
	 *	Every ExternalComponent besides its type also has a state. The state represents the current phase in the lifetime of an 
	 *	ExternalComponent. The flow of such a lifetime is not imposed nor defined by the Erf framework. And as such the developer can 
	 *	create flows as he/she sees fit or choose not to use states at all. The state, in general, is used to communicate the readiness 
	 *	of a component to receive tasks. For example an ExternalExpert may choose to not use data from a sensor that is not yet initialize 
	 *	(as data reading might fail). It can also communicate the need for Deinitialization before the component is destroyed.
	 *
	 *	Furthermore an ExternalComponent has parameters. These parameters can be used to pass necessary data for initialization or to 
	 *	configure runtime parameters. Only string value parameters are supported.
	 *	
	 */
    class ESFCORE_API ExternalComponent : public virtual BasicObject
    {

		protected:
			ComponentType componentType;	///< The component type eg. Expert or Sensor
			ComponentState componentState;	///< The current component state eg. Initialized, Error etc.
			ExternalComponentConfiguration* configuration;	///< A pointer to the configuration of external component

		public:
			/*!	\brief The base constructor for ExternalComponent
			 *	
			 *	The base constructor for ExternalComponent. It will register itself inside supplied configuration and unregister upon
			 *	destruction.
			 *	\param configuration The configuration of this external component and the library it comes from
			 *	\param componentType The type of the component
			 */
			ExternalComponent(ExternalComponentConfiguration* configuration, ComponentType componentType);

			/*!	\brief The base copy constructor for ExternalComponent
			 *	
			 *	The base copy constructor for ExternalComponent
			 *	\param other The original ExternalComponent
			 */
			ExternalComponent(const ExternalComponent &other);

			virtual ~ExternalComponent();

			/*!	\brief Returns the component configuration
			 *	
			 *	Returns the component configuration
			 *	\return The ExternalComponent configuration
			 */
			inline ExternalComponentConfiguration& GetConfiguration() { return  (*this->configuration); }

			/*!	\brief Returns the component current state
			 *	
			 *	Returns the component current state
			 *	\return The ExternalComponent current state
			 */
			inline ComponentState GetComponentState() { return this->componentState; }
			
			/*!	\brief Returns the component type
			 *	
			 *	Returns the component type
			 *	\return The ExternalComponent type
			 */
			inline ComponentType GetComponentType() { return  this->componentType; }

			/*!	\brief Sets a new state for the component
			 *	
			 *	Sets a new state for the component
			 *	\param state The new ExternalComponent state
			 */
			void SetComponentState(ComponentState state) { this->componentState = state; }

			/*!	\brief Returns the last error message for the component
			 *	
			 *	Returns the last error message for the component
			 *	\return The error message as string
			 */
			virtual std::string GetComponentLastError() = 0;

			/*!	\brief Returns the names of supported parameters
			 *	
			 *	Returns the names of supported parameters
			 *	\param[out] paramHolder The destination vector where the name are pushed
			 */
			virtual void GetSupportedParameters(vector<string>& paramHolder) = 0;

			/*!	\brief Returns the name and values of the component parameters
			 *	
			 *	Returns the name and values of the component parameters
			 *	\param[out] paramHolder The destination map where the name and values are pushed
			 */
			virtual void GetParameters(map<string,string>& paramHolder) = 0;

			/*!	\brief Returns the value of parameter with given name
			 *	
			 *	Returns the value of parameter with given name
			 *	\param parameterName The name of the parameter to retrieve
			 *	\return value of the parameter, should return null if the parameter is not present
			 */
			virtual const string GetParameter(string parameterName) = 0;

			/*!	\brief Sets the value of parameter with given name
			 *	
			 *	Sets the value of parameter with given name
			 *	\param parameterName Name of the parameter
			 *	\param parameterValue New value of the parameter
			 *	\return Whenever the new value was set
			 */
			virtual bool SetParameter(string parameterName, string parameterValue) = 0;

			/*!	\brief Initialize the component
			 *	
			 *	Initialize the component. 
			 */
			virtual void Initialize() {};

			/*!	\brief Reinitialize the component
			 *	
			 *	Reinitialize the component. 
			 */
			virtual void Reinitialize() {};

			/*!	\brief Deinitialize the component
			 *	
			 *	Deinitialize the component. Will be called automaticly before deletion of component if the state during
			 *	deletion differs from ComponentState::UNINITIALIZED or ComponentState::DEINITIALIZED
			 */
			virtual void Deinitialize() {};

	};
}