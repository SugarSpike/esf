#pragma once
#include "GameConfiguration.h"
#include "ErfLogging.h"
namespace erf {

	GameConfiguration& GameConfiguration::operator= (const GameConfiguration &cSource)
	{
		this->gameName = cSource.gameName;
		this->gameType = cSource.gameType;
		return *this;
	}

	GameConfiguration::GameConfiguration(const GameConfiguration &other) : BasicObject(other) {
		LOG(debug) << "GameConfiguration::GameConfiguration(&other) -perform " << GetUniqueName();
		this->gameName = other.gameName;
		this->gameType = other.gameType;
	} 

	GameConfiguration::GameConfiguration() : BasicObject("GameConfiguration") {
		this->gameName = "Default_game_name";
		this->gameType = GameType::STD_NONE;
	} 

	GameConfiguration::GameConfiguration(string name, GameType gameType) : BasicObject("GameConfiguration") {
		this->gameName = name;
		this->gameType = gameType;
	}

	GameConfiguration::~GameConfiguration() {
	}

	GameType GameConfiguration::GetGameType() { return this->gameType;}
	void GameConfiguration::SetGameType(GameType gameType) { this->gameType = gameType;}

	void GameConfiguration::AddGameCharacteristic(GameCharateristics gameCharacteristic) {
		if(std::find(gameCharacteristics.begin(), gameCharacteristics.end(), 
				gameCharacteristic) == gameCharacteristics.end()) {
					gameCharacteristics.push_back(gameCharacteristic);
		} 
	}
	void GameConfiguration::RemoveGameCharacteristic(GameCharateristics gameCharacteristic) {
		vector< GameCharateristics >::iterator finder = 
			std::find(gameCharacteristics.begin(), gameCharacteristics.end(), 
				gameCharacteristic) ;
		if(finder!= gameCharacteristics.end()) {
			gameCharacteristics.erase(finder);
		} 
	}
	vector< GameCharateristics >& GameConfiguration::GetGameCharacteristics() {return vector<GameCharateristics>();}

	void GameConfiguration::AddGameChallenge(GameChallenges gameChallenge) {
		if(std::find(gameChallenges.begin(), gameChallenges.end(), 
				gameChallenge) == gameChallenges.end()) {
					gameChallenges.push_back(gameChallenge);
		} 
	}

	void GameConfiguration::RemoveGameChallenge(GameChallenges gameChallenge) {
		vector< GameChallenges >::iterator finder = 
			std::find(gameChallenges.begin(), gameChallenges.end(), 
				gameChallenge) ;
		if(finder!= gameChallenges.end()) {
			gameChallenges.erase(finder);
		} 
	}
	vector< GameChallenges >& GameConfiguration::GetGameChallenges() { return this->gameChallenges; }

	void GameConfiguration::SetGameName(string name) { this->gameName = name;}
	string GameConfiguration::GetGameName() { return this->gameName;}
	   
}