#pragma once
#include "VectorEvaluator.h"

/*! \file BaseVectorEvaluator.h
	\brief BaseVectorEvaluator header file

*/

namespace erf
{
	/*! \brief	Base evaluator returning the output of the first model that can handle the given event
	 *			
	 *	Base evaluator returning the output of the first emotion model that can handle the given event.
	 *	No further computations are done.
	 *	Only EmotionVectorFilter%s of type EmotionVectorFilterType::After are applied;
	 *
	 */
	class ESFCORE_API BaseVectorEvaluator : public VectorEvaluator
    {

	public:
		/*! \brief	Base constructor for BaseVectorEvaluator
		 *	
		 *	Base constructor for BaseVectorEvaluator
		 *	\param model The parent CharacterModel
		 */
		BaseVectorEvaluator(CharacterModel& model);

		virtual bool Evaluate(EecEvent& eecEvent);

	};

}