#include "Variant.h"
#include "ErfLogging.h"
#include <sstream> 

namespace erf {
	Variant::Variant() { 
		this->type = DataType::Empty;
		this->valueData = null;
	}

	Variant::Variant(BoostAny* raw) {
		BoostAny* wrappedVal = new BoostAny(*raw);
		this->type = DataType::Other;
		this->valueData = wrappedVal;
	}

	BoostAny* Variant::GetRaw() { return this->valueData; }

	Variant::~Variant() {
		if(this->type == DataType::Other && this->isMemoryOwner)
			delete this->valueData; 
	}

	Variant* Variant::Clone(){
		Variant* var = null;

		if(this->type == DataType::Float)
			var = Variant::Create(this->value.floatData);
		else if(this->type == DataType::Double)
			var = Variant::Create(this->value.doubleData);
		else if(this->type == DataType::Integer)
			var = Variant::Create(this->value.integerData);
		else if(this->type == DataType::Long)
			var = Variant::Create(this->value.longData);
		else
			var = new Variant(this->GetRaw());

		var->type = this->type;
		var->isMemoryOwner = this->isMemoryOwner;
		return var;
	}

	string Variant::Print() {
		std::stringstream ss;
		ss << "[ memOwn: " << this->isMemoryOwner << ", val: ";
		if(this->IsString())
			ss << this->AsString();
		else if(this->IsDouble())
			ss << this->AsDouble();
		else if(this->IsFloat())
			ss << this->AsFloat();
		else if(this->IsInteger())
			ss << this->AsInteger();
		else
#if !defined(ERF_VARIANT_HOLD_ANY)
			ss << " PointerVariantData::" << (this->valueData);
#else
			ss << " PointerVariantData::" << (*this->valueData);
#endif
		ss << "]";
		return ss.str();
	}

	bool Variant::IsOther() { return this->type == DataType::Other; }

	Variant* Variant::Create(int value) {
		Variant* v = new Variant();
		v->isMemoryOwner=true;
		v->type = DataType::Integer;
		v->value.integerData = value;
		return v;
	}
	bool Variant::IsInteger() {
		return this->type == DataType::Integer;
	} 

	int Variant::AsInteger() {
		return this->value.integerData; 
	}

	Variant* Variant::Create(double value) {
		Variant* v = new Variant();
		v->isMemoryOwner=true;
		v->type = DataType::Double;
		v->value.doubleData = value;
		return v;
	}
	bool Variant::IsDouble() {
		return this->type == DataType::Double;
	} 

	double Variant::AsDouble() {
		return this->value.doubleData; 
	}

	Variant* Variant::Create(float value) {
		Variant* v = new Variant();
		v->isMemoryOwner=true;
		v->type = DataType::Float;
		v->value.floatData = value;
		return v;
	}
	bool Variant::IsFloat() {
		return this->type == DataType::Float;
	} 

	float Variant::AsFloat() {
		return this->value.floatData; 
	}

	Variant* Variant::Create(long value) {
		Variant* v = new Variant();
		v->isMemoryOwner=true;
		v->type = DataType::Long;
		v->value.longData = value;
		return v;
	}
	bool Variant::IsLong() {
		return this->type == DataType::Long;
	} 

	long Variant::AsLong() {
		return this->value.longData; 
	}

	Variant* Variant::Create(string value) { 
		Variant* v = new Variant(); 
		v->isMemoryOwner=true; 
		v->type = DataType::Other;
		BoostAny* wrappedVal = new BoostAny(value); 
		v->valueData = wrappedVal; 
		return v; 
	} 
	bool Variant::IsString() { 
		try {  
			boostAny::any_cast<string >(*this->valueData);  
			return true;  
		}  
		catch(const boostAny::bad_any_cast &)  
		{  
			return false;  
		}  
	}  
	string Variant::AsString() { 
		return boostAny::any_cast<string >(*this->valueData);  
	}


}