Set args = Wscript.Arguments

Set oFSO = CreateObject("Scripting.FileSystemObject")

Wscript.Echo "Starting headers copy..."
Wscript.Echo Wscript.Arguments(0)
Wscript.Echo Wscript.Arguments(1)
destFolder = Wscript.Arguments(1) & "Headers"

Wscript.Echo "Destination: " & destFolder
Wscript.Echo destFolder
If (oFSO.FolderExists(destFolder)) Then
	oFSO.DeleteFolder(destFolder)
End If
oFSO.CreateFolder(destFolder)
	
sourceFolder = Wscript.Arguments(0)
Wscript.Echo sourceFolder
For Each oFile In oFSO.GetFolder(sourceFolder).Files
	If UCase(oFSO.GetExtensionName(oFile.Name)) = "H" Then
		ProcessFiles oFSO, oFile, destFolder, sourceFolder
	End if
Next

Sub ProcessFiles(FSO, File, DestFolder, SourceFolder)
	 outputDir = DestFolder & "\" & GetFilenameWithoutExtension(File.Name)
	 FSO.CreateFolder(outputDir)
	 destBaseFile = outputDir & "\" & File.name
	 
     FSO.CopyFile File.path, destBaseFile
	 
	 Set oFile2 = FSO.OpenTextFile(File.path, 1)
	
	 Wscript.Echo File.Name
     Do Until oFile2.AtEndOfStream
       strContents = oFile2.ReadLine
	   If left(strContents,8) = "#include" Then
		   intStart = InStr(strContents, """")+1
		   intEnd = nInStr(strContents,  """",2)
		   strText = Mid(strContents, intStart, intEnd - intStart)
		   strText = Replace(strText,"/","\")
		   Set oFile3 = FSO.GetFile(sourceFolder&strText)
		   
		   FSO.CopyFile oFile3.path, outputDir & "\" & oFile3.Name
	   End If
     Loop

     'sFile = File.path
     oFile2.close
     set oFile2 = Nothing
	 
	Const ForReading = 1
	Const ForWriting = 2

	Set objFile = FSO.OpenTextFile(destBaseFile, ForReading)
	strText = objFile.ReadAll
	objFile.Close
	strNewText = Replace(strText, "..", "")
	strNewText1 = Replace(strNewText, "/", "")

	Set objFile = FSO.OpenTextFile(destBaseFile, ForWriting)
	objFile.WriteLine strNewText1 
	objFile.Close

    ' Set File = FSO.OpenTextFile(sFile ,  ForWriting)
     'File.Write strText
    'File.Close
   ' Set File = Nothing

 end sub
 
Function GetFilenameWithoutExtension(ByVal FileName)
  Dim Result, i
  Result = FileName
  i = InStrRev(FileName, ".")
  If ( i > 0 ) Then
    Result = Mid(FileName, 1, i - 1)
  End If
  GetFilenameWithoutExtension = Result
End Function

Function nInstr(strSource,strFind,occurrence)
 StartPosition = 1
 For Findoccurrence = 1 to occurrence
  FoundPosition = Instr(StartPosition,strSource,strFind)
  StartPosition = FoundPosition + 1
  If FoundPosition = 0 Then
   Exit For
  End If
 Next
 nInstr = FoundPosition
End Function