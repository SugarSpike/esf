#pragma once
#include "VectorEvaluator.h"

/*! \file WeightedMeanVectorEvaluator.h
	\brief WeightedMeanVectorEvaluator header file

*/

namespace erf
{
	/*! \brief	Computes target emotion vector as an weighted average of all expert vectors
	 *			
	 *	The evaluator computes target emotion vector based on experts models output (both mood and emotion).
	 *  Every element of the vector will be a weighted average of all experts output.
	 *  The evaluator assumes that all intermediated vectors will be of the same emotion model type.
	 *	If they are not then only the vectors of type of the model with greates rank will be evaluated.
	 *	All vectors of other types will be discarded.
	 *	The weight takes into account the rank of the expert and certainty of the expert for given emotion (if such data is present)
	 *
	 *	The EmotionVectorFilter%s of EmotionVectorFilterType::Before will be applied to all intermediate vectors.
	 *	The EmotionVectorFilter%s of EmotionVectorFilterType::After will be applied to the target vector.
	 */
	class ESFCORE_API WeightedMeanVectorEvaluator : public VectorEvaluator
    {
	private:

		WeightedMeanVectorEvaluator* floatOptimizedVectorEvaluator; ///< Optimized instance of WeightedMeanVectorEvaluator for floats
		WeightedMeanVectorEvaluator* doubleOptimizedVectorEvaluator; ///< Optimized instance of WeightedMeanVectorEvaluator for doubles
		
		/*! \brief	Returns WeightedMeanVectorEvaluator optimized for float vector inputs
		 *	\return The optimized WeightedMeanVectorEvaluator for floats
		 */
		WeightedMeanVectorEvaluator* GetOptimizedForFloats();
		/*! \brief	Returns WeightedMeanVectorEvaluator optimized for double vector inputs
		 *	\return The optimized WeightedMeanVectorEvaluator for double
		 */
		WeightedMeanVectorEvaluator* GetOptimizedForDoubles();

	protected:
		
		/*! \brief Validates the supplied vectors
		 *  
		 *  The evaluator assumes that all intermediated vectors will be of the same emotion model type.
		 *	If they are not then only the vectors of type of the model with greates rank will be evaluated.
		 *	If the vector of expert with highest rank is uniform then all others also need to be uniform.
		 *	Furthermore if any of the vectors has no values it will be removed as well.
		 *	\param[in,out] vectors The vectors to be validated, the function will remove invalid vectors from this vector
		 *	\param[in] highestRankModelType All emotion model types supported by the expert with highest rank
		 *	\param[in] highestIsUniform The uniform flag of the expert with highest rank
		 */
		virtual void ValidateVectors(vector<EmotionVector*>& vectors,vector<int>& highestRankModelType, bool highestIsUniform);

		/*! \brief Applies EmotionVectorFilterType::Before to given vectors
		 *  
		 *	\param vectors emotion vectors that will have the filters applied
		 */
		void ApplyBeforeFilters(vector<EmotionVector*>& vectors);

		/*! \brief Applies EmotionVectorFilterType::After to given vectors
		 *  
		 *	\param vector emotion vectors that will have the filters applied
		 */
		void ApplyAfterFilters(EmotionVector& vector);

		
		/*! \brief Aggregates the vectors as an weighted mean
		 *
		 *	Aggregates the vectors. Every element of the vector will be computed seperately.
		 *	The weight is the sum of both rank of the model and certainty of the model of given emotion.
		 *
		 *	\param vectors emotion vectors that will be aggregated
		 *	\return the aggregated vector
		 */
		
		virtual EmotionVector* AggregateVectors(vector<EmotionVector*>& vectors);

	public:
		/*! \brief	Base constructor for WeightedMeanVectorEvaluator
		 *  
		 *	Base constructor for WeightedMeanVectorEvaluator
		 *	\param model The parent CharacterModel
		 */
		WeightedMeanVectorEvaluator(CharacterModel& model);

		virtual ~WeightedMeanVectorEvaluator();

		virtual bool Evaluate(EecEvent& eecEvent);

	};

}