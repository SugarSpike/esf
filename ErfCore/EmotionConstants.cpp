#include "EmotionConstants.h"

namespace erf {

	const string OccEmotions::JOY = "JOY";
	const string OccEmotions::DISTRESS = "DISTRESS";
	const string OccEmotions::PRIDE = "PRIDE";
	const string OccEmotions::SHAME = "SHAME";
	const string OccEmotions::ADMIRATION = "ADMIRATION";
	const string OccEmotions::REPROACH = "REPROACH";
	const string OccEmotions::LOVE = "LOVE";
	const string OccEmotions::HATE = "HATE";
	const string OccEmotions::HAPPY_FOR = "HAPPY_FOR";
	const string OccEmotions::RESENTMENT = "RESENTMENT";
	const string OccEmotions::GLOATING = "GLOATING";
	const string OccEmotions::PITY = "PITY";
	const string OccEmotions::HOPE = "HOPE";
	const string OccEmotions::FEAR = "FEAR";
	const string OccEmotions::SATISFACTION = "SATISFACTION";
	const string OccEmotions::FEARS_CONFIRMED = "FEARS_CONFIRMED";
	const string OccEmotions::RELIEF = "RELIEF";
	const string OccEmotions::DISAPPOINTMENT = "DISAPPOINTMENT";
	const string OccEmotions::GRATIFICATION = "GRATIFICATION";
	const string OccEmotions::REMORSE = "REMORSE";
	const string OccEmotions::GRATITUDE = "GRATITUDE";
	const string OccEmotions::ANGER = "ANGER";
	const string OccEmotions::LIKING = "LIKING";
	const string OccEmotions::DISLIKING = "DISLIKING";

	const string PadEmotions::PLEASURE = "PLEASURE";
	const string PadEmotions::AROUSAL = "AROUSAL";
	const string PadEmotions::DOMINANCE = "DOMINANCE";

}