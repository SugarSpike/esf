#pragma once
#include "PsychologicalExpert.h"
#include "ErfException.h"

namespace erf {


	PsychologicalExpert::PsychologicalExpert(ExternalComponentConfiguration* pConfiguration): 
		BasicObject("PsychologicalExpert"),ExternalExpert(pConfiguration, ExpertType::STD_PSYCHOLOGICAL)
	{
	}

	PsychologicalExpert::PsychologicalExpert(const PsychologicalExpert &other): 
		BasicObject(other.objectName),ExternalExpert(other.configuration, ExpertType::STD_PSYCHOLOGICAL)
	{
		throw new ErfException("PsychologicalExpert::PsychologicalExpert(const PsychologicalExpert &other) called");
	}

	PsychologicalExpert::~PsychologicalExpert() { }

}