#pragma once
#include "VariantCollection.h"

/*! \file EmotionSensorReading.h
	\brief EmotionSensorReading header file
*/


namespace erf
{
	/*! \brief	A vector containing sensory data from an EmotionSensor 
	 *			
	 *	The EmotionSensorReading is a container for sensory data retrieved from an EmotionSensor.
	 *	It is a multi-type, multi-value generic container. The values that it contains depends on the type of data the sensor
	 *	provides. For example a sensor that is of single type SensorType::STD_BVP would return a single value containing
	 *	the blood volume pulse. The parameter name under which the bvp is accessible depends on the implementation of EmotionSensor
	 *	and the information should be provided by the sensor author (eg. the parameter name could be either "bvp", "BVP" or any other name).
	 */
    class ESFCORE_API EmotionSensorReading : public VariantCollection
    {
	public:
		/*! \brief Base EmotionSensorReading constructor
		*	
		*	Base constructor for EmotionSensorReading
		*/
		EmotionSensorReading();

	    virtual ~EmotionSensorReading();

	};
}