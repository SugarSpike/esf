#pragma once
#include "BasicObject.h"
#include "ExternalComponent.h"
#include "ExternalComponentEnums.h"

/*! \file ExternalExpert.h
	\brief ExternalExpert header file

*/

//Forward declaration
namespace erf {
	class EmotionModel;
	class MoodModel;
	class PsychologicalModel;
}

namespace erf
{
	/*! \brief	Represents an external expert that was dynamically loaded into the library
	 *			
	 *	External expert are components that were loaded through externall DLL libraries with the use of
	 *	ErfContext. The role that an external expert fills in the framework is based on the expert type.
	 *	The expert can represent either:
	 *	- An EmotionModel if the expert type equals to ExpertType::STD_EMOTION
	 *	- A MoodModel if the expert type equals to ExpertType::STD_MOOD
	 *	- A PsychologicalModel if the expert type equals to ExpertType::STD_PSYCHOLOGICAL
	 *
	 *	See ExternalComponent about more information about component states and parameters.
	 *	
	 */
    class ESFCORE_API ExternalExpert : public ExternalComponent
    {
			friend class EmotionExpert; ///< Specialization of ExternalExpert, the expert acts as an EmotionModel
			friend class MoodExpert; ///< Specialization of ExternalExpert, the expert acts as an MoodModel
			friend class PsychologicalExpert; ///< Specialization of ExternalExpert, the expert acts as an PsychologicalModel

		private:
		
			/*!	\brief The base constructor for ExternalExpert
			 *	
			 *	The base constructor for ExternalExpert. It will register itself inside supplied configuration and unregister upon
			 *	destruction. The component type will be set to ComponentType::EXPERT. The constructor is private as to enforce the inheritance requirements
			 *	of the expert. Depending on the expert type it should inherit from either EmotionModel, MoodModel or PsychologicalModel.
			 *	The developer should use one of the specialized classes: EmotionExpert, MoodExpert or PsychologicalExpert.
			 *	\param configuration The configuration of this external component and the library it comes from
			 *	\param expertType The type of the expert
			 */
			ExternalExpert(ExternalComponentConfiguration* configuration, ExpertType expertType);

			/*!	\brief The base copy constructor for ExternalExpert
			 *	
			 *	The base copy constructor for ExternalExpert
			 *	\param other The original ExternalExpert
			 */
			ExternalExpert(const ExternalExpert &other);

		protected:
			ExpertType expertType; ///< The type of expert, dictates the role in the framework

		public:

			/*!	\brief Returns the expert type
			 *	
			 *	Returns the expert type
			 *	\return The type of expert
			 */
			ExpertType GetExpertType() { return this->expertType; }

			virtual ~ExternalExpert();

			/*!	\brief Clones an expert
			 *	
			 *	Clones an expert. The default implementation simply calls ExternalComponentLibrary::CreateExpertForName
			 *	with the name of the current expert.
			 *	\throws ErfException If cloning is not supported by ExternalComponentLibrary
			 *	\return The clone of expert
			 */
			virtual  ExternalExpert* CloneExpert();

			/*!	\brief Casts ExternalComponent pointer to ExternalExpert pointer
			 *	
			 *	Casts ExternalComponent pointer to ExternalExpert pointer. It is used in applications that use
			 *	SWIG exported Erf. It enables the casts of higher language wrappers in a class hierarchy.
			 *	\param base The component to cast
			 *	\return The casted pointer, null if cast fails
			 */
			static ExternalExpert* CastToDerived(ExternalComponent* base) {
				return dynamic_cast<ExternalExpert*>(base);
			}

			/*!	\brief Casts ExternalComponent pointer to EmotionModel pointer
			 *	
			 *	Casts ExternalComponent pointer to EmotionModel pointer. It is used in applications that use
			 *	SWIG exported Erf. It enables the casts of higher language wrappers in a class hierarchy.
			 *	\param base The expert to cast
			 *	\return The casted pointer, null if cast fails
			 */
			static EmotionModel* CastToEmotionModel(ExternalExpert* base); 

			/*!	\brief Casts ExternalComponent pointer to MoodModel pointer
			 *	
			 *	Casts ExternalComponent pointer to MoodModel pointer. It is used in applications that use
			 *	SWIG exported Erf. It enables the casts of higher language wrappers in a class hierarchy.
			 *	\param base The expert to cast
			 *	\return The casted pointer, null if cast fails
			 */
			static MoodModel* CastToMoodModel(ExternalExpert* base); 

			/*!	\brief Casts ExternalComponent pointer to PsychologicalModel pointer
			 *	
			 *	Casts ExternalComponent pointer to PsychologicalModel pointer. It is used in applications that use
			 *	SWIG exported Erf. It enables the casts of higher language wrappers in a class hierarchy.
			 *	\param base The expert to cast
			 *	\return The casted pointer, null if cast fails
			 */
			static PsychologicalModel* CastToPsychologicalModel(ExternalExpert* base); 


	};
}