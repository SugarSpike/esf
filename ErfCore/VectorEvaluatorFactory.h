#pragma once
#include "BasicObject.h"
#include "CharacterModelEnums.h"

/*! \file VectorEvaluatorFactory.h
	\brief VectorEvaluatorFactory header file

*/

//=================================
// forward declared dependencies
namespace erf
{
	class VectorEvaluator;
	class CharacterModel;
}

namespace erf
{

	/*! \brief	Creates VectorEvaluator for a ModelsEvaluationMode
	 *			
	 *	Factory class for creating VectorEvaluator%s based on provided ModelsEvaluationMode type for both EmotionModel%s and MoodModel%s.
	 *	See also EmotionVectorCharacteristics as to how enable vector evaluation optimization for some configurations (eg. average mean computation for uniform vectors)
	 */
	class ESFCORE_API VectorEvaluatorFactory : public BasicObject
    {

	public:
		/*! \brief Base VectorEvaluatorFactory constructor
		 *	
		 *	Base constructor for VectorEvaluatorFactory
		 */
		VectorEvaluatorFactory();

		/*!	\brief Build an EmotionModel VectorEvaluator for EmotionModel
		 *	
		 *	Build an EmotionModel VectorEvaluator for given ModelsEvaluationMode
		 *	\param model The model for which to construct the VectorEvaluator
		 *	\param type The mode for which to build an VectorEvaluator
		 *	\throws ErfException when a VectorEvaluator for given type cannot be constructed
		 *	\return the evaluator for given type
		 */
		virtual VectorEvaluator* BuildEmotionEvaluator(CharacterModel& model,ModelsEvaluationMode type);

		/*!	\brief Build an EmotionModel VectorEvaluator for MoodModel
		 *	
		 *	Build an MoodModel VectorEvaluator for given ModelsEvaluationMode 
		 *	\param model The model for which to construct the VectorEvaluator
		 *	\param type The mode for which to build an VectorEvaluator
		 *	\throws ErfException when a VectorEvaluator for given type cannot be constructed
		 *	\return the evaluator for given type
		 */
		virtual VectorEvaluator* BuildMoodEvaluator(CharacterModel& model,ModelsEvaluationMode type);

	};

}