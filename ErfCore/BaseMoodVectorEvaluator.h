#pragma once
#include "VectorEvaluator.h"

/*! \file BaseMoodVectorEvaluator.h
	\brief BaseMoodVectorEvaluator header file

*/

namespace erf
{
	/*! \brief	Base mood evaluator returning the output of the first model that can handle the given event
	 *			
	 *	Base mood evaluator returning the output of the first mood model that can handle the given event.
	 *	No further computations are done.
	 *
	 */
	class ESFCORE_API BaseMoodVectorEvaluator : public VectorEvaluator
    {

	public:
		/*! \brief	Base constructor for BaseMoodVectorEvaluator
		 *  
		 *	Base constructor for BaseMoodVectorEvaluator
		 *	\param model The parent CharacterModel
		 */
		BaseMoodVectorEvaluator(CharacterModel& model);

		virtual bool Evaluate(EecEvent& eecEvent);

	};

}