#pragma once
#include "ExternalComponent.h"
#include "ErfUtils.h"
#include "ErfExternalComponentException.h"
#include "ExternalComponentConfiguration.h"
#include <Windows.h>
#include "ErfLogging.h"
#include <algorithm> 

namespace erf {
	
	ExternalComponent::ExternalComponent(ExternalComponentConfiguration* pConfiguration, ComponentType componentType): 
		BasicObject("ExternalComponent"), configuration(pConfiguration), componentState(ComponentState::UNDEFINED), componentType(componentType) {
			(*configuration).components.push_back(this);

	}

	ExternalComponent::ExternalComponent(const ExternalComponent &other) : 
		configuration(other.configuration), componentState(ComponentState::UNDEFINED), componentType(other.componentType) {

	}

	ExternalComponent::~ExternalComponent() {
		if(this->componentState != ComponentState::UNINITIALIZED
				&& this->componentState != ComponentState::DEINITIALIZED)
			Deinitialize();

		vector<ExternalComponent*>& components =  (*configuration).components;
		vector<ExternalComponent*>::iterator position =  std::find(components.begin(), components.end(), this);
		if (position != components.end())
			components.erase(position);

		if(components.empty())
		{
			try {
				delete this->configuration;
			} catch(...) {
				LOG(error) << GetUniqueName() << " - could not delete configuration for '" << this->configuration->componentDllName << "'";
			}
		}
	}

}