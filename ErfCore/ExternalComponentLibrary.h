#pragma once
#include "BasicObject.h"
#include "ExternalComponent.h"
#include "ExternalSharedLibrary.h"

/*! \file ExternalComponentLibrary.h
	\brief ExternalComponentLibrary header file

*/

//=================================
// forward declared dependencies
namespace erf
{
	class ExternalComponentConfiguration;
	class ExternalExpert;
	class EmotionSensor;
	class ExternalFilter;
}

namespace erf
{

	/*! \brief	Represents an external component library that was dynamically loaded into the framework
	 *			
	 *	External components are components that were loaded through externall DLL libraries with the use of
	 *	ErfContext. The ExternalComponentLibrary represents the library itself. It is used to communicate with the DLL file
	 *	through a series of pointers to functions. The DLL itself needs to export (C linkage) a subset of these functions
	 *	in order to be properly loaded into the framework. There required functions and their signatures are as follows:
	 *		- bool SupportsComponentType(ComponentType) - Returns whenever the DLL creates component of passed type (eg. experts, sensors)
	 *		- bool SetParameter(string, string) - Sets global parameter for the DLL (see ExternalComponent::SetParameter)
	 *		- const string GetParameter(string) - Gets global parameter value from the DLL (see ExternalComponent::GetParameter)
	 *		- void GetSupportedParameters(vector<string>&) - Loads supported global parameters names into the passed vector (see ExternalComponent::GetSupportedParameters)
	 *		- void GetParameters(map<string, string >& ) - Loads global parameters into the passed map (see ExternalComponent::GetParameters)
	 *		- void RegisterComponents(ExternalComponentLibrary &) - Called on the initialization of the library, the passed object is an instance representing the called library
	 *																		the function can be used to initialize the library object (for example registering sensors and experts in the library)
	 *	The DLL may also expose those optional functions:
	 *		- ExternalExpert* CreateExpertForName(ExternalComponentLibrary &, string expertName) - Creates a new instance of ExternalExpert with the given name for the passed library
	 *		- EmotionSensor* CreateSensorForName(ExternalComponentLibrary &, string sensorName) - Creates a new instance of EmotionSensor with the given name for the passed library
	 *		- ExternalFilter* CreateFilterForName(ExternalComponentLibrary &, string filterName) - Creates a new instance of ExternalFilter with the given name for the passed library
	 *
	 *	The ExternalLibraryComponent is the memory owner of all ExternalComponent%s registerd in it (it is responsible for deleting them).
	 *	See ExternalComponent about more information about component states and parameters.
	 */
    class ESFCORE_API ExternalComponentLibrary : public ExternalComponent
    {

	private:

        ExternalSharedLibrary::HandleType sharedLibraryHandle; ///< Handle of the loaded shared library

		typedef bool SupportsComponentTypeFunction(ComponentType);	///< Supported component function signature
		SupportsComponentTypeFunction *getSupportsComponentTypeFunction;	///< Supported component function address

		typedef void GetSupportedParametersFunction(vector<string>&);	///< Supported parameters function signature
		GetSupportedParametersFunction *getSupportedParametersAddress;	///< Supported parameters function address

		typedef void GetParametersFunction(map<string, string >& );	///< Parameters retrieval function signature
		GetParametersFunction *getParametersAddress;	///< Parameters retrieval function address

		typedef bool SetParameterFunction(string, string);	///< Parameter set function signature
		SetParameterFunction *setParameterAddress;	///< Parameter set function address

		typedef const string GetParameterFunction(string);	///< Parameter retrieval function signature
		GetParameterFunction *getParameterAddress;	///< Parameter retrieval function address

		typedef void RegisterComponentsFunction(ExternalComponentLibrary &); ///< Component registration function signature
		RegisterComponentsFunction *registerComponentsAddress; ///< Component registration function address

		typedef ExternalExpert* CreateExpertForNameFunction(ExternalComponentLibrary &, string expertName); ///< Expert creation function signature
		CreateExpertForNameFunction *createExpertForNameAddress; ///< Expert creation function address

		typedef EmotionSensor* CreateSensorForNameFunction(ExternalComponentLibrary &, string sensorName); ///< Sensor creation function signature
		CreateSensorForNameFunction *createSensorForNameAddress; ///< Sensor creation function address

		typedef ExternalFilter* CreateFilterForNameFunction(ExternalComponentLibrary &, string filterName); ///< Filter creation function signature
		CreateFilterForNameFunction *createFilterForNameAddress; ///< Fukter creation function address

		vector<ComponentType> supportedComponentTypes;	///< Supported component types by the library (eg. sensors, experts)
		vector<int> supportedSensorTypes; ///< Supported data types by the sensors from the library (eg. heart rate, skin conductance)

	public:
	   /*!	\brief The base constructor for ExternalComponentLibrary
		*	
		*	The base constructor for ExternalComponentLibrary. It will register itself inside supplied configuration and unregister upon
		*	destruction. The component type will be set to ComponentType::LIBRARY. 
		*	\param configuration The configuration representing the loaded library
		*/
		ExternalComponentLibrary(ExternalComponentConfiguration* configuration);
		~ExternalComponentLibrary();

	   /*!	\brief Creates the library
		*	
		*	Calls the RegisterComponents function and registers all newly created experts,sensors and filters
		*/
		void CreateLibrary();

	   /*!	\brief Returns supported component types by the library (eg. sensors, experts)
		*	
		*	\returns Supported component types by the library
		*/
		vector<ComponentType>& GetSupportedComponentTypes();
	   /*!	\brief Returns supported data types by the senors in the library (eg. pulse, skin conductance)
		*	
		*	\returns Supported data types by the sensors in the library
		*/
		vector<int>& GetSupportedSensorTypes();

		virtual string GetComponentLastError() ;

		virtual void GetParameters(map<string,string>& paramHolder);

		virtual void GetSupportedParameters(vector<string>& paramHolder);

		virtual const string GetParameter(string parameterName) ;

		virtual bool SetParameter(string parameterName, string parameterValue);

		virtual void Initialize() {};

		virtual void Reinitialize() {};

		virtual void Deinitialize() {};

		/*!	\brief Creates an expert for the given name
		*	
		*	\throws ErfException when the cloning is not supported by the library
		*	\returns Created expert
		*/
		virtual ExternalExpert* CreateExpertForName(string expertName);
		/*!	\brief Creates an sensor for the given name
		*	
		*	\throws ErfException when the cloning is not supported by the library
		*	\returns Created sensor
		*/
		virtual EmotionSensor* CreateSensorForName(string sensorName);
		/*!	\brief Creates an filter for the given name
		*	
		*	\throws ErfException when the cloning is not supported by the library
		*	\returns Created filter
		*/
		virtual ExternalFilter* CreateFilterForName(string filterName);

		/*!	\brief Casts ExternalComponent pointer to ExternalComponentLibrary pointer
		*	
		*	Casts ExternalComponent pointer to ExternalComponentLibrary pointer. It is used in applications that use
		*	SWIG exported Erf. It enables the casts of higher language wrappers in a class hierarchy.
		*	\param base The component to cast
		*	\return The casted pointer, null if cast fails
		*/
		static ExternalComponentLibrary* CastToDerived(ExternalComponent* base) {
			return dynamic_cast<ExternalComponentLibrary*>(base);
		}
	};
}