#pragma once
#include "BasicObject.h"
#include "GameEnums.h"
#include "VariantCollection.h"
#include "Variant.h"

/*! \file EecEvent.h
	\brief EecEvent header file

*/

namespace erf
{
	/*! \brief	An emotion eliciting condition event
	 *			
	 *	The emotion eliciting condition event (EecEvent) is an event that is send to the CharacterData in order to compute
	 *	new emotion state. It is a multi-type, multi-value generic container. It should be used to pass data from the game
	 *	into the framework.
	 */
    class ESFCORE_API EecEvent : public VariantCollection
    {
	private:
		int eventType; ///< Unique event identifier, in most cases it is a value from GameEvent enum
		time_t timeOfEvent; ///< /< The time of the event (as number of miliseconds since start of UNIX epoch)
	public:
		/*! \brief	Base constructor
		 *		
		 *	Base constructor.
		 *
		 *	\param eventType the type of event
		 */
		EecEvent(int eventType);
		/*! \brief	Base constructor
		 *		
		 *	Base constructor.
		 *
		 *	\param eventType the type of event
		 *	\param timeOfEvent the time of the event
		 */
		EecEvent(int eventType, time_t timeOfEvent);

		virtual ~EecEvent();

		/*! \brief	Returns the type of the event
		 *	
		 *	Returns the type of the event
		 *	\return Returns the event type
		 */
		const int GetEventType();

		/*! \brief Gets the time the event occured
		*
		*  Returns the time the event occured (as number of miliseconds since start of UNIX epoch)
		*
		*	\return time of occurance
		*/
		const time_t GetTimeOfEvent();
		
		/*! \brief	Out stream operator, prints EevEvent data to stream
		 *	
		 *	Out stream operator, prints EevEvent data to stream
		 */
		friend ostream& operator<<(ostream& os, const EecEvent& dt);

	};
}