#pragma once
#include "EmotionModel.h"
#include "ExternalExpert.h"

/*! \file EmotionExpert.h
	\brief EmotionExpert header file

*/

namespace erf
{
	/*! \brief	Represents an external expert that acts as an EmotionModel
	 *			
	 *	This class is a specialization of ExternalExpert.	
	 *	Represents an external expert that was dynamically loaded into the library and acts as an EmotionModel.
	 *	See ExternalComponent about more information about component states and parameters.
	 *	
	 */
    class ESFCORE_API EmotionExpert : public ExternalExpert, public EmotionModel
    {
		private:
		public:
			/*!	\brief The base constructor for EmotionExpert
			 *	
			 *	The base constructor for EmotionExpert. It will register itself inside supplied configuration and unregister upon
			 *	destruction. The component type will be set to ComponentType::EXPERT and the expert type will be set to STD_EMOTION.
			 *	The expert will be recognized as an EmotionModel in the framework.
			 *	\param configuration The configuration of this external component and the library it comes from
			 */
			EmotionExpert(ExternalComponentConfiguration* configuration);

			/*!	\brief The base copy constructor for EmotionExpert
			 *	
			 *	The base copy constructor for EmotionExpert
			 *	\param other The original EmotionExpert
			 */
			EmotionExpert(const EmotionExpert &other);
			virtual ~EmotionExpert();
	};
}