#pragma once
#include "Common.h"

#include <string>
#include <stdexcept>
#include "ErfExternalComponentException.h"
#include "ErfUtils.h"

/*! \file ExternalSharedLibrary.h
	\brief ExternalSharedLibrary header file

<pre>
///////////////////////////////////////////////////////////////////////////////
// Plugin architecture example                                               //
// http://blog.nuclex-games.com/tutorials/cxx/plugin-architecture/           //
// This code serves as an example to the plugin architecture discussed in    //
// the article and can be freely used                                        //
///////////////////////////////////////////////////////////////////////////////
</pre>
*/


#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <windows.h>

namespace erf {

   /*! \brief	Shared library loading and access on windows 
	*	
	*	Heavly (very) based on http://blog.nuclex-games.com/tutorials/cxx/plugin-architecture/. Used to dynamically load and unload
	*	dll files and to retrieve exported functions.\n
	*	<pre>
	*	///////////////////////////////////////////////////////////////////////////////
	*	// Plugin architecture example                                               //
	*	// http://blog.nuclex-games.com/tutorials/cxx/plugin-architecture/           //
	*	// This code serves as an example to the plugin architecture discussed in    //
	*	// the article and can be freely used                                        //
	*	///////////////////////////////////////////////////////////////////////////////
	*	</pre>
	*
	*/
  class ExternalSharedLibrary {

    /// <summary>Handle by which DLLs are referenced</summary>
    public: typedef HMODULE HandleType;

   /*! \brief	Checks the validity of the function handle
	*	
	*	\throws ErfExternalComponentException if the handle was invalid (could not be retrieved)
	*/
	private: static void CheckValidFunctionHandle(void* handle, string funcName)
	{
		if(handle == null)
		{
			string errorMessage = "ExternalSharedLibrary::CheckValidFunctionHandle - function '"+funcName+
				"' not found";
			throw ErfExternalComponentException(errorMessage);
		}
	}


	/*! \brief	Loads the DLL from the specified path
	*	
	*	\throws ErfExternalComponentException if the handle for dll could not be aquired
	*	\param pathToFolder Path of the DLL that will be loaded
	*	\param libraryName Name of the DLL library to load
	*	\returns A handle to the DLL
	*/
    public: ESFCORE_API static HandleType Load(const std::string &pathToFolder, const std::string &libraryName) {

		std::string previousDllDir(MAX_PATH+1,0);
		GetCurrentDirectoryA(MAX_PATH+1, &previousDllDir[0]);
		if(pathToFolder != "")
			SetCurrentDirectoryA(pathToFolder.c_str());

		HMODULE moduleHandle = ::LoadLibraryA(libraryName.c_str());
		SetCurrentDirectoryA(previousDllDir.c_str());

		if(moduleHandle == NULL) {
			string lastError = erfutils::GetLastErrorStdString();
			throw ErfExternalComponentException("Could not load DLL: "+lastError);
		}

		return moduleHandle;
    }



	/*! \brief	Unloads the DLL 
	*	
	*	\throws ErfExternalComponentException if the handle could not be freed
	*	\param sharedLibraryHandle  Handle of the DLL that will be unloaded
	*/
    public: ESFCORE_API static void Unload(HandleType sharedLibraryHandle) {
      BOOL result = ::FreeLibrary(sharedLibraryHandle);
      if(result == FALSE) {
		string lastError = erfutils::GetLastErrorStdString();
        throw ErfExternalComponentException("Could not unload DLL: "+lastError);
      }
    }

	/*! \brief	Looks up a function exported by the DLL
	*	
	*	\throws ErfExternalComponentException if the handle could not be freed
	*	\param sharedLibraryHandle  Handle of the DLL in which the function will be looked up
	*	\param functionName Name of the function to look up
	*	\returns A handle to the specified function
	*/

    public: template<typename TSignature>
    static TSignature *GetFunctionPointer(
      HandleType sharedLibraryHandle, const std::string &functionName
    ) {
      FARPROC functionAddress = ::GetProcAddress(
        sharedLibraryHandle, functionName.c_str()
      );
	  CheckValidFunctionHandle(functionAddress,functionName);
      return reinterpret_cast<TSignature *>(functionAddress);
    }

  };

}