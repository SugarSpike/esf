#pragma once
#include "EmotionVectorFilter.h"
#include "ExternalComponent.h"
#include "ExternalComponentEnums.h"

/*! \file ExternalFilter.h
	\brief ExternalFilter header file

*/

namespace erf
{
	/*! \brief	Represents an external filter that acts as a EmotionVectorFilter within the framework
	 *			
	 *	External filters are components that were loaded through externall DLL libraries with the use of
	 *	ErfContext. Their role is act as EmotionVectorFilter within the framework.
	 *
	 *	See ExternalComponent about more information about component states and parameters.
	 *	
	 */
    class ESFCORE_API ExternalFilter : public ExternalComponent, public EmotionVectorFilter
    {
		protected:

		public:
		   /*!	\brief The base constructor for ExternalFilter
			*	
			*	The base constructor for ExternalFilter. It will register itself inside supplied configuration and unregister upon
			*	destruction. The component type will be set to ComponentType::FILTER.
			*	\param configuration The configuration of this external component and the library it comes from
			*/
			ExternalFilter(ExternalComponentConfiguration* configuration);

			/*!	\brief The base copy constructor for ExternalFilter
			*	
			*	The base copy constructor for ExternalFilter
			*	\param other The original ExternalFilter
			*/
			ExternalFilter(const ExternalFilter &other);

			virtual ~ExternalFilter();

		   /*!	\brief Clones a filter
			*	
			*	Clones a filter. The default implementation simply calls ExternalComponentLibrary::CreateFilterForName
			*	with the name of the current filter.
			*	\throws ErfException If cloning is not supported by ExternalComponentLibrary
			*	\return The clone of filter
			*/
			virtual  ExternalFilter* CloneFilter();

			/*!	\brief Casts ExternalComponent pointer to ExternalFilter pointer
			 *	
			 *	Casts ExternalComponent pointer to ExternalFilter pointer. It is used in applications that use
			 *	SWIG exported Erf. It enables the casts of higher language wrappers in a class hierarchy.
			 *	\param base The expert to cast
			 *	\return The casted pointer, null if cast fails
			 */
			static ExternalFilter* CastToDerived(ExternalComponent* base) {
				return dynamic_cast<ExternalFilter*>(base);
			}

			/*!	\brief Casts ExternalFilter pointer to EmotionVectorFilter pointer
			 *	
			 *	Casts ExternalFilter pointer to EmotionVectorFilter pointer. It is used in applications that use
			 *	SWIG exported Erf. It enables the casts of higher language wrappers in a class hierarchy.
			 *	\param base The expert to cast
			 *	\return The casted pointer, null if cast fails
			 */
			static EmotionVectorFilter* CastToEmotionVectorFilter(ExternalFilter* base); 

	};
}