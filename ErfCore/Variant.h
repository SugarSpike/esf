#pragma once
#include "Common.h"

/*! \file Variant.h
	\brief Variant header file
*/


/*! \brief Controls the type of multi-type container to use
*	If the ERF_VARIANT_HOLD_ANY is set to true then boost::spirit::hold_any is used instead of boost::any.
*	The former is faster than the latter but there are requirements imposed on type that is contained:
*	- It needs to implement the operator<<
*	- It needs to implement the operator>>
*	Taking this into account the boost::spirit::hold_any cannot be called a 'hold-all' container whenever
*	boost::any truly is.
*/
#if !defined(ERF_VARIANT_HOLD_ANY)

#include "boost\any.hpp"
/*! \brief Type of multi-type container used by Variant Class
*	Type of multi-type container used by Variant Class - in this case it is	boost::any
*/
typedef boost::any BoostAny;
/*! \brief Namespace of the type of multi-type container used by Variant Class
*	Namespace of the type of multi-type container used by Variant Class - in this case it is boost::
*/
namespace boostAny = boost;

#else
#include "boost\spirit\home\support\detail\hold_any.hpp"
typedef boost::spirit::hold_any BoostAny;
namespace boostAny = boost::spirit;
#endif

namespace erf
{
	/*! \brief Multi-type container
	*
	*	The Variant class is a single multi-type container. It can hold both primitive and pointer data in a trasnparent way.
	*	For pointer data it can either be the owner of it (and will call delete upon destruction) or not (in that case the programmer
	*	is responsible for cleaning data). The Variant class is assumed to be immutable and so any changes to the value it holds (for primitives)
	*	or it points to (complex) should not be changed. Ofcourse there is no restriction in modifying the underlying data it points to. 
	*/
    class ESFCORE_API Variant
    {
	protected:
		/*! \brief Base Variant constructor
		*	
		*	Base constructor for Variant
		*/
		Variant();

	private:

		/*! \brief Type of data that the Variant holds
		*	
		*	The type of data that the Variant holds, primitive types are seperately handled for better performance
		*/
		enum DataType
	   {
		  Empty,	///< The variant is empty
		  Float,	///< The variant holds float value
		  Double,	///< The variant holds double value
		  Integer,	///< The variant holds int value
		  Long,		///< The variant holds long value
		  Other		///< The variant holds a pointer to other data
	   };

		/*! \brief Type of data that the Variant holds
		*	
		*	The type of data that the Variant holds, primitive types are seperately handled for better performance
		*/
		union {
			float floatData;
			double doubleData;
			int	integerData;
			long longData;
		} value;
		BoostAny* valueData;
		DataType type; ///< The type of data that the Variant holds
		bool isMemoryOwner; ///< Whenever the Variant is owner of the memory of data it holds, if yes then the data will be deleted upon destruction of Variant
	public:
	   /*! \brief Variant constructor from raw BoostAny data
		*	
		*	Base constructor for Variant from other raw BoostAny data
		*	\param raw Raw data from which to construct the Variant
		*/
		Variant(BoostAny* raw);

		/*! \brief Gets the underlying raw data
		*	
		*	Base constructor for Variant from other raw BoostAny data
		*	\return Pointer to raw data, if the DataType is different than Other then the result is undefined
		*/
		BoostAny* GetRaw();

		virtual ~Variant();

		/*! \brief Constructs a Variant holding a pointer to T, the Variant is the new owner of data pointed by T*
		*
		*	Constructs a Variant holding a pointer to T, the Variant is the new owner of data pointed by T*.
		*	Upon desctuction of Variant deletion will also be called on T*.
		*
		*	\param value The pointer to value of type T for Variant to hold
		*	\return New instance of Variant holding the T pointer to value
		*/
		template <typename T>
		static Variant* Create(T* value) {
			return Variant::Create(value,true);
		}

		/*! \brief Constructs a Variant holding a pointer to T
		*
		*	Constructs a Variant holding a pointer to T. The Variant is the new owner of data pointed by T* if isMemoryOwner is true.
		*	In that case upon desctuction of Variant deletion will also be called on T*.
		*	If the isMemoryOwner is set to false then the data will not be deleted upon destruction of variant.
		*
		*	\param value The pointer to value of type T for Variant to hold
		*	\param isMemoryOwner Whenever the Variant is the owner of memory pointed by value
		*	\return New instance of Variant holding the T pointer to value
		*/
		template <typename T>
		static Variant* Create(T* value, bool isMemoryOwner) {
			Variant* v = new Variant();
			BoostAny* wrappedVal = null;
			v->isMemoryOwner = isMemoryOwner;
			if(isMemoryOwner)
				wrappedVal = new BoostAny(std::shared_ptr<T>(value));
			else
				wrappedVal = new BoostAny(value);
			v->type = DataType::Other;
			v->valueData = wrappedVal;
			return v;
		}

		/*! \brief Returns the data hold by Varaint as a pointer to T type
		*
		*	\throws boostAny::bad_any_cast If the value could not be cast
		*	\return Value cast to T*, null if the value held by Variant was not of DataType::Other
		*/
		template <typename T>
		T* As() {
			if(this->type != DataType::Other)
				return null;

			if(isMemoryOwner)
			{
				std::shared_ptr<T> val = boostAny::any_cast<std::shared_ptr<T> > (*this->valueData);
				return val.get();
			}
			T* val = boostAny::any_cast<T*> (*this->valueData);
			return val;
		}

		/*! \brief Returns whenever the data hold by Varaint is a pointer to T type
		*
		*	Returns whenever the data hold by Varaint is a pointer to T type
		*	\return True if Variant hold a pointer to T, false otherwise
		*/
		template <typename T>
		bool Is() {
			if(this->type != DataType::Other)
				return false;

			if(isMemoryOwner) {
					try { 
					//When created with ownership of data
					boostAny::any_cast<std::shared_ptr<T> >(this->valueData);
					return true; 
				} 
				catch(const boostAny::bad_any_cast &) 
				{ 
					return false;
				} 
			}
			try { 
				boostAny::any_cast<T* >(this->valueData);
				return true; 
			} 
			catch(const boostAny::bad_any_cast &) 
			{ 
				return false; 
			} 
		}

		/*! \brief Clones a Variant
		*
		*	Clones a variant. The primitive data will be deep cloned. The pointer data will not, it will be assigned as-is.
		*	If the Variant is the owner of memory then the pointer to instance will be shared between the original and all clones
		*	as an std::shared_ptr and the data will be realeased upon destruction of last Variant that holds the data.
		*	If the Variant is not the owner of memory then the pointer will simply be copied to the clone.
		*	\return Clone of the variant
		*/
		Variant* Clone();

		/*! \brief Prints the data of variant
		*
		*	Prints the data of variant
		*	\return Data from variant in a string
		*/
		string Print();

		/*! \brief Whenever the Variant holds data other than that of a primitive type
		*	
		*	Whenever the Variant holds data other than that of a primitive type
		*	\return Whenever the Variant holds data other than that of a primitive type
		*/
		bool IsOther();

		/*! \brief Creates a Variant holding primitive value of type integer
		*	
		*	Creates a Variant holding primitive value of type integer
		*	\param value The integer value to hold
		*	\return Variant holding the integer value
		*/
		static Variant* Create(int value); 
		/*! \brief Whenever the Variant is holding primitive value of type integer
		*	
		*	Whenever the Variant is holding primitive value of type integer
		*	\return Whenever the Variant is holding primitive value of type integer
		*/
		bool IsInteger(); 
		/*! \brief Returns the data held by Variant as integer
		*	
		*	Returns the data held by Variant as integer
		*	\return The integer value held by the Variant, if the Variant was not holding a integer then the return value is undefined
		*/
		int AsInteger();

		/*! \brief Creates a Variant holding primitive value of type float
		*	
		*	Creates a Variant holding primitive value of type float
		*	\param value The float value to hold
		*	\return Variant holding the float value
		*/
		static Variant* Create(float value);  
		/*! \brief Whenever the Variant is holding primitive value of type float
		*	
		*	Whenever the Variant is holding primitive value of type float
		*	\return Whenever the Variant is holding primitive value of type float
		*/
		bool IsFloat(); 
		/*! \brief Returns the data held by Variant as float
		*	
		*	Returns the data held by Variant as float
		*	\return The float value held by the Variant, if the Variant was not holding a float then the return value is undefined
		*/
		float AsFloat();

		/*! \brief Creates a Variant holding primitive value of type double
		*	
		*	Creates a Variant holding primitive value of type double
		*	\param value The double value to hold
		*	\return Variant holding the double value
		*/
		static Variant* Create(double value);  
		/*! \brief Whenever the Variant is holding primitive value of type double
		*	
		*	Whenever the Variant is holding primitive value of type double
		*	\return Whenever the Variant is holding primitive value of type double
		*/
		bool IsDouble(); 
		/*! \brief Returns the data held by Variant as double
		*	
		*	Returns the data held by Variant as double
		*	\return The double value held by the Variant, if the Variant was not holding a double then the return value is undefined
		*/
		double AsDouble();

		/*! \brief Creates a Variant holding primitive value of type long
		*	
		*	Creates a Variant holding primitive value of type long
		*	\param value The long value to hold
		*	\return Variant holding the long value
		*/
		static Variant* Create(long value);  
		/*! \brief Whenever the Variant is holding primitive value of type long
		*	
		*	Whenever the Variant is holding primitive value of type long
		*	\return Whenever the Variant is holding primitive value of type long
		*/
		bool IsLong(); 
		/*! \brief Returns the data held by Variant as long
		*	
		*	Returns the data held by Variant as long
		*	\return The long value held by the Variant, if the Variant was not holding a long then the return value is undefined
		*/
		long AsLong();

		/*! \brief Creates a Variant holding value of type string
		*
		*	Creates a Variant holding value of type string.
		*	This function is a synonym to Variant::Create<string> and was specialized to easly expose String data 
		*	throu a SWIG interface.
		*
		*	\param value The string value to hold
		*	\return Variant holding the string value
		*/
		static Variant* Create(string value); 
		/*! \brief Whenever the Variant is holding value of type string
		*
		*	Whenever the Variant is holding value of type string
		*	This function is a synonym to Variant::Is<string> and was specialized to easly expose String data 
		*	throu a SWIG interface.
		*	\return Whenever the Variant is holding value of type string
		*/
		bool IsString(); 
		/*! \brief Returns the data held by Variant as string
		*
		*	Returns the data held by Variant as string
		*	This function is a synonym to Variant::As<string> and was specialized to easly expose String data 
		*	throu a SWIG interface
		*	\throws boostAny::bad_any_cast If the value could not be cast to std::string
		*	\return The string held by the Variant
		*/
		string AsString();

	};
}