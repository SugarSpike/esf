#include "BasicObject.h"
#include "ErfLogging.h"

#include <boost/cstdint.hpp>
#include <boost/optional/optional.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

namespace erf  
{
	void BasicObject::InitializeBasicObject()
	{
		if(this->objectName.empty())
		{
			LOG(debug) << "BasicObject::InitializeBasicObject() - no name provided ";
		}

		this->objectIdStr = boost::lexical_cast<std::string>(this->objectId);
	}

	BasicObject::BasicObject(string objectName) : objectName(objectName), objectId(boost::uuids::random_generator()())
	{
		InitializeBasicObject();
		LOG(debug) << "BasicObject::BasicObject(string) - performed " << GetUniqueName();
	}

	BasicObject::BasicObject(const BasicObject &other) : objectName(other.objectName), objectId(boost::uuids::random_generator()())
	{
		InitializeBasicObject();
		LOG(debug) << "BasicObject::BasicObject(&other) - performed " << GetUniqueName();
	}

	BasicObject::BasicObject() :objectName("BasicObject"), objectId(boost::uuids::random_generator()())
	{
		InitializeBasicObject();
		LOG(debug) << "BasicObject::BasicObject - performed " << GetUniqueName();
	}

	BasicObject::~BasicObject()
	{
		LOG(debug) << "BasicObject::~BasicObject - performed " << GetUniqueName();
	}

	BasicObject& BasicObject::operator= (const BasicObject &cSource)
	{
//		LOG(debug) << "BasicObject::operator= - " << GetUniqueName() << " = " << cSource.GetUniqueName();
		return *this;
	}

	bool BasicObject::operator==(BasicObject const& rhs) const {
		return objectId == rhs.objectId;
	}

}