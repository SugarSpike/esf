#pragma once
/*! \file EmotionConstants.h
	\brief Contains all constants for various emotion models (PAD and OCC)

*/
#include "Common.h"


namespace erf {

	/*! \brief	Contains constants for OCC Model
	 *			
	 *	Contains map keys that should be used by all components in framework when populating EmotionVector%s with data for OCC model.
	 *	For example if the EmotionVector is of type OCC then it is expected that the method GetValue(OccEmotions::JOY) 
	 *	should return the current intensity of the emotion of joy.
	 */
	class ESFCORE_API OccEmotions {
	public:
		static const string JOY; ///< Joy emotion
		static const string DISTRESS; ///< Distress emotion
		static const string PRIDE; ///< Pride emotion
		static const string SHAME; ///< Shame emotion
		static const string ADMIRATION; ///< Admiration emotion
		static const string REPROACH; ///< Reproach emotion
		static const string LOVE; ///< Love emotion
		static const string HATE; ///< Hate emotion
		static const string HAPPY_FOR; ///< Happy-for emotion
		static const string RESENTMENT; ///< Resentment emotion
		static const string GLOATING; ///< Gloationg emotion
		static const string PITY; ///< Pity emotion
		static const string HOPE; ///< Hope emotion
		static const string FEAR; ///< Fear emotion
		static const string SATISFACTION; ///< Satisfaction emotion
		static const string FEARS_CONFIRMED; ///< Fears-confirmed emotion
		static const string RELIEF; ///< Relief emotion
		static const string DISAPPOINTMENT; ///< Disappointment emotion
		static const string GRATIFICATION; ///< Gratifitaction emotion
		static const string REMORSE; ///< Remorse emotion
		static const string GRATITUDE; ///< Gratitude emotion
		static const string ANGER; ///< Anger emotion
		static const string LIKING; ///< Liking emotion
		static const string DISLIKING; ///< Disliking emotion

	};

	/*! \brief	Contains constants for PAD Model
	 *			
	 *	Contains map keys that should be used by all components in framework when populating EmotionVector%s with data for PAD model.
	 *	For example if the EmotionVector is of type PAD then it is expected that the method GetValue(PadEmotions::PLEASURE) 
	 *	should return the pleasure component of the PAD emotion.
	 */
	class ESFCORE_API PadEmotions {
	public:
		static const string PLEASURE; ///< Pleasure component
		static const string AROUSAL; ///< Arousal component
		static const string DOMINANCE; ///< Dominance component

	};
}