#pragma once
#include "BasicObject.h"
#include <list>
#include <iostream>
#include <boost\foreach.hpp>

/*! \file PriorityList.h
	\brief PriorityList header file
*/

namespace erfutils
{
	/*! \brief	A single record in the PriorityList holding the priority and value of type T
	 *	
	 * A single record in the PriorityList holding the priority and value of type T
	 */
	template <typename T>
    class PriorityRecord
    {
	public:

		int priority; ///< Priority of the element

		T value; ///< The value of the element

		/*! \brief	Base constructor for PriorityRecord
		 *	
		 *	Base constructor for PriorityRecord
		 *
		 *	\param priority Priority of the element
		 *	\param value Value of the element
		 */
		PriorityRecord( int priority,T value )
		   : priority(priority), value(value)
		   { }

		/*! \brief	Prints a PriorityRecord to stream
		 *	
		 *	Prints a PriorityRecord to stream
		 */
		friend ostream& operator<<(ostream& os, const PriorityRecord<T>& dt) {
			os << dt.priority << '/' << typeid(dt.value).name();
		   return os;
		}

		/*! \brief	Compares two PriorityRecord%s
		 *	
		 *	\param first The lhs
		 *	\param second The rhs
		 *	\return Whenever the lhs priority is smaller than the rhs
		 */
		static bool compare (const PriorityRecord& first, const PriorityRecord& second)
		{
			return first.priority < second.priority;
		}
	};
	
	/*! \brief	A priority list implementation for type T
	 *
	 *	The priority list implementation for type T. The list is sorted ascending by priority.
	 */
	template <typename T>
	class PriorityList : public std::list<PriorityRecord<T> >
	{
		public:
			/*! \brief	Inserts new data into priority list
			 *	
			 *	Inserts new data into priority list
			 *	\param priority The priority of data
			 *	\param val The data
			 */
			void insert(int priority,T val) {
				insert(PriorityRecord<T>(priority,val));
			}

			/*! \brief	Inserts priority record into priority list
			 *	
			 *	Inserts priority record into priority list
			 *	\param val The record to insert
			 */
			void insert(const PriorityRecord<T>& val) {
				int count = 0;
				list::iterator iterator = this->begin();
				for (list::iterator end = this->end(); 
						iterator != end; ++iterator) {
					if(PriorityRecord<T>::compare(*iterator,val))
						break;
				}
				list::insert(iterator,val);

			}
			
			/*! \brief	Prints the list into a stream
			 *
			 *	Prints the list into a stream
			 */
			friend ostream& operator<<(ostream& os, const PriorityList<T>& l) {
				os << "[ ";
				BOOST_FOREACH( PriorityRecord<T> rec,  l )
				{
					os << rec.priority << ':' << typeid(rec.value).name() << " ";
				}
				os << "]";
			   return os;
			}

			/*! \brief	Checks if the priority list contains an item with specified priority and value
			 *	
			 *	Checks if the priority list contains an item with specified priority and value
			 *	\param priority Priority of the record to search
			 *	\param val Value of the record to search
			 *	\return Whenever the list contains specified record
			 */
			bool contains (int priority, T val)
			{
				list::iterator first = this->begin();
			  list::iterator last = this->end();

			  while (first!=last) {
				  if ((*first).priority == priority && typeid((*first).value) == typeid(val)) 
					  return true;
				++first;
			  }
			  return false;
			}

			/*! \brief	Checks if the priority list contains a specified record
			 *	
			 *	Checks if the priority list contains a specified record
			 *	\param val Priority record that is searched
			 *	\return Whenever the list contains specified record
			 */
			bool contains (const PriorityRecord<T>& val)
			{
			  list::iterator first = this->begin();
			  list::iterator last = this->end();

			  while (first!=last) {
				  if ((*first).priority == val.priority && typeid((*first).value) == typeid(val.value)) 
					  return true;
				++first;
			  }
			  return false;
			}

			/*! \brief	Removes all elements with specified value (ignoring priority)
			 *	
			 *	Removes all elements with specified value (ignoring priority)
			 *	\param val The value to remove from the priority list
			 */
			void remove(T val)
			{
			  list::iterator first = this->begin();
			  list::iterator last = this->end();

			  while (first!=last) {
				  if ((*first).value == val) 
				  {
					  this->erase(first);
					  return;
				  }
				++first;
			  }
			}
			
			/*! \brief	Removes all elements that are of specified class type
			 *
			 *	Removes all elements that are of specified class type
			 */
			struct RemoveIfTypeEquals
			{
				
				/*! \brief	The base RemoveIfTypeEquals constructor
				 *
				 *	The base RemoveIfTypeEquals constructor
				 *	\param x The name of the class name to remove
				 */
				RemoveIfTypeEquals(const char  *x) : m_x(x) {}

				/*! \brief	The invocation operator, will be called for each record in the list
				 *	
				 *	The invocation operator, will be called for each record in the list
				 *	\param record The record to check
				 *	\return Whenever the PriorityRecord type is equal to the searched name
				 */
				bool operator() (PriorityRecord<T> record)
				{
					return strcmp(typeid(record.value).name(), m_x) == 0;
				}

				const char * m_x; ///< The name of the class name to remove
			};

			/*! \brief	Removes all elements with the given name
			 *
			 *	Removes all elements with the given name
			 */
			struct RemoveIfNameEquals
			{
				/*! \brief	The base RemoveIfNameEquals constructor
				 *	
				 *	The base RemoveIfNameEquals constructor
				 *	\param x The name of the element to remove
				 */
				RemoveIfNameEquals(const char  *x) : m_x(x) {}

				/*! \brief	The invocation operator, will be called for each record in the list
				 *	
				 *	The invocation operator, will be called for each record in the list
				 *	\param record The record to check
				 *	\return Whenever the PriorityRecord type cholds an object with specified name
				 */
				bool operator() (PriorityRecord<T> record)
				{
					return strcmp(((erf::BasicObject*) record.value)->GetName().c_str(), m_x) == 0;
				}

				const char * m_x; ///< The name of the object to remove
			};

			/*! \brief	Remove all records from the list with specified type info
			*	
			*	Remove all records from the list with specified type info
			*	\param classType The class type to remove
			*/
			void remove(const type_info& classType)
			{
				this->remove_if(RemoveIfTypeEquals(classType.name()));
			}

			/*! \brief	Remove all records from the list with specified class name
			*	
			*	Remove all records from the list with specified class name
			*	\param className The class name to remove
			*/
			void removeByClassName(const string& className)
			{
				this->remove_if(RemoveIfTypeEquals(className.c_str()));
			}

			/*! \brief	Remove all records from the list with specified object name (BasicObject.name)
			*	
			*	Remove all records from the list with specified object name (BasicObject.name)
			*	\param objectName The object name to remove
			*/
			void removeByObjectName(const string& objectName)
			{
				this->remove_if(RemoveIfNameEquals(objectName.c_str()));
			}
			

			/*! \brief	Gets a list of items sorted by their priority (ascending)
			*	
			*	Gets a list of items sorted by their priority (ascending)
			*	\return List sorted by priority (ascending)
			*/
			std::list<T> getPrioritizedList()
			{
				std::list<T> values;
				list::iterator iterator = this->begin();
				for (list::iterator end = this->end(); 
						iterator != end; ++iterator) {
					values.push_back((*iterator).value);
				}
				return values;
			}

			/*! \brief	Inserts into priority list if the element was not already present
			*	
			*	Inserts into priority list if the element was not already present
			*	\param val The value of the record
			*	\param priority Priority of the element
			*	\return Whenever the insertion was successful
			*/
			bool InsertIntoPriorityList(T val, int priority){
				if(this->contains(0,val)){
					return false;
				}
				this->insert(0,val); return true;
			}

	};
}  
