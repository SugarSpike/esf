#pragma once
#include "Common.h"

/*! \file ErfException.h
	\brief ErfException header file
*/

namespace erf  
{
	/*! \brief Default exception thrown by framework components
	*	Default exception thrown by framework components
	*/
    class ESFCORE_API ErfException 
    {
	protected:
		 string message; ///< Message of the exception

	public:
	
		/*! \brief Base constructor for ErfException
		*	
		*	Base constructor for ErfException
		*	\param exceptionMessage The exception message
		*/
		ErfException(const string& exceptionMessage) : message(exceptionMessage) { }
		virtual ~ErfException() { }

		/*! \brief Returns the exception message
		*	\return Message of the exception
		*/
		inline string Message() { return this->message; }
	};
}