#include "EecEvent.h"
#include <ctime>

namespace erf {

	EecEvent::EecEvent(int eventType): BasicObject("EecEvent") { 
		this->eventType = eventType;
		this->timeOfEvent = time(0);
	}

	EecEvent::EecEvent(int eventType, time_t timeOfEvent): BasicObject("EecEvent") { 
		this->eventType = eventType;
		this->timeOfEvent = timeOfEvent;
	}

	EecEvent::~EecEvent() { } 

	const int EecEvent::GetEventType()  { return this->eventType; }

	const time_t EecEvent::GetTimeOfEvent()  { return this->timeOfEvent; }

	ostream& operator<<(ostream& os, const EecEvent& eecEvent)
	{
		os << eecEvent.objectName << '::' << eecEvent.objectIdStr << '::' << eecEvent.eventType << "::" << eecEvent.timeOfEvent;
		return os;
	}

}