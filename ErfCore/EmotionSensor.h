#pragma once
#include "ExternalComponent.h"
/*! \file EmotionSensor.h
	\brief EmotionSensor header file

*/

//Forward declaration
namespace erf {
	class EmotionSensorReading;
}

namespace erf
{

	/*! \brief	Represents an external sensor that provides sensory data to the framework
	 *			
	 *	Emotion sensors are external components that were loaded through externall DLL libraries with the use of
	 *	ErfContext. Their role is to supply sensory data to other components within the framework (mainly models eg. EmotionModel, MoodModel).
	 *	The data that they provide is then used to compute new emotional states of the modeled character.
	 *
	 *	See ExternalComponent about more information about component states and parameters.
	 *	
	 */
    class ESFCORE_API EmotionSensor : public ExternalComponent
    {

	protected:
		vector<int> sensorTypes; ///< List of sensory data that the vector provides

	public:
	   /*!	\brief The base constructor for EmotionSensor
		*	
		*	The base constructor for EmotionSensor. It will register itself inside supplied configuration and unregister upon
		*	destruction. The component type will be set to ComponentType::SENSOR.
		*	\param configuration The configuration of this external component and the library it comes from
		*	\param sensorType The type of data the sensor supports, will be pushed to sensorTypes
		*/
		EmotionSensor(ExternalComponentConfiguration* configuration, SensorType sensorType);

		/*!	\brief The base copy constructor for EmotionSensor
		*	
		*	The base copy constructor for EmotionSensor
		*	\param other The original EmotionSensor
		*/
		EmotionSensor(const EmotionSensor &other);

		virtual ~EmotionSensor();

		/*!	\brief Return the data types the sensor support
		*	
		*	Returns a vector of all the data types the sensor supports (eg. pulse, skin conductance)
		*	\return Vector of supported data types
		*/
		inline vector<int>& GetSensorTypes() { return this->sensorTypes; }

		/*!	\brief Clones a sensor
		*	
		*	Clones a sensor. The default implementation simply calls ExternalComponentLibrary::CreateSensorForName
		*	with the name of the current sensor.
		*	\throws ErfException If cloning is not supported by ExternalComponentLibrary
		*	\return The clone of sensor
		*/
		virtual  EmotionSensor* CloneSensor();
		
		/*!	\brief Return the current sensor data
		*	
		*	Reads raw sensor data from sensors and packs them into the Erf interface with the use of EmotionSensorReading class;
		*	\throws ErfSensorReadException If fail to retrieve data, instead of throwing it can also return null
		*	\return The current sensor data, null if retrieval fails
		*/
		virtual EmotionSensorReading* GetSensorReading() = 0;

		/*!	\brief Return the specified number of sensor data reading
		*	
		*	Return the specified number of sensor data reading. The framework does not impose what those readings are.
		*	They can either be new reading or previous readings.
		*	\throws ErfSensorReadException If fail to retrieve data, instead of throwing it can also return null
		*	\param[out] container The destination vector to push the readings to
		*	\param[in] numberOfReadings Number of readings to retrieve
		*/
		virtual void GetSensorReadings(vector<EmotionSensorReading*>& container, int numberOfReadings) = 0;


		/*!	\brief Casts ExternalComponent pointer to EmotionSensor pointer
		*	
		*	Casts ExternalComponent pointer to EmotionSensor pointer. It is used in applications that use
		*	SWIG exported Erf. It enables the casts of higher language wrappers in a class hierarchy.
		*	\param base The component to cast
		*	\return The casted pointer, null if cast fails
		*/
		static EmotionSensor* CastToDerived(ExternalComponent* base) {
			return dynamic_cast<EmotionSensor*>(base);
		}

	};
}