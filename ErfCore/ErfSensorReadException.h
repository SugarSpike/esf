#pragma once
#include "ErfException.h"
#include <string>

/*! \file ErfSensorReadException.h
	\brief ErfSensorReadException header file
*/

namespace erf  
{
	/*! \brief Exception thrown by framework emotion sensor
	*
	*	Exception thrown by framework emotion sensor, should be thrown if data could not be read by sensor
	*/
    class ESFCORE_API ErfSensorReadException : public ErfException
    {
	public:
		/*! \brief Base constructor for ErfSensorReadException
		*	
		*	Base constructor for ErfSensorReadException
		*	\param exceptionMessage The exception message
		*/
		ErfSensorReadException(const string& exceptionMessage) : ErfException(exceptionMessage) { }
		virtual ~ErfSensorReadException() { }
	};
}