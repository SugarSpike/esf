#pragma once
#include "ErfException.h"
#include <string>

/*! \file ErfExternalComponentException.h
	\brief ErfExternalComponentException header file
*/

namespace erf  
{
	/*! \brief Exception thrown by framework external components
	*	
	*	Exception thrown by framework external components
	*/
    class ESFCORE_API ErfExternalComponentException : public ErfException
    {
	public:
		/*! \brief Base constructor for ErfExternalComponentException
		*	
		*	Base constructor for ErfExternalComponentException
		*	\param exceptionMessage The exception message
		*/
		ErfExternalComponentException(const string& exceptionMessage) : ErfException(exceptionMessage) { }
		virtual ~ErfExternalComponentException() { }
	};
}