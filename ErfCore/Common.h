#pragma once
/*! \file Common.h
	\brief Contains common to all files includes and preprocessors definitions

*/


#pragma warning( disable: 4251 )
#pragma warning( disable: 4482 )
/*! \brief Defines BOOST_NO_CXX11_NULLPTR preprocessor definition, see boost docs for details
 *
 *	Defines BOOST_NO_CXX11_NULLPTR preprocessor definition, see boost docs for details
 */
#define BOOST_NO_CXX11_NULLPTR

/*! \brief The export/import DLL flag for framework
 *
 *	The export/import DLL flag for framework
 */
#ifdef ESFCORE_EXPORTS
    #define ESFCORE_API __declspec(dllexport)
#else
    #define ESFCORE_API __declspec(dllimport)
#endif

#include <map>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

/*! \brief Defines null preprocessor definition, null equals 0
 *
 *	Defines null preprocessor definition, null equals 0
 */
#define null 0
/*! \brief Defines NULL preprocessor definition, NULL equals 0
 *
 *	Defines NULL preprocessor definition, NULL equals 0
 */
#define NULL 0

/*! \brief Emotion Recognition Framework namespace
 *
 * The Emotion Recognition Framework (aka "Erf") namespace contains all core elements of the framework
 */
namespace erf { }

/*! \brief Emotion Recognition Framework Utility namespace
 *
 * The Emotion Recognition Framework Utility contains definitions for helper and utility classes/methods
 */
namespace erfutils { }
