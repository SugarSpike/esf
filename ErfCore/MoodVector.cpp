#include "MoodVector.h"
#include "Variant.h"
#include <ctime>

namespace erf {

	MoodVector::MoodVector(int moodModelType) :
		BasicObject("MoodVector"), EmotionVector(moodModelType,time(0))
	{
	}

		MoodVector::MoodVector(int moodModelType,time_t timeOfEvaluation) :
		BasicObject("MoodVector"), EmotionVector(moodModelType,timeOfEvaluation)
	{
	}

		MoodVector::MoodVector(vector<int>& emotionModelTypes,time_t timeOfEvaluation) :
		BasicObject("MoodVector"), EmotionVector(emotionModelTypes,timeOfEvaluation)
	{
	}

	MoodVector* MoodVector::Clone() {
		MoodVector* clone = new MoodVector(this->GetEmotionModelTypes(),this->timeOfEvaluation);
		clone->SetUniformType(this->GetUniformType());
		this->CopyInto(*clone);
		return clone;
	}

}