#include "ErfUtils.h"
#include <boost\filesystem\path.hpp>
#include <strsafe.h>
#include "ErfLogging.h"

namespace erfutils
{
	std::string GetLastErrorStdString()
	{
	   LPVOID lpMsgBuf;
		LPVOID lpDisplayBuf;
		DWORD dw = GetLastError(); 

		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			dw,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR) &lpMsgBuf,
			0, NULL );

		// Display the error message and exit the process

		lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, (lstrlen((LPCTSTR)lpMsgBuf)+40) * sizeof(TCHAR)); 
		StringCchPrintf((LPTSTR)lpDisplayBuf, 
			LocalSize(lpDisplayBuf) / sizeof(TCHAR),
			TEXT("failed with error %d: %s"), dw, lpMsgBuf); 
		
		string ret;

		#ifdef UNICODE
			 std::wstring w;
			 w = (LPCTSTR)lpDisplayBuf;
			 ret = std::string(w.begin(), w.end()); // magic here
		#else
			 ret = (LPCTSTR)lpDisplayBuf;
		#endif
		LocalFree(lpMsgBuf);
		LocalFree(lpDisplayBuf);
		return ret;
	}


	bool FileExists (const std::string& name) {
		if (FILE *file = fopen(name.c_str(), "r")) {
			fclose(file);
			return true;
		} else {
			return false;
		}   
	}

	LPCWSTR GetWStr(string& str)
	{
		std::wstring stemp = std::wstring(str.begin(), str.end());
		return stemp.c_str();
	}

	std::wstring GeWCStr(const std::string& s)
	{
		int len;
		int slength = (int)s.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0); 
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
		std::wstring r(buf);
		delete[] buf;
		return r;
	}


	string GetCurrentExecutingDirectory()
	{
		char path[MAX_PATH];
		HMODULE hm = NULL;

		if (!GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | 
				GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
				(LPCSTR) &GetCurrentExecutingDirectory, 
				&hm))
		{
			int ret = GetLastError();
			LOG(error) << "ErfUtils::GetCurrentDirectory returned error: " << ret;
		}
		GetModuleFileNameA(hm, path, sizeof(path));
		boost::filesystem::path p(path);
		boost::filesystem::path dir = p.parent_path();
	
		return dir.normalize().string();
	}
}