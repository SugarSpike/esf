#include "WeightedMeanDoubleVectorEvaluator.h"
#include "EmotionVector.h"
#include "ErfForEach.h"
#include "Variant.h"


namespace erf {

	WeightedMeanDoubleVectorEvaluator::WeightedMeanDoubleVectorEvaluator(CharacterModel& model) :
		BasicObject("WeightedMeanDoubleVectorEvaluator"), WeightedMeanVectorEvaluator(model)
	{

	}

	void WeightedMeanDoubleVectorEvaluator::ValidateVectors(vector<EmotionVector*>& vectors,vector<int>& highestRankModelType, bool highestIsUniform) { }

	EmotionVector* WeightedMeanDoubleVectorEvaluator::AggregateVectors(vector<EmotionVector*>& vectors) {

		EmotionVector* aggregatedVector = new EmotionVector(vectors[0]->GetEmotionModelTypes());
		map<string,Variant*>& parametersMapOfFirst =  vectors[0]->GetValues();
		for (auto parameterIterator = parametersMapOfFirst.begin() ; parameterIterator != parametersMapOfFirst.end(); ++parameterIterator)
		{
			string emotionName = parameterIterator->first;
			double averageDivisor = 0.0f;
			double averageDividend = 0.0f;
			double temporary = 0.0f;

			for_each_erf(EmotionVector* vector, vectors)
			{
				temporary = (vector->GetCertainty(emotionName)+vector->GetModelRank());
				averageDividend += temporary * vector->GetValue(emotionName)->AsDouble();
				averageDivisor += temporary;
			}

			aggregatedVector->AddValue(parameterIterator->first,Variant::Create(averageDividend/averageDivisor));
		}
		return aggregatedVector;
	}

}