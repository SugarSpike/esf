#pragma once
#include "ExternalExpert.h"
#include "ErfLogging.h"
#include "ErfException.h"
#include "EmotionModel.h"
#include "MoodModel.h"
#include "PsychologicalModel.h"
#include "ExternalComponentConfiguration.h"
#include "ExternalComponentLibrary.h"

namespace erf {

	ExternalExpert::ExternalExpert(ExternalComponentConfiguration* pConfiguration, ExpertType expertType): 
			BasicObject("ExternalExpert"), ExternalComponent(pConfiguration, ComponentType::EXPERT),
			expertType(expertType)
	{
	}

	ExternalExpert::ExternalExpert(const ExternalExpert &other): 
		BasicObject(other.objectName), ExternalComponent( other.configuration, ComponentType::EXPERT),
		expertType(other.expertType)
	{
		throw new ErfException("ExternalExpert::ExternalExpert(const ExternalExpert &other) called");
	}

	ExternalExpert* ExternalExpert::CloneExpert()
	{
		return this->configuration->GetLibraryComponent()->CreateExpertForName(this->GetName());
	}

	ExternalExpert::~ExternalExpert() { }

	EmotionModel* ExternalExpert::CastToEmotionModel(ExternalExpert* base) {
				return dynamic_cast<EmotionModel*>(base);
	}

	MoodModel* ExternalExpert::CastToMoodModel(ExternalExpert* base) {
		return dynamic_cast<MoodModel*>(base);
	}

	PsychologicalModel* ExternalExpert::CastToPsychologicalModel(ExternalExpert* base){
		return dynamic_cast<PsychologicalModel*>(base);
	}

}