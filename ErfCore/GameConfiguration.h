#pragma once
#include "BasicObject.h"
#include "GameEnums.h"

/*! \file GameConfiguration.h
	\brief GameConfiguration header file

*/

namespace erf
{

	/*! \brief	The game configuration - containing base information about the game
	 *			
	 *	The game configuration - containing base information about the game. Containing such information as:
	 *	- The name of the game
	 *	- The type of the game
	 *	- Challenges that can be found in the game
	 *	- Characteristics (keywords) of the game
	 */
    class ESFCORE_API GameConfiguration : public BasicObject
    {
		private:
			string gameName; ///< Name of the game
			GameType gameType; ///< Type of the game
			vector< GameChallenges > gameChallenges; ///< Challenges that can be found in the game
			vector< GameCharateristics > gameCharacteristics; ///< Characteristics/keywords associated with the game
		public:
			/*! \brief	Base constructor
			 *		
			 *	Base constructor.
			 *
			 */
			GameConfiguration();
			/*! \brief	Base copy constructor
			 *		
			 *	Base copy constructor.
			 *
			 *	\param other the configuration to copy from
			 */
			GameConfiguration(const GameConfiguration &other);
			/*! \brief	Base constructor
			 *		
			 *	Base constructor.
			 *	\param name the name of the game
			 *	\param gameType the type of the game
			 */
			GameConfiguration(string name, GameType gameType);
			virtual ~GameConfiguration();

			/*! \brief	Returns the type of the game
			 *		
			 *	Returns the type of the game
			 *	\return Type of the game
			 */
			GameType GetGameType();
			/*! \brief	Sets the type of the game
			 *		
			 *	Sets the type of the game
			 *	\param gameType New game type.
			 */
			void SetGameType(GameType gameType);

			/*! \brief	 Adds specified GameCharateristics to the configuration
			 *		
			 *	Adds specified GameCharateristics to the configuration
			 *	\param gameCharacteristic GameCharateristics to add
			 */
			void AddGameCharacteristic(GameCharateristics gameCharacteristic);
			/*! \brief	 Removes specified GameCharateristics to the configuration
			 *		
			 *	Removes specified GameCharateristics to the configuration
			 *	\param gameCharacteristic GameCharateristics to remove
			 */
			void RemoveGameCharacteristic(GameCharateristics gameCharacteristic);
			/*! \brief Returns the characteristics/keywords for the game
			 *		
			 *	Returns the characteristics/keywords for the game
			 *	\return Vector of the game characteristics
			 */
			vector< GameCharateristics >& GetGameCharacteristics();

			/*! \brief	 Adds specified GameChallenge to the configuration
			 *		
			 *	Adds specified GameChallenge to the configuration
			 *	\param gameChallenge GameChallenge to add
			 */
			void AddGameChallenge(GameChallenges gameChallenge);
			/*! \brief	 Removes specified GameChallenge to the configuration
			 *		
			 *	Removes specified GameChallenge to the configuration
			 *	\param gameChallenge GameChallenge to remove
			 */
			void RemoveGameChallenge(GameChallenges gameChallenge);
			/*! \brief Returns the challenges for the game
			 *		
			 *	Returns the challenges for the game
			 *	\return Vector of the challenges
			 */
			vector< GameChallenges >& GetGameChallenges();

			/*! \brief	Sets the name of the game
			 *		
			 *	Sets the name of the game
			 *	\param name New game name.
			 */
			void SetGameName(string name);
			
			/*! \brief	Returns the name of the game
			 *		
			 *	Returns the name of the game	
			 *	\return Name of the game
			 */
			string GetGameName();

			/*! \brief	The assignment operator
			 *		
			 *	The assignment operator
			 *	\param rhs The configuration to assign from
			 */
			GameConfiguration& operator= (const GameConfiguration &rhs);
	};
}