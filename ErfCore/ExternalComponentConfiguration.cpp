#pragma once
#include "ExternalComponentConfiguration.h"
#include "ExternalComponentLibrary.h"
#include <Windows.h>
#include "ErfUtils.h"

namespace erf {

	ExternalComponentConfiguration::ExternalComponentConfiguration() 
		: BasicObject("ExternalComponentConfiguration")
	{
	}

	ExternalComponentConfiguration::~ExternalComponentConfiguration() {
	}

	ExternalComponentLibrary* ExternalComponentConfiguration::GetLibraryComponent(){
		return this->libraryComponent;
	}

	vector<ExternalComponent*>& ExternalComponentConfiguration::GetComponents(){
		return this->components;
	}

}