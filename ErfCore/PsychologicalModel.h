#pragma once
#include "BasicObject.h"

/*! \file PsychologicalModel.h
	\brief PsychologicalModel header file
*/

//=================================
// forward declared dependencies
namespace erf
{
	class MoodVector;
	class Personality;
	class CharacterData;
	class EecEvent;
}
using namespace std;

namespace erf
{

	/*! \brief	Psychological model computes changes in character personality based on the data supplied by Erf Framework
	 *			
	 *	The PsychologicalModel is one of the core components of the framework. It is used to compute changes in Personality based on the data supplied by Erf Framework.
	 *	The data includes (but is not limited to):
	 *		- Both current and previous EmotionVector%s and MoodVector%s
	 *		- Information from the game itself (passed throu characterData)
	 *		- Sensory data supplied by external sensors (passed throu CharacterData)
	 *		- Information about the previous personality of the character
	 *		- Other user data passed by CharacterData
	 *
	 */
    class ESFCORE_API PsychologicalModel : public virtual BasicObject
    {
	public:
		/*! \brief	Returns the model rank
		 *			
		 *	Return the model rank. The rank is an arbitrary number of range <0.0f,1.0f> supplied by the creator of the PsychologicalModel.
		 *	The rank should reflect the overall correctivnes of the algoritm for computing personality changes that this model implements.
		 *
		 *	\return the model rank
		 */
		virtual float GetModelRank() = 0;

		/*! \brief	Initialize the model for given CharacterModel/CharacterData
		 *			
		 *	The method is called upon registering the model to a certain CharacterModel. All character dependend initialization
		 *	should be done in this function (like setting initial personality data in CharacteData).
		 *
		 *	\param characterData the character data for initialization
		 */
		virtual void InitializeForCharacter(CharacterData& characterData) = 0;

		/*! \brief	Updates the character Personality
		 *			
		 *	Updates the Personality that can be found in CharacterData.
		 *	\param event Emotion eliciting condition event in response to which the new emotion vector is computed
		 *	\param characterData Holds information about the character (see CharacterData for more information)
		 */
		virtual void UpdatePersonality(EecEvent& event, CharacterData& characterData) = 0;

	};
}