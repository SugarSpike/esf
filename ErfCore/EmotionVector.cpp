#include "EmotionVector.h"
#include "Variant.h"
#include "ErfUtils.h"
#include <ctime>

namespace erf {

	EmotionVector::EmotionVector(int emotionModelType) :
		BasicObject("EmotionVector"), uniformType(false)
	{
		this->emotionModelTypes.push_back(emotionModelType); 
		this->timeOfEvaluation = time(0);
	}

	EmotionVector::EmotionVector(int emotionModelType,time_t timeOfEvaluation) :
		BasicObject("EmotionVector"), uniformType(false)
	{
		this->emotionModelTypes.push_back(emotionModelType);
		this->timeOfEvaluation = timeOfEvaluation;
	}

	EmotionVector::EmotionVector(vector<int>& emotionModelTypes,time_t timeOfEvaluation) :
		BasicObject("EmotionVector"), uniformType(false)
	{
		this->emotionModelTypes = emotionModelTypes;
		this->timeOfEvaluation = timeOfEvaluation;
	}

	EmotionVector::EmotionVector(vector<int>& emotionModelTypes) :
		BasicObject("EmotionVector"), uniformType(false)
	{
		this->emotionModelTypes = emotionModelTypes;
		this->timeOfEvaluation = time(0);
	}

	void EmotionVector::AddEmotionModelType(int type) { 
		this->emotionModelTypes.push_back(type);
	}

	vector<int>& EmotionVector::GetEmotionModelTypes() { return this->emotionModelTypes; }
	
	time_t EmotionVector::GetTimeOfEvaluation() { return this->timeOfEvaluation; }
	
	void EmotionVector::SetTimeOfEvaluation(time_t newTime) { this->timeOfEvaluation = newTime; }

	float EmotionVector::GetCertainty(string name) { return this->emotionCertainties[this->nameToIndex[name]]; }

	float EmotionVector::GetCetainity(int index) { return this->emotionCertainties[index]; }

	vector<float>& EmotionVector::GetCertainties() { return this->emotionCertainties; }

	bool EmotionVector::IsOfEmotionModelType(int modelType) {
		return erfutils::Contains(this->emotionModelTypes,modelType);
	}

	void EmotionVector::AddValue(string name, Variant* data) {
		this->AddValue(name,data,0.0f);
	}

	void EmotionVector::AddValue(string name, Variant* data, float certainty) {
		auto it = this->nameToValue.find(name);
		if(it != this->nameToValue.end()) {
		   this->values[this->nameToIndex[name]] = data;
		   this->emotionCertainties[this->nameToIndex[name]] = certainty;
		   delete it->second;
		   this->nameToValue[name] = data;
		}
		else {
			this->values.push_back(data);
			this->emotionCertainties.push_back(certainty);
			this->nameToIndex[name] = this->values.size()-1;
			this->indexToName[this->values.size()-1] = name;
			this->nameToValue[name] = data;
		}
	}

	
	bool EmotionVector::GetUniformType() { 
		return this->uniformType; 
	}

	void EmotionVector::SetUniformType(bool newFlag) {
		this->uniformType = newFlag;
	}

	void EmotionVector::SetModelRank(float rank) {
		this->modelRank = rank;
	}

	float EmotionVector::GetModelRank() {
		return this->modelRank;
	}

	EmotionVector* EmotionVector::Clone() {
		EmotionVector* clone = new EmotionVector(this->GetEmotionModelTypes(),this->timeOfEvaluation);
		clone->SetUniformType(this->GetUniformType());
		this->CopyInto(*clone);
		clone->emotionCertainties = this->emotionCertainties;
		return clone;
	}

}