#pragma once;

/*! \file CharacterModelEnums.h
	\brief Contains enum declarations for Character Model Module

*/

namespace erf {
	/*! \brief	Types of supported modes of evaluation of emotion models vectors
	 *			
	 *	Each registered emotion/mood models generates an vector. The ModelsEvaluationMode
	 *	field tells us how to merge them into a single vector. Currently these modes are
	 *	available:
	 */
	enum ModelsEvaluationMode {
		STD_BASIC = 0, /**< Default mode. Computation of the first model that can handle a given model will be returned. Recommended for NPCs or configurations that do not use multiple models of the same time (emotion,mood)*/
		STD_STANDARD, /**< The value of emotion is selected from expert with greatest rank and certainty */
		STD_WEIGHTED_AVERAGE /**< Each emotion is a weighted average of all vectors (where the weight  is composed of expert ranking and its certainty of that specific emotion) */
	};

}