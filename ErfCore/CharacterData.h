#pragma once
#include "VariantCollection.h"
#include <memory>

/*! \file CharacterData.h
	\brief Character data header file

*/

//=================================
// forward declared dependencies
namespace erf
{
	class EmotionSensor;
	class EmotionVector;
	class MoodVector;
	class Personality;
	class GameConfiguration;
}

namespace boost {
	/*! \brief Forward declaration of boost::circular_buffer
	 *
	 */
	template <class T, class Alloc = std::allocator<T> >
	class circular_buffer;
}


namespace erf
{
   /*! \brief The character data model, containing data associated with a modeled character.
	*	
	*	The characte data model, containing data associated with a modeled character:
	*		- previous and current computed EmotionVector%s and MoodVector%s
	*		- GameConfiguration object
	*		- Personality object - is a variant collection
	*		- Registered sensors for this model
	*
	*	This object, as stated before, acts as a container for data associated with this
	*	particular character, this object will be passed to most component when their
	*	methods are invoked (experts computing vectors, game registering components etc.)
	*	
	*
	*/
    class ESFCORE_API CharacterData : public VariantCollection
    {
		friend class CharacterModel; ///< CharacterModel is the owner of the data
		friend class VectorEvaluator; ///< Vector evaluators save their output in the CharacterData

	private: 
		unsigned int emotionVectorBufferSize; ///< Size of the cyclic buffer for previous EmotionVector%s
		unsigned int unfilteredEmotionVectorBufferSize; ///< Size of the cyclic buffer for previous EmotionVector%s before EmotionVectorFilterType::After were applied
		unsigned int moodVectorBufferSize; ///< Size of the cyclic buffer for previous MoodVectors%s
		
		GameConfiguration* gameConfiguration; ///< Contains basic data about the game (genre, challenges etc.)
		bool isGameConfigurationOwner; ///< Whenever the CharacterData is the owner of the configuration (should delete it on destruction)
		Personality* personality; ///< The personality of the character, another VariantCollection to store data about traits, PsychologicalModel output is saved here
		bool isPersonalityOwner; ///< Whenever the CharacterData is the owner of the personality (should delete it on destruction)

		vector<EmotionSensor* > sensors; ///< Registered external sensors, stored in CharacterData in order to easly pass them to other components (experts, models etc.)

		EmotionVector* currentEmotionVector; ///< The current EmotionVector after all applicable EmotionVectorFilters were applied
		boost::circular_buffer<std::shared_ptr<EmotionVector> >* previousEmotionVectors; ///< Circular buffer of size emotionVectorBufferSize of previous EmotionVector%s

		EmotionVector* currentUnfilteredEmotionVector;  ///< The current EmotionVector before EmotionVectorFilterType::After were applied
		boost::circular_buffer<std::shared_ptr<EmotionVector> >* previousUnfilteredEmotionVectors;  ///< Circular buffer of size unfilteredEmotionVectorBufferSize of previous unfiltered EmotionVector%s

		MoodVector* currentMoodVector; ///< The current MoodVector 
		boost::circular_buffer<std::shared_ptr<MoodVector> >* previousMoodVectors; ///< Circular buffer of size moodVectorBufferSize of previous MoodVector%s

		/*! \brief Unregisters a sensor from the CharacterData/CharacterModel
		 *  
		 *	Unregisters a sensor from the CharacterData/CharacterModel, the sensor will no longer be visible to other components
		 *	
		 *	\param sensor to unregister
		 *	\return true if the sensor was unregistered, false otherwise
		 */
		bool UnregisterSensor(EmotionSensor* sensor);

		/*! \brief Unregisters a sensor from the CharacterData/CharacterModel
		 *  
		 *	Unregisters a sensor from the CharacterData/CharacterModel by name, the sensor will no longer be visible to other components
		 *	
		 *	\param sensorName name of the sensor to unregister
		 *	\return true if the sensor was unregistered, false otherwise
		 */
		bool UnregisterSensorByName(string sensorName);

		/*! \brief Sets the current emotion vector
		 *  
		 *	Sets the current emotion vector. Previous emotion vector will be automaticly pushed to previous vectors.
		 *	
		 *	\param vector the new emotion vector
		 */
		void SetCurrentEmotionVector(EmotionVector* vector);

		/*! \brief Sets the current unfiltered emotion vector
		 *  
		 *	Sets the current unfiltered emotion vector. Previous emotion vector will be automaticly pushed to previous vectors.
		 *	
		 *	\param vector the new emotion vector
		 */
		void SetCurrentUnfilteredEmotionVector(EmotionVector* vector);

		/*! \brief Sets the current mood emotion vector
		 *  
		 *	Sets the current unfiltered mood vector. Previous mood vector will be automaticly pushed to previous vectors.
		 *	
		 *	\param vector the new mood vector
		 */
		void SetCurrentMoodVector(MoodVector* vector);

		/*! \brief Registers a sensor from the CharacterData/CharacterModel
		 *  
		 *	Registers a sensor for the CharacterData/CharacterModel, the sensor will afterwards be visible to other components
		 *	
		 *	\param sensor to register
		 *	\return true if the sensor was registered, false otherwise
		 */
		bool RegisterSensor(EmotionSensor* sensor);
	public:
		/*! \brief The default constructor
		 *	The default constructor of the CharacterData
		 *
		 */
		CharacterData();

		~CharacterData();

		//Configuration
		/*! \brief Returns the current size of the circular buffer of previous EmotionVector%s
		 *	
		 *	\return current size of the buffer
		 */
		unsigned int GetEmotionVectorBufferSize();
		/*! \brief Returns the current size of the circular buffer of previous unfiltered EmotionVector%s
		 *	
		 *	\return current size of the buffer
		 */
		unsigned int GetUnfilteredEmotionVectorBufferSize();
		/*! \brief Returns the current size of the circular buffer of previous MoodVector%s
		 *	
		 *	\return current size of the buffer
		 */
		unsigned int GetMoodVectorBufferSize();
		
		/*! \brief Sets the new size of the circular buffer of previous EmotionVector%s
		 *	
		 *	Sets the new size of the circular buffer of previous EmotionVector%s. The overflow will be deleted.
		 *	The minimum size is 1.
		 *
		 *	\param newSize the new size of the buffer
		 */
		void SetEmotionVectorBufferSize(unsigned int newSize);
		/*! \brief Sets the new size of the circular buffer of previous unfiltered EmotionVector%s
		 *	
		 *	Sets the new size of the circular buffer of previous unfiltered EmotionVector%s. The overflow will be deleted.
		 *	The minimum size is 1.
		 *
		 *	\param newSize the new size of the buffer
		 */
		void SetUnfilteredEmotionVectorBufferSize(unsigned int newSize);
		/*! \brief Sets the new size of the circular buffer of previous  MoodVector%s
		 *	
		 *	Sets the new size of the circular buffer of previous MoodVector%s. The overflow will be deleted.
		 *	The minimum size is 1.
		 *
		 *	\param newSize the new size of the buffer
		 */
		void SetMoodVectorBufferSize(unsigned int newSize);

		//Emotion and Mood Vectors
		/*! \brief Returns the current EmotionVector
		 *	
		 *	Returns the current EmotionVector after all applicable EmotionVectorFilters were applied
		 *	\return Current emotion vector
		 */
		EmotionVector* GetCurrentEmotionVector();

		/*! \brief Returns the previous EmotionVector
		 *	
		 *	Returns the previous EmotionVector if there is any
		 *	\return Previous emotion vector
		 */
		EmotionVector* GetPreviousEmotionVector();

		/*! \brief Returns the specified number of previous EmotionVector
		 *	
		 *	Returns the specified number of previous EmotionVector. If the number of vectors to retrieve surpass
		 *	the number of saved previous vectors then all vectors are returned.
		 *	\param[out] Destination the destination vector into which to push the pervious EmotionVector%s
		 *	\param previousVectorsCount Number of vectors to retrieve
		 */
		void GetPreviousEmotionVectors(vector<EmotionVector* >& destination, int previousVectorsCount);

		/*! \brief Returns the current unfiltered EmotionVector
		 *	
		 *	Returns the current EmotionVector before filters of type EmotionVectorFilterType::After were applied
		 *	\return current unfiltered emotion vector
		 */
		EmotionVector* GetCurrentUnfilteredEmotionVector();

		/*! \brief Returns the previous unfiltered emotion vector
		 *	
		 *	Returns the previous emotion vector before filters of type EmotionVectorFilterType::After were applied
		 *	\return previous unfiltered emotion vector
		 */
		EmotionVector* GetPreviousUnfilteredEmotionVector();

		/*! \brief Returns the specified number of previous unfiltered EmotionVector
		 *	
		 *	Returns the specified number of previous unfiltered EmotionVector. If the number of vectors to retrieve surpass
		 *	the number of saved previous vectors then all vectors are returned.
		 *	\param[out] destination the destination vector into which to push the pervious unfiltered EmotionVector%s
		 *	\param previousVectorsCount number of vectors to retrieve
		 */
		void GetPreviousUnfilteredEmotionVectors(vector<EmotionVector* >& destination,int previousVectorsCount);
		
		/*! \brief Returns the current MoodVector
		 *  
		 *	Returns the current MoodVector
		 *	\return current mood vector
		 */
		MoodVector* GetCurrentMoodVector();

		/*! \brief Returns the previous MoodVector
		 *  
		 *	Returns the previous MoodVector
		 *	\return previous mood vector
		 */
		MoodVector* GetPreviousMoodVector();

		/*! \brief Returns the specified number of previous MoodVector%s
		 *	
		 *	Returns the specified number of previous MoodVector. If the number of vectors to retrieve surpass
		 *	the number of saved previous vectors then all vectors are returned.
		 *	\param[out] destination the destination vector into which to push the pervious MoodVector%s
		 *	\param previousVectorsCount number of vectors to retrieve
		 */
		void GetPreviousMoodVectors(vector<MoodVector* >& destination,int previousVectorsCount);


		//Personality
		/*! \brief Sets the character personality
		 *  
		 *	Sets the current personality of the character. The CharacterData will become the owner of the personality
		 *	deleteing the configuration instance on destruction
		 *	
		 *	\param personality the new personality
		 */
		void SetPersonality(Personality* personality);

			/*! \brief Sets the character personality
		 *  
		 *	Sets the current personality of the character. 
		 *	
		 *	\param isMemoryOwner Whenever the CharacterData is the owner of the pointer
		 *	\param personality the new personality
		 */
		void SetPersonality(Personality* personality, bool isMemoryOwner);


		/*! \brief Returns the current Personality
		 *	
		 *	Returns the current Personality
		 *	\return current personality
		 */
		Personality& GetPersonality();

		//Game Configuration
		/*! \brief Sets the character basic game information
		 *
		 *	Sets the basic game information for the character. The CharacterData will become the owner of the configuration
		 *	deleteing the configuration instance on destruction
		 *	
		 *	\param configuration the new game information configuration
		 */
		void SetGameConfiguration(GameConfiguration* configuration);


		/*! \brief Sets the character basic game information
		 *
		 *
		 *	\param isMemoryOwner Whenever the CharacterData is the owner of the pointer
		 *	\param configuration the new game information configuration
		 */
		void SetGameConfiguration(GameConfiguration* configuration,bool isMemoryOwner);

		/*! \brief Returns basic game information (GameConfiguration)
		 *	
		 *	Returns basic game information (GameConfiguration)
		 *	\return basic GameConfiguration data
		 */
		GameConfiguration& GetGameConfiguration();

		//Sensors
		/*! \brief Returns all registered sensors for this character
		 *	
		 *	Returns all registered sensors for this character
		 *	\return vector of registered sensors
		 */
		const vector<EmotionSensor*>& GetSensors();
		
		/*! \brief Finds all sensors of given SensorType (cast to int)
		 *	
		 *	Finds all sensors of given SensorType (cast to int)
		 *	\param[out] destination the destination vector into which to push the matching sensors
		 *	\param sensorType type of sensor to search for
		 */
		void GetSensorsOfType(vector<EmotionSensor*>& destination, int sensorType);

		/*! \brief Finds the first sensor of given SensorType (cast to int)
		 *	
		 *	Finds the first sensor of given SensorType (cast to int)
		 *	\param sensorType type of sensor to search for
		 *	\return the sensor of given type, null if none found
		 */
		EmotionSensor* GetFirstSensorOfType(int sensorType);

		/*! \brief Finds the first sensor with given name
		 *	
		 *	Finds the first sensor with given name
		 *	\param sensorName name of sensor to search for
		 *	\return the sensor with given name, null if none found
		 */
		EmotionSensor* GetSensorByName(string sensorName);

	};
}