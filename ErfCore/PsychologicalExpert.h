#pragma once
#include "PsychologicalModel.h"
#include "ExternalExpert.h"

/*! \file PsychologicalExpert.h
	\brief PsychologicalExpert header file

*/

namespace erf
{
	/*! \brief	Represents an external expert that acts as an PsychologicalModel
	 *			
	 *	This class is a specialization of ExternalExpert.
	 *	Represents an external expert that was dynamically loaded into the library and acts as a PsychologicalModel.
	 *	See ExternalComponent about more information about component states and parameters.
	 *	
	 */
    class ESFCORE_API PsychologicalExpert : public ExternalExpert, public PsychologicalModel
    {
		private:
		public:
			/*!	\brief The base constructor for PsychologicalExpert
			 *	
			 *	The base constructor for PsychologicalExpert. It will register itself inside supplied configuration and unregister upon
			 *	destruction. The component type will be set to ComponentType::EXPERT and the expert type will be set to STD_PSYCHOLOGICAL.
			 *	The expert will be recognized as an PsychologicalModel in the framework.
			 *	\param configuration The configuration of this external component and the library it comes from
			 */
			PsychologicalExpert(ExternalComponentConfiguration* configuration);

			/*!	\brief The base copy constructor for PsychologicalExpert
			 *	
			 *	The base copy constructor for PsychologicalExpert
			 *	\param other The original PsychologicalExpert
			 */
			PsychologicalExpert(const PsychologicalExpert &other);
			virtual ~PsychologicalExpert();

	};
}