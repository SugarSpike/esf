#pragma once
#include "BasicObject.h"
#include <vector>

/*! \file ErfContext.h
	\brief ErfContext header file
*/
//=================================
// forward declared dependencies
namespace erf
{
	class EmotionSensor;
	class ExternalExpert;
	class ExternalFilter;
	class ExternalComponentLibrary;
}

namespace erf
{
	/*! \brief	The singleton context of Erf
	 *			
	 *	The singleton context of Erf. The context contains all external components (see ExternalComponent and ExternalComponentLibrary
	 *	for more details). It is responsible for loading and unloading all such external components.
	 *
	 */
    class ESFCORE_API ErfContext : public BasicObject
    {
	private:

		vector<ExternalFilter*> registeredFilters; ///< All currently loaded ExternalFitlers from external library 
		vector<EmotionSensor*> registeredSensors; ///< All currently loaded EmotionSensor from external library
		vector<ExternalExpert*> registeredExperts; ///< All currently loaded ExternalExpert from external library
		vector<ExternalComponentLibrary*> registeredExternalComponentLibrary; ///< All currently loaded ExternalComponentLibrary

		/*! \brief The default constructor of ErfContext
		 *	
		 *	The default constructor of ErfContext - the class is a singleton and as such the constructor is private
		 *
		 */
		ErfContext();
		/*! \brief The default copy constructor of ErfContext
		 *	
		 *	The copy constructor of ErfContext - the class is a singleton and as such the constructor is private
		 *
		 */
		ErfContext(ErfContext const&) {}
		/*! \brief The assign operator of ErfContext
		 *	
		 *	The assign operator of ErfContext - the class is a singleton and as such the operator is private
		 *
		 */
		void operator=(ErfContext const&) {}
		

	public:
		
		virtual ~ErfContext();
		
		/*! \brief Returns the current instance of ErfContext
		 *	
		 *	Returns the current instance of ErfContext
		 *	\return Current ErfContext
		 */
		static ErfContext& GetInstance()
        {
            static ErfContext instance; ///< The static/singleton instance of ErfContext
            return instance;
        }
		
		/*! \brief Returns all currently loaded ExternalFitler's in the context
		 *	
		 *	Returns all currently loaded ExternalFitler's in the context
		 *	\return All currently loaded ExternalFitler's
		 */
		vector<ExternalFilter*>& GetRegisteredFilters();
		/*! \brief Returns all currently loaded EmotionSensor's in the context
		 *
		 *	Returns all currently loaded EmotionSensor's in the context
		 *	\return All currently loaded EmotionSensor's
		 */
		vector<EmotionSensor*>& GetRegisteredSensors();
		/*! \brief Returns all currently loaded ExternalExpert's in the context
		 *
		 *	Returns all currently loaded ExternalExpert's in the context
		 *	\return All currently loaded ExternalExpert's
		 */
		vector<ExternalExpert*>& GetRegisteredExperts();
		/*! \brief Returns all currently loaded ExternalComponentLibrary's in the context
		 *
		 *	Returns all currently loaded ExternalComponentLibrary's in the context
		 *	\return All currently loaded ExternalComponentLibrary's
		 */
		vector<ExternalComponentLibrary*>& GetRegisteredLibraries();
		
		
		/*! \brief Returns the first ExternalFilter with specified name (BasicObject.name field is compared)
		 *	
		 *	Returns the first ExternalFilter with specified name (BasicObject.name field is compared)
		 *	\param filterName The name of ExternalFilter to search for
		 *	\return The first ExternalFilter with specified name, null if none was found
		 */
		ExternalFilter* FindFilter(string filterName);
		/*! \brief Returns the first ExternalExpert with specified name (BasicObject.name field is compared)
		 *	
		 *	Returns the first ExternalExpert with specified name (BasicObject.name field is compared)
		 *	\param expertName The name of ExternalExpert to search for
		 *	\return The first ExternalExpert with specified name, null if none was found
		 */
		ExternalExpert* FindExpert(string expertName);
		/*! \brief Returns the first EmotionSensor with specified name (BasicObject.name field is compared)
		 *	
		 *	Returns the first EmotionSensor with specified name (BasicObject.name field is compared)
		 *	\param sensorName The name of EmotionSensor to search for
		 *	\return The first EmotionSensor with specified name, null if none was found
		 */
		EmotionSensor* FindSensor(string sensorName);
		/*! \brief Returns the first ExternalComponentLibrary with specified name (BasicObject.name field is compared)
		 *	
		 *	Returns the first ExternalComponentLibrary with specified name (BasicObject.name field is compared)
		 *	\param libraryName The name of ExternalComponentLibrary to search for
		 *	\return The first ExternalComponentLibrary with specified name, null if none was found
		 */
		ExternalComponentLibrary* FindLibrary(string libraryName);

		/*! \brief Loads the configuration of ErfContext from default file
		 *	
		 *	Loads the configuration of ErfContext from the file named erf-config.xml in the current working directory.
		 *	The file should be a valid xml containing paths to the ExternalComponentLibraries that should be loaded on startup.
		 *	An example of configuration file:
		 *	<PRE> 
		 *	\<erfConfiguration\>
		 *		\<externalLibrary\>
		 *			\<dllPath\>/path/to/ExternalComponentLibrary1.dll\</dllPath\>
		 *			\<dllPath\>/path/to/ExternalComponentLibrary2.dll\</dllPath\>
		 *			\<dllPath\>...\</dllPath\>
		 *		\</externalLibrary\>
		 *	\</erfConfiguration\>
		 *	</PRE>
		 */
		void LoadConfiguration();
		/*! \brief Loads the configuration of ErfContext from specified file
		 *	
		 *	Loads the configuration of ErfContext from the file specified (both relative and absolute paths are supported).
		 *	The file should be a valid xml containing paths to the ExternalComponentLibraries that should be loaded on startup.
		 *	An example of configuration file:
		 *	<PRE> 
		 *	\<erfConfiguration\>
		 *		\<externalLibrary\>
		 *			\<dllPath\>/path/to/ExternalComponentLibrary1.dll\</dllPath\>
		 *			\<dllPath\>/path/to/ExternalComponentLibrary2.dll\</dllPath\>
		 *			\<dllPath\>...\</dllPath\>
		 *		\</externalLibrary\>
		 *	\</erfConfiguration\>
		 *	</PRE>
		 *
		 *	\param file The relative/absolute path to the configuration file
		 */
		void LoadConfiguration(string file);

		/*! \brief Loads the ExternalComponentLibrary into the ErfContext
		 *	Loads the ExternalComponentLibrary into the ErfContext. 
		 *	After the creation of ExternalComponentLibrary the method ExternalComponentLibrary::RegisterComponentsFunction will be invoced.
		 *	\param file The relative/absolute path to the dll file
		 *	\return Newly loaded library, null if unable to load
		 */
		ExternalComponentLibrary* LoadComponentLibrary(string file);
		/*! \brief Unloads the ExternalComponentLibrary from the ErfContext by library path
		 *	
		 *	Unloads the ExternalComponentLibrary from the ErfContext by library path
		 *	\param file The relative/absolute path to the dll file
		 */
		void UnloadComponentLibrary(string file);
		/*! \brief Unloads the ExternalComponentLibrary from the ErfContext 
		 *	
		 *	Unloads the ExternalComponentLibrary from the ErfContext 
		 *	\param library The library to unload
		 */
		void UnloadComponentLibrary(ExternalComponentLibrary* library);
		
		/*! \brief Used only in debug configuration, starts memory logging 
		 *	
		 *	Used only in debug configuration, starts memory loggin
		 *	\return Whenever memory logging has successfully started
		 */
		static bool StartMemLeakLogging();
		/*! \brief Used only in debug configuration, stops memory logging 
		 *	
		 *	Used only in debug configuration, stops memory logging 
		 *	\return Whenever memory logging has successfully stoped
		 */
		static bool StopMemLeakLoggingAndLog();

	};
}