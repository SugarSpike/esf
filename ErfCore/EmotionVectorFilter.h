#pragma once
#include "BasicObject.h"

/*! \file EmotionVectorFilter.h
	\brief EmotionVectorFilter header file
*/


//=================================
// forward declared dependencies
namespace erf
{
	class EmotionVector;
	class CharacterData;
}

namespace erf
{
	/*! \brief	Type of an EmotionVectorFilter, specifies the phase of filter application
	 *			
	 *	The enum that specifies at what phase of vector evaluation a filter should be applied.
	 *	The time of filter application (and if they are used at all) is dependend on the concrete implementation of a VectorEvaluator.
	 *	For example the BasicVectorEvaluator does not use Before filters. But the general rules is as follows:
	 *	- Before filters are applied on intermediate vectors before they are passed to a VectorEvaluator
	 *	- After filters are used on the vector computed by a VectorEvaluator
	 */
	enum EmotionVectorFilterType
	{
		Before, ///< Filters applied by VectorEvaluator on intermediate vectors (or none at all)
		After ///< Filters applied on target EmotionVector returned by a VectorEvaluator
	};

	/*! \brief	A filter class, used to transform EmotionVector%s
	 *			
	 *	The emotion vector filter is used to apply transformations on supplied EmotionVector.
	 *	The transform can be as trivial as normalizaion or as complicated as transforming the EmotionVector from one EmotionModelType
	 *	to another one. Another example of usage is the equalization of a group of EmotionVector%s in order to allow aggregating VectorEvaluator%s
	 *	to combine vectors of different type (as aggregators have strict rules about the structure of an vector, see WeightedMeanFloatVectorEvaluator
	 *	for an example). The filter is always of a certaing EmotionVectorFilterType that specifies at what time will it be applied, or not at all.
	 *	For example the BasicVectorEvaluator does not use Before filters. But the general rules is as follows:
	 *	- Before filters are applied on intermediate vectors before they are passed to a VectorEvaluator
	 *	- After filters are used on the vector computed by a VectorEvaluator
	 */
    class ESFCORE_API EmotionVectorFilter : public virtual BasicObject
    {
		public:
			/*! \brief Applies filter, transforming the given vector
			 *
			 *	Applies filter, transforming the given vector. The transform can be as trivial as normalizaion or
			 *	as complicated as transforming the EmotionVector from one EmotionModelType to another one.
			 *
			 *	\param[in,out] emotionVector the vector to transform
			 *	\param characterData holds information about the character (see CharacterData for more information)
			 */
			virtual void DoFilter(EmotionVector& emotionVector, CharacterData& characterData) = 0;

			/*! \brief Whenever the filter supports a EmotionVector type
			 *	
			 *	Returns whenever the filter supports a EmotionVector type eg. can apply any meaningful transformation to it.
			 *	The filter will most likely be skipped when returning false (but that depends on VectorEvaluator implementation).
			 *	\param emotionVector the vector to check
			 *	\return whenever the filter supports specified EmotionVector
			 */
			virtual bool IsEmotionVectorSupported(EmotionVector& emotionVector) = 0;
	};
}  

