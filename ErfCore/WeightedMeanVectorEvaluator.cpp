#include "WeightedMeanVectorEvaluator.h"
#include "EmotionModel.h"
#include "ErfLogging.h"
#include "ErfForEach.h"
#include "ErfUtils.h"
#include "EmotionVectorFilter.h"
#include "EmotionVector.h"
#include "EecEvent.h"
#include "WeightedMeanDoubleVectorEvaluator.h"
#include "WeightedMeanFloatVectorEvaluator.h"

namespace erf {

	WeightedMeanVectorEvaluator::WeightedMeanVectorEvaluator(CharacterModel& model) :
		BasicObject("WeightedMeanVectorEvaluator"), VectorEvaluator(model), floatOptimizedVectorEvaluator(null)
	{

	}

	WeightedMeanVectorEvaluator::~WeightedMeanVectorEvaluator()
	{
		delete this->floatOptimizedVectorEvaluator;
	}

	WeightedMeanVectorEvaluator* WeightedMeanVectorEvaluator::GetOptimizedForFloats()
	{
		if(this->floatOptimizedVectorEvaluator != null)
			return this->floatOptimizedVectorEvaluator;
		this->floatOptimizedVectorEvaluator = new WeightedMeanFloatVectorEvaluator(model);

		return this->floatOptimizedVectorEvaluator;
	}

	WeightedMeanVectorEvaluator* WeightedMeanVectorEvaluator::GetOptimizedForDoubles()
	{
		if(this->doubleOptimizedVectorEvaluator != null)
			return this->doubleOptimizedVectorEvaluator;
		this->doubleOptimizedVectorEvaluator = new WeightedMeanDoubleVectorEvaluator(model);

		return this->doubleOptimizedVectorEvaluator;
	}

	void WeightedMeanVectorEvaluator::ValidateVectors(vector<EmotionVector*>& vectors, vector<int>& highestRankModelType, bool highestIsUniform) {
		for(auto vectorIterator = vectors.begin(); vectorIterator != vectors.end() ; ++vectorIterator)
		{
			if((*vectorIterator)->ValueCount() == 0)
			{
				LOG(info) << "WeightedMeanVectorEvaluator::ValidateVectors - found empty vector: "
								<< (*vectorIterator)->GetUniqueName();
				delete (*vectorIterator);
				vectorIterator = vectors.erase(vectorIterator);
				continue;
			}

			if((*vectorIterator)->GetUniformType() != highestIsUniform)
			{
				LOG(info) << "WeightedMeanVectorEvaluator::ValidateVectors - vector uniformity inconsistent received: "
								<< (*vectorIterator)->GetUniformType() <<", expected:" << highestIsUniform 
								<< ". Vector: "<< (*vectorIterator)->GetUniqueName();
				delete (*vectorIterator);
				vectorIterator = vectors.erase(vectorIterator);
				continue;

			}

			for_each_erf(int& vectorType, (*vectorIterator)->GetEmotionModelTypes())
			{
				if(!erfutils::Contains(highestRankModelType,vectorType))
				{
					LOG(info) << "WeightedMeanVectorEvaluator::ValidateVectors - vector is not uniform: " << vectorType <<
						", " << (*vectorIterator)->GetUniqueName();
					delete (*vectorIterator);
					vectorIterator = vectors.erase(vectorIterator);
					continue;
				}
			}
		}
	}

	void  WeightedMeanVectorEvaluator::ApplyBeforeFilters(vector<EmotionVector*>& vectors){ 
		
			for_each_erf(EmotionVectorFilter* filter,this->model.GetRegisteredBeforeFilters().getPrioritizedList())
			{
				for_each_erf(EmotionVector* vector,vectors)
				{
					if(filter->IsEmotionVectorSupported(*vector))
					{
						LOG(debug) << "WeightedMeanVectorEvaluator::Evaluate - After Filter to Emotion Vector applied: "
										<< filter->GetUniqueName();
						filter->DoFilter(*vector,this->model.GetCharacterData());
					}
				}
			}
	}

	void WeightedMeanVectorEvaluator::ApplyAfterFilters(EmotionVector& vector) {
		for_each_erf(EmotionVectorFilter* filter,this->model.GetRegisteredAfterFilters().getPrioritizedList())
		{
			if(filter->IsEmotionVectorSupported(vector))
			{
				LOG(debug) << "BaseVectorEvaluator::Evaluate - After Filter to Emotion Vector applied: " << filter->GetUniqueName();
				filter->DoFilter(vector,this->model.GetCharacterData());
			}
		}
	}

	EmotionVector* WeightedMeanVectorEvaluator::AggregateVectors(vector<EmotionVector*>& vectors) {
		if(vectors[0]->GetUniformType())
		{
			Variant* firstVal = vectors[0]->GetValue(0);
			if( firstVal->IsFloat())
			{
				return this->GetOptimizedForFloats()->AggregateVectors(vectors);
			}
			if( firstVal->IsDouble())
			{
				return this->GetOptimizedForDoubles()->AggregateVectors(vectors);
			}
		}
		// No optimized versions of evaluator were found, the full check aggregation will be performed

		EmotionVector* aggregatedVector = new EmotionVector(vectors[0]->GetEmotionModelTypes());
		map<string,Variant*>& parametersMapOfFirst =  vectors[0]->GetValues();
		for (auto parameterIterator = parametersMapOfFirst.begin() ; parameterIterator != parametersMapOfFirst.end(); ++parameterIterator)
		{
			string emotionName = parameterIterator->first;
			double averageDivisor = 0.0f;
			double averageDividend = 0.0f;
			double temporary = 0.0f;

			for_each_erf(EmotionVector* vector, vectors)
			{
				Variant* value= vector->GetValue(emotionName);
				if(!value->IsDouble() && 
					!value->IsInteger() &&
					!value->AsFloat())
					continue;

				double emotionValue = 0.0;
				if(value->IsDouble())
					emotionValue = value->AsDouble();
				else if(value->IsFloat())
					emotionValue = (double)value->AsFloat();
				else
					emotionValue = (double)value->AsInteger();

				temporary = (vector->GetCertainty(emotionName)+vector->GetModelRank());
				averageDividend += temporary * emotionValue;
				averageDivisor += temporary;
			}

			aggregatedVector->AddValue(parameterIterator->first,Variant::Create(averageDividend/averageDivisor));
		}
		return aggregatedVector;
	}

	bool WeightedMeanVectorEvaluator::Evaluate(EecEvent& eecEvent) {
		EmotionVector* newEmotionVector = null;

		float highestRank = -1.0f;
		vector<int>* highestRankModelType = null;
		bool highestIsUniform = false;
		vector<EmotionVector*> expertVectors;
		for_each_erf(EmotionModel* model,this->model.GetRegisteredEmotionModels())
		{
			if(model->IsEventSupported(eecEvent))
			{
				LOG(debug) << "WeightedMeanVectorEvaluator::Evaluate - Delegate to EmotionModel: " << model->GetUniqueName();
				newEmotionVector = model->HandleEvent(eecEvent,this->model.GetCharacterData());
				if(newEmotionVector == null)
				{
					LOG(info) << "WeightedMeanVectorEvaluator::Evaluate - EmotionModel: " << model->GetUniqueName()
						<< " returned null for EecEvent" << eecEvent;
					continue;
				}
				float rank = model->GetModelRank();
				if(rank > highestRank)
				{
					highestRank = rank;
					highestRankModelType = &newEmotionVector->GetEmotionModelTypes();
					highestIsUniform = newEmotionVector->GetUniformType();
				}
				expertVectors.push_back(newEmotionVector);
			}
		}

		if(expertVectors.size() == 0)
		{
			LOG(warning) << "WeightedMeanVectorEvaluator::Evaluate - None of registered models support: " << eecEvent;
			return false;
		}

		if(this->model.GetRegisteredBeforeFilters().size() > 0)
		{
			this->ApplyBeforeFilters(expertVectors);
		}

		if(expertVectors.size() == 0)
		{
			LOG(warning) << "WeightedMeanVectorEvaluator::Evaluate - No vectors returned after filtration";
			return false;
		}
		
		this->ValidateVectors(expertVectors,*highestRankModelType,highestIsUniform);
		highestRankModelType = null;

		if(expertVectors.size() == 0)
		{
			LOG(warning) << "WeightedMeanVectorEvaluator::Evaluate - No vectors returned after validation";
			return false;
		}

		newEmotionVector = this->AggregateVectors(expertVectors);
		for(unsigned int i = 0 ; i < expertVectors.size() ; i++)
			delete expertVectors[i];

		if(newEmotionVector==null)
		{
			LOG(error) << "BaseVectorEvaluator::Evaluate - Could not aggregate vectors for event: " << eecEvent;
			return false;
		}

		EmotionVector* afterFilterVector = newEmotionVector;
		if(this->model.GetRegisteredAfterFilters().size() > 0)
		{
			afterFilterVector = newEmotionVector->Clone();
			this->ApplyAfterFilters(*afterFilterVector);
		}
		this->SetCurrentEmotionVector(afterFilterVector);
		this->SetUnfilteredEmotionVector(newEmotionVector);
		return true;
	}
}