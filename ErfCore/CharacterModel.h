#pragma once
#include "stdafx.h"

/*! \file CharacterModel.h
	\brief CharacterModel header file

*/

//=================================
// included dependencies
#include "CharacterData.h"
#include "PriorityList.h"
#include "BasicObject.h"
#include "GameEnums.h"
#include "GameConfiguration.h"
#include "CharacterModelEnums.h"
#include "VectorEvaluatorFactory.h"
//=================================
// forward declared dependencies
namespace erf
{
	class MoodVector;
	class Personality;
	class CharacterData;
	class EecEvent;
	class EmotionSensor;
	class EmotionModel;
	class MoodModel;
	class PsychologicalModel;
	class EmotionVectorFilter;
	class EmotionVector;
	class ExternalExpert;
	class ExternalFilter;
	class VectorEvaluator;
	enum  EmotionVectorFilterType;
}

using namespace std;

/*! \brief The container type for EmotionModels registered in the CharacterModel
 */
typedef std::vector<erf::EmotionModel* > EmotionModelContainer;
/*! \brief The container type for MoodModel registered in the CharacterModel
 */
typedef std::vector<erf::MoodModel* > MoodModelContainer;
/*! \brief The container type for PsychologicalModel registered in the CharacterModel
 */
typedef std::vector<erf::PsychologicalModel* > PsychologicalModelContainer;
/*! \brief The container type for EmotionVectorFilter registered in the CharacterModel
 */
typedef erfutils::PriorityList< erf::EmotionVectorFilter* > FilterContainer;


namespace erf
{

	/*! \brief	Represents a single character for whom emotions are modeled.
	 *			
	 *	Character model is the representation of a single character in the world for whom we
	 *	want to model emotions. It is a container for all generated emotion vector as well
	 *	as an container for registration and communication between framework components:
	 *	experts, sensors, vector models etc.
	 *
	 */
    class ESFCORE_API CharacterModel : public BasicObject
    {

	protected:
		VectorEvaluatorFactory vectorEvaluatorFactory; ///< Creates new instances of VectorEvaluator%s on models mode changes
	private:
		ModelsEvaluationMode emotionModelEvaluation;	///< Evaluation mode fo emotion models / emotion vectors 
		ModelsEvaluationMode moodModelEvaluation;	///< Evaluation mode for mood models
		int emotionModelEvaluationCharacteristics; ///< Vector characteristics of EmotionVectors produced by EmotionModels
		int moodModelEvaluationCharacteristics; ///< Vector characteristics of MoodVectors produced by MoodModels
		VectorEvaluator* emotionEvaluator; ///< Evaluates emotion vectors based on emotionModelEvaluation
		VectorEvaluator* moodEvaluator; ///< Evaluates emotion vectors based on moodModelEvaluation

		CharacterData	 characterData;	///< Contains the data associated with character
		
		EmotionModelContainer emotionModels;				///< All registered emotion models 
		MoodModelContainer moodModels;						///< All registered mood models 
		PsychologicalModelContainer psychologicalModels;	///< All registered psychological models 
		vector<ExternalExpert* > experts; ///< All registered emotion models 
		
		FilterContainer beforeEmotionVectorFilters;	///< Filters that are applied on all emotion vectors before the evaluation of the final one

		FilterContainer afterEmotionVectorFilters;	///< Filters that are applied on the result vector

		/*! \brief Finds the expert by name among the registered experts
		 *	
		 *	Finds the expert by name among the registered experts
		 *	\param expertName The expert name
		 *	\return the expert with the given name if found, null otherwise
		 */
		ExternalExpert* CharacterModel::FindExpert(string expertName);

	public:
		
		/*! \brief The default constructor
		 *	
		 *	The default constructor of the CharacterModel
		 *
		 */
		CharacterModel();
		/*! \brief The copy constructor - should not be used
		 *	
		 *	\throws ErfException the copy construtor is not supported and will throw an exception upon invocation
		 *
		 */
		CharacterModel(const CharacterModel& model);
		~CharacterModel();

		//Model getters for computed values
		/*! \brief Returns character data model, containing data associated with this character
		 *	
		 *	Returns the character data model, containing data associated with this character:
		 *		- previous and current computed EmotionVector%s and MoodVector%s
		 *		- GameConfiguration object
		 *		- Personality object - is a variant collection
		 *		- Registered sensors for this model
		 *
		 *	This object, as stated before, acts as a container for data associated with this
		 *	particular character, this object will be passed to most component when their
		 *	methods are invoked (experts computing vectors etc.)
		 *
		 *	\return CharacteData object
		 */
		virtual CharacterData& GetCharacterData();
		
		/*! \brief Returns the original vector of the emotion model before filters were applied
		 *	
		 *	Returns the original vector of the emotion model before filters were applied
		 *	is only available for ModelsEvaluationMode::STD_STANDARD
		 *	\return orgininal emotion vector before filters were applied
		 */
		virtual EmotionVector* GetUnfilteredEmotionVector();

		/*!	\brief Returns the current emotion vector after all filters were applied
		 *	
		 *	Returns the current emotion vector after all filters were applied
		 *	\return current emotion vector before filters were applied
		 */
		virtual EmotionVector* GetEmotionVector();
		
		/*! \brief Returns the current mood vector
		 *
		 *	Returns the current mood vector
		 *	\return current mood vector
		 */
		virtual MoodVector* GetMoodVector();
		
		/*! \brief Returns the personality of the character
		 *	
		 *	Returns the personality of the character, personality is a VariantCollection
		 *  where additional data associated with this character can be stored, by default
		 *	it is an empty collection. It is used by PsychologicalModels to store data about
		 *	the personality of the character that is later passed to other models
		 *	\return character personality
		 */
		virtual Personality& GetPersonality();
		
		/*!	\brief Returns the game configuration
		 *	
		 *	Returns the game configuration that may have been passed to the character model. Game configuration 
		 *  contains a set of challenges, themes and keywords associated with the game as a whole
		 *	\return game configuration
		 */
		virtual GameConfiguration& GetGameConfiguration();
		
		/*!	\brief Sets the personality
		 *	
		 *	Sets the new presonality for the model. The CharacterData will become the owner of the personality
		 *	deleteing the configuration instance on destruction
		 *	\throws ErfException When a null pointer is passed
		 *	\param personality The new personality
		 */
		virtual void SetPersonality(Personality* personality);

		/*!	\brief Sets the personality
		 *	
		 *	Sets the new presonality for the model.
		 *
		 *	\throws ErfException When a null pointer is passed
		 *	\param isMemoryOwner Whenever the CharacterData is the owner of the pointer
		 *	\param personality The new personality
		 */
		virtual void SetPersonality(Personality* personality,bool isMemoryOwner);

		/*!	\brief Sets the game configuration
		 *	
		 *	Sets the new game configuration for the model.  The CharacterData will become the owner of the configuration
		 *	deleteing the configuration instance on destruction
		 *	\throws ErfException When a null pointer is passed
		 *	\param gameConfiguration The new game configuration
		 */
		virtual void SetGameConfiguration(GameConfiguration* gameConfiguration);

		/*!	\brief Sets the game configuration
		 *	
		 *	Sets the new game configuration for the model.
		 *	\throws ErfException When a null pointer is passed
		 *	\param isMemoryOwner Whenever the CharacterData is the owner of the pointe
		 *	\param gameConfiguration The new game configuration
		 */
		virtual void SetGameConfiguration(GameConfiguration* gameConfiguration, bool isMemoryOwner);

		/*!	\brief Returns all registered emotion models
		 *	
		 *	Returns all registered emotion models
		 *	\return All registered emotion models
		 */
		virtual EmotionModelContainer GetRegisteredEmotionModels();
		/*!	\brief Returns all registered mood models
		 *	
		 *	Returns all registered mood models
		 *	\return All registered mood models
		 */
		virtual MoodModelContainer GetRegisteredMoodModels();
		/*!	\brief Returns all registered before filters
		 *	
		 *	Returns all registered before filters
		 *	\return All registered before filters
		 */
		virtual FilterContainer GetRegisteredBeforeFilters();
		/*!	\brief Returns all registered after filters
		 *	
		 *	Returns all registered after filters
		 *	\return All registered after filters
		 */
		virtual FilterContainer GetRegisteredAfterFilters();
		/*!	\brief Returns the emotion models characteristics
		 *	
		 *  Returns the emotion models characteristics
		 *	\return Bitmap of characteristics
		 */
		int GetEmotionModelEvaluationCharacteristics();
		/*!	\brief Sets the emotion models characteristics
		 *	
		 *	Sets the emotion models characteristics
		 *	\param newVal Bitmap of characteristics
		 */
		void SetEmotionModelEvaluationCharacteristics(int newVal);
		/*!	\brief Returns the mood models characteristics
		 *	
		 *	Returns the mood models characteristics
		 *	\return Bitmap of characteristics
		 */
		int GetMoodModelEvaluationCharacteristics();
		
		/*!	\brief Sets the mood models characteristics
		 *	
		 *	Sets the mood models characteristics
		 *	\param newVal Bitmap of characteristics
		 */
		void SetMoodModelEvaluationCharacteristics(int newVal);

		/*!	\brief Returns all registered psychological models
		 *	
		 *	Returns all registered psychological models
		 *	\return All registered psychological models
		 */
		virtual PsychologicalModelContainer GetRegisteredPsychologicalModels();

		/*! \brief Sets the current ModelsEvaluationMode for EmotionModel%s
		 *	
		 *	Sets the current ModelsEvaluationMode for EmotionModel%s. Creates new VectorEvaluator for given type.
		 *	\throws ErfException when a VectorEvaluator for given type cannot be constructed
		 */
		virtual void SetEmotionModelEvaluationMode(ModelsEvaluationMode mode);
		
		/*!	\brief Returns the evaluation mode for emotion models
		 *	
		 *	Returns the evaluation mode for emotion models
		 *	\return The evaluation mode
		 */
		virtual ModelsEvaluationMode GetEmotionModelEvaluationMode();
		
		/*!	\brief Sets the current ModelsEvaluationMode for MoodModels%s
		 *	
		 *	Sets the current ModelsEvaluationMode for MoodModels%s. Creates new VectorEvaluator for given type.
		 *	\throws ErfException when a VectorEvaluator for given type cannot be constructed
		 */
		virtual void SetMoodModelEvaluationMode(ModelsEvaluationMode mode);
		
		/*!	\brief Returns the evaluation mode for mood models
		 *	
		 *	Returns the evaluation mode for mood models
		 *	\return The evaluation mode
		 */
		virtual ModelsEvaluationMode GetMoodModelEvaluationMode();


		/*! \brief Handles an emotion eliciting event, computing new emotion and mood vectors as a result
		 *
		 *	The main function of the character model. Handles a specific emotion eliciting event and computes
		 *  new emotion and mood vectors as a result. The algorithm that is used to compute new vectors depends
		 *	on currently chosen VectorEvaluator%s but the overall algorithm steps are as follows:
		 *		- Compute the current emotion vectors by using EmotionModel%s
		 *		- Supply new data to MoodModel%s in order to compute new MoodVector
		 *		- Update character Personality by using PsychologicalModel%s
		 *	\return true if the event was handled and new vectors computed, false otherwise
		 */
		virtual bool HandleEvent(EecEvent& eecEvent);

		//Register models functions
		/*!
		 *	Below are the functions used for registration and unregistration of various
		 *  components to this character model
		 */

		/*!	\brief Registers an emotion model
		 *	
		 *	Registers an emotion model
		 *	\param emotionModel Model to be registered
		 *	\return true if model was registered, false if the model was registered before in this model
		 */
		virtual bool RegisterEmotionModel(EmotionModel* emotionModel);

		/*!	\brief Registers a mood model
		 *	
		 *	Registers a mood model
		 *	\param moodModel Model to be registered
		 *	\return true if model was registered, ffalse if the model was registered before in this model
		 */
		virtual bool RegisterMoodModel(MoodModel* moodModel);

		/*!	\brief Registers a vector filter at the specified phase
		 *	
		 *	Registers a vector filter at the specified phase, the priority of the filter will be set to 0
		 *	\param filter Filter to be registered
		 *  \param filterType phase of the registered filter
		 *	\return True if filter was registered, false if a filter with the same class and priority was registered before
		 */
		virtual bool RegisterEmotionVectorFilter(EmotionVectorFilter* filter, EmotionVectorFilterType filterType);

		/*!	\brief Registers an external vector filter at the specified phase
		 *	
		 *	Registers an external vector filter at the specified phase, the priority of the filter will be set to 0
		 *	\param filter Filter to be registered
		 *  \param filterType phase of the registered filter
		 *	\return True if filter was registered, false if a filter with the same class and priority was registered before
		 */
		virtual bool RegisterExternalFilter(ExternalFilter* filter, EmotionVectorFilterType filterType);

		/*!	\brief Registers a vector filter at the specified phase with given priority
		 *	
		 *	Registers a vector filter at the specified phase with given priority
		 *	\param filter Filter to be registered
		 *  \param filterType Phase of the registered filter
		 *  \param priority The priority of the filter
		 *	\return True if filter was registered, false if a filter with the same class and priority was registered before
		 */
		virtual bool RegisterEmotionVectorFilter(EmotionVectorFilter* filter, EmotionVectorFilterType filterType, int priority);

		/*!	\brief Registers an external vector filter at the specified phase with given priority
		 *	
		 *	\param filter Filter to be registered
		 *  \param filterType Phase of the registered filter
		 *  \param priority The priority of the filter
		 *	\return True if filter was registered, false if a filter with the same class and priority was registered before
		 */
		virtual bool RegisterExternalFilter(ExternalFilter* filter, EmotionVectorFilterType filterType, int priority);

		/*!	\brief Registers a psychological model
		 *	
		 *	Registers a psychological model
		 *	\param psychologicalModel Model to be registered
		 *	\return true if model was registered, ffalse if the model was registered before in this model
		 */
		virtual bool RegisterPsychologicalModel(PsychologicalModel* psychologicalModel);
		
		/*!	\brief Registers an external sensor
		 *	
		 *	Registers an external sensor
		 *	\param emotionSensor Sensor to be registered
		 *	\return true if sensor was registered, false if the sensor was registered before in this model
		 */
		virtual bool RegisterEmotionSensor(EmotionSensor* emotionSensor);

		/*!	\brief Registers an expert
		 *	
		 *	Registers an expert, based on the expert type the expert will also be registered as one of:
		 *  emotion, mood or psychological model.
		 *	\param expert Expert to be registered
		 *	\return true if expert was registered, false if the expert was registered before in this model
		 */
		virtual bool RegisterExpert(ExternalExpert* expert);

		//Unregister functions
		/*!	\brief Unregisters an emotion model
		 *	
		 *	Unregisters an emotion model
		 *	\param emotionModel Model to be unregistered
		 *	\return true if model was unregistered, false if the model was not registered for this character
		 */
		virtual bool UnregisterEmotionModel(EmotionModel* emotionModel);

		/*!	\brief Unregisters an emotion model by name
		 *	
		 *	Unregisters an emotion model by name
		 *	\param emotionModelName Model name to be unregistered
		 *	\return true if model was unregistered, false if the model was not registered for this character
		 */
		virtual bool UnregisterEmotionModelByName(string emotionModelName);
		
		/*!	\brief Unregisters a mood model
		 *	
		 *	Unregisters a mood model
		 *	\param moodModel Model to be unregistered
		 *	\return true if model was unregistered, false if the model was not registered for this character
		 */
		virtual bool UnregisterMoodModel(MoodModel* moodModel);
		/*!	\brief Unregisters a mood model by name
		 *	
		 *	Unregisters a mood model by name
		 *	\param moodModelName Model name to be unregistered
		 *	\return true if model was unregistered, false if the model was not registered for this character
		 */
		virtual bool UnregisterMoodModelByName(string moodModelName);
		
		/*!	\brief Unregisters an emotion filter from all phases
		 *
		 *	Unregisters an emotion filter from all phases
		 *	\param emotionVectorFilter Filter to be unregistered
		 *	\return true if filter was unregistered for any phase, false if the filter was never registered for this character
		 */
		virtual bool UnregisterEmotionVectorFilter(EmotionVectorFilter* emotionVectorFilter);

		/*!	\brief Unregisters an emotion filter from all phases by name
		 *	
		 *	Unregisters an emotion filter from all phases by name
		 *	\param filterName Filter name to be unregistered
		 *	\return true if filter was unregistered for any phase, false if the filter was never registered for this character
		 */
		virtual bool UnregisterEmotionVectorFilterByName(string filterName);

		/*!	\brief Unregisters an emotion filter from a specific phases
		 *	
		 *	Unregisters an emotion filter from a specific phases
		 *	\param emotionVectorFilter Filter to be unregistered
		 *	\param filterType Filter phase
		 *	\return true if filter was unregistered for the specific phase, false if the filter was never registered for this phase
		 */
		virtual bool UnregisterEmotionVectorFilter(EmotionVectorFilter* emotionVectorFilter, EmotionVectorFilterType filterType);

		
		/*!	\brief Unregisters an external filter from all phases
		 *
		 *	Unregisters an emotion filter from all phases
		 *	\param filter Filter to be unregistered
		 *	\return true if filter was unregistered for any phase, false if the filter was never registered for this character
		 */
		virtual bool UnregisterExternalFilter(ExternalFilter* filter);

		/*!	\brief Unregisters an external filter from all phases by name
		 *	
		 *	Unregisters an external filter from all phases by name
		 *	\param filterName Filter name to be unregistered
		 *	\return true if filter was unregistered for any phase, false if the filter was never registered for this character
		 */
		virtual bool UnregisteExternalFilterByName(string filterName);

		/*!	\brief Unregisters an emotion filter from a specific phases
		 *	
		 *	Unregisters an emotion filter from a specific phases
		 *	\param filter Filter to be unregistered
		 *	\param filterType Filter phase
		 *	\return true if filter was unregistered for the specific phase, false if the filter was never registered for this phase
		 */
		virtual bool UnregisterExternalFilter(ExternalFilter* filter, EmotionVectorFilterType filterType);

		/*!	\brief Unregisters an emotion filter by name from a specific phases
		 *	
		 *	Unregisters an emotion filter by name from a specific phases
		 *	\param filterName Filter name to be unregistered
		 *	\param filterType Filter phase
		 *	\return true if filter was unregistered for the specific phase, false if the filter was never registered for this phase
		 */
		virtual bool UnregisterEmotionVectorFilterByName(string filterName, EmotionVectorFilterType filterType);
		
		/*!	\brief Unregisters a psychological model
		 *	
		 *	Unregisters a psychological model
		 *	\param psychologicalModel Model to be unregistered
		 *	\return true if model was unregistered, false if the model was not registered for this character
		 */
		virtual bool UnregisterPsychologicalModel(PsychologicalModel* psychologicalModel);
		/*!	\brief Unregisters a psychological model by name
		 *
		 *	Unregisters a psychological model by name
		 *	\param psychologicalModelName Model name to be unregistered
		 *	\return true if model was unregistered, false if the model was not registered for this character
		 */
		virtual bool UnregisterPsychologicalModelByName(string psychologicalModelName);
		
		/*! \brief Unregisters an external sensor
		 *	
		 *	Unregisters an external sensor
		 *	\param emotionSensor Sensor to be unregistered
		 *	\return true if sensor was unregistered, false if the sensor was never registered for this character model
		 */
		virtual bool UnregisterEmotionSensor(EmotionSensor* emotionSensor);
		/*!	\brief Unregisters an external sensor by name
		 *	
		 *	Unregisters an external sensor by name
		 *	\param sensorName Sensor name to be unregistered
		 *	\return true if sensor was unregistered, false if the sensor was never registered for this character model
		 */
		virtual bool UnregisterEmotionSensorByName(string sensorName);
		
		/*!	\brief Unregisters an external expert
		 *
		 *	Unregisters an external expert
		 *	\param expert Expert to be unregistered
		 *	\return true if expert was unregistered, false if the expert was never registered for this character model
		 */
		virtual bool UnregisterExpert(ExternalExpert* expert);
		/*!	\brief Unregisters an external expert by name
		 *	
		 *	Unregisters an external expert by name
		 *	\param expertName Expert name to be unregistered
		 *	\return true if expert was unregistered, false if the expert was never registered for this character model
		 */
		virtual bool UnregisterExpertByName(string expertName);
	};
}