package pl.gda.pg.isi.erf;

public class EnumConstants {

	 public static class OccEmotions
    {
	    private OccEmotions() {}
        public static final String JOY = "JOY";
        public static final String DISTRESS = "DISTRESS";
        public static final String PRIDE = "PRIDE";
        public static final String SHAME = "SHAME";
        public static final String ADMIRATION = "ADMIRATION";
        public static final String REPROACH = "REPROACH";
        public static final String LOVE = "LOVE";
        public static final String HATE = "HATE";
        public static final String HAPPY_FOR = "HAPPY_FOR";
        public static final String RESENTMENT = "RESENTMENT";
        public static final String GLOATING = "GLOATING";
        public static final String PITY = "PITY";
        public static final String HOPE = "HOPE";
        public static final String FEAR = "FEAR";
        public static final String SATISFACTION = "SATISFACTION";
        public static final String FEARS_CONFIRMED = "FEARS_CONFIRMED";
        public static final String RELIEF = "RELIEF";
        public static final String DISAPPOINTMENT = "DISAPPOINTMENT";
        public static final String GRATIFICATION = "GRATIFICATION";
        public static final String REMORSE = "REMORSE";
        public static final String GRATITUDE = "GRATITUDE";
        public static final String ANGER = "ANGER";
    }

    public static class PadEmotions
    {
    	private PadEmotions() {}
        public static final String PLEASURE = "PLEASURE";
        public static final String AROUSAL = "AROUSAL";
        public static final String DOMINANCE = "DOMINANCE";
    }
	
}
