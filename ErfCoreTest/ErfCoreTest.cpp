// ErfCoreTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ErfContext.h"
#include "Variant.h"
#include "VariantCollection.h"
#include "EmotionVector.h"
#include "ExternalComponentLibrary.h"
#include "ExternalExpert.h"
#include "boost/test/execution_monitor.hpp"
#include "EmotionEnums.h"
#include <memory>
#include "Shlwapi.h"
#include "CharacterModel.h"
#include "EecEvent.h"
#include "ErfForEach.h"
#include "ErfLogging.h"
#include <vector>
#include "EmotionConstants.h"
#include "ExternalComponent.h"
#include "ExternalComponentConfiguration.h"
#include "EmotionSensor.h"
#include "ExternalFilter.h"
#include "EmotionSensorReading.h"
#include "WeightedMeanFloatVectorEvaluator.h"
#include <boost/progress.hpp>

#ifdef _DEBUG
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#else
	#include <stdlib.h>
#endif

#define ERF_VARIANT_HOLD_ANY 0
using namespace std;
using namespace erf;

long long GetTimeMs64()
{
 /* Windows */
 FILETIME ft;
 LARGE_INTEGER li;

 /* Get the amount of 100 nano seconds intervals elapsed since January 1, 1601 (UTC) and copy it
  * to a LARGE_INTEGER structure. */
 GetSystemTimeAsFileTime(&ft);
 li.LowPart = ft.dwLowDateTime;
 li.HighPart = ft.dwHighDateTime;

 long long ret = li.QuadPart;
 ret -= 116444736000000000LL; /* Convert from file time to UNIX epoch time. */
 ret /= 10000; /* From 100 nano seconds (10^-7) to 1 millisecond (10^-3) intervals */

 return ret;
}





class Test {

public:
	Test() { }

	Test(int t) { 
		this->testVal = t; 
	}
	int testVal;
	friend std::ostream& operator <<( std::ostream& outs, const Test * e)
	{  
		outs << e->testVal;
		return outs;
	}
	friend std::istream& operator >>( std::istream& ins, Test* & e){
		ins >> e->testVal;
		return ins;
	}
};

void TestVariantSimple()
{
#if !defined(ERF_VARIANT_HOLD_ANY)
    cout << "******************************************" << endl;
    cout << "Simple variant test" << endl;
    cout << "******************************************" << endl;
	Test* t = new Test(15);
	Variant* var = Variant::Create<Test>(t);
	Variant* var2 = Variant::Create<Test>(t,false);

	cout << "1.1 T=" << t->testVal << endl;
	cout << "1.2 V=" << var->As<Test>()->testVal << endl;
	cout << "1.3 VN=" << var2->As<Test>()->testVal << endl;

	cout << "Delete VN" << endl;
	delete var2;
	cout << "2.1 T=" << t->testVal << endl;
	cout << "2.2 V=" << var->As<Test>()->testVal << endl;
	
	cout << "Delete V" << endl;
	delete var;

	cout << "T should be invalid" << endl;
	cout << "3.1 T=" << t->testVal << endl;
#endif
}

void TestVariantCloning()
{
#if !defined(ERF_VARIANT_HOLD_ANY)
    cout << "******************************************" << endl;
    cout << "Clone variant test" << endl;
    cout << "******************************************" << endl;
	Test* t = new Test(15);
	Variant* var = Variant::Create<Test>(t);
	Variant* clone = var->Clone();
	cout << "1.1 T=" << t->testVal << endl;
	cout << "1.2 V=" << var->As<Test>()->testVal << endl;
	cout << "1.3 C=" << clone->As<Test>()->testVal << endl;
	
	cout << "Deleting V" << endl;
	delete var;
	cout << "2.1 T=" << t->testVal << endl;
	cout << "2.2 C=" << clone->As<Test>()->testVal << endl;

	cout << "Deleting C" << endl;
	delete clone;

	cout << "T should be invalid" << endl;
	cout << "3.1 T=" << t->testVal << endl;
#endif
}

void TestEmotionVectorCloning() {
#if !defined(ERF_VARIANT_HOLD_ANY)
    cout << "******************************************" << endl;
    cout << "Clone emotion vector test" << endl;
    cout << "******************************************" << endl;
	Test* t = new Test(15);
	EmotionVector* original = new EmotionVector(EmotionModelType::STD_OCC);
	original->AddValue("Fear",Variant::Create(0.57f));
	original->AddValue("Test",Variant::Create<Test>(t));

	EmotionVector* clone = original->Clone();

	cout << "1.1 T=" << t->testVal << endl;
	cout << "1.2 V=" << original->GetValue("Test")->As<Test>()->testVal << "," 
		<< original->GetValue("Fear")->AsFloat() << endl;
	cout << "1.3 C=" << clone->GetValue("Test")->As<Test>()->testVal << "," 
		<< clone->GetValue("Fear")->AsFloat() << endl;

	cout << "Modifying values o->test, c->fear" << endl;
	original->GetValue("Test")->As<Test>()->testVal = 20;
	clone->AddValue("Fear",Variant::Create(0.67f)); 

	cout << "2.1 T=" << t->testVal << endl;
	cout << "2.2 V=" << original->GetValue("Test")->As<Test>()->testVal << "," 
		<< original->GetValue("Fear")->AsFloat() << endl;
	cout << "2.3 C=" << clone->GetValue("Test")->As<Test>()->testVal << "," 
		<< clone->GetValue("Fear")->AsFloat() << endl;
	
	cout << "Deleting original" << endl;
	delete original;

	cout << "3.1 T=" << t->testVal << endl;
	cout << "3.2 C=" << clone->GetValue("Test")->As<Test>()->testVal << "," 
		<< clone->GetValue("Fear")->AsFloat() << endl;

	cout << "Deleting clone" << endl;
	delete clone;
	
	cout << "T should be invalid" << endl;
	cout << "4.1 T=" << t->testVal << endl;
#endif
}


EmotionVector*  InitializeEmotionVector(){
	EmotionVector* vector = new EmotionVector(EmotionModelType::STD_OCC);
	vector->AddValue(OccEmotions::JOY, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::DISTRESS, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::PRIDE, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::SHAME, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::ADMIRATION, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::REPROACH, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::LOVE, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::HATE, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::HAPPY_FOR, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::RESENTMENT, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::GLOATING, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::PITY, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::HOPE, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::FEAR, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::SATISFACTION, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::FEARS_CONFIRMED, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::RELIEF, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::DISAPPOINTMENT, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::GRATIFICATION, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::REMORSE, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::GRATITUDE, Variant::Create(0.0f));
	vector->AddValue(OccEmotions::ANGER, Variant::Create(0.0f));
	return vector;
}

void TestEmotionVectorCloningSpeed(int noOfPasses) {
    cout << "******************************************" << endl;
    cout << "Emotion Vector Clone Speed Test " << endl;
    cout << "******************************************" << endl;
	EmotionVector* vector = InitializeEmotionVector();
	ErfLogger::SetLoggingEnabled(false);
	for(int i = 0 ; i < noOfPasses ; i++) {
		EmotionVector* tmp = vector->Clone();
		for(int i = 0 ; i < tmp->ValueCount() ; i++)
			tmp->GetValueByIndex(i)->IsDouble();

		delete vector;
		vector = tmp;
	}
	ErfLogger::SetLoggingEnabled(true);
	delete vector;
    cout << "Emotion Vector Clone Speed Test End " << endl;
    cout << "******************************************" << endl;
}

void ExternalEmotionExpertTest(string libraryPath, string expertName,ModelsEvaluationMode mode, int characteristics, vector<int>& eventTypeIds)
{
	boost::progress_timer timer;
    cout << "******************************************" << endl;
    cout << "External Expert Test " << endl;
    cout << "******************************************" << endl;
    cout << "Library: " << libraryPath << endl;
    cout << "Expert Name: " << expertName << endl;
    cout << "Characteristics: " << characteristics << endl;
    cout << "Evaluation Mode: " << mode << endl;
	cout << "Events count: " << eventTypeIds.size() << endl;
    cout << "******************************************" << endl;
    cout << "Loading library into context..." << endl;
	ErfContext& context = ErfContext::GetInstance();
	ExternalComponentLibrary* library = context.LoadComponentLibrary(libraryPath);

    cout << "Library loaded: " << library->GetUniqueName() << endl;
	cout << "Initializing CharacterModel... " << endl;
	CharacterModel* model = new CharacterModel();

	if(expertName != "ALL")
	{
		cout << "Loading expert... " << endl;
		ExternalExpert* expert = context.FindExpert(expertName);
		if(expert == null)
		{
			cout << "Expert not found... " << endl;
			cout << "******************************************" << endl;
			return;
		}
		cout << "Expert loaded: " << expert->GetUniqueName() << endl;
		model->RegisterExpert(expert);
	}
	for_each_erf(ExternalComponent* component, library->GetConfiguration().GetComponents())
	{
		if(component->GetComponentType() == ComponentType::SENSOR)
		{
			EmotionSensor* sensor = dynamic_cast<EmotionSensor*>(component);
			if(sensor != null)
			{
				model->RegisterEmotionSensor(sensor);
				sensor->Initialize();
			}
		}
		else if(component->GetComponentType() == ComponentType::FILTER)
		{
			ExternalFilter* filter = dynamic_cast<ExternalFilter*>(component);
			if(filter != null)
			{
				model->RegisterExternalFilter(filter,EmotionVectorFilterType::After);
			}
		}
		else if(component->GetComponentType() == ComponentType::EXPERT &&
					expertName == "ALL")
		{
			ExternalExpert* expert = dynamic_cast<ExternalExpert*>(component);
			if(expert != null)
			{
				model->RegisterExpert(expert);
				cout << "Expert loaded: " << expert->GetUniqueName() << endl;
			}
		}
	}

	if(mode != ModelsEvaluationMode::STD_BASIC)
		model->SetEmotionModelEvaluationMode(mode);

	if(characteristics != EmotionVectorCharacteristics::STD_NO_CHARACTERISTICS)
		model->SetEmotionModelEvaluationCharacteristics(characteristics);

	cout << "Calculate and print EmotionVector... " << endl;
	//LOG(info) << "Disabling logging";
	//ErfLogger::SetLoggingEnabled(false);
	long long start = GetTimeMs64();
	int i = 0;
	for_each_erf(int & eventId , eventTypeIds)
	{
		EecEvent event(eventId);
		model->HandleEvent(event);
		//cout << "Before filter vector: " << endl << model->GetUnfilteredEmotionVector()->Print() << endl;
		//cout << "Current vector: " << endl << model->GetEmotionVector()->Print() << endl;
		float p =  model->GetUnfilteredEmotionVector()->GetValue(PadEmotions::PLEASURE)->AsFloat();
		float a =  model->GetUnfilteredEmotionVector()->GetValue(PadEmotions::AROUSAL)->AsFloat();
		float d =  model->GetUnfilteredEmotionVector()->GetValue(PadEmotions::DOMINANCE)->AsFloat();
		float joy = model->GetEmotionVector()->GetValue(OccEmotions::JOY)->AsFloat();;
		float distress = model->GetEmotionVector()->GetValue(OccEmotions::DISTRESS)->AsFloat();
		cout << i++ << "\t" << "p: " << p << "\t" << "a: " << a << "\t" << "d: " << d << "\t" << "joy: " << joy << "\t" << "dis: " << distress << endl;
	}
	long long stop = GetTimeMs64();
	double totalTime = ((double)(stop-start))/ (double)CLOCKS_PER_SEC;
	cout << "RESULT: " << totalTime  << " seconds"<<  endl;
	cout << "AVG PER CYCLE: " << totalTime/((double)eventTypeIds.size())  << " seconds"<<  endl;

	cout << "Done printing..." << endl;
	//ErfLogger::SetLoggingEnabled(false);
	
	cout << "Deleting CharacterModel... " << endl;
	delete model;
	cout << "Unloading library... " << endl;
	//context.UnloadComponentLibrary(library);
	cout << "Expert test end " << endl;
    cout << "******************************************" << endl;
}

void ExternalEmotionExpertTest(string libraryPath, string expertName, ModelsEvaluationMode mode, vector<int>& eventTypeIds)
{
	ExternalEmotionExpertTest(
		libraryPath,
		expertName,
		mode,
		EmotionVectorCharacteristics::STD_NO_CHARACTERISTICS, 
		eventTypeIds);
}

void ExternalEmotionExpertTest(string libraryPath, string expertName, vector<int>& eventTypeIds)
{
	ExternalEmotionExpertTest(
		libraryPath,
		expertName,
		ModelsEvaluationMode::STD_BASIC,
		EmotionVectorCharacteristics::STD_NO_CHARACTERISTICS, 
		eventTypeIds);
}

void ExternalSensorTest(string libraryPath, string sensorName)
{
	boost::progress_timer timer;
    cout << "******************************************" << endl;
    cout << "Sensor Expert Test " << endl;
    cout << "******************************************" << endl;
    cout << "Library: " << libraryPath << endl;
    cout << "Sensor Name: " << sensorName << endl;
    cout << "******************************************" << endl;
    cout << "Loading library into context..." << endl;
	ErfContext& context = ErfContext::GetInstance();
	ExternalComponentLibrary* library = context.LoadComponentLibrary(libraryPath);

    cout << "Library loaded: " << library->GetUniqueName() << endl;
	cout << "Initializing CharacterModel... " << endl;
	EmotionSensor* sensor = context.FindSensor(sensorName);
	sensor->Initialize();
	EmotionSensorReading* reading = sensor->GetSensorReading();
	sensor->Deinitialize();
}


void WeightedAverageTest()
{
	CharacterModel model;
	WeightedMeanVectorEvaluator weightedMeanFloatVectorEvaluator(model);
	EmotionVector vector1(EmotionModelType::STD_PAD);
	vector1.SetModelRank(0.45f);
	vector1.AddValue("AROUSAL",Variant::Create(0.45f),1.0f);
	
	EmotionVector vector2(EmotionModelType::STD_PAD);
	vector2.SetModelRank(0.25f);
	vector2.AddValue("AROUSAL",Variant::Create(0.25f),0.45f);
	
	EmotionVector vector3(EmotionModelType::STD_PAD);
	vector3.SetModelRank(0.75f);
	vector3.AddValue("AROUSAL",Variant::Create(0.11f),0.8f);


	vector<EmotionVector*> toAggregate;
	toAggregate.push_back(&vector1);
	toAggregate.push_back(&vector2);
	toAggregate.push_back(&vector3);

//	EmotionVector* mean = weightedMeanFloatVectorEvaluator.AggregateVectors(toAggregate);
//	cout << "RESULT: " << mean->Print() << endl;
}


// The following macros set and clear, respectively, given bits
// of the C runtime library debug flag, as specified by a bitmask.
#ifdef   _DEBUG
#define  SET_CRT_DEBUG_FIELD(a) \
            _CrtSetDbgFlag((a) | _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG))
#define  CLEAR_CRT_DEBUG_FIELD(a) \
            _CrtSetDbgFlag(~(a) & _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG))
#else
#define  SET_CRT_DEBUG_FIELD(a)   ((void) 0)
#define  CLEAR_CRT_DEBUG_FIELD(a) ((void) 0)
#endif


int _tmain(int argc, _TCHAR* argv[])
{

	//WeightedAverageTest();

	ErfLogger::SetLoggingEnabled(false);
//	LOG(info) << "Disabling logging";
   _CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_FILE );
   _CrtSetReportFile( _CRT_WARN, _CRTDBG_FILE_STDOUT );
   _CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_FILE );
   _CrtSetReportFile( _CRT_ERROR, _CRTDBG_FILE_STDOUT );
   _CrtSetReportMode( _CRT_ASSERT, _CRTDBG_MODE_FILE );
   _CrtSetReportFile( _CRT_ASSERT, _CRTDBG_FILE_STDOUT );
   //TestVariantSimple();
   //TestVariantCloning();
   //TestEmotionVectorCloning();
   //TestEmotionVectorCloningSpeed(10);

   int noOfEvents = 500;
	if(argc > 1)
		noOfEvents = atoi(argv[1]);

   vector<int> eventTypes;
   for(int i = 0 ; i < noOfEvents ; i++)
   {
		double randRange = rand() / double(RAND_MAX);
		if(randRange < 0.15)
			eventTypes.push_back((int)GameEvent::STD_SKILL_USED);
		else if(randRange < 0.30)
			eventTypes.push_back(GameEvent::STD_OBSTACLES_CLOSE);
		else
			eventTypes.push_back(GameEvent::STD_TIME_ELAPSED);
   }

   
#if !defined(ERF_VARIANT_HOLD_ANY)
   // ExternalEmotionExpertTest("PhotronErf.dll","PhotronEmotionModel",eventTypes);
    ExternalEmotionExpertTest("ExternalLibraryMock.dll","PadAffectionExpert",eventTypes);
	//ExternalEmotionExpertTest("ExternalLibraryMock.dll","ALL", eventTypes);
	//ExternalEmotionExpertTest("ExternalLibraryMock.dll","ALL",ModelsEvaluationMode::STD_WEIGHTED_AVERAGE, eventTypes);
	//ExternalEmotionExpertTest("ExternalLibraryMock.dll","ALL",ModelsEvaluationMode::STD_WEIGHTED_AVERAGE,EmotionVectorCharacteristics::STD_VECTOR_OF_FLOATS, eventTypes);
#endif
	ExternalSensorTest("KinectErf.dll","KinectErfSensor");

   //Test* t = new Test(10);
   cout << "******************************************" << endl;
   cout << "Exiting, dumping potential memory leaks..." << endl;

   SET_CRT_DEBUG_FIELD( _CRTDBG_LEAK_CHECK_DF );
   return 0;
}

