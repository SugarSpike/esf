﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Erf;
using ErfClips;

namespace ErfClipsCsharpTest
{
    public class FeelBallEmotionalModelFactory
    {
        private static Random random = new Random();

        private static float GetRandomNumber(double minimum, double maximum)
        {
            return (float)(random.NextDouble() * (maximum - minimum) + minimum);
        }
        public static CharacterModel CreateModel()
        {
            CharacterModel model = new CharacterModel();
            float relationshipToPlayer = GetRandomNumber(-1.0, 1.0);
            Console.Out.WriteLine("Creating ball with relationship: " + relationshipToPlayer);
            OccClipsPersonality personality = new OccClipsPersonality();
            personality.neurotic = (float)GetRandomNumber(-1.0, 1.0);
            personality.extravert = (float)GetRandomNumber(-1.0, 1.0);
            personality.relationshipToPlayer = (float)GetRandomNumber(-1.0, 1.0);
            model.SetPersonality(personality);

            ClipsEmotionModel emotionModel = new ClipsEmotionModel("occ.clp");
            model.RegisterEmotionModel(emotionModel);
            try
            {
                OccClipsPersonality pers = OccClipsPersonality.CastToDerived(model.GetPersonality());
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Message);
            }

            return model;
        }

    }
}
