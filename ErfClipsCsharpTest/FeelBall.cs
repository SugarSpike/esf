﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Erf;

namespace ErfClipsCsharpTest
{
    public class FeelBall
    {
        public CharacterModel EmotionalModel { get; set; }


        public FeelBall()
        {
            EmotionalModel = FeelBallEmotionalModelFactory.CreateModel();
        }

    }
}
