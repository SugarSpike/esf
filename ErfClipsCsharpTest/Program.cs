﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Erf;

namespace ErfClipsCsharpTest
{

    class Program
    {
        private static void LoadErfMock()
        {
            ErfContext.GetInstance().LoadComponentLibrary("ExternalLibraryMock.dll");
        }
        static void Main(string[] args)
        {
            ErfLogger.SetLoggingEnabled(false);
            LoadErfMock();
            PlayerModel player = new PlayerModel();
            EecEvent timeElapsedEvent = new EecEvent((int)GameEvent.STD_TIME_ELAPSED);
            player.EmotionalModel.HandleEvent(timeElapsedEvent); // Get the current...


            FeelBall ball1 = new FeelBall();
            EmotionVector previousVector = new EmotionVector((int)EmotionModelType.STD_OCC);
            previousVector.AddValue(OccEmotions.DISTRESS, Variant.Create(0.3f));
            previousVector.AddValue(OccEmotions.JOY, Variant.Create(0.3f));

            EmotionVector currentVector = new EmotionVector((int)EmotionModelType.STD_OCC);
            currentVector.AddValue(OccEmotions.DISTRESS, Variant.Create(0.5f));
            currentVector.AddValue(OccEmotions.JOY, Variant.Create(0.1f));



            Console.Out.WriteLine();
            for (int i = 0; i < 100; i++)
            {
                player.EmotionalModel.HandleEvent(timeElapsedEvent);
                /*float p =  player.EmotionalModel.GetUnfilteredEmotionVector().GetValue(PadEmotions.PLEASURE).AsFloat();
                float a =  player.EmotionalModel.GetUnfilteredEmotionVector().GetValue(PadEmotions.AROUSAL).AsFloat();
                float d =  player.EmotionalModel.GetUnfilteredEmotionVector().GetValue(PadEmotions.DOMINANCE).AsFloat();
                float joy = player.EmotionalModel.GetEmotionVector().GetValue(OccEmotions.JOY).AsFloat();
                float distress = player.EmotionalModel.GetEmotionVector().GetValue(OccEmotions.DISTRESS).AsFloat();
                Console.WriteLine("{0}\tp:{1}\ta:{2}\td:{3}\tj:{4}\td:{5}",i,p,a,d,joy,distress);*/

                EecEvent eecEvent = new EecEvent((int)GameEvent.STD_PLAYER_EMOTIONAL_STATE_CHANGE);
                eecEvent.AddValue("PlayerEmotionVector", Variant.CreateEmotionVector(player.EmotionalModel.GetEmotionVector()));
                eecEvent.AddValue("PreviousPlayerEmotionVector", Variant.CreateEmotionVector(player.EmotionalModel.GetCharacterData().GetPreviousEmotionVector()));
                ball1.EmotionalModel.HandleEvent(eecEvent);
                EmotionVector vector = ball1.EmotionalModel.GetEmotionVector();
                Console.Out.WriteLine("Retrieved: " + vector.Print());

                if(i%10==0)
                {
                    GC.Collect(); GC.WaitForPendingFinalizers();
                }

            }
            //Console.In.Read();
        }
    }
}
