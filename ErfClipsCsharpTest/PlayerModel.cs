﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Erf;

namespace ErfClipsCsharpTest
{
    class PlayerModel
    {

        public CharacterModel EmotionalModel { get; set; }


        public PlayerModel()
        {
            EmotionalModel = new CharacterModel();
            ErfContext context = ErfContext.GetInstance();
            EmotionSensor fileReadingSensor = context.FindSensor("SensorDataFileMock");
            ExternalExpert padExpert = context.FindExpert("PadAffectionExpert");
            ExternalFilter padToOccFiter = context.FindFilter("PadToOccVectorFilter");
            fileReadingSensor.Initialize();
            EmotionalModel.RegisterEmotionSensor(fileReadingSensor);
            EmotionalModel.RegisterExpert(padExpert);
            EmotionalModel.RegisterExternalFilter(padToOccFiter, EmotionVectorFilterType.After);
        }

    }
}
