﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ErfCoreGui.Controls
{

    class ComponentListBox : ListBox
    {
        private const int WM_LBUTTONDOWN = 0x201;

        public event EventHandler PreSelect;

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_LBUTTONDOWN:
                    OnPreSelect();
                    break;
            }

            base.WndProc(ref m);
        }

        protected void OnPreSelect()
        {
            if (null != PreSelect)
                PreSelect(this, new EventArgs());
        }

    }
}
