﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Erf;

namespace ErfCoreGui.Utils
{
    public enum WrappedItemType
    {
        Component,Expert,Sensor,EmotionModel,MoodModel,PsychologicalModel, Filter
    }

    public class ListViewItemWrapper : ListViewItem
    {
        public WrappedItemType Type { get; set; }
        public ExternalComponent Component { get; set; }
        public EmotionModel EmotionModelComponent { get; set; }
        public MoodModel MoodModelComponent { get; set; }
        public ExternalFilter EmotionVectorFilterComponent { get; set; }
        public PsychologicalModel PsychologicalModelComponent { get; set; }

        public ListViewItemWrapper(ExternalComponent component)
        {
            this.Type = WrappedItemType.Component;
            this.Component = component;
            this.Text = component.GetName();

            int index = (int)component.GetComponentType();
            if (index > (int)ComponentType.EXT_COMPONENT)
                index = (int)ComponentType.EXT_COMPONENT;

            this.ImageIndex = index;
        }

        public ListViewItemWrapper(ExternalExpert component)
        {
            this.Type = WrappedItemType.Expert;
            this.Component = component;
            this.Text = component.GetName();

            int index = (int)component.GetExpertType();
            if (index > (int)ExpertType.EXT_EXPERT)
                index = (int)ExpertType.EXT_EXPERT;

            this.ImageIndex = index;
        }

        public ListViewItemWrapper(EmotionSensor component)
        {
            this.Type = WrappedItemType.Sensor;
            this.Component = component;
            this.Text = component.GetName();

            int index = (int)component.GetSensorTypes()[0];
            if (index > (int)SensorType.EXT_SENSOR)
                index = (int)SensorType.EXT_SENSOR;

            this.ImageIndex = index;
        }

        public ListViewItemWrapper(ExternalFilter filter,EmotionVectorFilterType type)
        {
            this.Type = WrappedItemType.Filter;
            this.Component = filter;
            this.Text = filter.GetName();

            int index = type == EmotionVectorFilterType.After ? 0 : 1;
            this.ImageIndex = index;
        }

        public ListViewItemWrapper(EmotionModel component)
        {
            this.Type = WrappedItemType.EmotionModel;
            this.EmotionModelComponent = component;
            this.Text = component.GetName();

          //  int index = (int)component.Get();
          //  if (index > (int)ExpertType.EXT_EXPERT)
          //      index = (int)ExpertType.EXT_EXPERT;

            this.ImageIndex = 0;
        }

        public ListViewItemWrapper(MoodModel component)
        {
            this.Type = WrappedItemType.MoodModel;
            this.MoodModelComponent = component;
            this.Text = component.GetName();

            //  int index = (int)component.Get();
            //  if (index > (int)ExpertType.EXT_EXPERT)
            //      index = (int)ExpertType.EXT_EXPERT;

            this.ImageIndex = 0;
        }

        public ListViewItemWrapper(PsychologicalModel component)
        {
            this.Type = WrappedItemType.EmotionModel;
            this.PsychologicalModelComponent = component;
            this.Text = component.GetName();

            //  int index = (int)component.Get();
            //  if (index > (int)ExpertType.EXT_EXPERT)
            //      index = (int)ExpertType.EXT_EXPERT;

            this.ImageIndex = 0;
        }
        
    }
}
