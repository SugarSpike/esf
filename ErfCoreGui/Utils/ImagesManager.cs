﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace ErfCoreGui.Utils
{
    static class ImagesManager
    {
        static ImageList externalComponentTypesImages;
        static ImageList sensorTypeImages;
        static ImageList expertTypeImages;
        static ImageList emotionModelImages;
        static ImageList moodModelImages;
        static ImageList filterImages;
        static ImageList psychologicalModelImages;
        static Bitmap checkedImage;

        public static ImageList GetExternalComponentImages()
        {
            if(externalComponentTypesImages != null)
                return externalComponentTypesImages;

            externalComponentTypesImages = new ImageList();
            externalComponentTypesImages.Images.Add(ErfCoreGui.Properties.Resources.plugin); //LIBRARY = 0,
            externalComponentTypesImages.Images.Add(ErfCoreGui.Properties.Resources.eye); // SENSOR
            externalComponentTypesImages.Images.Add(ErfCoreGui.Properties.Resources.status_offline); //EXPERT
            externalComponentTypesImages.Images.Add(ErfCoreGui.Properties.Resources.plugin_disabled); // EXT_COMPONENT
            return externalComponentTypesImages;
        }

        public static ImageList GetSensorTypeImages()
        {
            if(sensorTypeImages != null)
                return sensorTypeImages;

            sensorTypeImages = new ImageList();
            sensorTypeImages.Images.Add(ErfCoreGui.Properties.Resources.eye); //STD_UNDEF_SENSOR = 0,
            sensorTypeImages.Images.Add(ErfCoreGui.Properties.Resources.heart); // STD_HEART_RATE
            sensorTypeImages.Images.Add(ErfCoreGui.Properties.Resources.heart); //STD_BVP
            sensorTypeImages.Images.Add(ErfCoreGui.Properties.Resources.lightning); //STD_SKIN_CONDUCTANCE
            sensorTypeImages.Images.Add(ErfCoreGui.Properties.Resources.lightning); //STD_EMG
            sensorTypeImages.Images.Add(ErfCoreGui.Properties.Resources.user); //STD_AU
            sensorTypeImages.Images.Add(ErfCoreGui.Properties.Resources.user); //STD_FPP
            sensorTypeImages.Images.Add(ErfCoreGui.Properties.Resources.plugin_disabled); // EXT_SENSOR
            return sensorTypeImages;
        }

        public static ImageList GetExpertTypeImages()
        {
            if(expertTypeImages != null)
                return expertTypeImages;

            expertTypeImages = new ImageList();
            expertTypeImages.Images.Add(ErfCoreGui.Properties.Resources.status_offline); //STD_UNDEF_EXPERT = 0,
            expertTypeImages.Images.Add(ErfCoreGui.Properties.Resources.status_online); // STD_EMOTION
            expertTypeImages.Images.Add(ErfCoreGui.Properties.Resources.status_online); //STD_MOOD
            expertTypeImages.Images.Add(ErfCoreGui.Properties.Resources.status_online); // STD_PSYCHOLOGICAL
            expertTypeImages.Images.Add(ErfCoreGui.Properties.Resources.plugin_disabled); // EXT_EXPERT
            return expertTypeImages;
        }

        public static ImageList GetEmotionModelImages()
        {
            if (emotionModelImages != null)
                return emotionModelImages;

            emotionModelImages = new ImageList();
            emotionModelImages.Images.Add(ErfCoreGui.Properties.Resources.emoticon_smile);
            return emotionModelImages;
        }

        public static ImageList GetMoodModelImages()
        {
            if (moodModelImages != null)
                return moodModelImages;

            moodModelImages = new ImageList();
            moodModelImages.Images.Add(ErfCoreGui.Properties.Resources.emoticon_smile);
            return moodModelImages;
        }

        public static ImageList GetFilterImages()
        {
            if (filterImages != null)
                return filterImages;

            filterImages = new ImageList();
            filterImages.Images.Add(ErfCoreGui.Properties.Resources.control_end);
            filterImages.Images.Add(ErfCoreGui.Properties.Resources.control_start);
            return filterImages;
        }

        public static ImageList GetPsychologicalModelImages()
        {
            if (psychologicalModelImages != null)
                return psychologicalModelImages;

            psychologicalModelImages = new ImageList();
            psychologicalModelImages.Images.Add(ErfCoreGui.Properties.Resources.emoticon_smile);
            return psychologicalModelImages;
        }

        public static Bitmap GetCheckedImage()
        {
            if (checkedImage != null)
                return checkedImage;
            checkedImage = ErfCoreGui.Properties.Resources.tick;

            return checkedImage;
        }

    }
}
