﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Erf;

namespace ErfCoreGui.Utils
{
    public class VectorHistoryItem : ListViewItem
    {
        private static int EventCount = 0;

        private int eventId;
        public EecEvent EecEvent { get; set; }
        public EmotionVector EmotionVector { get; set; }
        public EmotionVector FilteredVector { get; set; }
        public MoodVector MoodVector { get; set; }

        public VectorHistoryItem(EecEvent e,EmotionVector ev, EmotionVector fv, MoodVector mv)
        {
            eventId = ++EventCount;
            this.EecEvent = e;
            this.EmotionVector = ev;
            this.FilteredVector = fv;
            this.MoodVector = mv;
        }


        public override string ToString()
        {
            return eventId + ": " + EecEvent.CSharpUniqueName;
        }
    }
}
