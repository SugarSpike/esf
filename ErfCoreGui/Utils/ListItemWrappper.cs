﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ErfCoreGui.Utils
{
    public class ListItemWrapper
    {
        private Object data;

        public Object Data
        {
            get { return data; }
            set { data = value; }
        }
        private String description;

        public String Description
        {
            get
            {
                if (description != null)
                    return description;

                return data.ToString();
            }
            set { Description = value; }
        }

        public ListItemWrapper(Object data, String description)
        {
            this.data = data;
            this.description = description;
        }

        public override bool Equals(object obj)
        {
            Object compareTo = obj;
            if (obj is ListItemWrapper)
                compareTo = (obj as ListItemWrapper).data;

            return data.Equals(compareTo);
        }

        public override int GetHashCode()
        {
            return data.GetHashCode();
        }

        public override string ToString()
        {
            return Description;
        }

    }
}
