﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Erf;
using ErfCoreGui.Utils;
using System.IO;
using ErfCoreGui.Controls;



namespace ErfCoreGui
{

    public partial class MainForm : Form
    {
        private Timer cyclicEventTimer; 
        List<DataGridView> grids;
        List<Label> gridLabels;
        List<ExternalComponentLibrary> loadedLibraries;
        CharacterModel characterModel;
        bool eventCustom = false;
        bool eventCyclic = false;
        bool eventCyclicSendingInProgress = false;
        ToolTip characterModelErrorTT = new ToolTip();
        public MainForm()
        {


            InitializeComponent();
            grids = new List<DataGridView>(new[] { 
                emotionVectorGrid, filteredVectorGrid, moodVectorGrid });
            gridLabels = new List<Label>(new[] { 
                emotionVectorType, filteredVectorType, moodVectorType });


            characterModel = new CharacterModel();
            loadedLibraries = new List<ExternalComponentLibrary>();
            Console.WriteLine("Welcome to Emotion Recognition Framework GUI Tester!");
            componentNameLabel.Text = "";
            componentStateLabel.Text = "";
            this.failedToCopyPB.Visible = false;
            this.failedStateChangePB.Visible = false;
            this.characterModelErrorPB.Visible = false;

            foreach(DataGridView grid in grids)
            {
                grid.Columns.Add("Name", "Name");
                grid.Columns["Name"].ReadOnly = true;
                grid.Columns.Add("Value", "Value");
                grid.Columns["Value"].ReadOnly = true;
            }
            foreach (Label label in gridLabels)
            {
                label.Text = "";
            }

            componentParametersDGV.Columns.Add("Name", "Name");
            componentParametersDGV.Columns["Name"].ReadOnly = true;
            componentParametersDGV.Columns.Add("Value", "Value");

            sensorsListView.SmallImageList = ImagesManager.GetSensorTypeImages();
            expertListView.SmallImageList = ImagesManager.GetExpertTypeImages();
            emotionModelListView.SmallImageList = ImagesManager.GetEmotionModelImages();
            moodModelListView.SmallImageList = ImagesManager.GetMoodModelImages();
            psychologicalModelListView.SmallImageList = ImagesManager.GetPsychologicalModelImages();
            vectorFiltersListView.SmallImageList = ImagesManager.GetFilterImages();

            ToolTip tt = new ToolTip();
            tt.SetToolTip(this.cloneComponentPB, "Clone selected component");

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            componentFilterCB.Items.Add("ALL");
            foreach(ComponentType type in Enum.GetValues(typeof(ComponentType)))
                componentFilterCB.Items.Add(type);

            foreach (GameEvent eventType in Enum.GetValues(typeof(GameEvent)))
                eventTypeCB.Items.Add(eventType);

            foreach (ModelsEvaluationMode mode in Enum.GetValues(typeof(ModelsEvaluationMode)))
            {
                emotionEvaluationModeCB.Items.Add(mode);
                moodEvaluationModeCB.Items.Add(mode);
            }

            foreach (EmotionVectorCharacteristics characteristics in Enum.GetValues(typeof(EmotionVectorCharacteristics)))
            {
                emotionVectorCharacteristicsCB.Items.Add(characteristics);
                moodVectorCharacteristicsCB.Items.Add(characteristics);
            }
            emotionVectorCharacteristicsCB.SelectedIndex = 0;
            moodVectorCharacteristicsCB.SelectedIndex = 0;
            eventTypeCB.SelectedIndex = 0;
            emotionEvaluationModeCB.SelectedIndex = 0;
            moodEvaluationModeCB.SelectedIndex = 0;
            componentFilterCB.SelectedIndex = 0;
            eventTypeNUD.Visible = false;
            ErfContext.StartMemLeakLogging();
        }

        private void loadLibraryBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                ExternalComponentLibrary library = ErfContext.GetInstance().LoadComponentLibrary(openFileDialog.FileName);

                if (library == null)
                    return;


                if (!loadedLibraries.Contains(library))
                {
                    loadedLibraries.Add(library);
                    librariesLB.Items.Add(new ListItemWrapper(library, library.GetConfiguration().componentDllName));
                }
            }
        }

        private T GetSelectedItem<T>(ListBox lb) where T : BasicObject
        {
            if (lb == null || lb.SelectedItem == null)
                return null;

            ListItemWrapper wrapper = (lb.SelectedItem as ListItemWrapper);
            return wrapper.Data as T;
        }

        private void unloadLibraryBtn_Click(object sender, EventArgs e)
        {
            if (librariesLB.SelectedItem == null)
                return;

            ListItemWrapper wrapper = (librariesLB.SelectedItem as ListItemWrapper);
            ExternalComponentLibrary lib = wrapper.Data as ExternalComponentLibrary;

            ClearComponentsSelection();

            librariesLB.Items.Remove(wrapper);
            loadedLibraries.Remove(lib);

            ErfContext.GetInstance().UnloadComponentLibrary(lib);
            GC.Collect();
            GC.WaitForPendingFinalizers();


        }

        private void ClearComponentsSelection()
        {
            componentLB.Items.Clear();
            ClearDetailsPanel();
        }

        private void ClearDetailsPanel()
        {
            componentParametersDGV.Rows.Clear();
            componentNameLabel.Text = "";
            componentStateLabel.Text = "";
            componentDetailsTabs.SelectedIndex = 0;
        }

        private void componentFilterCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExternalComponentLibrary library = GetSelectedItem<ExternalComponentLibrary>(librariesLB);
            PopulateComponentListBox(library);
        }

        private void librariesLB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExternalComponentLibrary library = GetSelectedItem<ExternalComponentLibrary>(librariesLB);
            PopulateComponentListBox(library);
        }

        private void PopulateComponentListBox(ExternalComponentLibrary library)
        {
            componentLB.Items.Clear();
            ClearDetailsPanel();
            if (library == null)
                return;

            if (componentFilterCB.SelectedValue == null)
            {
                foreach (ExternalComponent component in library.GetConfiguration().GetComponents())
                {
                    componentLB.Items.Add(new ListItemWrapper(component, component.GetName()));
                }
                return;
            }

            ComponentType filterType = (ComponentType)Enum.Parse(typeof(ComponentType), componentFilterCB.SelectedValue.ToString());
            foreach (ExternalComponent component in library.GetConfiguration().GetComponents())
            {
                if (component.GetComponentType().Equals(filterType))
                    componentLB.Items.Add(new ListItemWrapper(component, component.GetUniqueName()));

            }
        }

        private void componentLB_SelectedIndexChanged(object sender, EventArgs e)
        {
            componentParametersDGV.Rows.Clear();
            ExternalComponent selectedComponent = GetSelectedItem<ExternalComponent>(componentLB);
            if (selectedComponent == null)
                return;
            StringToStringMap componentParameters = new StringToStringMap();
            selectedComponent.GetParameters(componentParameters);

            foreach (KeyValuePair<string, string> parameterKeyValue in componentParameters)
                componentParametersDGV.Rows.Add(parameterKeyValue.Key, parameterKeyValue.Value);

            componentNameLabel.Text = selectedComponent.GetUniqueName().Replace("::", "\r\n");
            componentStateLabel.Text = selectedComponent.GetComponentState().ToString();

            ShowDetailsTab(selectedComponent);
        }

        private void ShowDetailsTab(ExternalComponent component)
        {

            switch (component.GetComponentType())
            {
                case ComponentType.SENSOR:
                    ShowSensorTab(component);
                    return;
                case ComponentType.EXPERT:
                    ShowExpertTab(component);
                    return;
                case ComponentType.LIBRARY:
                    ShowLibraryTab(component);
                    return;
                default:
                    componentDetailsTabs.SelectedIndex = 0;
                    return;
            }
        }

        private void ShowSensorTab(ExternalComponent component)
        {
            EmotionSensor sensor = EmotionSensor.CastToDerived(component);
            if (sensor == null)
            {
                Console.Out.WriteLine("Unable to cast component: {0} to EmotionSensor", component.GetUniqueName());
                return;
            }

            componentDetailsTabs.SelectedIndex = 3;
            sensorNameLabel.Text = sensor.GetType().ToString();

        }

        private void ShowExpertTab(ExternalComponent component)
        {
            ExternalExpert expert = ExternalExpert.CastToDerived(component);
            if (expert == null)
            {
                Console.Out.WriteLine("Unable to cast component: {0} to ExternalExpert", component.GetUniqueName());
                return;
            }

            componentDetailsTabs.SelectedIndex = 2;
            expertTypeLabel.Text = expert.GetExpertType().ToString();

        }

        private void ShowLibraryTab(ExternalComponent component)
        {
            componentDetailsTabs.SelectedIndex = 1;

        }


        private void componentParametersDGV_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != 1)
                return;

            string oldValue = componentParametersDGV.Rows[e.RowIndex].Cells[1].Value.ToString();
            string newValue = e.FormattedValue.ToString();
            if (oldValue == newValue)
                return;

            string parameterName = componentParametersDGV.Rows[e.RowIndex].Cells[0].Value.ToString();

            ExternalComponent selectedComponent = GetSelectedItem<ExternalComponent>(componentLB);
            if (!selectedComponent.SetParameter(parameterName, newValue))
            {
                e.Cancel = true;
                componentParametersDGV.Rows[e.RowIndex].ErrorText = selectedComponent.GetComponentLastError();
                Console.Out.WriteLine("Unable to set parameter {0} with value {1}", parameterName, newValue);
            }
            else
                componentParametersDGV.Rows[e.RowIndex].ErrorText = "";
        }

        private void getSensorData_Click(object sender, EventArgs e)
        {
            ExternalComponent component = GetSelectedItem<ExternalComponent>(componentLB);
            EmotionSensor sensor = EmotionSensor.CastToDerived(component);
            if (sensor == null)
                Console.Out.WriteLine("Unable to cast component: {0} to EmotionSensor", component.GetUniqueName());

            if (bulkSensorDataChbx.Checked)
            {
                sensorDataLB.Items.Clear();
                int noOfReadings = Convert.ToInt32(bulkSensorDataNUD.Value);
                EmotionSensorReadingVector readings = new EmotionSensorReadingVector();
                sensor.GetSensorReadings(readings,noOfReadings);
                if (readings.Count < noOfReadings)
                    Console.Out.WriteLine(
                        "Could not retrieve selected number of readings, req: {0} got: {1}",
                        noOfReadings,
                        readings.Count);

                foreach (EmotionSensorReading reading in readings)
                    PrintReading(reading,false);
            }
            else
            {
                EmotionSensorReading reading = sensor.GetSensorReading();

                if (reading == null)
                {
                    Console.Out.WriteLine("Could not retrieve reading, component error: {0}", sensor.GetComponentLastError());
                    return;
                }
                Console.Out.WriteLine("Retrieve reading with size: {0}", reading.ValueCount());
                PrintReading(reading, true);
            }

        }

        private String PrintVariant(Variant data)
        {
            String ret = "";
            if (data.IsInteger())
                ret += data.AsInteger() + " [i]";
            else if (data.IsString())
                ret += data.AsString() + " [s]";
            else if (data.IsFloat())
                ret += data.AsFloat() + " [f]";
            else if (data.IsDouble())
                ret += data.AsDouble() + " [d]";
            else
                ret += " [Variant pointer data]";

            return ret;
        }

        private void PrintReading(EmotionSensorReading reading, bool toFront)
        {
            try
            {
                String readString = "[" + reading.ValueCount() + "]: ";
                for (uint i = 0; i < reading.ValueCount(); i++)
                {
                    Variant data = reading.GetValueByIndex(i);
                    readString += PrintVariant(data) + ",";
                }
                readString = readString.Substring(0, readString.Length - 1);

                if (toFront)
                    sensorDataLB.Items.Insert(0, readString);
                else
                    sensorDataLB.Items.Add(readString);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Exception while retrieving sensor data: {0}", e);
            }
        }

        private void sensorNameLabel_Click(object sender, EventArgs e)
        {

        }

        private void componentLB_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            ExternalComponent component = GetSelectedItem<ExternalComponent>(componentLB);
            if (component == null)
                return;

            componentLB.DoDragDrop(component, DragDropEffects.All);
            componentLB_SelectedIndexChanged(null, null);
        }

        private void pictureBox1_DragDrop(object sender, DragEventArgs e)
        {
          //  if(e.Data.GetData(
        }

        private void pictureBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ExternalComponent)))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void panel1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void panel1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ExternalComponent)))
            {
                ExternalComponent component = e.Data.GetData(typeof(ExternalComponent)) as ExternalComponent;
                if (component != null)
                    RegisterComponentInModel(component);
                else
                    Console.WriteLine("Null component");
            }
        }

        private void RegisterComponentInModel(ExternalComponent component)
        {
            switch (component.GetComponentType())
            {
                case ComponentType.SENSOR:
                    RegisterSensorInModel(component);
                    return;
                case ComponentType.EXPERT:
                    RegisterExpertInModel(component);
                    return;
                case ComponentType.FILTER:
                    RegisterFilterInModel(component);
                    return;
                default:
                    return;
            }

        }

        private void RegisterSensorInModel(ExternalComponent component)
        {
            EmotionSensor sensor = EmotionSensor.CastToDerived(component);
            if (sensor == null)
            {
                Console.Out.WriteLine("Unable to cast component: {0} to EmotionSensor", component.GetUniqueName());
                return;
            }

            if(characterModel.RegisterEmotionSensor(sensor)) {
                ListViewItem item = new ListViewItemWrapper(sensor);
                sensorsListView.Items.Add(item);
                sensor.Initialize();
            }
        }

        private void RegisterExpertInModel(ExternalComponent component)
        {
            ExternalExpert expert = ExternalExpert.CastToDerived(component);
            if (expert == null)
            {
                Console.Out.WriteLine("Unable to cast component: {0} to ExternalExpert", component.GetUniqueName());
                return;
            }

            if (characterModel.RegisterExpert(expert))
            {
                ListViewItem item = new ListViewItemWrapper(expert);
                expertListView.Items.Add(item);

                if (expert.GetExpertType().Equals(ExpertType.STD_EMOTION))
                {
                    EmotionModel model = ExternalExpert.CastToEmotionModel(expert);
                    item = new ListViewItemWrapper(model);
                    emotionModelListView.Items.Add(item);
                }
                else if (expert.GetExpertType().Equals(ExpertType.STD_MOOD))
                {
                    MoodModel model = ExternalExpert.CastToMoodModel(expert);
                    item = new ListViewItemWrapper(model);
                    moodModelListView.Items.Add(item);
                }
                else if (expert.GetExpertType().Equals(ExpertType.STD_PSYCHOLOGICAL))
                {
                    PsychologicalModel model = ExternalExpert.CastToPsychologicalModel(expert);
                    item = new ListViewItemWrapper(model);
                    psychologicalModelListView.Items.Add(item);
                }

                

            }
        }

        private void RegisterFilterInModel(ExternalComponent component)
        {
            ExternalFilter filter = ExternalFilter.CastToDerived(component);
            if (filter == null)
            {
                Console.Out.WriteLine("Unable to cast component: {0} to ExternalFilter", component.GetUniqueName());
                return;
            }
            EmotionVectorFilter newFilter = ExternalFilter.CastToEmotionVectorFilter(filter);
            if (newFilter == null)
            {
                Console.Out.WriteLine("Unable to cast component: {0} to EmotionVectorFilter", component.GetUniqueName());
                return;
            }

            EmotionVectorFilterType type ;
            using (FilterTypeChooseDialog dialog = new FilterTypeChooseDialog())
            {
                DialogResult result = dialog.ShowDialog();

                if (DialogResult.Yes.Equals(result))
                {
                    type = EmotionVectorFilterType.Before;
                }
                else if(DialogResult.No.Equals(result))
                {
                    type = EmotionVectorFilterType.After;
                }
                else
                {
                    return;
                }
            }

            if (characterModel.RegisterEmotionVectorFilter(newFilter, type))
            {
                ListViewItem item = new ListViewItemWrapper(filter, type);
                vectorFiltersListView.Items.Add(item);

            }
        }


        private void executeEventBtn_Click(object sender, EventArgs e)
        {
            
            if (eventCyclic)
                HandleCyclicEvent();
            else
                HandleImmediateEvent();

           
        }

        private void HandleCyclicEvent()
        {
            if (eventCyclicSendingInProgress)
            {
                //Stop sending
                cyclicEventTimer.Stop();
                cyclicEventTimer = null;
                //
                eventCyclicSendingInProgress = false;
                eventCyclicPeriodNUD.Enabled = true;
                eventTypeCB.Enabled = true;
                eventTypeNUD.Enabled = true;
                executeEventBtn.BackgroundImage = ErfCoreGui.Properties.Resources.lightning_go;
                return;
            }
            // start sending
            cyclicEventTimer = new Timer();
            cyclicEventTimer.Tick += new EventHandler(cyclicEventTimer_Tick);
            int seconds = Convert.ToInt32(eventCyclicPeriodNUD.Value);
            cyclicEventTimer.Interval = seconds * 1000;
            cyclicEventTimer.Start();
            //
            eventCyclicSendingInProgress = true;
            eventCyclicPeriodNUD.Enabled = false;
            eventTypeCB.Enabled = false;
            eventTypeNUD.Enabled = false;
            executeEventBtn.BackgroundImage = ErfCoreGui.Properties.Resources.lightning_delete;
        }

        private void cyclicEventTimer_Tick(object sender, EventArgs e)
        {
            HandleImmediateEvent();
        }

        private void HandleImmediateEvent()
        {
            int eventId = 0;
            if (eventCustom)
                eventId = Convert.ToInt32(eventTypeNUD.Value);
            else
                eventId = (int)((GameEvent)eventTypeCB.SelectedItem);

            EecEvent eecEvent = new EecEvent(eventId);
            if (characterModel.HandleEvent(eecEvent))
            {
                EmotionVector currentVector = characterModel.GetEmotionVector();
                if (currentVector == null)
                {
                    Console.Out.WriteLine("Could not generate emotion vector for event: {0}", eventId);
                    return;
                }

                RefreshVectors(eecEvent);
            }
        }

        private void RefreshVectors(EecEvent eecEvent)
        {
            vectorHistoryCB.SelectedItem = null;
            foreach (DataGridView grid in grids)
            {
                grid.Rows.Clear();
            }
            foreach (Label label in gridLabels)
            {
                label.Text = "";
            }
            // Print emotion vector
            EmotionVector vector = characterModel.GetUnfilteredEmotionVector();
            if(vector != null) {
                PrintVectorToGrid(emotionVectorGrid, vector);
                emotionVectorType.Text = GetTypesString(vector.GetEmotionModelTypes());
            }

            EmotionVector filteredvector = characterModel.GetEmotionVector();
            if (filteredvector != null)
            {
                PrintVectorToGrid(filteredVectorGrid, filteredvector);
                filteredVectorType.Text = GetTypesString(filteredvector.GetEmotionModelTypes());
            }

            MoodVector moodVector = characterModel.GetMoodVector();
            if (moodVector != null)
            {
                PrintVectorToGrid(moodVectorGrid, moodVector);
                moodVectorType.Text = GetTypesString(moodVector.GetEmotionModelTypes());
            }
            AddToVectorHistory(eecEvent, vector, filteredvector, moodVector);
        }

        private void AddToVectorHistory(EecEvent e, EmotionVector ev, EmotionVector fv, MoodVector mv)
        {
            if (ev == null && fv == null && mv == null)
                return;
            VectorHistoryItem item = new VectorHistoryItem(e,ev, fv, mv);

            vectorHistoryCB.Items.Insert(0, item);

            if (vectorHistoryCB.Items.Count > 10)
                vectorHistoryCB.Items.RemoveAt(10);
        }

        private string GetTypesString(IntegerVector types)
        {
            string typesStr = "";

            foreach (int type in types)
            {

                string typeStr = type.ToString();
                if (type < (int)EmotionModelType.EXT_OTHER)
                    typeStr = ((EmotionModelType)type).ToString();
                typesStr += typeStr + ",";
            }
            if (typesStr.Length > 0)
                typesStr = typesStr.Remove(typesStr.Length - 1);

            return typesStr;
        }

        private void PrintVectorToGrid(DataGridView grid,VariantCollection collection)
        {
            grid.Rows.Clear();
            if (collection == null)
                return;

            StringToVariantMap valuesMap = collection.GetValues();


            foreach (KeyValuePair<string, Variant> entry in valuesMap)
            {
                grid.Rows.Add(entry.Key, PrintVariant(entry.Value));
            }

        }


        private void pictureBox2_Click(object sender, EventArgs e)
        {
            sensorDataLB.Items.Clear();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (eventCyclicSendingInProgress)
                return;

            eventCustom = !eventCustom;
            eventTypeCB.Visible = !eventCustom;
            eventTypeNUD.Visible = eventCustom;
            customEventCB.Image = eventCustom ? ImagesManager.GetCheckedImage() : null;
        }

        private void eventCyclicCB_Click(object sender, EventArgs e)
        {
            if(eventCyclicSendingInProgress)
                return;

            eventCyclic = !eventCyclic;
            eventCyclicPeriodNUD.Enabled = eventCyclic;
            eventCyclicCB.Image = eventCyclic ? ImagesManager.GetCheckedImage() : null;
            executeEventBtn.BackgroundImage = eventCyclic ?
                ErfCoreGui.Properties.Resources.lightning_go :
                ErfCoreGui.Properties.Resources.lightning;
        }

        private void createExpertBtn_Click(object sender, EventArgs e)
        {
            String expertName = newComponentNameTB.Text;
            if (expertName == "")
                return;

            ExternalComponent selectedComponent = GetSelectedItem<ExternalComponent>(componentLB);
            if (selectedComponent == null)
                return;

            ExternalComponentLibrary library = ExternalComponentLibrary.CastToDerived(selectedComponent);
            try
            {
                ExternalExpert createdExpert = library.CreateExpertForName(expertName);
                if (createdExpert != null)
                {
                    creationResultLabel.Text = "Creation Result: Success";
                    PopulateComponentListBox(library);
                }

            }
            catch (Exception ex)
            {
                creationResultLabel.Text = "Creation Result: " + ex.Message;

            }
        }

        private void createSensorBtn_Click(object sender, EventArgs e)
        {
            String sensorName = newComponentNameTB.Text;
            if (sensorName == "")
                return;

            ExternalComponent selectedComponent = GetSelectedItem<ExternalComponent>(componentLB);
            if (selectedComponent == null)
                return;

            ExternalComponentLibrary library = ExternalComponentLibrary.CastToDerived(selectedComponent);
            try
            {
                EmotionSensor createdSensor = library.CreateSensorForName(sensorName);
                if (createdSensor != null)
                {
                    creationResultLabel.Text = "Creation Result: Success";
                    PopulateComponentListBox(library);
                }

            }
            catch (Exception ex)
            {
                creationResultLabel.Text = "Creation Result: " + ex.Message;

            }
        }

        private void createFilterBtn_Click(object sender, EventArgs e)
        {
            String filterName = newComponentNameTB.Text;
            if (filterName == "")
                return;

            ExternalComponent selectedComponent = GetSelectedItem<ExternalComponent>(componentLB);
            if (selectedComponent == null)
                return;

            ExternalComponentLibrary library = ExternalComponentLibrary.CastToDerived(selectedComponent);
            try
            {
                ExternalFilter createdFilter = library.CreateFilterForName(filterName);
                if (createdFilter != null)
                {
                    creationResultLabel.Text = "Creation Result: Success";
                    PopulateComponentListBox(library);
                }

            }
            catch (Exception ex)
            {
                creationResultLabel.Text = "Creation Result: " + ex.Message;

            }
        }

        private void cloneComponentPB_Click(object sender, EventArgs e)
        {
            failedToCopyPB.Visible = false;
            ExternalComponent component = GetSelectedItem<ExternalComponent>(componentLB);
            if (component == null)
                return;
            try
            {
                switch (component.GetComponentType())
                {
                    case ComponentType.SENSOR:
                        {
                            EmotionSensor sensor = EmotionSensor.CastToDerived(component);
                            EmotionSensor clone = sensor.CloneSensor();
                            if (clone == null)
                                throw new Exception("Unable to clone");

                            componentLB.Items.Add(new ListItemWrapper(clone, clone.GetName()));
                            return;
                        }
                    case ComponentType.FILTER:
                        {
                            ExternalFilter filter = ExternalFilter.CastToDerived(component);
                            ExternalFilter clone = filter.CloneFilter();
                            if (clone == null)
                                throw new Exception("Unable to clone");

                            componentLB.Items.Add(new ListItemWrapper(clone, clone.GetName()));
                            return;
                        }
                    case ComponentType.LIBRARY:
                        {

                            failedToCopyPB.Visible = true;
                            characterModelErrorTT.SetToolTip(this.failedToCopyPB, "Libraries component cannot be cloned");
                            return;
                        }
                    case ComponentType.EXPERT:
                        {
                            ExternalExpert expert = ExternalExpert.CastToDerived(component);
                            ExternalExpert clone = expert.CloneExpert();
                            if (clone == null)
                                throw new Exception("Unable to clone");

                            componentLB.Items.Add(new ListItemWrapper(clone, clone.GetName()));
                            return;
                        }
                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                failedToCopyPB.Visible = true;
                characterModelErrorTT.SetToolTip(this.failedToCopyPB, ex.Message);
            }
        }

        private void emotionEvaluationModeCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.characterModelErrorPB.Visible = false;
            try
            {
                ModelsEvaluationMode newMode = (ModelsEvaluationMode)emotionEvaluationModeCB.SelectedItem;
                this.characterModel.SetEmotionModelEvaluationMode(newMode);
            }
            catch (Exception ex)
            {
                characterModelErrorPB.Visible = true;
                characterModelErrorTT.SetToolTip(this.characterModelErrorPB, ex.Message);
            }
        }

        private void moodEvaluationModeCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.characterModelErrorPB.Visible = false;
            try {
                ModelsEvaluationMode newMode = (ModelsEvaluationMode)moodEvaluationModeCB.SelectedItem;
                this.characterModel.SetMoodModelEvaluationMode(newMode);
            }
            catch (Exception ex)
            {
                characterModelErrorPB.Visible = true;
                characterModelErrorTT.SetToolTip(this.characterModelErrorPB, ex.Message);
            }
        }



        private void emotionVectorCharacteristicsCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.characterModelErrorPB.Visible = false;
            try
            {
                EmotionVectorCharacteristics newCharacteritics = 
                    (EmotionVectorCharacteristics)emotionVectorCharacteristicsCB.SelectedItem;
                this.characterModel.SetEmotionModelEvaluationCharacteristics((int)newCharacteritics);
            }
            catch (Exception ex)
            {
                characterModelErrorPB.Visible = true;
                characterModelErrorTT.SetToolTip(this.characterModelErrorPB, ex.Message);
            }
        }

        private void moodVectorCharacteristicsCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.characterModelErrorPB.Visible = false;
            try
            {
                EmotionVectorCharacteristics newCharacteritics =
                    (EmotionVectorCharacteristics)moodVectorCharacteristicsCB.SelectedItem;
                this.characterModel.SetMoodModelEvaluationCharacteristics((int)newCharacteritics);
            }
            catch (Exception ex)
            {
                characterModelErrorPB.Visible = true;
                characterModelErrorTT.SetToolTip(this.characterModelErrorPB, ex.Message);
            }
        }

        bool logsEnabled = true;
        private void button2_Click(object sender, EventArgs e)
        {
            logsEnabled = !logsEnabled;
            if (!logsEnabled)
                button2.Text = "Enable Logs";
            else
                button2.Text = "Disable Logs";

            ErfLogger.SetLoggingEnabled(logsEnabled);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void initializeComponentBtn_Click(object sender, EventArgs e)
        {
            failedStateChangePB.Visible = false;
            ExternalComponent component = GetSelectedItem<ExternalComponent>(componentLB);
            if (component == null)
                return;

            component.Initialize();
            componentStateLabel.Text = component.GetComponentState().ToString();
            if (component.GetComponentState() == ComponentState.COMPONENT_ERROR)
            {
                failedStateChangePB.Visible = true;
                characterModelErrorTT.SetToolTip(this.failedStateChangePB, component.GetComponentLastError());
            }
        }

        private void reinitializeComponentBtn_Click(object sender, EventArgs e)
        {
            failedStateChangePB.Visible = false;
            ExternalComponent component = GetSelectedItem<ExternalComponent>(componentLB);
            if (component == null)
                return;

            component.Reinitialize();
            componentStateLabel.Text = component.GetComponentState().ToString();
            if (component.GetComponentState() == ComponentState.COMPONENT_ERROR)
            {
                failedStateChangePB.Visible = true;
                characterModelErrorTT.SetToolTip(this.failedStateChangePB, component.GetComponentLastError());
            }
        }

        private void deinitializeComponentBtn_Click(object sender, EventArgs e)
        {
            failedStateChangePB.Visible = false;
            ExternalComponent component = GetSelectedItem<ExternalComponent>(componentLB);
            if (component == null)
                return;

            component.Deinitialize();
            componentStateLabel.Text = component.GetComponentState().ToString();
            if (component.GetComponentState() == ComponentState.COMPONENT_ERROR)
            {
                failedStateChangePB.Visible = true;
                characterModelErrorTT.SetToolTip(this.failedStateChangePB, component.GetComponentLastError());
            }
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void clearCharacterModelBtn_Click(object sender, EventArgs e)
        {
            
            var confirmResult = MessageBox.Show("Are you sure to clear the character model?",
                        "Confirm model clear!",
                        MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                characterModel = new CharacterModel();
                GC.Collect();
                GC.WaitForPendingFinalizers();


                sensorsListView.Items.Clear();
                expertListView.Items.Clear();
                emotionModelListView.Items.Clear();
                vectorFiltersListView.Items.Clear();
                psychologicalModelListView.Items.Clear();
                moodModelListView.Items.Clear();


            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            vectorHistoryCB.Items.Clear();
        }

        private void vectorHistoryCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            VectorHistoryItem history = (VectorHistoryItem) vectorHistoryCB.SelectedItem;
            // Print emotion vector
            EmotionVector vector = history.EmotionVector;
            emotionVectorGrid.Rows.Clear();
            if (vector != null)
            {
                PrintVectorToGrid(emotionVectorGrid, vector);
                emotionVectorType.Text = GetTypesString(vector.GetEmotionModelTypes());
            }

            EmotionVector filteredvector = history.FilteredVector;
            filteredVectorGrid.Rows.Clear();
            if (filteredvector != null)
            {
                PrintVectorToGrid(filteredVectorGrid, filteredvector);
                filteredVectorType.Text = GetTypesString(filteredvector.GetEmotionModelTypes());
            }

            MoodVector moodVector = history.MoodVector;
            moodVectorGrid.Rows.Clear();
            if (moodVector != null)
            {
                PrintVectorToGrid(moodVectorGrid, moodVector);
                moodVectorType.Text = GetTypesString(moodVector.GetEmotionModelTypes());
            }
        }
    }
}
