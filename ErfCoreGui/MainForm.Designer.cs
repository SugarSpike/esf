﻿namespace ErfCoreGui
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.failedStateChangePB = new System.Windows.Forms.PictureBox();
            this.deinitializeComponentBtn = new System.Windows.Forms.Button();
            this.reinitializeComponentBtn = new System.Windows.Forms.Button();
            this.initializeComponentBtn = new System.Windows.Forms.Button();
            this.failedToCopyPB = new System.Windows.Forms.PictureBox();
            this.cloneComponentPB = new System.Windows.Forms.PictureBox();
            this.componentLB = new ErfCoreGui.Controls.ComponentListBox();
            this.componentStateLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.componentNameLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.componentParametersDGV = new System.Windows.Forms.DataGridView();
            this.componentFilterCB = new System.Windows.Forms.ComboBox();
            this.unloadLibraryBtn = new System.Windows.Forms.Button();
            this.loadLibraryBtn = new System.Windows.Forms.Button();
            this.librariesLB = new System.Windows.Forms.ListBox();
            this.componentDetailsTabs = new ErfCoreGui.Controls.TablessControl();
            this.emptyTB = new System.Windows.Forms.TabPage();
            this.libraryTB = new System.Windows.Forms.TabPage();
            this.creationResultLabel = new System.Windows.Forms.Label();
            this.createFilterBtn = new System.Windows.Forms.Button();
            this.createSensorBtn = new System.Windows.Forms.Button();
            this.createExpertBtn = new System.Windows.Forms.Button();
            this.newComponentNameTB = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.expertTB = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.expertTypeLabel = new System.Windows.Forms.Label();
            this.sensorsTB = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.sensorDataLB = new System.Windows.Forms.ListBox();
            this.bulkSensorDataNUD = new System.Windows.Forms.NumericUpDown();
            this.bulkSensorDataChbx = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.sensorNameLabel = new System.Windows.Forms.Label();
            this.getSensorData = new System.Windows.Forms.Button();
            this.filtersTB = new System.Windows.Forms.TabPage();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.expertListView = new System.Windows.Forms.ListView();
            this.sensorsListView = new System.Windows.Forms.ListView();
            this.emotionModelListView = new System.Windows.Forms.ListView();
            this.moodModelListView = new System.Windows.Forms.ListView();
            this.psychologicalModelListView = new System.Windows.Forms.ListView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.vectorFiltersListView = new System.Windows.Forms.ListView();
            this.vectorsTabControl = new System.Windows.Forms.TabControl();
            this.emotionTab = new System.Windows.Forms.TabPage();
            this.emotionVectorGrid = new System.Windows.Forms.DataGridView();
            this.emotionVectorType = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.emotionFilteredTab = new System.Windows.Forms.TabPage();
            this.filteredVectorGrid = new System.Windows.Forms.DataGridView();
            this.filteredVectorType = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.moodTab = new System.Windows.Forms.TabPage();
            this.moodVectorGrid = new System.Windows.Forms.DataGridView();
            this.moodVectorType = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.vectorHistoryCB = new System.Windows.Forms.ComboBox();
            this.eventCyclicPeriodNUD = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.eventCyclicCB = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.customEventCB = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.eventTypeNUD = new System.Windows.Forms.NumericUpDown();
            this.eventTypeCB = new System.Windows.Forms.ComboBox();
            this.executeEventBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.moodVectorCharacteristicsCB = new System.Windows.Forms.ComboBox();
            this.emotionVectorCharacteristicsCB = new System.Windows.Forms.ComboBox();
            this.characterModelErrorPB = new System.Windows.Forms.PictureBox();
            this.moodEvaluationModeCB = new System.Windows.Forms.ComboBox();
            this.emotionEvaluationModeCB = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.clearCharacterModelBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.failedStateChangePB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failedToCopyPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cloneComponentPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.componentParametersDGV)).BeginInit();
            this.componentDetailsTabs.SuspendLayout();
            this.libraryTB.SuspendLayout();
            this.expertTB.SuspendLayout();
            this.sensorsTB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bulkSensorDataNUD)).BeginInit();
            this.panel1.SuspendLayout();
            this.vectorsTabControl.SuspendLayout();
            this.emotionTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.emotionVectorGrid)).BeginInit();
            this.emotionFilteredTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filteredVectorGrid)).BeginInit();
            this.moodTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moodVectorGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventCyclicPeriodNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventCyclicCB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customEventCB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventTypeNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.characterModelErrorPB)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.failedStateChangePB);
            this.groupBox1.Controls.Add(this.deinitializeComponentBtn);
            this.groupBox1.Controls.Add(this.reinitializeComponentBtn);
            this.groupBox1.Controls.Add(this.initializeComponentBtn);
            this.groupBox1.Controls.Add(this.failedToCopyPB);
            this.groupBox1.Controls.Add(this.cloneComponentPB);
            this.groupBox1.Controls.Add(this.componentLB);
            this.groupBox1.Controls.Add(this.componentStateLabel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.componentNameLabel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.componentParametersDGV);
            this.groupBox1.Controls.Add(this.componentFilterCB);
            this.groupBox1.Controls.Add(this.unloadLibraryBtn);
            this.groupBox1.Controls.Add(this.loadLibraryBtn);
            this.groupBox1.Controls.Add(this.librariesLB);
            this.groupBox1.Controls.Add(this.componentDetailsTabs);
            this.groupBox1.Location = new System.Drawing.Point(21, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(917, 203);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sensors && Experts";
            // 
            // failedStateChangePB
            // 
            this.failedStateChangePB.Image = global::ErfCoreGui.Properties.Resources.exclamation;
            this.failedStateChangePB.Location = new System.Drawing.Point(585, 72);
            this.failedStateChangePB.Name = "failedStateChangePB";
            this.failedStateChangePB.Size = new System.Drawing.Size(17, 17);
            this.failedStateChangePB.TabIndex = 35;
            this.failedStateChangePB.TabStop = false;
            // 
            // deinitializeComponentBtn
            // 
            this.deinitializeComponentBtn.Location = new System.Drawing.Point(500, 69);
            this.deinitializeComponentBtn.Name = "deinitializeComponentBtn";
            this.deinitializeComponentBtn.Size = new System.Drawing.Size(82, 23);
            this.deinitializeComponentBtn.TabIndex = 34;
            this.deinitializeComponentBtn.Text = "Deinitialize";
            this.deinitializeComponentBtn.UseVisualStyleBackColor = true;
            this.deinitializeComponentBtn.Click += new System.EventHandler(this.deinitializeComponentBtn_Click);
            // 
            // reinitializeComponentBtn
            // 
            this.reinitializeComponentBtn.Location = new System.Drawing.Point(420, 69);
            this.reinitializeComponentBtn.Name = "reinitializeComponentBtn";
            this.reinitializeComponentBtn.Size = new System.Drawing.Size(81, 23);
            this.reinitializeComponentBtn.TabIndex = 33;
            this.reinitializeComponentBtn.Text = "Renitialize";
            this.reinitializeComponentBtn.UseVisualStyleBackColor = true;
            this.reinitializeComponentBtn.Click += new System.EventHandler(this.reinitializeComponentBtn_Click);
            // 
            // initializeComponentBtn
            // 
            this.initializeComponentBtn.Location = new System.Drawing.Point(333, 69);
            this.initializeComponentBtn.Name = "initializeComponentBtn";
            this.initializeComponentBtn.Size = new System.Drawing.Size(87, 23);
            this.initializeComponentBtn.TabIndex = 32;
            this.initializeComponentBtn.Text = "Initialize";
            this.initializeComponentBtn.UseVisualStyleBackColor = true;
            this.initializeComponentBtn.Click += new System.EventHandler(this.initializeComponentBtn_Click);
            // 
            // failedToCopyPB
            // 
            this.failedToCopyPB.Image = global::ErfCoreGui.Properties.Resources.exclamation;
            this.failedToCopyPB.Location = new System.Drawing.Point(307, 25);
            this.failedToCopyPB.Name = "failedToCopyPB";
            this.failedToCopyPB.Size = new System.Drawing.Size(17, 17);
            this.failedToCopyPB.TabIndex = 31;
            this.failedToCopyPB.TabStop = false;
            // 
            // cloneComponentPB
            // 
            this.cloneComponentPB.Image = global::ErfCoreGui.Properties.Resources.page_copy;
            this.cloneComponentPB.Location = new System.Drawing.Point(284, 25);
            this.cloneComponentPB.Name = "cloneComponentPB";
            this.cloneComponentPB.Size = new System.Drawing.Size(17, 17);
            this.cloneComponentPB.TabIndex = 30;
            this.cloneComponentPB.TabStop = false;
            this.cloneComponentPB.Click += new System.EventHandler(this.cloneComponentPB_Click);
            // 
            // componentLB
            // 
            this.componentLB.FormattingEnabled = true;
            this.componentLB.Location = new System.Drawing.Point(169, 50);
            this.componentLB.Name = "componentLB";
            this.componentLB.Size = new System.Drawing.Size(156, 147);
            this.componentLB.TabIndex = 25;
            this.componentLB.SelectedIndexChanged += new System.EventHandler(this.componentLB_SelectedIndexChanged);
            this.componentLB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.componentLB_MouseDown);
            // 
            // componentStateLabel
            // 
            this.componentStateLabel.AutoSize = true;
            this.componentStateLabel.Location = new System.Drawing.Point(371, 45);
            this.componentStateLabel.Name = "componentStateLabel";
            this.componentStateLabel.Size = new System.Drawing.Size(35, 13);
            this.componentStateLabel.TabIndex = 24;
            this.componentStateLabel.Text = "label2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(331, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "State:";
            // 
            // componentNameLabel
            // 
            this.componentNameLabel.AutoSize = true;
            this.componentNameLabel.Location = new System.Drawing.Point(371, 15);
            this.componentNameLabel.Name = "componentNameLabel";
            this.componentNameLabel.Size = new System.Drawing.Size(35, 13);
            this.componentNameLabel.TabIndex = 7;
            this.componentNameLabel.Text = "label2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(331, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Name:";
            // 
            // componentParametersDGV
            // 
            this.componentParametersDGV.AllowUserToAddRows = false;
            this.componentParametersDGV.AllowUserToDeleteRows = false;
            this.componentParametersDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.componentParametersDGV.Location = new System.Drawing.Point(331, 92);
            this.componentParametersDGV.Name = "componentParametersDGV";
            this.componentParametersDGV.Size = new System.Drawing.Size(272, 105);
            this.componentParametersDGV.TabIndex = 5;
            this.componentParametersDGV.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.componentParametersDGV_CellValidating);
            // 
            // componentFilterCB
            // 
            this.componentFilterCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.componentFilterCB.FormattingEnabled = true;
            this.componentFilterCB.Location = new System.Drawing.Point(169, 22);
            this.componentFilterCB.Name = "componentFilterCB";
            this.componentFilterCB.Size = new System.Drawing.Size(109, 21);
            this.componentFilterCB.TabIndex = 4;
            this.componentFilterCB.SelectedIndexChanged += new System.EventHandler(this.componentFilterCB_SelectedIndexChanged);
            // 
            // unloadLibraryBtn
            // 
            this.unloadLibraryBtn.Location = new System.Drawing.Point(88, 21);
            this.unloadLibraryBtn.Name = "unloadLibraryBtn";
            this.unloadLibraryBtn.Size = new System.Drawing.Size(75, 23);
            this.unloadLibraryBtn.TabIndex = 2;
            this.unloadLibraryBtn.Text = "Unload";
            this.unloadLibraryBtn.UseVisualStyleBackColor = true;
            this.unloadLibraryBtn.Click += new System.EventHandler(this.unloadLibraryBtn_Click);
            // 
            // loadLibraryBtn
            // 
            this.loadLibraryBtn.Location = new System.Drawing.Point(7, 21);
            this.loadLibraryBtn.Name = "loadLibraryBtn";
            this.loadLibraryBtn.Size = new System.Drawing.Size(75, 23);
            this.loadLibraryBtn.TabIndex = 1;
            this.loadLibraryBtn.Text = "Load";
            this.loadLibraryBtn.UseVisualStyleBackColor = true;
            this.loadLibraryBtn.Click += new System.EventHandler(this.loadLibraryBtn_Click);
            // 
            // librariesLB
            // 
            this.librariesLB.FormattingEnabled = true;
            this.librariesLB.Location = new System.Drawing.Point(7, 50);
            this.librariesLB.Name = "librariesLB";
            this.librariesLB.Size = new System.Drawing.Size(156, 147);
            this.librariesLB.TabIndex = 0;
            this.librariesLB.SelectedIndexChanged += new System.EventHandler(this.librariesLB_SelectedIndexChanged);
            // 
            // componentDetailsTabs
            // 
            this.componentDetailsTabs.Controls.Add(this.emptyTB);
            this.componentDetailsTabs.Controls.Add(this.libraryTB);
            this.componentDetailsTabs.Controls.Add(this.expertTB);
            this.componentDetailsTabs.Controls.Add(this.sensorsTB);
            this.componentDetailsTabs.Controls.Add(this.filtersTB);
            this.componentDetailsTabs.Location = new System.Drawing.Point(609, 7);
            this.componentDetailsTabs.Name = "componentDetailsTabs";
            this.componentDetailsTabs.SelectedIndex = 0;
            this.componentDetailsTabs.Size = new System.Drawing.Size(302, 189);
            this.componentDetailsTabs.TabIndex = 22;
            // 
            // emptyTB
            // 
            this.emptyTB.Location = new System.Drawing.Point(4, 22);
            this.emptyTB.Name = "emptyTB";
            this.emptyTB.Size = new System.Drawing.Size(294, 163);
            this.emptyTB.TabIndex = 3;
            this.emptyTB.Text = "Empty";
            this.emptyTB.UseVisualStyleBackColor = true;
            // 
            // libraryTB
            // 
            this.libraryTB.Controls.Add(this.creationResultLabel);
            this.libraryTB.Controls.Add(this.createFilterBtn);
            this.libraryTB.Controls.Add(this.createSensorBtn);
            this.libraryTB.Controls.Add(this.createExpertBtn);
            this.libraryTB.Controls.Add(this.newComponentNameTB);
            this.libraryTB.Controls.Add(this.label9);
            this.libraryTB.Location = new System.Drawing.Point(4, 22);
            this.libraryTB.Name = "libraryTB";
            this.libraryTB.Size = new System.Drawing.Size(294, 163);
            this.libraryTB.TabIndex = 2;
            this.libraryTB.Text = "Library";
            this.libraryTB.UseVisualStyleBackColor = true;
            // 
            // creationResultLabel
            // 
            this.creationResultLabel.AutoSize = true;
            this.creationResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.creationResultLabel.Location = new System.Drawing.Point(3, 87);
            this.creationResultLabel.Name = "creationResultLabel";
            this.creationResultLabel.Size = new System.Drawing.Size(102, 13);
            this.creationResultLabel.TabIndex = 31;
            this.creationResultLabel.Text = "Creation Result: ";
            // 
            // createFilterBtn
            // 
            this.createFilterBtn.Location = new System.Drawing.Point(178, 51);
            this.createFilterBtn.Name = "createFilterBtn";
            this.createFilterBtn.Size = new System.Drawing.Size(84, 23);
            this.createFilterBtn.TabIndex = 30;
            this.createFilterBtn.Text = "Create Filter";
            this.createFilterBtn.UseVisualStyleBackColor = true;
            this.createFilterBtn.Click += new System.EventHandler(this.createFilterBtn_Click);
            // 
            // createSensorBtn
            // 
            this.createSensorBtn.Location = new System.Drawing.Point(88, 51);
            this.createSensorBtn.Name = "createSensorBtn";
            this.createSensorBtn.Size = new System.Drawing.Size(84, 23);
            this.createSensorBtn.TabIndex = 29;
            this.createSensorBtn.Text = "Create Sensor";
            this.createSensorBtn.UseVisualStyleBackColor = true;
            this.createSensorBtn.Click += new System.EventHandler(this.createSensorBtn_Click);
            // 
            // createExpertBtn
            // 
            this.createExpertBtn.Location = new System.Drawing.Point(3, 51);
            this.createExpertBtn.Name = "createExpertBtn";
            this.createExpertBtn.Size = new System.Drawing.Size(79, 23);
            this.createExpertBtn.TabIndex = 28;
            this.createExpertBtn.Text = "Create Expert";
            this.createExpertBtn.UseVisualStyleBackColor = true;
            this.createExpertBtn.Click += new System.EventHandler(this.createExpertBtn_Click);
            // 
            // newComponentNameTB
            // 
            this.newComponentNameTB.Location = new System.Drawing.Point(5, 25);
            this.newComponentNameTB.Name = "newComponentNameTB";
            this.newComponentNameTB.Size = new System.Drawing.Size(281, 20);
            this.newComponentNameTB.TabIndex = 27;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(3, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "ComponentName:";
            // 
            // expertTB
            // 
            this.expertTB.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.expertTB.Controls.Add(this.label3);
            this.expertTB.Controls.Add(this.expertTypeLabel);
            this.expertTB.Location = new System.Drawing.Point(4, 22);
            this.expertTB.Name = "expertTB";
            this.expertTB.Padding = new System.Windows.Forms.Padding(3);
            this.expertTB.Size = new System.Drawing.Size(294, 163);
            this.expertTB.TabIndex = 0;
            this.expertTB.Text = "Experts";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Type:";
            // 
            // expertTypeLabel
            // 
            this.expertTypeLabel.AutoSize = true;
            this.expertTypeLabel.Location = new System.Drawing.Point(43, 9);
            this.expertTypeLabel.Name = "expertTypeLabel";
            this.expertTypeLabel.Size = new System.Drawing.Size(35, 13);
            this.expertTypeLabel.TabIndex = 24;
            this.expertTypeLabel.Text = "label3";
            // 
            // sensorsTB
            // 
            this.sensorsTB.Controls.Add(this.pictureBox2);
            this.sensorsTB.Controls.Add(this.sensorDataLB);
            this.sensorsTB.Controls.Add(this.bulkSensorDataNUD);
            this.sensorsTB.Controls.Add(this.bulkSensorDataChbx);
            this.sensorsTB.Controls.Add(this.label2);
            this.sensorsTB.Controls.Add(this.label4);
            this.sensorsTB.Controls.Add(this.sensorNameLabel);
            this.sensorsTB.Controls.Add(this.getSensorData);
            this.sensorsTB.Location = new System.Drawing.Point(4, 22);
            this.sensorsTB.Name = "sensorsTB";
            this.sensorsTB.Padding = new System.Windows.Forms.Padding(3);
            this.sensorsTB.Size = new System.Drawing.Size(294, 163);
            this.sensorsTB.TabIndex = 1;
            this.sensorsTB.Text = "Sensors";
            this.sensorsTB.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ErfCoreGui.Properties.Resources.cross;
            this.pictureBox2.Location = new System.Drawing.Point(269, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(17, 17);
            this.pictureBox2.TabIndex = 29;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // sensorDataLB
            // 
            this.sensorDataLB.FormattingEnabled = true;
            this.sensorDataLB.Location = new System.Drawing.Point(138, 30);
            this.sensorDataLB.Name = "sensorDataLB";
            this.sensorDataLB.Size = new System.Drawing.Size(150, 121);
            this.sensorDataLB.TabIndex = 28;
            // 
            // bulkSensorDataNUD
            // 
            this.bulkSensorDataNUD.Location = new System.Drawing.Point(9, 63);
            this.bulkSensorDataNUD.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.bulkSensorDataNUD.Name = "bulkSensorDataNUD";
            this.bulkSensorDataNUD.Size = new System.Drawing.Size(123, 20);
            this.bulkSensorDataNUD.TabIndex = 27;
            this.bulkSensorDataNUD.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // bulkSensorDataChbx
            // 
            this.bulkSensorDataChbx.AutoSize = true;
            this.bulkSensorDataChbx.Location = new System.Drawing.Point(9, 40);
            this.bulkSensorDataChbx.Name = "bulkSensorDataChbx";
            this.bulkSensorDataChbx.Size = new System.Drawing.Size(107, 17);
            this.bulkSensorDataChbx.TabIndex = 25;
            this.bulkSensorDataChbx.Text = "Retrieve old data";
            this.bulkSensorDataChbx.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(135, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Retrieved data:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(6, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Type:";
            // 
            // sensorNameLabel
            // 
            this.sensorNameLabel.AutoSize = true;
            this.sensorNameLabel.Location = new System.Drawing.Point(43, 9);
            this.sensorNameLabel.Name = "sensorNameLabel";
            this.sensorNameLabel.Size = new System.Drawing.Size(35, 13);
            this.sensorNameLabel.TabIndex = 3;
            this.sensorNameLabel.Text = "label3";
            this.sensorNameLabel.Click += new System.EventHandler(this.sensorNameLabel_Click);
            // 
            // getSensorData
            // 
            this.getSensorData.Location = new System.Drawing.Point(6, 90);
            this.getSensorData.Name = "getSensorData";
            this.getSensorData.Size = new System.Drawing.Size(123, 23);
            this.getSensorData.TabIndex = 1;
            this.getSensorData.Text = "Try Get Data";
            this.getSensorData.UseVisualStyleBackColor = true;
            this.getSensorData.Click += new System.EventHandler(this.getSensorData_Click);
            // 
            // filtersTB
            // 
            this.filtersTB.Location = new System.Drawing.Point(4, 22);
            this.filtersTB.Name = "filtersTB";
            this.filtersTB.Size = new System.Drawing.Size(294, 163);
            this.filtersTB.TabIndex = 4;
            this.filtersTB.Text = "Filters";
            this.filtersTB.UseVisualStyleBackColor = true;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "Component Libraries|*.dll";
            this.openFileDialog.InitialDirectory = "./";
            this.openFileDialog.Title = "Select component library to load";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(961, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // expertListView
            // 
            this.expertListView.Location = new System.Drawing.Point(17, 152);
            this.expertListView.Name = "expertListView";
            this.expertListView.Size = new System.Drawing.Size(121, 97);
            this.expertListView.TabIndex = 22;
            this.expertListView.UseCompatibleStateImageBehavior = false;
            this.expertListView.View = System.Windows.Forms.View.SmallIcon;
            // 
            // sensorsListView
            // 
            this.sensorsListView.Location = new System.Drawing.Point(16, 24);
            this.sensorsListView.Name = "sensorsListView";
            this.sensorsListView.Size = new System.Drawing.Size(121, 97);
            this.sensorsListView.TabIndex = 23;
            this.sensorsListView.UseCompatibleStateImageBehavior = false;
            this.sensorsListView.View = System.Windows.Forms.View.SmallIcon;
            // 
            // emotionModelListView
            // 
            this.emotionModelListView.Location = new System.Drawing.Point(230, 24);
            this.emotionModelListView.Name = "emotionModelListView";
            this.emotionModelListView.Size = new System.Drawing.Size(121, 97);
            this.emotionModelListView.TabIndex = 24;
            this.emotionModelListView.UseCompatibleStateImageBehavior = false;
            this.emotionModelListView.View = System.Windows.Forms.View.SmallIcon;
            // 
            // moodModelListView
            // 
            this.moodModelListView.Location = new System.Drawing.Point(371, 24);
            this.moodModelListView.Name = "moodModelListView";
            this.moodModelListView.Size = new System.Drawing.Size(121, 97);
            this.moodModelListView.TabIndex = 25;
            this.moodModelListView.UseCompatibleStateImageBehavior = false;
            this.moodModelListView.View = System.Windows.Forms.View.SmallIcon;
            // 
            // psychologicalModelListView
            // 
            this.psychologicalModelListView.Location = new System.Drawing.Point(371, 143);
            this.psychologicalModelListView.Name = "psychologicalModelListView";
            this.psychologicalModelListView.Size = new System.Drawing.Size(121, 97);
            this.psychologicalModelListView.TabIndex = 26;
            this.psychologicalModelListView.UseCompatibleStateImageBehavior = false;
            this.psychologicalModelListView.View = System.Windows.Forms.View.SmallIcon;
            // 
            // panel1
            // 
            this.panel1.AllowDrop = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.vectorFiltersListView);
            this.panel1.Controls.Add(this.vectorsTabControl);
            this.panel1.Controls.Add(this.vectorHistoryCB);
            this.panel1.Controls.Add(this.eventCyclicPeriodNUD);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.eventCyclicCB);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.customEventCB);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.eventTypeNUD);
            this.panel1.Controls.Add(this.eventTypeCB);
            this.panel1.Controls.Add(this.psychologicalModelListView);
            this.panel1.Controls.Add(this.executeEventBtn);
            this.panel1.Controls.Add(this.expertListView);
            this.panel1.Controls.Add(this.moodModelListView);
            this.panel1.Controls.Add(this.sensorsListView);
            this.panel1.Controls.Add(this.emotionModelListView);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(29, 270);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(909, 261);
            this.panel1.TabIndex = 29;
            this.panel1.DragDrop += new System.Windows.Forms.DragEventHandler(this.panel1_DragDrop);
            this.panel1.DragEnter += new System.Windows.Forms.DragEventHandler(this.panel1_DragEnter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.Location = new System.Drawing.Point(368, 127);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(129, 13);
            this.label19.TabIndex = 45;
            this.label19.Text = "Psychological Models";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.Location = new System.Drawing.Point(235, 127);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 13);
            this.label18.TabIndex = 44;
            this.label18.Text = "Vector Filters";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.Location = new System.Drawing.Point(387, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 13);
            this.label17.TabIndex = 43;
            this.label17.Text = "Mood Models";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(235, 4);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 13);
            this.label16.TabIndex = 42;
            this.label16.Text = "Emotion Models";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(13, 131);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(99, 13);
            this.label15.TabIndex = 41;
            this.label15.Text = "External Experts";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(13, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "External Sensors";
            // 
            // vectorFiltersListView
            // 
            this.vectorFiltersListView.Location = new System.Drawing.Point(230, 143);
            this.vectorFiltersListView.Name = "vectorFiltersListView";
            this.vectorFiltersListView.Size = new System.Drawing.Size(121, 97);
            this.vectorFiltersListView.TabIndex = 40;
            this.vectorFiltersListView.UseCompatibleStateImageBehavior = false;
            this.vectorFiltersListView.View = System.Windows.Forms.View.SmallIcon;
            // 
            // vectorsTabControl
            // 
            this.vectorsTabControl.Controls.Add(this.emotionTab);
            this.vectorsTabControl.Controls.Add(this.emotionFilteredTab);
            this.vectorsTabControl.Controls.Add(this.moodTab);
            this.vectorsTabControl.Location = new System.Drawing.Point(645, 82);
            this.vectorsTabControl.Name = "vectorsTabControl";
            this.vectorsTabControl.SelectedIndex = 0;
            this.vectorsTabControl.Size = new System.Drawing.Size(247, 175);
            this.vectorsTabControl.TabIndex = 39;
            // 
            // emotionTab
            // 
            this.emotionTab.Controls.Add(this.emotionVectorGrid);
            this.emotionTab.Controls.Add(this.emotionVectorType);
            this.emotionTab.Controls.Add(this.label8);
            this.emotionTab.Location = new System.Drawing.Point(4, 22);
            this.emotionTab.Name = "emotionTab";
            this.emotionTab.Padding = new System.Windows.Forms.Padding(3);
            this.emotionTab.Size = new System.Drawing.Size(239, 149);
            this.emotionTab.TabIndex = 0;
            this.emotionTab.Text = "Emotion";
            this.emotionTab.UseVisualStyleBackColor = true;
            // 
            // emotionVectorGrid
            // 
            this.emotionVectorGrid.AllowUserToAddRows = false;
            this.emotionVectorGrid.AllowUserToDeleteRows = false;
            this.emotionVectorGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.emotionVectorGrid.Location = new System.Drawing.Point(6, 24);
            this.emotionVectorGrid.Name = "emotionVectorGrid";
            this.emotionVectorGrid.RowHeadersVisible = false;
            this.emotionVectorGrid.Size = new System.Drawing.Size(227, 119);
            this.emotionVectorGrid.TabIndex = 26;
            // 
            // emotionVectorType
            // 
            this.emotionVectorType.AutoSize = true;
            this.emotionVectorType.Location = new System.Drawing.Point(42, 5);
            this.emotionVectorType.Name = "emotionVectorType";
            this.emotionVectorType.Size = new System.Drawing.Size(35, 13);
            this.emotionVectorType.TabIndex = 30;
            this.emotionVectorType.Text = "label3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(4, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 38;
            this.label8.Text = "Type:";
            // 
            // emotionFilteredTab
            // 
            this.emotionFilteredTab.Controls.Add(this.filteredVectorGrid);
            this.emotionFilteredTab.Controls.Add(this.filteredVectorType);
            this.emotionFilteredTab.Controls.Add(this.label10);
            this.emotionFilteredTab.Location = new System.Drawing.Point(4, 22);
            this.emotionFilteredTab.Name = "emotionFilteredTab";
            this.emotionFilteredTab.Padding = new System.Windows.Forms.Padding(3);
            this.emotionFilteredTab.Size = new System.Drawing.Size(239, 149);
            this.emotionFilteredTab.TabIndex = 1;
            this.emotionFilteredTab.Text = "Filtered";
            this.emotionFilteredTab.UseVisualStyleBackColor = true;
            // 
            // filteredVectorGrid
            // 
            this.filteredVectorGrid.AllowUserToAddRows = false;
            this.filteredVectorGrid.AllowUserToDeleteRows = false;
            this.filteredVectorGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.filteredVectorGrid.Location = new System.Drawing.Point(6, 24);
            this.filteredVectorGrid.Name = "filteredVectorGrid";
            this.filteredVectorGrid.RowHeadersVisible = false;
            this.filteredVectorGrid.Size = new System.Drawing.Size(227, 119);
            this.filteredVectorGrid.TabIndex = 40;
            // 
            // filteredVectorType
            // 
            this.filteredVectorType.AutoSize = true;
            this.filteredVectorType.Location = new System.Drawing.Point(42, 5);
            this.filteredVectorType.Name = "filteredVectorType";
            this.filteredVectorType.Size = new System.Drawing.Size(35, 13);
            this.filteredVectorType.TabIndex = 40;
            this.filteredVectorType.Text = "label3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(4, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 41;
            this.label10.Text = "Type:";
            // 
            // moodTab
            // 
            this.moodTab.Controls.Add(this.moodVectorGrid);
            this.moodTab.Controls.Add(this.moodVectorType);
            this.moodTab.Controls.Add(this.label11);
            this.moodTab.Location = new System.Drawing.Point(4, 22);
            this.moodTab.Name = "moodTab";
            this.moodTab.Size = new System.Drawing.Size(239, 149);
            this.moodTab.TabIndex = 2;
            this.moodTab.Text = "Mood";
            this.moodTab.UseVisualStyleBackColor = true;
            // 
            // moodVectorGrid
            // 
            this.moodVectorGrid.AllowUserToAddRows = false;
            this.moodVectorGrid.AllowUserToDeleteRows = false;
            this.moodVectorGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.moodVectorGrid.Location = new System.Drawing.Point(6, 24);
            this.moodVectorGrid.Name = "moodVectorGrid";
            this.moodVectorGrid.RowHeadersVisible = false;
            this.moodVectorGrid.Size = new System.Drawing.Size(227, 119);
            this.moodVectorGrid.TabIndex = 43;
            // 
            // moodVectorType
            // 
            this.moodVectorType.AutoSize = true;
            this.moodVectorType.Location = new System.Drawing.Point(42, 5);
            this.moodVectorType.Name = "moodVectorType";
            this.moodVectorType.Size = new System.Drawing.Size(35, 13);
            this.moodVectorType.TabIndex = 42;
            this.moodVectorType.Text = "label3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(4, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 13);
            this.label11.TabIndex = 44;
            this.label11.Text = "Type:";
            // 
            // vectorHistoryCB
            // 
            this.vectorHistoryCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.vectorHistoryCB.FormattingEnabled = true;
            this.vectorHistoryCB.Location = new System.Drawing.Point(651, 54);
            this.vectorHistoryCB.Name = "vectorHistoryCB";
            this.vectorHistoryCB.Size = new System.Drawing.Size(156, 21);
            this.vectorHistoryCB.TabIndex = 37;
            this.vectorHistoryCB.SelectedIndexChanged += new System.EventHandler(this.vectorHistoryCB_SelectedIndexChanged);
            // 
            // eventCyclicPeriodNUD
            // 
            this.eventCyclicPeriodNUD.Enabled = false;
            this.eventCyclicPeriodNUD.Location = new System.Drawing.Point(651, 28);
            this.eventCyclicPeriodNUD.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.eventCyclicPeriodNUD.Name = "eventCyclicPeriodNUD";
            this.eventCyclicPeriodNUD.Size = new System.Drawing.Size(156, 20);
            this.eventCyclicPeriodNUD.TabIndex = 36;
            this.eventCyclicPeriodNUD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(833, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Cyclic event";
            // 
            // eventCyclicCB
            // 
            this.eventCyclicCB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.eventCyclicCB.Location = new System.Drawing.Point(811, 28);
            this.eventCyclicCB.Name = "eventCyclicCB";
            this.eventCyclicCB.Size = new System.Drawing.Size(20, 20);
            this.eventCyclicCB.TabIndex = 34;
            this.eventCyclicCB.TabStop = false;
            this.eventCyclicCB.Click += new System.EventHandler(this.eventCyclicCB_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(833, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Custom Event";
            // 
            // customEventCB
            // 
            this.customEventCB.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.customEventCB.Location = new System.Drawing.Point(811, 4);
            this.customEventCB.Name = "customEventCB";
            this.customEventCB.Size = new System.Drawing.Size(20, 20);
            this.customEventCB.TabIndex = 32;
            this.customEventCB.TabStop = false;
            this.customEventCB.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::ErfCoreGui.Properties.Resources.cross;
            this.pictureBox3.Location = new System.Drawing.Point(812, 57);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(17, 17);
            this.pictureBox3.TabIndex = 30;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // eventTypeNUD
            // 
            this.eventTypeNUD.Location = new System.Drawing.Point(651, 4);
            this.eventTypeNUD.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.eventTypeNUD.Name = "eventTypeNUD";
            this.eventTypeNUD.Size = new System.Drawing.Size(156, 20);
            this.eventTypeNUD.TabIndex = 29;
            this.eventTypeNUD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // eventTypeCB
            // 
            this.eventTypeCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.eventTypeCB.FormattingEnabled = true;
            this.eventTypeCB.Location = new System.Drawing.Point(651, 4);
            this.eventTypeCB.Name = "eventTypeCB";
            this.eventTypeCB.Size = new System.Drawing.Size(156, 21);
            this.eventTypeCB.TabIndex = 26;
            // 
            // executeEventBtn
            // 
            this.executeEventBtn.BackgroundImage = global::ErfCoreGui.Properties.Resources.lightning;
            this.executeEventBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.executeEventBtn.Location = new System.Drawing.Point(617, 3);
            this.executeEventBtn.Name = "executeEventBtn";
            this.executeEventBtn.Size = new System.Drawing.Size(31, 23);
            this.executeEventBtn.TabIndex = 30;
            this.executeEventBtn.UseVisualStyleBackColor = false;
            this.executeEventBtn.Click += new System.EventHandler(this.executeEventBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(608, 250);
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.pictureBox1_DragDrop);
            this.pictureBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.pictureBox1_DragEnter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.moodVectorCharacteristicsCB);
            this.groupBox2.Controls.Add(this.emotionVectorCharacteristicsCB);
            this.groupBox2.Controls.Add(this.characterModelErrorPB);
            this.groupBox2.Controls.Add(this.moodEvaluationModeCB);
            this.groupBox2.Controls.Add(this.emotionEvaluationModeCB);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(33, 537);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(905, 68);
            this.groupBox2.TabIndex = 30;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Character Model Parameters";
            // 
            // moodVectorCharacteristicsCB
            // 
            this.moodVectorCharacteristicsCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.moodVectorCharacteristicsCB.FormattingEnabled = true;
            this.moodVectorCharacteristicsCB.Location = new System.Drawing.Point(354, 35);
            this.moodVectorCharacteristicsCB.Name = "moodVectorCharacteristicsCB";
            this.moodVectorCharacteristicsCB.Size = new System.Drawing.Size(180, 21);
            this.moodVectorCharacteristicsCB.TabIndex = 34;
            this.moodVectorCharacteristicsCB.SelectedIndexChanged += new System.EventHandler(this.moodVectorCharacteristicsCB_SelectedIndexChanged);
            // 
            // emotionVectorCharacteristicsCB
            // 
            this.emotionVectorCharacteristicsCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.emotionVectorCharacteristicsCB.FormattingEnabled = true;
            this.emotionVectorCharacteristicsCB.Location = new System.Drawing.Point(354, 10);
            this.emotionVectorCharacteristicsCB.Name = "emotionVectorCharacteristicsCB";
            this.emotionVectorCharacteristicsCB.Size = new System.Drawing.Size(180, 21);
            this.emotionVectorCharacteristicsCB.TabIndex = 33;
            this.emotionVectorCharacteristicsCB.SelectedIndexChanged += new System.EventHandler(this.emotionVectorCharacteristicsCB_SelectedIndexChanged);
            // 
            // characterModelErrorPB
            // 
            this.characterModelErrorPB.Image = global::ErfCoreGui.Properties.Resources.exclamation;
            this.characterModelErrorPB.Location = new System.Drawing.Point(147, 0);
            this.characterModelErrorPB.Name = "characterModelErrorPB";
            this.characterModelErrorPB.Size = new System.Drawing.Size(17, 17);
            this.characterModelErrorPB.TabIndex = 32;
            this.characterModelErrorPB.TabStop = false;
            // 
            // moodEvaluationModeCB
            // 
            this.moodEvaluationModeCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.moodEvaluationModeCB.FormattingEnabled = true;
            this.moodEvaluationModeCB.Location = new System.Drawing.Point(168, 35);
            this.moodEvaluationModeCB.Name = "moodEvaluationModeCB";
            this.moodEvaluationModeCB.Size = new System.Drawing.Size(180, 21);
            this.moodEvaluationModeCB.TabIndex = 27;
            this.moodEvaluationModeCB.SelectedIndexChanged += new System.EventHandler(this.moodEvaluationModeCB_SelectedIndexChanged);
            // 
            // emotionEvaluationModeCB
            // 
            this.emotionEvaluationModeCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.emotionEvaluationModeCB.FormattingEnabled = true;
            this.emotionEvaluationModeCB.Location = new System.Drawing.Point(168, 10);
            this.emotionEvaluationModeCB.Name = "emotionEvaluationModeCB";
            this.emotionEvaluationModeCB.Size = new System.Drawing.Size(180, 21);
            this.emotionEvaluationModeCB.TabIndex = 26;
            this.emotionEvaluationModeCB.SelectedIndexChanged += new System.EventHandler(this.emotionEvaluationModeCB_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(10, 38);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Mood Evaluation Mode:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(10, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(155, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Emotion Evaluation Mode:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(167, 241);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 23);
            this.button2.TabIndex = 31;
            this.button2.Text = "Disable Logging";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(268, 241);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 23);
            this.button3.TabIndex = 32;
            this.button3.Text = "Force GC";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // clearCharacterModelBtn
            // 
            this.clearCharacterModelBtn.Location = new System.Drawing.Point(24, 241);
            this.clearCharacterModelBtn.Name = "clearCharacterModelBtn";
            this.clearCharacterModelBtn.Size = new System.Drawing.Size(137, 23);
            this.clearCharacterModelBtn.TabIndex = 33;
            this.clearCharacterModelBtn.Text = "Clear Character Model";
            this.clearCharacterModelBtn.UseVisualStyleBackColor = true;
            this.clearCharacterModelBtn.Click += new System.EventHandler(this.clearCharacterModelBtn_Click);
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(961, 636);
            this.Controls.Add(this.clearCharacterModelBtn);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Erf Core Tester";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.failedStateChangePB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failedToCopyPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cloneComponentPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.componentParametersDGV)).EndInit();
            this.componentDetailsTabs.ResumeLayout(false);
            this.libraryTB.ResumeLayout(false);
            this.libraryTB.PerformLayout();
            this.expertTB.ResumeLayout(false);
            this.expertTB.PerformLayout();
            this.sensorsTB.ResumeLayout(false);
            this.sensorsTB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bulkSensorDataNUD)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.vectorsTabControl.ResumeLayout(false);
            this.emotionTab.ResumeLayout(false);
            this.emotionTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.emotionVectorGrid)).EndInit();
            this.emotionFilteredTab.ResumeLayout(false);
            this.emotionFilteredTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filteredVectorGrid)).EndInit();
            this.moodTab.ResumeLayout(false);
            this.moodTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moodVectorGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventCyclicPeriodNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventCyclicCB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customEventCB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventTypeNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.characterModelErrorPB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button unloadLibraryBtn;
        private System.Windows.Forms.Button loadLibraryBtn;
        private System.Windows.Forms.ListBox librariesLB;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ComboBox componentFilterCB;
        private System.Windows.Forms.DataGridView componentParametersDGV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label componentNameLabel;
        private Controls.TablessControl componentDetailsTabs;
        private System.Windows.Forms.TabPage expertTB;
        private System.Windows.Forms.TabPage sensorsTB;
        private System.Windows.Forms.TabPage libraryTB;
        private System.Windows.Forms.NumericUpDown bulkSensorDataNUD;
        private System.Windows.Forms.CheckBox bulkSensorDataChbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label sensorNameLabel;
        private System.Windows.Forms.Button getSensorData;
        private System.Windows.Forms.ListBox sensorDataLB;
        private System.Windows.Forms.ListView expertListView;
        private System.Windows.Forms.ListView sensorsListView;
        private System.Windows.Forms.ListView emotionModelListView;
        private System.Windows.Forms.ListView moodModelListView;
        private System.Windows.Forms.ListView psychologicalModelListView;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage emptyTB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label expertTypeLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label componentStateLabel;
        private Controls.ComponentListBox componentLB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox eventTypeCB;
        private System.Windows.Forms.Button executeEventBtn;
        private System.Windows.Forms.NumericUpDown eventTypeNUD;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.NumericUpDown eventCyclicPeriodNUD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox eventCyclicCB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox customEventCB;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.ComboBox vectorHistoryCB;
        private System.Windows.Forms.TabControl vectorsTabControl;
        private System.Windows.Forms.TabPage emotionTab;
        private System.Windows.Forms.TabPage emotionFilteredTab;
        private System.Windows.Forms.TabPage moodTab;
        private System.Windows.Forms.DataGridView emotionVectorGrid;
        private System.Windows.Forms.Label emotionVectorType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView filteredVectorGrid;
        private System.Windows.Forms.Label filteredVectorType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView moodVectorGrid;
        private System.Windows.Forms.Label moodVectorType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label creationResultLabel;
        private System.Windows.Forms.Button createFilterBtn;
        private System.Windows.Forms.Button createSensorBtn;
        private System.Windows.Forms.Button createExpertBtn;
        private System.Windows.Forms.TextBox newComponentNameTB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage filtersTB;
        private System.Windows.Forms.PictureBox cloneComponentPB;
        private System.Windows.Forms.PictureBox failedToCopyPB;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox moodEvaluationModeCB;
        private System.Windows.Forms.ComboBox emotionEvaluationModeCB;
        private System.Windows.Forms.PictureBox characterModelErrorPB;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button reinitializeComponentBtn;
        private System.Windows.Forms.Button initializeComponentBtn;
        private System.Windows.Forms.Button deinitializeComponentBtn;
        private System.Windows.Forms.PictureBox failedStateChangePB;
        private System.Windows.Forms.ListView vectorFiltersListView;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button clearCharacterModelBtn;
        private System.Windows.Forms.ComboBox moodVectorCharacteristicsCB;
        private System.Windows.Forms.ComboBox emotionVectorCharacteristicsCB;
    }
}

