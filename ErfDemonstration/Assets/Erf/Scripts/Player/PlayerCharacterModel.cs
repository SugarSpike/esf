using UnityEngine;
using System.Collections;
using Erf;
public abstract class PlayerCharacterModel 
{

	public CharacterModel Model {
		get;
		set;
	}

	public abstract bool UpdateModel(EecEvent eecEvent);

	public virtual void DeinitializeModel() { } 

	public virtual void ReinitializeModel() { }

	public abstract void PrettyPrintVectors ();

	public abstract EmotionVector GetOccVector();

	public abstract EmotionVector GetPadVector();

	public abstract EmotionVector GetPreviousOccVector();
	
	public abstract EmotionVector GetPreviousPadVector();

}

