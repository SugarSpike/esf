using UnityEngine;
using System.Collections;
using Erf;

public class PlayerOccKinectCharacterModel : PlayerCharacterModel
{
	private EmotionSensor kinectSensor;

	public PlayerOccKinectCharacterModel(){
		this.Model = BuildModel ();
	}

	public CharacterModel BuildModel()
	{
		CharacterModel model = new CharacterModel ();
		ErfContext context = ErfContext.GetInstance();
		kinectSensor = PlayerModelUtils.FindSensorByName(context,"KinectErfSensor");
		model.RegisterEmotionSensor (kinectSensor);
		ExternalExpert expert = PlayerModelUtils.InitializeExpert(model, context, "KinectEmotionModel");
		model.RegisterExpert (expert);
		return model;
	}

	public override bool UpdateModel(EecEvent eecEvent)
	{
		return this.Model.HandleEvent (eecEvent);
	}

	public override EmotionVector GetOccVector() {
		return Model.GetEmotionVector ();
	}
	
	public override EmotionVector GetPadVector(){
		return null;
	}

	public override EmotionVector GetPreviousOccVector(){
		return Model.GetCharacterData ().GetPreviousEmotionVector ();
	}
	
	public override EmotionVector GetPreviousPadVector(){
		return null;
	}

	public override void DeinitializeModel ()
	{
		if (kinectSensor != null)
			kinectSensor.Deinitialize ();
		base.DeinitializeModel ();
	}

	public override void ReinitializeModel ()
	{
		if (kinectSensor != null)
			kinectSensor.Reinitialize ();
		base.ReinitializeModel ();
	}

	int i = 1;
	public override void PrettyPrintVectors ()
	{
		float joy = this.Model.GetEmotionVector().GetValue(OccEmotions.JOY).AsFloat();
		float distress = this.Model.GetEmotionVector().GetValue(OccEmotions.DISTRESS).AsFloat();
		string message = string.Format("{0}\tj:{4:0.00}\td:{5:0.00}",i++,joy,distress);
		Debug.Log (message);
	}

}

