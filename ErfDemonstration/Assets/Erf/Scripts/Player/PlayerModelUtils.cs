using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Erf;

public class PlayerModelUtils
{
	public static EmotionSensor FindSensorByName(ErfContext context,string sensorName)
	{
		EmotionSensor sensor = context.FindSensor(sensorName);
		if (sensor == null) {
			return null;
		}
		ComponentState currentState = sensor.GetComponentState ();
		if (currentState == ComponentState.UNINITIALIZED
		    ||	currentState == ComponentState.DEINITIALIZED) {
			Debug.Log ("Initializing sensor:" + sensorName);
			sensor.Initialize ();
		} else {
			Debug.Log ("Reinitializing sensor:" + sensorName);
			sensor.Reinitialize();
		}
		if(sensor.GetComponentState() == ComponentState.COMPONENT_ERROR)
		{
			string message = sensor.GetComponentLastError();
			Debug.Log(message);
			//Debug.LogError(fileReadingSensor.GetComponentLastError());
			return null;
		}
		EmotionSensorReading reading =  sensor.GetSensorReading ();
		return sensor;
	}

	public static ExternalExpert InitializeExpert(CharacterModel model, ErfContext context, string expertName)
	{
		ExternalExpert expert = context.FindExpert(expertName);
		if (expert == null) {
			
			Debug.LogError("Could not find expert: " +expertName);
			return null;
		}
		expert.Initialize ();
		if (expert.GetComponentState () == ComponentState.COMPONENT_ERROR) {
			Debug.LogError (expert.GetComponentLastError ());
			Debug.Log (expert.GetComponentLastError ());
			return null;
		}
		return expert;
	}
}

