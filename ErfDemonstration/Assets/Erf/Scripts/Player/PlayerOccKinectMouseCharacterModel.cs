using UnityEngine;
using System.Collections;
using Erf;

public class PlayerOccKinectMouseCharacterModel : PlayerCharacterModel
{
	private EmotionSensor mouseSensor;
	private EmotionSensor kinectSensor;

	public PlayerOccKinectMouseCharacterModel(){
		this.Model = BuildModel ();
	}

	private CharacterModel BuildModel()
	{
		CharacterModel model = new CharacterModel ();
		ErfContext context = ErfContext.GetInstance();
		mouseSensor = PlayerModelUtils.FindSensorByName(context,"MouseSensor");
		model.RegisterEmotionSensor (mouseSensor);
		ExternalExpert expert = PlayerModelUtils.InitializeExpert(model, context, "ErfDemonstrationExpert");
		model.RegisterExpert (expert);
		
		kinectSensor = PlayerModelUtils.FindSensorByName(context,"KinectErfSensor");
		model.RegisterEmotionSensor (kinectSensor);
		expert = PlayerModelUtils.InitializeExpert(model, context, "KinectEmotionModel");
		model.RegisterExpert (expert);
		
		model.SetEmotionModelEvaluationMode (ModelsEvaluationMode.STD_WEIGHTED_AVERAGE);
		model.SetEmotionModelEvaluationCharacteristics ((int)EmotionVectorCharacteristics.STD_VECTOR_OF_FLOATS);

		return model;
	}

	public override bool UpdateModel(EecEvent eecEvent)
	{
		eecEvent.Clear ();
		GameStats currentStats = GameManager.Instance.GameStats [0];
		eecEvent.AddValue ("ErfDroppedShots", Variant.Create(currentStats.DroppedBalls));
		eecEvent.AddValue ("ErfMissedShots", Variant.Create(currentStats.MissedBalls));
		eecEvent.AddValue ("ErfSuccessfulShots", Variant.Create(currentStats.ScoredBalls));
		return this.Model.HandleEvent (eecEvent);
	}

	public override EmotionVector GetOccVector() {
		return Model.GetEmotionVector ();
	}
	
	public override EmotionVector GetPadVector(){
		return null;
	}

	public override EmotionVector GetPreviousOccVector(){
		return Model.GetCharacterData ().GetPreviousEmotionVector ();
	}
	
	public override EmotionVector GetPreviousPadVector(){
		return null;
	}

	public override void DeinitializeModel ()
	{
		if (mouseSensor != null)
			mouseSensor.Deinitialize ();
		if (kinectSensor != null)
			kinectSensor.Deinitialize ();
		base.DeinitializeModel ();
	}
	
	public override void ReinitializeModel ()
	{
		if (kinectSensor != null)
			kinectSensor.Reinitialize ();
		if (mouseSensor != null)
			mouseSensor.Reinitialize ();
		base.ReinitializeModel ();
	}

	int i = 1;
	public override void PrettyPrintVectors ()
	{
		float joy = this.Model.GetEmotionVector().GetValue(OccEmotions.JOY).AsFloat();
		float distress = this.Model.GetEmotionVector().GetValue(OccEmotions.DISTRESS).AsFloat();
		string message = string.Format("{0}\tj:{4:0.00}\td:{5:0.00}",i++,joy,distress);
		Debug.Log (message);
	}

}

