﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SolidColor : MonoBehaviour {

	public Color ObjectColor;
	
	private Color currentColor;
	private Material materialColored;


	// Use this for initialization
	void Start () {
		materialColored = new Material(Shader.Find("Diffuse"));
	
	}
	
	// Update is called once per frame
	void Update () {
		if (ObjectColor != currentColor)
		{
			//stop the leaks
			//if (materialColored != null)
			//	UnityEditor.AssetDatabase.DeleteAsset(UnityEditor.AssetDatabase.GetAssetPath(materialColored));
			
			//create a new material
			this.GetComponent<Renderer>().material.color = currentColor = ObjectColor;
		}
	}
}
