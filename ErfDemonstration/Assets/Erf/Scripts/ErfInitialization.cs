﻿using UnityEngine;
using System.Collections;
using Erf;
using System.IO;
using System;
public class ErfInitialization : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	void Awake() { 
		Debug.Log ("Initializing ERF for ErfDemo, trying to load "+Directory.GetCurrentDirectory()+"\\ExternalComponentLibrary.dll");
		//ErfLogger.SetLoggingEnabled (true);
		//ErfLogger.SetAllStreamsToLoggingFile (@"d://debug.txt");
		ErfContext context = ErfContext.GetInstance ();
		try {
			ExternalComponentLibrary library = ErfContext.GetInstance ().LoadComponentLibrary ("ExternalComponentLibrary.dll");
				if (library == null) { 
					Debug.LogError("Could not load ExternalComponentLibrary library");
					return;
				}
		} catch (Exception e)
		{
			Debug.Log (e.Message);
		}
		//ExternalComponentLibrary library = ErfContext.GetInstance ().LoadComponentLibrary ("D://Studia//MGR//SourceCode//ErfDemonstration//ExternalComponentLibrary");
		try {
			ExternalComponentLibrary library = ErfContext.GetInstance ().LoadComponentLibrary ("KinectErf.dll");
			if (library == null) { 
				Debug.LogError("Could not load KinectErf library");
				return;
			}
		} catch (Exception e)
		{
			Debug.Log (e.Message);
		}

		Debug.Log ("Library successfully loaded");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
