using UnityEngine;
using System.Collections;
using System.Diagnostics;
public class GameProfilerStopWatch : GameProfilerMeasurer
{

	private Stopwatch playerWatch;
	private Stopwatch ballWatch;

	public override void PlayerPassStart(){ 
		playerWatch = Stopwatch.StartNew ();
	}
	
	public override GameProfilerEntry PlayerPassStop(){ 
		playerWatch.Stop ();
		GameProfilerEntry entry = new GameProfilerEntry();
		entry.ElapsedTime = playerWatch.Elapsed.TotalMilliseconds;
		entry.NoOfObjects = 1;
		return entry;
	}
	
	public override void BallFirstPassStart(){ 
		ballWatch = Stopwatch.StartNew ();
	}
	
	public override GameProfilerEntry BallFirstPassStop(int noOfBalls){ 
		ballWatch.Stop ();
		GameProfilerEntry entry = new GameProfilerEntry();
		entry.ElapsedTime = ballWatch.Elapsed.TotalMilliseconds;
		entry.NoOfObjects = noOfBalls;
		return entry;
	}
	
	public override void BallSecondPassStart(){ 
		ballWatch = Stopwatch.StartNew ();
	}
	
	public override GameProfilerEntry BallSecondPassStop(int noOfBalls){ 
		ballWatch.Stop ();
		GameProfilerEntry entry = new GameProfilerEntry();
		entry.ElapsedTime = ballWatch.Elapsed.TotalMilliseconds;
		entry.NoOfObjects = noOfBalls;
		return entry;
	}
}

