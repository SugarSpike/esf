using UnityEngine;
using System.Collections;
using Erf;

public partial class GuiHolderScript : MonoBehaviour {
	
	public Texture PanelTexture = null;
	public Texture GrayPowerslider = null;
	public Texture Powerslider = null;
	public Texture Timeout = null;
	
	private void RenderSliders(float currentPower,float timeLeft)
	{
		const float slidersWidth = 50;
		const float slidersMargin = 5;
		float pointX = Screen.width - slidersWidth*2 - slidersMargin;
		float pointY = 10; // Screen.height - (PanelHeight + HeaderHeight + 110);
		
		float slidersHeight = Screen.height - HeaderHeight - PanelHeight - pointY;
		//if(rightPanelExpanded)
		//	slidersHeight -=  PanelHeight;
		//float slidersHeight =
		
		
		
		GUI.Box (new Rect (pointX-slidersMargin, pointY-slidersMargin, 
		                   (slidersWidth+slidersMargin)*2, slidersHeight+slidersMargin*2), "");
		GUI.DrawTexture (new Rect (pointX+slidersWidth, pointY, 
		                           slidersWidth, slidersHeight),
		                 GrayPowerslider);
		
		float powerPercentage = currentPower / 100 * slidersHeight;
		GUI.BeginGroup(new Rect (pointX+slidersWidth, pointY+powerPercentage, 
		                         slidersWidth, slidersHeight));
		GUI.DrawTexture (new Rect(0,-powerPercentage,slidersWidth,slidersHeight),
		                 Powerslider);
		GUI.EndGroup();
		
		float timePercentage = timeLeft / 100 * slidersHeight;
		GUI.BeginGroup(new Rect (pointX, pointY+timePercentage, 
		                         slidersWidth, slidersHeight));
		GUI.DrawTexture (new Rect(0,-timePercentage,slidersWidth,slidersHeight),
		                 Timeout);
		GUI.EndGroup();
	}
}

