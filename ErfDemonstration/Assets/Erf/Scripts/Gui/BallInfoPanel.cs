using UnityEngine;
using System.Collections;
using Erf;

public partial class GuiHolderScript : MonoBehaviour {
	
	public GUIStyle RightHeaderStyle = null;
	bool rightPanelExpanded = false;
	bool rightPanelIsExpanding = false;
	
	private void RenderBallInformation(FeelBall selectedBall)
	{
		if (!rightPanelExpanded) {
			if (GUI.Button (new Rect (Screen.width - PanelWidth, 
			                          Screen.height - HeaderHeight, 
			                          PanelWidth, HeaderHeight), 
			                selectedBall.gameObject.name, 
			                RightHeaderStyle))
				rightPanelExpanded = true;
			
			return;
		} 
		if (GUI.Button (new Rect (Screen.width - PanelWidth, 
		                          Screen.height - (HeaderHeight + PanelHeight), 
		                          PanelWidth, HeaderHeight), 
		                selectedBall.gameObject.name, 
		                RightHeaderStyle))
			rightPanelExpanded = false;
		GUI.DrawTexture (new Rect (Screen.width - PanelWidth, Screen.height - PanelHeight, PanelWidth, PanelHeight), PanelTexture);
		EmotionVector currentBallEmotions = selectedBall.EmotionalModel.GetEmotionVector ();
		if (currentBallEmotions != null) {
			//Debug.Log(string.Format("Before GUI: {0}", currentBallEmotions.Print()));
			double currentJoy = 0.0;
			Variant joyVariant = currentBallEmotions.GetValue (OccEmotions.JOY);
			if (joyVariant != null)
				currentJoy = joyVariant.AsDouble ();
			
			double currentDistress = 0.0;
			Variant distressVariant = currentBallEmotions.GetValue (OccEmotions.DISTRESS);
			if (distressVariant != null)
				currentDistress = distressVariant.AsDouble ();
			
			string joyDisplay = string.Format ("Joy: {0:0.00000}", currentJoy);
			string distressDisplay = string.Format ("Distress: {0:0.00000}", currentDistress);
			string behaviourDisplay = string.Format ("Behaviour: {0}", selectedBall.Ai.CurrentState);
			string relationShipDisplay = string.Format ("Relationship: {0:0.000}", selectedBall.personality.relationshipToPlayer);
			GUI.Box (new Rect (Screen.width - PanelWidth, Screen.height - PanelHeight, PanelWidth, PanelHeight), joyDisplay);
			GUI.Box (new Rect (Screen.width - PanelWidth, Screen.height - PanelHeight + linesHeight, PanelWidth, PanelHeight), distressDisplay);
			GUI.Box (new Rect (Screen.width - PanelWidth, Screen.height - PanelHeight + (linesHeight * 2), PanelWidth, PanelHeight), behaviourDisplay);
			GUI.Box (new Rect (Screen.width - PanelWidth, Screen.height - PanelHeight + (linesHeight * 3), PanelWidth, PanelHeight), relationShipDisplay);
		}
		
	}
	
	
}

