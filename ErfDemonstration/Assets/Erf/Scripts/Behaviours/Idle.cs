﻿using UnityEngine;
using System;
using System.Collections;

public class Idle : AiBehavior {

	#region Events & Delegates
	public delegate void EnterEventHandler(object sender, EventArgs e);
	public event EnterEventHandler OnEnter;
	
	public delegate void UpdateEventHandler(object sender, EventArgs e);
	public event UpdateEventHandler OnUpdate;
	
	public delegate void ExitEventHandler(object sender, EventArgs e);
	public event ExitEventHandler OnExit;
	#endregion
	
	#region Initialization
	
	public Idle() { }
	
	#endregion
	
	public override void Enter()
	{
		if (OnEnter != null)
			OnEnter(this, new BehaviorEventArgs() { Owner = Owner });

	}

	public override void Update()
	{
		if (OnUpdate != null)
			OnUpdate(this, new BehaviorEventArgs() { Owner = Owner });

	}
	
	public override void Exit()
	{
		if (OnExit != null)
			OnExit(this, new BehaviorEventArgs() { Owner = Owner });
	}

}
