#pragma once
#include "ClipsCommon.h"
#include "Personality.h"

namespace erfclips
{
    class ESFCLIPS_API OccClipsPersonality : public erf::Personality
    {
		public:
			
			static const std::string PERSONALITY_ATTR;
			static const std::string RELATIONSHIPS_ATTR;

			OccClipsPersonality();
			~OccClipsPersonality();
			
			float relationshipToPlayer;
			float optimistic;
			float choleric;
			float extravert;
			float social;
			float neurotic;

			static OccClipsPersonality* CastToDerived(erf::Personality* base) {
				return dynamic_cast<OccClipsPersonality*>(base);
			}
	};
}