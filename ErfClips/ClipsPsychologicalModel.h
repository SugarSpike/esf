#pragma once
#include "ClipsCommon.h"
#include "PsychologicalModel.h"
#include "clipscpp.h"
//#include <boost/thread/thread.hpp>
using namespace std;
using namespace erf;

namespace erf {
	class EmotionVector;
}

namespace erfclips
{
    class ESFCLIPS_API ClipsPsychologicalModel : public PsychologicalModel
    {
	private:
		float GetEmotionChange(EmotionVector* current, EmotionVector* previous, string emotionName);
		float RandomFloat(float min, float max);
	public:
		ClipsPsychologicalModel();
		~ClipsPsychologicalModel();
		
		virtual float GetModelRank() { return 1.0f; }
		
		virtual void InitializeForCharacter(CharacterData& characterData);

		virtual void UpdatePersonality(EecEvent& event, CharacterData& characterData);

		static ClipsPsychologicalModel* CastToDerived(PsychologicalModel* base) {
			return dynamic_cast<ClipsPsychologicalModel*>(base);
		}

	};
}