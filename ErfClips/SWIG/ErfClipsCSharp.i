/* File : ErfClipsCSharp.i */
%module ErfClips
#ifdef ESFCLIPS_EXPORTS
    #define ESFCLIPS_API __declspec(dllexport)
#else
    #define ESFCLIPS_API __declspec(dllimport)
#endif

%import "../ErfCore/SWIG/ErfCoreBase.i"

InitializeErfCore(../ErfCore/Swig/,../../ErfCore/,../../../ErfCore/);

%typemap(csimports) SWIGTYPE
%{
using System;
using System.Runtime.InteropServices;
using Erf;
%}


//swig support for std -> std_list.i not included in base SWIG installation:
//Included in patch: http://sourceforge.net/p/swig/patches/134/
//It is far from perfect and that is why we ignore 302 warn
#pragma SWIG nowarn=302 //Warning(302): Identifier 'list' redefined (ignored),


%shared_ptr(erfclips::OccClipsPersonality)
%shared_ptr(erfclips::ClipsVariant)
%shared_ptr(erfclips::ClipsEmotionModel)
%shared_ptr(erfclips::ClipsPsychologicalModel)


%inline %{
	#include "../../OccClipsPersonality.h"
	#include "../../ClipsEmotionModel.h"
	#include "../../ClipsVariant.h"
	#include "../../ClipsPsychologicalModel.h"
%}

ExternalVariantSupport(erfclips,ClipsVariant,erfclips,OccClipsPersonality)
ExternalVariantSupport(erfclips,ClipsVariant,erfclips,ClipsEmotionModel)
ExternalVariantSupport(erfclips,ClipsVariant,erfclips,ClipsPsychologicalModel)


%include "../OccClipsPersonality.h"
%include "../ClipsEmotionModel.h"
%include "../ClipsVariant.h"
%include "../ClipsPsychologicalModel.h"
