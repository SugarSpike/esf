/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace ErfClips {

using System;
using System.Runtime.InteropServices;

public class EecEvent : VariantCollection {
  private HandleRef swigCPtr;
  private bool swigCMemOwnDerived;

  public EecEvent(IntPtr cPtr, bool cMemoryOwn) : base(ErfClipsPINVOKE.EecEvent_SWIGSmartPtrUpcast(cPtr), true) {
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  public static HandleRef getCPtr(EecEvent obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~EecEvent() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwnDerived) {
          swigCMemOwnDerived = false;
          ErfClipsPINVOKE.delete_EecEvent(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public EecEvent(int eventType) : this(ErfClipsPINVOKE.new_EecEvent__SWIG_0(eventType), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public EecEvent(int eventType, long timeOfEvent) : this(ErfClipsPINVOKE.new_EecEvent__SWIG_1(eventType, timeOfEvent), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public int GetEventType() {
    int ret = ErfClipsPINVOKE.EecEvent_GetEventType(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public long GetTimeOfEvent() {
    long ret = ErfClipsPINVOKE.EecEvent_GetTimeOfEvent(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

}

}
