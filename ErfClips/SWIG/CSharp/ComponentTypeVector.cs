/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace ErfClips {

using System;
using System.Runtime.InteropServices;

public class ComponentTypeVector : IDisposable, System.Collections.IEnumerable
#if !SWIG_DOTNET_1
    , System.Collections.Generic.IEnumerable<ComponentType>
#endif
 {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal ComponentTypeVector(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(ComponentTypeVector obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ComponentTypeVector() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          ErfClipsPINVOKE.delete_ComponentTypeVector(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public ComponentTypeVector(System.Collections.ICollection c) : this() {
    if (c == null)
      throw new ArgumentNullException("c");
    foreach (ComponentType element in c) {
      this.Add(element);
    }
  }

  public bool IsFixedSize {
    get {
      return false;
    }
  }

  public bool IsReadOnly {
    get {
      return false;
    }
  }

  public ComponentType this[int index]  {
    get {
      return getitem(index);
    }
    set {
      setitem(index, value);
    }
  }

  public int Capacity {
    get {
      return (int)capacity();
    }
    set {
      if (value < size())
        throw new ArgumentOutOfRangeException("Capacity");
      reserve((uint)value);
    }
  }

  public int Count {
    get {
      return (int)size();
    }
  }

  public bool IsSynchronized {
    get {
      return false;
    }
  }

#if SWIG_DOTNET_1
  public void CopyTo(System.Array array)
#else
  public void CopyTo(ComponentType[] array)
#endif
  {
    CopyTo(0, array, 0, this.Count);
  }

#if SWIG_DOTNET_1
  public void CopyTo(System.Array array, int arrayIndex)
#else
  public void CopyTo(ComponentType[] array, int arrayIndex)
#endif
  {
    CopyTo(0, array, arrayIndex, this.Count);
  }

#if SWIG_DOTNET_1
  public void CopyTo(int index, System.Array array, int arrayIndex, int count)
#else
  public void CopyTo(int index, ComponentType[] array, int arrayIndex, int count)
#endif
  {
    if (array == null)
      throw new ArgumentNullException("array");
    if (index < 0)
      throw new ArgumentOutOfRangeException("index", "Value is less than zero");
    if (arrayIndex < 0)
      throw new ArgumentOutOfRangeException("arrayIndex", "Value is less than zero");
    if (count < 0)
      throw new ArgumentOutOfRangeException("count", "Value is less than zero");
    if (array.Rank > 1)
      throw new ArgumentException("Multi dimensional array.", "array");
    if (index+count > this.Count || arrayIndex+count > array.Length)
      throw new ArgumentException("Number of elements to copy is too large.");
    for (int i=0; i<count; i++)
      array.SetValue(getitemcopy(index+i), arrayIndex+i);
  }

#if !SWIG_DOTNET_1
  System.Collections.Generic.IEnumerator<ComponentType> System.Collections.Generic.IEnumerable<ComponentType>.GetEnumerator() {
    return new ComponentTypeVectorEnumerator(this);
  }
#endif

  System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
    return new ComponentTypeVectorEnumerator(this);
  }

  public ComponentTypeVectorEnumerator GetEnumerator() {
    return new ComponentTypeVectorEnumerator(this);
  }

  // Type-safe enumerator
  /// Note that the IEnumerator documentation requires an InvalidOperationException to be thrown
  /// whenever the collection is modified. This has been done for changes in the size of the
  /// collection but not when one of the elements of the collection is modified as it is a bit
  /// tricky to detect unmanaged code that modifies the collection under our feet.
  public sealed class ComponentTypeVectorEnumerator : System.Collections.IEnumerator
#if !SWIG_DOTNET_1
    , System.Collections.Generic.IEnumerator<ComponentType>
#endif
  {
    private ComponentTypeVector collectionRef;
    private int currentIndex;
    private object currentObject;
    private int currentSize;

    public ComponentTypeVectorEnumerator(ComponentTypeVector collection) {
      collectionRef = collection;
      currentIndex = -1;
      currentObject = null;
      currentSize = collectionRef.Count;
    }

    // Type-safe iterator Current
    public ComponentType Current {
      get {
        if (currentIndex == -1)
          throw new InvalidOperationException("Enumeration not started.");
        if (currentIndex > currentSize - 1)
          throw new InvalidOperationException("Enumeration finished.");
        if (currentObject == null)
          throw new InvalidOperationException("Collection modified.");
        return (ComponentType)currentObject;
      }
    }

    // Type-unsafe IEnumerator.Current
    object System.Collections.IEnumerator.Current {
      get {
        return Current;
      }
    }

    public bool MoveNext() {
      int size = collectionRef.Count;
      bool moveOkay = (currentIndex+1 < size) && (size == currentSize);
      if (moveOkay) {
        currentIndex++;
        currentObject = collectionRef[currentIndex];
      } else {
        currentObject = null;
      }
      return moveOkay;
    }

    public void Reset() {
      currentIndex = -1;
      currentObject = null;
      if (collectionRef.Count != currentSize) {
        throw new InvalidOperationException("Collection modified.");
      }
    }

#if !SWIG_DOTNET_1
    public void Dispose() {
        currentIndex = -1;
        currentObject = null;
    }
#endif
  }

  public void Clear() {
    ErfClipsPINVOKE.ComponentTypeVector_Clear(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void Add(ComponentType x) {
    ErfClipsPINVOKE.ComponentTypeVector_Add(swigCPtr, (int)x);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  private uint size() {
    uint ret = ErfClipsPINVOKE.ComponentTypeVector_size(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private uint capacity() {
    uint ret = ErfClipsPINVOKE.ComponentTypeVector_capacity(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private void reserve(uint n) {
    ErfClipsPINVOKE.ComponentTypeVector_reserve(swigCPtr, n);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public ComponentTypeVector() : this(ErfClipsPINVOKE.new_ComponentTypeVector__SWIG_0(), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public ComponentTypeVector(ComponentTypeVector other) : this(ErfClipsPINVOKE.new_ComponentTypeVector__SWIG_1(ComponentTypeVector.getCPtr(other)), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public ComponentTypeVector(int capacity) : this(ErfClipsPINVOKE.new_ComponentTypeVector__SWIG_2(capacity), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  private ComponentType getitemcopy(int index) {
    ComponentType ret = (ComponentType)ErfClipsPINVOKE.ComponentTypeVector_getitemcopy(swigCPtr, index);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private ComponentType getitem(int index) {
    ComponentType ret = (ComponentType)ErfClipsPINVOKE.ComponentTypeVector_getitem(swigCPtr, index);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private void setitem(int index, ComponentType val) {
    ErfClipsPINVOKE.ComponentTypeVector_setitem(swigCPtr, index, (int)val);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void AddRange(ComponentTypeVector values) {
    ErfClipsPINVOKE.ComponentTypeVector_AddRange(swigCPtr, ComponentTypeVector.getCPtr(values));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public ComponentTypeVector GetRange(int index, int count) {
    IntPtr cPtr = ErfClipsPINVOKE.ComponentTypeVector_GetRange(swigCPtr, index, count);
    ComponentTypeVector ret = (cPtr == IntPtr.Zero) ? null : new ComponentTypeVector(cPtr, true);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void Insert(int index, ComponentType x) {
    ErfClipsPINVOKE.ComponentTypeVector_Insert(swigCPtr, index, (int)x);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void InsertRange(int index, ComponentTypeVector values) {
    ErfClipsPINVOKE.ComponentTypeVector_InsertRange(swigCPtr, index, ComponentTypeVector.getCPtr(values));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void RemoveAt(int index) {
    ErfClipsPINVOKE.ComponentTypeVector_RemoveAt(swigCPtr, index);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void RemoveRange(int index, int count) {
    ErfClipsPINVOKE.ComponentTypeVector_RemoveRange(swigCPtr, index, count);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public static ComponentTypeVector Repeat(ComponentType value, int count) {
    IntPtr cPtr = ErfClipsPINVOKE.ComponentTypeVector_Repeat((int)value, count);
    ComponentTypeVector ret = (cPtr == IntPtr.Zero) ? null : new ComponentTypeVector(cPtr, true);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void Reverse() {
    ErfClipsPINVOKE.ComponentTypeVector_Reverse__SWIG_0(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void Reverse(int index, int count) {
    ErfClipsPINVOKE.ComponentTypeVector_Reverse__SWIG_1(swigCPtr, index, count);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void SetRange(int index, ComponentTypeVector values) {
    ErfClipsPINVOKE.ComponentTypeVector_SetRange(swigCPtr, index, ComponentTypeVector.getCPtr(values));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

}

}
