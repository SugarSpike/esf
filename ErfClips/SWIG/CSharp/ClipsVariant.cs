/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace ErfClips {

using System;
using System.Runtime.InteropServices;
using Erf;

public class ClipsVariant : Variant {
  private HandleRef swigCPtr;
  private bool swigCMemOwnDerived;

  public ClipsVariant(IntPtr cPtr, bool cMemoryOwn) : base(ErfClipsPINVOKE.ClipsVariant_SWIGSmartPtrUpcast(cPtr), true) {
    swigCMemOwnDerived = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  public static HandleRef getCPtr(ClipsVariant obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ClipsVariant() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwnDerived) {
          swigCMemOwnDerived = false;
          ErfClipsPINVOKE.delete_ClipsVariant(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public static Variant CreateOccClipsPersonality(OccClipsPersonality arg) {
	IntPtr cPtr = ErfClipsPINVOKE.ClipsVariant_CreateOccClipsPersonality(OccClipsPersonality.getCPtr(arg));
	Variant ret = (cPtr == IntPtr.Zero) ? null : new Variant(cPtr, false);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
	(ret as ClipsVariant).SetVariantValue(arg);
	return ret;
}

  public bool IsOccClipsPersonality() {
    bool ret = ErfClipsPINVOKE.ClipsVariant_IsOccClipsPersonality(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public OccClipsPersonality AsOccClipsPersonality() {
    IntPtr cPtr = ErfClipsPINVOKE.ClipsVariant_AsOccClipsPersonality(swigCPtr);
    OccClipsPersonality ret = (cPtr == IntPtr.Zero) ? null : new OccClipsPersonality(cPtr, true);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public static Variant CreateClipsEmotionModel(ClipsEmotionModel arg) {
	IntPtr cPtr = ErfClipsPINVOKE.ClipsVariant_CreateClipsEmotionModel(ClipsEmotionModel.getCPtr(arg));
	Variant ret = (cPtr == IntPtr.Zero) ? null : new Variant(cPtr, false);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
	(ret as ClipsVariant).SetVariantValue(arg);
	return ret;
}

  public bool IsClipsEmotionModel() {
    bool ret = ErfClipsPINVOKE.ClipsVariant_IsClipsEmotionModel(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public ClipsEmotionModel AsClipsEmotionModel() {
    IntPtr cPtr = ErfClipsPINVOKE.ClipsVariant_AsClipsEmotionModel(swigCPtr);
    ClipsEmotionModel ret = (cPtr == IntPtr.Zero) ? null : new ClipsEmotionModel(cPtr, true);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public ClipsVariant() : this(ErfClipsPINVOKE.new_ClipsVariant(), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

}

}
