/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace ErfClips {

using System;
using System.Runtime.InteropServices;

public class ErfExternalComponentException : ErfException {
  private HandleRef swigCPtr;

  internal ErfExternalComponentException(IntPtr cPtr, bool cMemoryOwn) : base(ErfClipsPINVOKE.ErfExternalComponentException_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(ErfExternalComponentException obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ErfExternalComponentException() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          ErfClipsPINVOKE.delete_ErfExternalComponentException(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public ErfExternalComponentException(string exceptionMessage) : this(ErfClipsPINVOKE.new_ErfExternalComponentException(exceptionMessage), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

}

}
