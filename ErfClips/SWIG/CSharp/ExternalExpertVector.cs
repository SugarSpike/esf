/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace ErfClips {

using System;
using System.Runtime.InteropServices;

public class ExternalExpertVector : IDisposable, System.Collections.IEnumerable
#if !SWIG_DOTNET_1
    , System.Collections.Generic.IList<ExternalExpert>
#endif
 {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal ExternalExpertVector(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(ExternalExpertVector obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~ExternalExpertVector() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          ErfClipsPINVOKE.delete_ExternalExpertVector(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public ExternalExpertVector(System.Collections.ICollection c) : this() {
    if (c == null)
      throw new ArgumentNullException("c");
    foreach (ExternalExpert element in c) {
      this.Add(element);
    }
  }

  public bool IsFixedSize {
    get {
      return false;
    }
  }

  public bool IsReadOnly {
    get {
      return false;
    }
  }

  public ExternalExpert this[int index]  {
    get {
      return getitem(index);
    }
    set {
      setitem(index, value);
    }
  }

  public int Capacity {
    get {
      return (int)capacity();
    }
    set {
      if (value < size())
        throw new ArgumentOutOfRangeException("Capacity");
      reserve((uint)value);
    }
  }

  public int Count {
    get {
      return (int)size();
    }
  }

  public bool IsSynchronized {
    get {
      return false;
    }
  }

#if SWIG_DOTNET_1
  public void CopyTo(System.Array array)
#else
  public void CopyTo(ExternalExpert[] array)
#endif
  {
    CopyTo(0, array, 0, this.Count);
  }

#if SWIG_DOTNET_1
  public void CopyTo(System.Array array, int arrayIndex)
#else
  public void CopyTo(ExternalExpert[] array, int arrayIndex)
#endif
  {
    CopyTo(0, array, arrayIndex, this.Count);
  }

#if SWIG_DOTNET_1
  public void CopyTo(int index, System.Array array, int arrayIndex, int count)
#else
  public void CopyTo(int index, ExternalExpert[] array, int arrayIndex, int count)
#endif
  {
    if (array == null)
      throw new ArgumentNullException("array");
    if (index < 0)
      throw new ArgumentOutOfRangeException("index", "Value is less than zero");
    if (arrayIndex < 0)
      throw new ArgumentOutOfRangeException("arrayIndex", "Value is less than zero");
    if (count < 0)
      throw new ArgumentOutOfRangeException("count", "Value is less than zero");
    if (array.Rank > 1)
      throw new ArgumentException("Multi dimensional array.", "array");
    if (index+count > this.Count || arrayIndex+count > array.Length)
      throw new ArgumentException("Number of elements to copy is too large.");
    for (int i=0; i<count; i++)
      array.SetValue(getitemcopy(index+i), arrayIndex+i);
  }

#if !SWIG_DOTNET_1
  System.Collections.Generic.IEnumerator<ExternalExpert> System.Collections.Generic.IEnumerable<ExternalExpert>.GetEnumerator() {
    return new ExternalExpertVectorEnumerator(this);
  }
#endif

  System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
    return new ExternalExpertVectorEnumerator(this);
  }

  public ExternalExpertVectorEnumerator GetEnumerator() {
    return new ExternalExpertVectorEnumerator(this);
  }

  // Type-safe enumerator
  /// Note that the IEnumerator documentation requires an InvalidOperationException to be thrown
  /// whenever the collection is modified. This has been done for changes in the size of the
  /// collection but not when one of the elements of the collection is modified as it is a bit
  /// tricky to detect unmanaged code that modifies the collection under our feet.
  public sealed class ExternalExpertVectorEnumerator : System.Collections.IEnumerator
#if !SWIG_DOTNET_1
    , System.Collections.Generic.IEnumerator<ExternalExpert>
#endif
  {
    private ExternalExpertVector collectionRef;
    private int currentIndex;
    private object currentObject;
    private int currentSize;

    public ExternalExpertVectorEnumerator(ExternalExpertVector collection) {
      collectionRef = collection;
      currentIndex = -1;
      currentObject = null;
      currentSize = collectionRef.Count;
    }

    // Type-safe iterator Current
    public ExternalExpert Current {
      get {
        if (currentIndex == -1)
          throw new InvalidOperationException("Enumeration not started.");
        if (currentIndex > currentSize - 1)
          throw new InvalidOperationException("Enumeration finished.");
        if (currentObject == null)
          throw new InvalidOperationException("Collection modified.");
        return (ExternalExpert)currentObject;
      }
    }

    // Type-unsafe IEnumerator.Current
    object System.Collections.IEnumerator.Current {
      get {
        return Current;
      }
    }

    public bool MoveNext() {
      int size = collectionRef.Count;
      bool moveOkay = (currentIndex+1 < size) && (size == currentSize);
      if (moveOkay) {
        currentIndex++;
        currentObject = collectionRef[currentIndex];
      } else {
        currentObject = null;
      }
      return moveOkay;
    }

    public void Reset() {
      currentIndex = -1;
      currentObject = null;
      if (collectionRef.Count != currentSize) {
        throw new InvalidOperationException("Collection modified.");
      }
    }

#if !SWIG_DOTNET_1
    public void Dispose() {
        currentIndex = -1;
        currentObject = null;
    }
#endif
  }

  public void Clear() {
    ErfClipsPINVOKE.ExternalExpertVector_Clear(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void Add(ExternalExpert x) {
    ErfClipsPINVOKE.ExternalExpertVector_Add(swigCPtr, ExternalExpert.getCPtr(x));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  private uint size() {
    uint ret = ErfClipsPINVOKE.ExternalExpertVector_size(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private uint capacity() {
    uint ret = ErfClipsPINVOKE.ExternalExpertVector_capacity(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private void reserve(uint n) {
    ErfClipsPINVOKE.ExternalExpertVector_reserve(swigCPtr, n);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public ExternalExpertVector() : this(ErfClipsPINVOKE.new_ExternalExpertVector__SWIG_0(), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public ExternalExpertVector(ExternalExpertVector other) : this(ErfClipsPINVOKE.new_ExternalExpertVector__SWIG_1(ExternalExpertVector.getCPtr(other)), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public ExternalExpertVector(int capacity) : this(ErfClipsPINVOKE.new_ExternalExpertVector__SWIG_2(capacity), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  private ExternalExpert getitemcopy(int index) {
    IntPtr cPtr = ErfClipsPINVOKE.ExternalExpertVector_getitemcopy(swigCPtr, index);
    ExternalExpert ret = (cPtr == IntPtr.Zero) ? null : new ExternalExpert(cPtr, true);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private ExternalExpert getitem(int index) {
    IntPtr cPtr = ErfClipsPINVOKE.ExternalExpertVector_getitem(swigCPtr, index);
    ExternalExpert ret = (cPtr == IntPtr.Zero) ? null : new ExternalExpert(cPtr, true);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private void setitem(int index, ExternalExpert val) {
    ErfClipsPINVOKE.ExternalExpertVector_setitem(swigCPtr, index, ExternalExpert.getCPtr(val));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void AddRange(ExternalExpertVector values) {
    ErfClipsPINVOKE.ExternalExpertVector_AddRange(swigCPtr, ExternalExpertVector.getCPtr(values));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public ExternalExpertVector GetRange(int index, int count) {
    IntPtr cPtr = ErfClipsPINVOKE.ExternalExpertVector_GetRange(swigCPtr, index, count);
    ExternalExpertVector ret = (cPtr == IntPtr.Zero) ? null : new ExternalExpertVector(cPtr, true);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void Insert(int index, ExternalExpert x) {
    ErfClipsPINVOKE.ExternalExpertVector_Insert(swigCPtr, index, ExternalExpert.getCPtr(x));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void InsertRange(int index, ExternalExpertVector values) {
    ErfClipsPINVOKE.ExternalExpertVector_InsertRange(swigCPtr, index, ExternalExpertVector.getCPtr(values));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void RemoveAt(int index) {
    ErfClipsPINVOKE.ExternalExpertVector_RemoveAt(swigCPtr, index);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void RemoveRange(int index, int count) {
    ErfClipsPINVOKE.ExternalExpertVector_RemoveRange(swigCPtr, index, count);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public static ExternalExpertVector Repeat(ExternalExpert value, int count) {
    IntPtr cPtr = ErfClipsPINVOKE.ExternalExpertVector_Repeat(ExternalExpert.getCPtr(value), count);
    ExternalExpertVector ret = (cPtr == IntPtr.Zero) ? null : new ExternalExpertVector(cPtr, true);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void Reverse() {
    ErfClipsPINVOKE.ExternalExpertVector_Reverse__SWIG_0(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void Reverse(int index, int count) {
    ErfClipsPINVOKE.ExternalExpertVector_Reverse__SWIG_1(swigCPtr, index, count);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void SetRange(int index, ExternalExpertVector values) {
    ErfClipsPINVOKE.ExternalExpertVector_SetRange(swigCPtr, index, ExternalExpertVector.getCPtr(values));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public bool Contains(ExternalExpert value) {
    bool ret = ErfClipsPINVOKE.ExternalExpertVector_Contains(swigCPtr, ExternalExpert.getCPtr(value));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public int IndexOf(ExternalExpert value) {
    int ret = ErfClipsPINVOKE.ExternalExpertVector_IndexOf(swigCPtr, ExternalExpert.getCPtr(value));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public int LastIndexOf(ExternalExpert value) {
    int ret = ErfClipsPINVOKE.ExternalExpertVector_LastIndexOf(swigCPtr, ExternalExpert.getCPtr(value));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public bool Remove(ExternalExpert value) {
    bool ret = ErfClipsPINVOKE.ExternalExpertVector_Remove(swigCPtr, ExternalExpert.getCPtr(value));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

}

}
