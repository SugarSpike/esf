/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace ErfClips {

using System;
using System.Runtime.InteropServices;

public class LongVector : IDisposable, System.Collections.IEnumerable
#if !SWIG_DOTNET_1
    , System.Collections.Generic.IList<int>
#endif
 {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal LongVector(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(LongVector obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~LongVector() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          ErfClipsPINVOKE.delete_LongVector(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public LongVector(System.Collections.ICollection c) : this() {
    if (c == null)
      throw new ArgumentNullException("c");
    foreach (int element in c) {
      this.Add(element);
    }
  }

  public bool IsFixedSize {
    get {
      return false;
    }
  }

  public bool IsReadOnly {
    get {
      return false;
    }
  }

  public int this[int index]  {
    get {
      return getitem(index);
    }
    set {
      setitem(index, value);
    }
  }

  public int Capacity {
    get {
      return (int)capacity();
    }
    set {
      if (value < size())
        throw new ArgumentOutOfRangeException("Capacity");
      reserve((uint)value);
    }
  }

  public int Count {
    get {
      return (int)size();
    }
  }

  public bool IsSynchronized {
    get {
      return false;
    }
  }

#if SWIG_DOTNET_1
  public void CopyTo(System.Array array)
#else
  public void CopyTo(int[] array)
#endif
  {
    CopyTo(0, array, 0, this.Count);
  }

#if SWIG_DOTNET_1
  public void CopyTo(System.Array array, int arrayIndex)
#else
  public void CopyTo(int[] array, int arrayIndex)
#endif
  {
    CopyTo(0, array, arrayIndex, this.Count);
  }

#if SWIG_DOTNET_1
  public void CopyTo(int index, System.Array array, int arrayIndex, int count)
#else
  public void CopyTo(int index, int[] array, int arrayIndex, int count)
#endif
  {
    if (array == null)
      throw new ArgumentNullException("array");
    if (index < 0)
      throw new ArgumentOutOfRangeException("index", "Value is less than zero");
    if (arrayIndex < 0)
      throw new ArgumentOutOfRangeException("arrayIndex", "Value is less than zero");
    if (count < 0)
      throw new ArgumentOutOfRangeException("count", "Value is less than zero");
    if (array.Rank > 1)
      throw new ArgumentException("Multi dimensional array.", "array");
    if (index+count > this.Count || arrayIndex+count > array.Length)
      throw new ArgumentException("Number of elements to copy is too large.");
    for (int i=0; i<count; i++)
      array.SetValue(getitemcopy(index+i), arrayIndex+i);
  }

#if !SWIG_DOTNET_1
  System.Collections.Generic.IEnumerator<int> System.Collections.Generic.IEnumerable<int>.GetEnumerator() {
    return new LongVectorEnumerator(this);
  }
#endif

  System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
    return new LongVectorEnumerator(this);
  }

  public LongVectorEnumerator GetEnumerator() {
    return new LongVectorEnumerator(this);
  }

  // Type-safe enumerator
  /// Note that the IEnumerator documentation requires an InvalidOperationException to be thrown
  /// whenever the collection is modified. This has been done for changes in the size of the
  /// collection but not when one of the elements of the collection is modified as it is a bit
  /// tricky to detect unmanaged code that modifies the collection under our feet.
  public sealed class LongVectorEnumerator : System.Collections.IEnumerator
#if !SWIG_DOTNET_1
    , System.Collections.Generic.IEnumerator<int>
#endif
  {
    private LongVector collectionRef;
    private int currentIndex;
    private object currentObject;
    private int currentSize;

    public LongVectorEnumerator(LongVector collection) {
      collectionRef = collection;
      currentIndex = -1;
      currentObject = null;
      currentSize = collectionRef.Count;
    }

    // Type-safe iterator Current
    public int Current {
      get {
        if (currentIndex == -1)
          throw new InvalidOperationException("Enumeration not started.");
        if (currentIndex > currentSize - 1)
          throw new InvalidOperationException("Enumeration finished.");
        if (currentObject == null)
          throw new InvalidOperationException("Collection modified.");
        return (int)currentObject;
      }
    }

    // Type-unsafe IEnumerator.Current
    object System.Collections.IEnumerator.Current {
      get {
        return Current;
      }
    }

    public bool MoveNext() {
      int size = collectionRef.Count;
      bool moveOkay = (currentIndex+1 < size) && (size == currentSize);
      if (moveOkay) {
        currentIndex++;
        currentObject = collectionRef[currentIndex];
      } else {
        currentObject = null;
      }
      return moveOkay;
    }

    public void Reset() {
      currentIndex = -1;
      currentObject = null;
      if (collectionRef.Count != currentSize) {
        throw new InvalidOperationException("Collection modified.");
      }
    }

#if !SWIG_DOTNET_1
    public void Dispose() {
        currentIndex = -1;
        currentObject = null;
    }
#endif
  }

  public void Clear() {
    ErfClipsPINVOKE.LongVector_Clear(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void Add(int x) {
    ErfClipsPINVOKE.LongVector_Add(swigCPtr, x);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  private uint size() {
    uint ret = ErfClipsPINVOKE.LongVector_size(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private uint capacity() {
    uint ret = ErfClipsPINVOKE.LongVector_capacity(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private void reserve(uint n) {
    ErfClipsPINVOKE.LongVector_reserve(swigCPtr, n);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public LongVector() : this(ErfClipsPINVOKE.new_LongVector__SWIG_0(), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public LongVector(LongVector other) : this(ErfClipsPINVOKE.new_LongVector__SWIG_1(LongVector.getCPtr(other)), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public LongVector(int capacity) : this(ErfClipsPINVOKE.new_LongVector__SWIG_2(capacity), true) {
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  private int getitemcopy(int index) {
    int ret = ErfClipsPINVOKE.LongVector_getitemcopy(swigCPtr, index);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private int getitem(int index) {
    int ret = ErfClipsPINVOKE.LongVector_getitem(swigCPtr, index);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  private void setitem(int index, int val) {
    ErfClipsPINVOKE.LongVector_setitem(swigCPtr, index, val);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void AddRange(LongVector values) {
    ErfClipsPINVOKE.LongVector_AddRange(swigCPtr, LongVector.getCPtr(values));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public LongVector GetRange(int index, int count) {
    IntPtr cPtr = ErfClipsPINVOKE.LongVector_GetRange(swigCPtr, index, count);
    LongVector ret = (cPtr == IntPtr.Zero) ? null : new LongVector(cPtr, true);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void Insert(int index, int x) {
    ErfClipsPINVOKE.LongVector_Insert(swigCPtr, index, x);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void InsertRange(int index, LongVector values) {
    ErfClipsPINVOKE.LongVector_InsertRange(swigCPtr, index, LongVector.getCPtr(values));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void RemoveAt(int index) {
    ErfClipsPINVOKE.LongVector_RemoveAt(swigCPtr, index);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void RemoveRange(int index, int count) {
    ErfClipsPINVOKE.LongVector_RemoveRange(swigCPtr, index, count);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public static LongVector Repeat(int value, int count) {
    IntPtr cPtr = ErfClipsPINVOKE.LongVector_Repeat(value, count);
    LongVector ret = (cPtr == IntPtr.Zero) ? null : new LongVector(cPtr, true);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void Reverse() {
    ErfClipsPINVOKE.LongVector_Reverse__SWIG_0(swigCPtr);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void Reverse(int index, int count) {
    ErfClipsPINVOKE.LongVector_Reverse__SWIG_1(swigCPtr, index, count);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public void SetRange(int index, LongVector values) {
    ErfClipsPINVOKE.LongVector_SetRange(swigCPtr, index, LongVector.getCPtr(values));
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
  }

  public bool Contains(int value) {
    bool ret = ErfClipsPINVOKE.LongVector_Contains(swigCPtr, value);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public int IndexOf(int value) {
    int ret = ErfClipsPINVOKE.LongVector_IndexOf(swigCPtr, value);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public int LastIndexOf(int value) {
    int ret = ErfClipsPINVOKE.LongVector_LastIndexOf(swigCPtr, value);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public bool Remove(int value) {
    bool ret = ErfClipsPINVOKE.LongVector_Remove(swigCPtr, value);
    if (ErfClipsPINVOKE.SWIGPendingException.Pending) throw ErfClipsPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

}

}
