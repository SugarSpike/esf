// NativeCppCaller.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

/*
#include "PriorityList.h"
#include <iostream>
#include <list>
#include <string>

#include <fstream>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include "BasicUserModel.h"
#include "Emotion.h"
#include "ClipsEmotionModel.h"
#include "OccEmotion.h"
#include "UserEmotionAffectVector.h"
#include "UserEmotionChangedEecEvent.h"
#include "FacialEmotion.h"
#include "OccClipsPersonality.h"
#include "Personality.h"
namespace sinks = boost::log::sinks;


using namespace std;
using namespace erf;
using namespace esfclips;

void init()
{
    // Construct the sink
    typedef sinks::synchronous_sink< sinks::text_ostream_backend > text_sink;
    boost::shared_ptr< text_sink > sink = boost::make_shared< text_sink >();

    // Add a stream to write log to
    sink->locked_backend()->add_stream(
        boost::make_shared< std::ofstream >("sample.log"));

    // Register the sink in the logging core
   // logging::core::get()->add_sink(sink);

}

int main()
{
	init();

	ClipsEmotionModel* model = new ClipsEmotionModel();

	UserEmotionAffectVector* av = new UserEmotionAffectVector();

	OccEmotion* emotion = new OccEmotion();
	emotion->SetOccId(OccId::OCC_ANGER);
	emotion->SetIntensity(static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
	emotion->SetEmotionOwner("PLAYER");
	av->GetEmotions().push_back(emotion);

	emotion = new OccEmotion();
	emotion->SetOccId(OccId::OCC_JOY);
	emotion->SetIntensity(static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
	av->GetEmotions().push_back(emotion);

	emotion = new OccEmotion();
	emotion->SetOccId(OccId::OCC_DISTRESS);
	emotion->SetIntensity(static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
	av->GetEmotions().push_back(emotion);

	
	FacialEmotion* femotion = new FacialEmotion();
	femotion->SetFacialId(FacialId::FACIAL_ANGER);
	femotion->SetMatching(static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
	femotion->SetEmotionOwner("PLAYER");
	av->GetEmotions().push_back(femotion);

	UserEmotionChangedEecEvent* e = new UserEmotionChangedEecEvent(GameEvent::ACTOR_ATTACKED);
	e->SetName("KinectEmotionDetection");
	//e->SetId("123");
	e->SetUserEmotions(av);

	BasicUserModel* bum = new BasicUserModel();
	bum->RegisterAffectionModel(model);
	
	esfclips::OccClipsPersonality* personality = new esfclips::OccClipsPersonality();
	personality->extravert = 0.2f;
	personality->neurotic = 0.3f;
	personality->choleric = -0.2f;
	Personality* clipsPersonality = new Personality();
	//boost::shared_ptr<void> occPersonality(personality);
	clipsPersonality->Attributes()[OccClipsPersonality::PERSONALITY_ATTR] = personality;

	map<string,float>* relationships = new map<string,float>();
	(*relationships)["PLAYER"] = -0.5f;
	(*relationships)["BALL 1 2"] = 1.0f;
	//boost::shared_ptr<void> relationshipsShrd(relationships);
	clipsPersonality->Attributes()[OccClipsPersonality::RELATIONSHIPS_ATTR] = relationships;
	bum->SetPersonality(clipsPersonality);
	

	if(bum->HandleEvent(e))
	{
		UserEmotionAffectVector* av =(UserEmotionAffectVector*)bum->GetAffectVector();
		cout << endl << endl;
		for (std::vector<Emotion* >::iterator it = av->GetEmotions().begin(); 
			it !=  av->GetEmotions().end(); ++it) {
				OccEmotion* emotion = (OccEmotion*)(*it);
				cout << emotion->GetName() << ": " << emotion->GetIntensity() << endl;
		}

	}

	delete bum;
	delete model;
	delete av;
	//delete e;
	system("pause");
	return 0;

}

*/