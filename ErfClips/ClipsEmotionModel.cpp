#pragma once
#include "stdafx.h"
#include "ClipsEmotionModel.h"
#include "MoodVector.h"
#include "Personality.h"
#include "EecEvent.h"
#include "GameEnums.h"
#include "CharacterData.h"
#include "OccClipsPersonality.h"
#include "EmotionConstants.h"
#include "EmotionEnums.h"
using namespace CLIPS;
using namespace erf;

namespace erfclips {

	ClipsEmotionModel::ClipsEmotionModel(string clipsRuleFilePath) {
		if(this->ClipsEnviroment.Load((char *)clipsRuleFilePath.c_str()) < 0)
		{
			cout << "ClipsEmotionModel::ClipsEmotionModel - cannot load CLIPS rules" << endl;
		}
	}

	ClipsEmotionModel::~ClipsEmotionModel() {
		this->ClipsEnviroment.Clear();
	}

	void ClipsEmotionModel::InitializeForCharacter(CharacterData& characterData) {
		OccClipsPersonality* occPersonality = dynamic_cast<OccClipsPersonality*>(&characterData.GetPersonality());
		if(occPersonality == null)
		{
			OccClipsPersonality* newPersonality = new OccClipsPersonality();
			characterData.SetPersonality(newPersonality);
			return ;
		}
	}

	bool ClipsEmotionModel::IsEventSupported(EecEvent& e) { 
		if(e.GetEventType() == GameEvent::STD_PLAYER_EMOTIONAL_STATE_CHANGE ||
			e.GetEventType() == GameEvent::STD_ACTOR_EMOTIONAL_STATE_CHANGE ||
			e.GetEventType() == GameEvent::STD_TIME_ELAPSED)
			return true;

		return false;
	}

	float ClipsEmotionModel::GetEmotionChange(EmotionVector* current, EmotionVector* previous, string emotionName)
	{
		//cout << "ClipsEmotionModel::GetEmotionChange: " << (int) current << " " << (int) previous << endl;
		Variant* previousEmotion = previous->GetValue(emotionName);
		Variant* currentEmotion = current->GetValue(emotionName);
		if(previousEmotion == null && currentEmotion == null)
			return 0.0f;
		else if(previousEmotion == null)
			return currentEmotion->AsFloat();
		else if( currentEmotion == null)
			return (-1)*previousEmotion->AsFloat();

		return currentEmotion->AsFloat() - previousEmotion->AsFloat();
	}

	float ClampFloat(float val, float min, float max)
	{
		return  val < min ? min : (val > max ? max : val);
	}


	EmotionVector* ClipsEmotionModel::HandlePlayerEmotionalStateChanged(EecEvent& event, CharacterData& characterData) {
		EmotionVector* playerVector = event.GetValue("PlayerEmotionVector")->As<EmotionVector>();
		EmotionVector* previousPlayerVector = event.GetValue("PreviousPlayerEmotionVector")->As<EmotionVector>();
		
		//cout << "ClipsEmotionModel::HandlePlayerEmotionalStateChanged - vectors: " << (int)playerVector
		//	<< " " << (int)previousPlayerVector << " " << (int)&event << endl;
		if(playerVector == null || previousPlayerVector == null)
		{
			cout << "ClipsEmotionModel::HandlePlayerEmotionalStateChanged - null vectors: " << (int)playerVector
				<< " " << (int)previousPlayerVector << endl;
			return null;
		}
		else if(playerVector->GetValue(OccEmotions::JOY) == null)
		{
			cout << "ClipsEmotionModel::HandlePlayerEmotionalStateChanged - null joy: " << (int)playerVector << endl;
			return null;
		}
		else if(playerVector->GetValue(OccEmotions::DISTRESS) == null)
		{
			cout << "ClipsEmotionModel::HandlePlayerEmotionalStateChanged - null distress: " << (int)playerVector << endl;
			return null;
		}

		float deltaJoy = GetEmotionChange(playerVector,previousPlayerVector,OccEmotions::JOY);
		float currentJoy = playerVector->GetValue(OccEmotions::JOY)->AsFloat();
		float deltaDistress = GetEmotionChange(playerVector,previousPlayerVector,OccEmotions::DISTRESS);
		float currentDistress = playerVector->GetValue(OccEmotions::DISTRESS)->AsFloat();

		//Make stable emotion also contribute
		deltaJoy += currentJoy * 0.1f + deltaJoy;
		deltaDistress += currentDistress * 0.1f + deltaDistress;
		deltaJoy = ClampFloat(deltaJoy,-1.0f,1.0f);
		deltaDistress = ClampFloat(deltaDistress,-1.0f,1.0f);

		this->ClipsEnviroment.Reset();
		bool personalityFound = false;
		float relationshipsToPlayer = 0.0f;
		
		OccClipsPersonality* occPersonality = dynamic_cast<OccClipsPersonality*>(&characterData.GetPersonality());
		if(occPersonality!= null)
		{
			personalityFound = true;
			char buff[200];
			sprintf(buff, 
				"(personality (optimistic %.3f) (choleric %.3f) (extravert %.3f) (social %.3f) (neurotic %.3f))", 
				occPersonality->optimistic,
				occPersonality->choleric,
				occPersonality->extravert,
				occPersonality->social,
				occPersonality->neurotic);
			addFact(buff);

			relationshipsToPlayer = occPersonality->relationshipToPlayer;
		}
		if(!personalityFound)
		{
			addFact("(personality (optimistic 0.0) (choleric 0.0) (extravert 0.0) (social 0.0) (neurotic 0.0))");
		}

		AddSimpleEvent("RelationshipsToPlayer_DeltaJoy",deltaJoy * relationshipsToPlayer);
		AddSimpleEvent("RelationshipsToPlayer_DeltaDistress",-1 * (deltaDistress * relationshipsToPlayer));

		this->ClipsEnviroment.Run(-1);

		DataObject ret = EvaluateFacts("(get-emotion-list)");
		Value* val = ret.getValue();
		MultifieldValue* multiField = dynamic_cast<MultifieldValue*>(val);

		EmotionVector* newEmotionalState = new EmotionVector(EmotionModelType::STD_OCC);

		if(multiField)
		{
			//std::string str = multiField->print();
			for (int i = 0; i < multiField->theMultifield.size(); i++)
			 { 

			   Value* val = multiField->getValues()[i];
			   DataObject emotionTypeDo = val->GetFactSlot("type");
			   DataObject emotionIntensityDo = val->GetFactSlot("intense");
			   if(emotionTypeDo.getValue() != null && emotionIntensityDo.getValue() != null)
			   {
				    SymbolValue* emotionTypeVal =  dynamic_cast<SymbolValue*>(emotionTypeDo.getValue());
				    FloatValue* emotionIntensityVal =  dynamic_cast<FloatValue*>(emotionIntensityDo.getValue());
					if(emotionTypeVal != null && emotionIntensityVal != null)
					{
#ifdef LOG_ENABLED
						cout << "Retrieved Fact: " << emotionTypeVal->theString << " : " << emotionIntensityVal->theFloat << endl;
#endif
						newEmotionalState->AddValue(
							emotionTypeVal->theString,
							Variant::Create(emotionIntensityVal->theFloat));
					}
			   }

			 }
			multiField = null;
		}


		return newEmotionalState;
	}

	void ClipsEmotionModel::AddSimpleEvent(string eventName, float desireForEvent)
	{
		char buff[200];
		sprintf(buff, 
			"(simple-event (id %s) (desire %.3f))", 
			eventName.c_str(),
			desireForEvent);
		addFact(buff);
	}

	EmotionVector* ClipsEmotionModel::HandleEvent(EecEvent& event, CharacterData& characterData) {
		
		switch (event.GetEventType())
		{
		case GameEvent::STD_PLAYER_EMOTIONAL_STATE_CHANGE:
			return HandlePlayerEmotionalStateChanged(event,characterData);
		case GameEvent::STD_ACTOR_EMOTIONAL_STATE_CHANGE:
			return null;
		case GameEvent::STD_TIME_ELAPSED:
		default:
			return null;
		}
	}

		
	void ClipsEmotionModel::addFact(string fact)
	{
#ifdef LOG_ENABLED
		cout << "Add Fact: " << fact << endl;
#endif

		ClipsEnviroment.AssertStr(&fact[0]);
	}

	CLIPS::DataObject ClipsEmotionModel::EvaluateFacts(char * evalString)
	{
#ifdef LOG_ENABLED
		cout << "Eval:" << evalString << endl;
#endif
		return ClipsEnviroment.Eval(evalString);
	}

}