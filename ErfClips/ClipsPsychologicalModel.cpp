#pragma once
#include "stdafx.h"
#include "ClipsPsychologicalModel.h"
#include "EmotionVector.h"
#include "Personality.h"
#include "EecEvent.h"
#include "GameEnums.h"
#include "CharacterData.h"
#include "OccClipsPersonality.h"
#include "EmotionConstants.h"
#include "EmotionEnums.h"
using namespace erf;

namespace erfclips {


	ClipsPsychologicalModel::ClipsPsychologicalModel() {

	}
	ClipsPsychologicalModel::~ClipsPsychologicalModel() {

	}
		
	float ClipsPsychologicalModel::GetEmotionChange(EmotionVector* current, EmotionVector* previous, string emotionName)
	{
		Variant* previousEmotion = previous->GetValue(emotionName);
		Variant* currentEmotion = current->GetValue(emotionName);
		if(previousEmotion == null && currentEmotion == null)
			return 0.0f;
		else if(previousEmotion == null)
			return currentEmotion->AsFloat();
		else if( currentEmotion == null)
			return (-1)*previousEmotion->AsFloat();

		return currentEmotion->AsFloat() - previousEmotion->AsFloat();
	}

	float ClipsPsychologicalModel::RandomFloat(float min, float max)
	{
		return min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max-min)));
	}

	void ClipsPsychologicalModel::InitializeForCharacter(CharacterData& characterData) 
	{

	}

	void ClipsPsychologicalModel::UpdatePersonality(EecEvent& event, CharacterData& characterData) {
		OccClipsPersonality* occPersonality = dynamic_cast<OccClipsPersonality*>(&characterData.GetPersonality());
		if(occPersonality == null)
			return;

		EmotionVector* current = characterData.GetCurrentEmotionVector();
		EmotionVector* previous = characterData.GetPreviousEmotionVector();

		if(current == null || previous == null)
			return;

		float dJoy = GetEmotionChange(current,previous,OccEmotions::JOY);
		float dDistress = GetEmotionChange(current,previous,OccEmotions::DISTRESS);

		if(dJoy > 0)
			occPersonality->optimistic += RandomFloat(0.0f,0.01f);
		else
			occPersonality->optimistic -= RandomFloat(0.0f,0.01f);

		if(dDistress > 0)
			occPersonality->choleric += RandomFloat(0.0f,0.01f);
		else
			occPersonality->choleric -= RandomFloat(0.0f,0.01f);
	}

}