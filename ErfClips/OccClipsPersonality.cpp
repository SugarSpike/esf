#pragma once
#include "stdafx.h"
#include "OccClipsPersonality.h"


namespace erfclips {
	const std::string OccClipsPersonality::PERSONALITY_ATTR = "OCC_PERSONALITY";
	const std::string OccClipsPersonality::RELATIONSHIPS_ATTR = "OCC_RELATIONSHIPS";

	OccClipsPersonality::OccClipsPersonality() {
		this->choleric = 0.0f;
		this->extravert = 0.0f;
		this->neurotic = 0.0f;
		this->optimistic = 0.0f;
		this->social = 0.0f;
		this->relationshipToPlayer = 0.0f;
	}

	OccClipsPersonality::~OccClipsPersonality() {

	}

}