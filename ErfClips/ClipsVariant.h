#pragma once
#include "Variant.h"

namespace erfclips
{
    class ESFCLIPS_API ClipsVariant : public Variant
    {
		static ClipsVariant* CastToDerived(Variant* base) {
			return dynamic_cast<ClipsVariant*>(base);
		}
	};
}