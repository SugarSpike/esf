#pragma once
#include "ClipsCommon.h"
#include "EmotionModel.h"
#include "clipscpp.h"
//#include <boost/thread/thread.hpp>
using namespace std;
using namespace erf;
namespace erfclips
{
    class ESFCLIPS_API ClipsEmotionModel : public EmotionModel
    {
	private:
		CLIPS::CLIPSCPPEnv ClipsEnviroment;

		void prepareEnviroment();
		void addFact(std::string fact);
		CLIPS::DataObject EvaluateFacts(char *);
		EmotionVector* HandlePlayerEmotionalStateChanged(EecEvent& event, CharacterData& characterData);
		float GetEmotionChange(EmotionVector* current, EmotionVector* previous, string emotionName);
		void AddSimpleEvent(string eventName, float desireForEvent);
	public:
		ClipsEmotionModel(string clipsRuleFilePath);
		~ClipsEmotionModel();
		
		virtual float GetModelRank() { return 1.0f; }
		
		virtual void InitializeForCharacter(CharacterData& characterData);

		virtual bool IsEventSupported(EecEvent& event);

		virtual EmotionVector* HandleEvent(EecEvent& event, CharacterData& userData);

		static ClipsEmotionModel* CastToDerived(EmotionModel* base) {
			return dynamic_cast<ClipsEmotionModel*>(base);
		}

	};
}