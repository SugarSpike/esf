using UnityEngine;
using System.Collections;
using Erf;

public class PlayerPadMockCharacterModel : PlayerCharacterModel
{
	public PlayerPadMockCharacterModel() : this(1){ 
	}

	public PlayerPadMockCharacterModel(int numberOfPadExperts){
		this.Model = BuildPadMockModel (numberOfPadExperts);
		EecEvent timeElapsedEvent = new EecEvent ((int)GameEvent.STD_TIME_ELAPSED);
		this.Model.HandleEvent(timeElapsedEvent); 
	}

	private ExternalExpert InitializePadExpert(CharacterModel model, ErfContext context, string expertName)
	{
		ExternalExpert expert = context.FindExpert(expertName);
		if (expert == null) {
			
			Debug.LogError("Could not find expert:" + expertName);
			return null;
		}
		
		expert.SetParameter ("BVP_SENSOR_NAME", "BvpSensor_"+expertName);
		expert.SetParameter ("EMG_SENSOR_NAME", "EmgSensor_"+expertName);
		expert.SetParameter ("SC_SENSOR_NAME", "ScSensor_"+expertName);
		EmotionSensor sensor = FindFileSensorMockForPadExpertAndRegister(context,"BvpSensor_"+expertName,"BvpSensor");
		model.RegisterEmotionSensor (sensor);
		sensor = FindFileSensorMockForPadExpertAndRegister(context,"EmgSensor_"+expertName,"EmgSensor");
		model.RegisterEmotionSensor (sensor);
		sensor = FindFileSensorMockForPadExpertAndRegister(context,"ScSensor_"+expertName,"ScSensor");
		model.RegisterEmotionSensor (sensor);
		expert.Initialize ();
		if (expert.GetComponentState () == ComponentState.COMPONENT_ERROR) {
			Debug.LogError (expert.GetComponentLastError ());
			return null;
		}
		return expert;
	}
	
	private EmotionSensor FindFileSensorMockForPadExpertAndRegister(ErfContext context,string expertSensor, string mainSensor)
	{
		EmotionSensor fileReadingSensor = context.FindSensor(expertSensor);
		if (fileReadingSensor == null) {
			EmotionSensor mainSensorInstance = context.FindSensor(mainSensor);
			fileReadingSensor = mainSensorInstance.CloneSensor();
			fileReadingSensor.SetName(expertSensor);
		}
		
		if (fileReadingSensor.GetComponentState () == ComponentState.UNINITIALIZED) {
			Debug.Log("Loading sensor data for path:" + GameUtils.GetApplicationPath() + GameConsts.SensorDataFilePath);
			fileReadingSensor.SetParameter("FILE_PATH",GameUtils.GetApplicationPath() +GameConsts.SensorDataFilePath);
			fileReadingSensor.Initialize ();
		}
		else
			fileReadingSensor.Reinitialize ();
		if(fileReadingSensor.GetComponentState() == ComponentState.COMPONENT_ERROR)
		{
			string message = fileReadingSensor.GetComponentLastError();
			Debug.Log(message);
			//Debug.LogError(fileReadingSensor.GetComponentLastError());
			return null;
		}
		return fileReadingSensor;
	}
	
	private CharacterModel BuildPadMockModel(int numberOfPadExperts)
	{
		CharacterModel model = new CharacterModel ();
		ErfContext context = ErfContext.GetInstance();
		ExternalFilter padToOccFiter = context.FindFilter("PadToOccVectorFilter");
		ExternalExpert expert = InitializePadExpert (model, context, "PadAffectionExpert");
		model.RegisterExpert (expert);
		for (int i=1; i < numberOfPadExperts; i++) {
			ExternalExpert expert2 =InitializePadExpert (model, context, "PadAffectionExpert"+(i+1));
			model.RegisterExpert (expert2);
		}
		//InitializePadExpert (context, "PadAffectionExpert3");
		if (numberOfPadExperts > 1) {
			model.SetEmotionModelEvaluationMode (ModelsEvaluationMode.STD_WEIGHTED_AVERAGE);
			model.SetEmotionModelEvaluationCharacteristics ((int)EmotionVectorCharacteristics.STD_VECTOR_OF_FLOATS);
		}
		model.RegisterExternalFilter(padToOccFiter, EmotionVectorFilterType.After);
		return model;
	}

	public override bool UpdateModel(EecEvent eecEvent)
	{
		return this.Model.HandleEvent (eecEvent);
	}

	public override EmotionVector GetOccVector() {
		return Model.GetEmotionVector ();
	}
	
	public override EmotionVector GetPadVector(){
		return Model.GetUnfilteredEmotionVector ();
	}

	
	
	public override EmotionVector GetPreviousOccVector(){
		return Model.GetCharacterData ().GetPreviousEmotionVector ();
	}
	
	public override EmotionVector GetPreviousPadVector(){
		return Model.GetCharacterData ().GetPreviousUnfilteredEmotionVector ();
	}

	int i = 1;
	public override void PrettyPrintVectors ()
	{
		float p =  this.Model.GetUnfilteredEmotionVector().GetValue(PadEmotions.PLEASURE).AsFloat();
		float a =  this.Model.GetUnfilteredEmotionVector().GetValue(PadEmotions.AROUSAL).AsFloat();
		float d =  this.Model.GetUnfilteredEmotionVector().GetValue(PadEmotions.DOMINANCE).AsFloat();
		float joy = this.Model.GetEmotionVector().GetValue(OccEmotions.JOY).AsFloat();
		float distress = this.Model.GetEmotionVector().GetValue(OccEmotions.DISTRESS).AsFloat();
		string message = string.Format("{0}\tp:{1:0.00}\ta:{2:0.00}\td:{3:0.00}\tj:{4:0.00}\td:{5:0.00}",i++,p,a,d,joy,distress);
		Debug.Log (message);
	}

}

