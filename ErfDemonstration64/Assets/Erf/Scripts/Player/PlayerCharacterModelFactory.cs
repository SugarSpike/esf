using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Erf;

public enum PlayerCharacterModelType
{
	PadMock,
	PadMock_Weighted,
	MouseOcc
}

public static class PlayerCharacterModelFactory
{
	private static Dictionary<PlayerCharacterModelType, PlayerCharacterModel> ModelCache;

	public static bool CacheModels { get; set; }

	static PlayerCharacterModelFactory()
	{
		CacheModels = true;
		ModelCache = new Dictionary<PlayerCharacterModelType, PlayerCharacterModel> ();
	}

	public static PlayerCharacterModel BuildModel(PlayerCharacterModelType type)
	{
		PlayerCharacterModel model = null;

		if (CacheModels && ModelCache.ContainsKey (type))
			return ModelCache [type];

		switch(type)
		{
		case PlayerCharacterModelType.MouseOcc:
			model = new PlayerOccMouseCharacterModel();
			break;
		case PlayerCharacterModelType.PadMock_Weighted:
			model = new PlayerPadMockCharacterModel(2);
			break;
		case PlayerCharacterModelType.PadMock:
			model = new PlayerPadMockCharacterModel();
			break;
		default:
			throw new UnityException("PlayerEmotionalModelFactory::BuildModel type not supported: " + type);
		}
		
		if(CacheModels)
			ModelCache.Add(type,model);

		model.Model.GetCharacterData ().SetEmotionVectorBufferSize (2000);

		return model;
	}

}

