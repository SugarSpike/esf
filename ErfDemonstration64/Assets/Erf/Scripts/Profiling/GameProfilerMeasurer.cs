using UnityEngine;
using System.Collections;

public abstract class GameProfilerMeasurer 
{

	public abstract void PlayerPassStart ();
	
	public abstract GameProfilerEntry PlayerPassStop ();
	
	public abstract void BallFirstPassStart ();
	
	public abstract GameProfilerEntry BallFirstPassStop (int noOfBalls);
	
	public abstract void BallSecondPassStart ();
	
	public abstract GameProfilerEntry BallSecondPassStop (int noOfBalls);
}

