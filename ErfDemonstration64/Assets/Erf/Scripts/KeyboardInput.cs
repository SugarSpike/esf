﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class KeyboardInput : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (FeelBallsManager.Instance != null)
				FeelBallsManager.Instance.SelectedFeelBall = null;
		} else if (Input.GetKeyDown (KeyCode.P)) {
			GameConsts.ShowPlayerEmotion = !GameConsts.ShowPlayerEmotion;
		} else if (Input.GetKeyDown (KeyCode.O)) {
			GameConsts.ShowBallEmotion = !GameConsts.ShowBallEmotion;
		}  else if (Input.GetKeyDown (KeyCode.T)) {
			GameConsts.ShowTestsPanel = !GameConsts.ShowTestsPanel;
		}
	}
}
