﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum AiState {
	Idle,
	Run,
	CheerUp,
	MoveToPlayer,
	Erratic,
	HeldByPlayer,
	IsThrown
}


public class FeelBallAi  {

	public BehaviorManager BehaviorManager { get; set; }
	public FeelBall FeelBall;
	public AiState	CurrentState {get;set;}

	private Dictionary<Type,AiBehavior> CachedBehaviours;

	public FeelBallAi(FeelBall feelBall) {
		CurrentState = AiState.Idle;
		BehaviorManager = new BehaviorManager ();
		BehaviorManager.Owner = feelBall.gameObject;
		FeelBall = feelBall;
		CachedBehaviours = new Dictionary<Type, AiBehavior> ();
		//CurrentState = AiState.MoveToPlayer;
	}

	public void ChangeState(AiState state)
	{
		//Debug.Log ("FBall("+FeelBall.gameObject.name+"): change " + CurrentState + " to " + state);
		if(state.Equals(CurrentState))
			return;

		if (FeelBall.IsHeld)
			return;

		CurrentState = state;
		switch(state)
		{
		case AiState.MoveToPlayer:
			MoveToTarget moveToPlayer = null;
			if(CachedBehaviours.ContainsKey(typeof(MoveToTarget)))
			{
				moveToPlayer = CachedBehaviours[typeof(MoveToTarget)] as MoveToTarget;
			}
			else {
				moveToPlayer = new MoveToTarget();
				CachedBehaviours.Add(typeof(MoveToTarget),moveToPlayer);
			}
			moveToPlayer.OnTargetInRange += new MoveToTarget.TargetInRangeHandler(MoveToPlayer_PlayerInRange);
			BehaviorManager.ChangeBehavior (moveToPlayer);
			break;
		case AiState.Run:
			RunFromTarget runFromTarget = null;
			if(CachedBehaviours.ContainsKey(typeof(RunFromTarget)))
			{
				runFromTarget = CachedBehaviours[typeof(RunFromTarget)] as RunFromTarget;
			}
			else {
				runFromTarget = new RunFromTarget();
				CachedBehaviours.Add(typeof(RunFromTarget),runFromTarget);
			}
			BehaviorManager.ChangeBehavior (runFromTarget);
			break;
		case AiState.CheerUp:
			CheerUp cheerUp = null;
			if(CachedBehaviours.ContainsKey(typeof(CheerUp)))
			{
				cheerUp = CachedBehaviours[typeof(CheerUp)] as CheerUp;
			}
			else {
				cheerUp = new CheerUp();
				CachedBehaviours.Add(typeof(CheerUp),cheerUp);
			}
			cheerUp.OnTargetOutOfRange += new CheerUp.TargetOutOfRangeHandler(CheerUp_HandleOnTargetOutOfRange);
			BehaviorManager.ChangeBehavior (cheerUp);
			break;
		case AiState.Erratic:
			Erratic erratic = null;
			if(CachedBehaviours.ContainsKey(typeof(Erratic)))
			{
				erratic = CachedBehaviours[typeof(Erratic)] as Erratic;
			}
			else {
				erratic = new Erratic();
				CachedBehaviours.Add(typeof(Erratic),erratic);
			}
			erratic.OnTargetOutOfRange += new Erratic.TargetOutOfRangeHandler(Erratic_HandleOnTargetOutOfRange);
			BehaviorManager.ChangeBehavior (erratic);
			break;
		case AiState.HeldByPlayer:
			HeldByPlayer heldByPlayer = null;
			if(CachedBehaviours.ContainsKey(typeof(HeldByPlayer)))
			{
				heldByPlayer = CachedBehaviours[typeof(HeldByPlayer)] as HeldByPlayer;
			}
			else {
				heldByPlayer = new HeldByPlayer();
				CachedBehaviours.Add(typeof(HeldByPlayer),heldByPlayer);
			}
			//heldByPlayer.OnTargetOutOfRange += new Erratic.TargetOutOfRangeHandler(Erratic_HandleOnTargetOutOfRange);
			BehaviorManager.ChangeBehavior (heldByPlayer);
			break;
		case AiState.IsThrown:
			IsThrown isThrown = null;
			if(CachedBehaviours.ContainsKey(typeof(IsThrown)))
			{
				isThrown = CachedBehaviours[typeof(IsThrown)] as IsThrown;
			}
			else {
				isThrown = new IsThrown(FeelBall);
				CachedBehaviours.Add(typeof(IsThrown),isThrown);
			}
			//heldByPlayer.OnTargetOutOfRange += new Erratic.TargetOutOfRangeHandler(Erratic_HandleOnTargetOutOfRange);
			BehaviorManager.ChangeBehavior (isThrown);
			break;
			
		case AiState.Idle:
				/*Idle idleBehaviour = null;
				if(CachedBehaviours.ContainsKey(typeof(IdleBehaviour)))
				{
					idleBehaviour = CachedBehaviours[typeof(IdleBehaviour)] as IdleBehaviour;
				}
				else {
					idleBehaviour = new IdleBehaviour();
					CachedBehaviours.Add(typeof(IdleBehaviour),idleBehaviour);
				}*/
				BehaviorManager.ChangeBehavior (null);
				break;
			default:
				Debug.Log("Unknown AI state: "+state);
				BehaviorManager.ChangeBehavior (null);
				break;
		}

	}

	void Erratic_HandleOnTargetOutOfRange (object sender, EventArgs e)
	{
		this.ChangeState (AiState.MoveToPlayer);
	}

	void CheerUp_HandleOnTargetOutOfRange (object sender, EventArgs e)
	{
		this.ChangeState (AiState.MoveToPlayer);
	}

	private void MoveToPlayer_PlayerInRange(object sender, EventArgs e)
	{
		this.ChangeState (AiState.CheerUp);
	}

	public void Update()
	{
		if (BehaviorManager != null)
			BehaviorManager.Update ();
	}

}
