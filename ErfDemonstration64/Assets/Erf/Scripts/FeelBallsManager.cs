﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Erf;
using ErfClips;

public class FeelBallsManager : Singleton<FeelBallsManager> {

	protected FeelBallsManager () {
		FeelBalls = new List<FeelBall> ();
		nameToFeelBallMap = new Dictionary<string, FeelBall> ();
		scheduledEvents = new Queue<EecEvent> ();
	}

	public List<FeelBall> FeelBalls { get; set; }
	private Dictionary<string,FeelBall> nameToFeelBallMap;
	private object eventsLock = new object();
	private Queue<EecEvent> scheduledEvents;
	private FeelBallsEmotionUpdateJob updateEmotionalStateJob;

	public void AddFeelBall(FeelBall ball)
	{
		FeelBalls.Add (ball);
		nameToFeelBallMap [ball.gameObject.name] = ball;
	}

	public FeelBall GetFeelBall(string ballName)
	{
		return nameToFeelBallMap [ballName];
	}

	private FeelBall selectedFeelBall;
	public FeelBall SelectedFeelBall { get {return selectedFeelBall; }
		set {
			if(selectedFeelBall!= null && GameConsts.GameIsRunning)
			{
				selectedFeelBall.ReleasePlayerHold();
			}
			selectedFeelBall = value;
			
			if(selectedFeelBall!= null)
			{
				selectedFeelBall.StartPlayerHold();
			}
		
		}}

	void Awake () {
	}

	void Update() {

		if (updateEmotionalStateJob != null)
		{
			if (updateEmotionalStateJob.Update())
			{
				//Debug.LogWarning (string.Format("Job done for event: {0}",updateEmotionalStateJob.RaisedEecEvent.CSharpUniqueName));
				updateEmotionalStateJob = null;
			}
		}
		//Schedule next
		if (updateEmotionalStateJob == null
		    && scheduledEvents.Count > 0) {
			updateEmotionalStateJob = new FeelBallsEmotionUpdateJob();
			EecEvent eecEvent = scheduledEvents.Dequeue();
           		updateEmotionalStateJob.RaisedEecEvent = eecEvent;
			updateEmotionalStateJob.FeelBallsToUpdate.AddRange(FeelBalls);
			//updateEmotionalStateJob.profiler = GameProfiler.Instance;
			updateEmotionalStateJob.Start();
		}

	}

	public CharacterModel CreateRandomFeelBallEmotionalModel()
	{
		float neurotic = (float)GameUtils.GetRandomNumber (-1.0, 1.0);
		float extravert = (float)GameUtils.GetRandomNumber (-1.0, 1.0);
		float relationshipToPlayer = (float)GameUtils.GetRandomNumber (-1.0, 1.0);
		return CreateFeelBallEmotionalModel (relationshipToPlayer, neurotic, extravert);
	}

	public CharacterModel CreateFeelBallEmotionalModel(float relationshipToPlayer,
	                                                         float neurotic,
	                                                         float extravert)
	{
		CharacterModel model = new CharacterModel();
		Debug.Log("Creating ball with relationship: " + relationshipToPlayer);
		OccClipsPersonality personality = new OccClipsPersonality();
		personality.neurotic = neurotic;
		personality.extravert = extravert;
		personality.relationshipToPlayer = relationshipToPlayer;
		model.SetPersonality(personality);
		
		ClipsEmotionModel emotionModel = new ClipsEmotionModel (GameUtils.GetApplicationPath()+ GameConsts.OccClipsRulesFilePath);
		model.RegisterEmotionModel(emotionModel);
		
		return model;
	}

	public void SendEecEventToFeelBalls(EecEvent eecEvent)
	{
		if (!GameConsts.IsAffectiveMode)
			return;

		//Debug.LogWarning (string.Format("Enqueue event: {0}",eecEvent.CSharpUniqueName));
		scheduledEvents.Enqueue (eecEvent);
	}

}
