﻿using UnityEngine;
using System.Collections;

using Erf;
using ErfClips;

public enum BallToPlayerState
{
	PlayerDistressedBallFriendly = 0,
	PlayerJoyfullBallFriendly = 1,
	PlayerDistressedBallUnfriendly = 2,
	PlayerJoyfullBallUnfriendly = 3,
	NoAffection = 4
}

//[ExecuteInEditMode]
public class FeelBall : MonoBehaviour {


	private float[] minPowerByState = {70.0f,50.0f,40.0f,10.0f,40.0f};
	private float[] incrementPowerByState = {1.0f,1.0f,2.0f,3.0f, 1.0f};
	private float[] holdTimeByState = {5.0f,4.0f,4.0f,3.0f, 4.0f};
	private int[] pointsAchievedByState = {20,15,20,30,20};

	public bool RandomPersonality = true;
	public float RelationshipToPlayer = 1.0f;
	public float Extravert = 1.0f;
	public float Neurotic = 1.0f;

	private float throwPowerDirectionIncrement;
	public float ThrowPower {
		get;
		set;
	}
	public float StartingHoldTime {
		get;
		set;
	}
	public float HoldTimeLeft {
		get;
		set;
	}
	public float PickTimeout {
		get;
		set;
	}
	public OccClipsPersonality personality;
	private BallToPlayerState currentBallToPlayerState;
	public BallToPlayerState CurrentBallToPlayerState {
		get { return currentBallToPlayerState; }
	}
	private bool isFriendlyToPlayer;
	public bool IsFriendlyToPlayer {
		get { return isFriendlyToPlayer; }
	}
	private bool distressDominates = false;
	private Color currentBallColor;
	private Color pickTimeoutBallColor;
	public bool DistressDominates {
		get { return distressDominates; }
	}

	public bool IsHeld {
		get;
		set;
	}
	public bool HasScored {
		get;
		set;
	}
	public bool WasDropped {
		get;
		set;
	}
	public AiState DefaultState = AiState.Idle;
	private SolidColor colorManager;
	public FeelBallAi Ai;
	public CharacterModel EmotionalModel {
		get;
		set;
	}

	private object emotionalStateLock = new object();
	public FeelBall() {
		IsHeld = false;
		ThrowPower = 50.0f;
		throwPowerDirectionIncrement = 1.0f;
	}

	
	// Use this for initialization
	void Start () {
		Debug.Log ("FBall: " + this.gameObject);
		FeelBallsManager manager = FeelBallsManager.Instance;
		if (manager != null)
			manager.AddFeelBall(this);
		colorManager = this.GetComponent<SolidColor> ();
		currentBallColor = new Color (0.5f, 0.5f, 0.0f);
		colorManager.ObjectColor = currentBallColor;
		pickTimeoutBallColor = Color.black;
		Ai = new FeelBallAi (this);
		Ai.ChangeState (DefaultState);
		if (manager == null)
			return;

		if (RandomPersonality)
			this.EmotionalModel = manager.CreateRandomFeelBallEmotionalModel ();
		else
			this.EmotionalModel = manager.CreateFeelBallEmotionalModel (RelationshipToPlayer, Neurotic, Extravert);

		try {
			personality = OccClipsPersonality.CastToDerived (this.EmotionalModel.GetPersonality ());
			isFriendlyToPlayer = personality.relationshipToPlayer > 0.0 ? true : false;
		} catch (System.Exception e) {
			Debug.LogError(e.Message);
		}
		currentBallToPlayerState = GetCurrentBallToPlayerState ();
	}


	private BallToPlayerState GetCurrentBallToPlayerState()
	{
		if (!GameConsts.IsAffectiveMode)
			return BallToPlayerState.NoAffection;

		bool playerDistressed = PlayerModel.Instance.DistressDominates;
		if (playerDistressed) {
			if (isFriendlyToPlayer)
				return BallToPlayerState.PlayerDistressedBallFriendly;
			else
				return BallToPlayerState.PlayerDistressedBallUnfriendly;
		} else {
			
			if (isFriendlyToPlayer)
				return BallToPlayerState.PlayerJoyfullBallFriendly;
			else
				return BallToPlayerState.PlayerJoyfullBallUnfriendly;
		}

	}
	
	// Update is called once per frame
	private float timeSinceLastCall;
	void Update () {

		if(Ai != null)
			Ai.Update ();

		if (!GameConsts.IsAffectiveMode)
			currentBallToPlayerState = GetCurrentBallToPlayerState ();

		if (IsHeld) {
			HoldTimeLeft -= Time.deltaTime;
			if(HoldTimeLeft <= 0.0f)
			{
				FeelBallsManager.Instance.SelectedFeelBall = null;
				WasDropped = true;
				return;
			}

			float throwPowerIncrement = incrementPowerByState[(int)currentBallToPlayerState];
			float throwPowerMin = minPowerByState[(int)currentBallToPlayerState];
			ThrowPower += throwPowerDirectionIncrement*throwPowerIncrement;
			if(ThrowPower > 100.0f)
			{
				ThrowPower = 100.0f;
				throwPowerDirectionIncrement *= -1;
			}
			else if(ThrowPower < throwPowerMin)
			{
				ThrowPower = throwPowerMin;
				throwPowerDirectionIncrement *= -1;
			}
			return;
		}

		if (this.PickTimeout > 0) {
			this.PickTimeout -= Time.deltaTime;
			if(this.PickTimeout < 0)
			{
				colorManager.ObjectColor = currentBallColor;

				if(WasDropped)
				{
					GameManager.Instance.AddDroppedBallStat();
				}
				else if(HasScored)
				{
					GameManager.Instance.AddScoredBallStat();
				}
				else 
				{
					GameManager.Instance.AddMissedBallStat();
				}
			}
			else
			{
				return;
			}
		}

		if (personality == null)
			return;
		
		timeSinceLastCall += Time.deltaTime;
		if (timeSinceLastCall <= 1)
			return;

		timeSinceLastCall = 0;

		switch(currentBallToPlayerState)
		{
		case BallToPlayerState.PlayerDistressedBallFriendly:
			this.Ai.ChangeState(AiState.CheerUp);
			break;
		case BallToPlayerState.PlayerDistressedBallUnfriendly:
			this.Ai.ChangeState(AiState.Erratic);
			break;
		case BallToPlayerState.PlayerJoyfullBallFriendly:
			this.Ai.ChangeState(AiState.Erratic);
			break;
		case BallToPlayerState.PlayerJoyfullBallUnfriendly:
			this.Ai.ChangeState(AiState.Run);
			break;
		default:
			this.Ai.ChangeState(AiState.Erratic);
			break;
		}
	}

	public bool HandleEecEvent(EecEvent eecEvent)
	{
		lock (emotionalStateLock) {
			if (this.EmotionalModel == null)
				return false;

			bool result = this.EmotionalModel.HandleEvent (eecEvent) ;
			return result;
		}
	}

	public void AfterEmotionUpdate(bool result)
	{
		EmotionVector currentVector = null;
		lock (emotionalStateLock) {
			if(!result)
			{
				Debug.LogError (string.Format ("Unable to handle event for ball {0}", this.gameObject));
				return;
			}
			currentVector = this.EmotionalModel.GetEmotionVector();

		}
		if (currentVector == null)
			return;

		if(GameConsts.EnableErfLog)
			Debug.Log(string.Format("BallUpdate {0}: {1}, {2}",this.gameObject.name,currentVector.GetTimeOfEvaluation(),currentVector.Print()));

		if (colorManager == null)
			return;

		double currentJoy = GetCurrentEmotion (currentVector,OccEmotions.JOY);
		double currentDistress = GetCurrentEmotion (currentVector,OccEmotions.DISTRESS);
		if (currentJoy == currentDistress)
			return;
		distressDominates = currentDistress > currentJoy;
		currentBallToPlayerState = GetCurrentBallToPlayerState ();

		CalculateBallCollor ();

	}

	private void CalculateBallCollor()
	{
		if (distressDominates) {
			currentBallColor.r += GameConsts.FeelBallColorChange;
			currentBallColor.g -= GameConsts.FeelBallColorChange;
		} else {
			currentBallColor.g += GameConsts.FeelBallColorChange;
			currentBallColor.r -= GameConsts.FeelBallColorChange;
		}
		currentBallColor.r = GameUtils.Clamp(currentBallColor.r,0.0f,1.0f);
		currentBallColor.g = GameUtils.Clamp(currentBallColor.g,0.0f,1.0f);

		if(PickTimeout <=0.0f)
			colorManager.ObjectColor = currentBallColor;
	}

	public double GetCurrentEmotion(EmotionVector vector, string emotionName)
	{
		double currentEmotion = 0.0;
		Variant emotionVariant= vector.GetValue (emotionName);
		if(emotionVariant != null)
			currentEmotion = emotionVariant.AsDouble();
		return currentEmotion;
	}

	public void OnLeftClick()
	{
		if (GameConsts.GameIsRunning && this.PickTimeout <= 0.0f) {
			if(this.IsHeld)
				return;
			FeelBallsManager.Instance.SelectedFeelBall = this;
		}
	}

	public void OnRightClick() {
		if (!GameConsts.GameIsRunning)
			return;

		float power = 0.0f;
		Transform cameraTransform = Camera.main.transform;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (IsHeld) {
			FeelBallsManager.Instance.SelectedFeelBall = null;
			power = 5.0f* ThrowPower;
			ThrowPower = 50.0f;
			this.gameObject.GetComponent<Rigidbody>().AddForce ((ray.direction+cameraTransform.up) * power);
			if(GameConsts.IsAffectiveMode)
				this.Ai.ChangeState(AiState.IsThrown);
			else
				this.Ai.ChangeState(AiState.Idle);
			HasScored = false;
			WasDropped = false;
			return;
		}
		power = GameUtils.GetRandomNumber (300.0f, 500.0f);
		this.gameObject.GetComponent<Rigidbody>().AddForce (ray.direction * power);
	}

	public void RespawnBall()
	{
		float x = GameUtils.GetRandomNumber (-7.0, 7.0);
		float z = GameUtils.GetRandomNumber (-7.0, 7.0);
		float y = GameUtils.GetRandomNumber (5.0, 6.0);
		Vector3 newPosition = new Vector3 (x, y, z);
		this.gameObject.transform.position = newPosition;
		this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
		currentBallColor = new Color (0.5f, 0.5f, 0.0f);
		distressDominates = false;
		PickTimeout = 0.0f;
		colorManager.ObjectColor = currentBallColor;

	}

	public void ReleasePlayerHold()
	{
		(this.GetComponent("Halo") as Behaviour).enabled = false;
		this.IsHeld = false;
		this.PickTimeout = GameConsts.FeelBallPickTimeout;
		colorManager.ObjectColor = pickTimeoutBallColor;

		this.Ai.ChangeState (AiState.Idle);
		Rigidbody feelBallRigidBody = this.gameObject.GetComponent<Rigidbody>();
		feelBallRigidBody.useGravity = true;
		//this.gameObject.transform.parent = Camera.main.transform;
	}

	public void StartPlayerHold() {
		(this.GetComponent("Halo") as Behaviour).enabled = true;
		this.Ai.ChangeState (AiState.HeldByPlayer);
		this.IsHeld = true;
		Rigidbody feelBallRigidBody = this.gameObject.GetComponent<Rigidbody>();
		feelBallRigidBody.useGravity = false;
		feelBallRigidBody.velocity = Vector3.zero;
		HoldTimeLeft = holdTimeByState [(int)currentBallToPlayerState];
		StartingHoldTime = holdTimeByState [(int)currentBallToPlayerState];
		//this.gameObject.transform.parent = null;
	}

	public void WentThroughHoop(float pointsModifier) {
		int pointsAchieved = pointsAchievedByState[(int)currentBallToPlayerState];
		GameManager.Instance.CurrentScore += (int)(pointsModifier * pointsAchieved);
		HasScored = true;
	}

}
