using UnityEngine;
using System.Collections;
using Erf;

public partial class GuiHolderScript : MonoBehaviour {
	
	
	public GUIStyle LeftHeaderStyle = null;
	bool leftPanelExpanded = false;
	bool leftPanelIsExpanding = false;
	
	private void RenderPlayerInformation(PlayerModel playerModel, EmotionVector current)
	{
		float currentJoy = current.GetValue (OccEmotions.JOY).AsFloat ();
		float currentDistress = current.GetValue (OccEmotions.DISTRESS).AsFloat ();
		string dominatingEmotion = currentJoy > currentDistress ? "Joy" : currentJoy < currentDistress ? "Distress" : "Neutral";
		if (!leftPanelExpanded) {
			if (GUI.Button (new Rect (0, Screen.height - HeaderHeight, PanelWidth, HeaderHeight), dominatingEmotion, LeftHeaderStyle))
				leftPanelExpanded = true;
			return;
		} 
		
		if (GUI.Button (new Rect (0, Screen.height - (HeaderHeight + PanelHeight), PanelWidth, HeaderHeight), dominatingEmotion, LeftHeaderStyle))
			leftPanelExpanded = false;
		GUI.DrawTexture (new Rect (0, Screen.height - PanelHeight, PanelWidth, PanelHeight), PanelTexture);
		//GUI.
		EmotionVector inPadScale = playerModel.CurrentUnfilteredVector;
		if (inPadScale != null) {
			float p = inPadScale.GetValue (PadEmotions.PLEASURE).AsFloat ();
			float a = inPadScale.GetValue (PadEmotions.AROUSAL).AsFloat ();
			float d = inPadScale.GetValue (PadEmotions.DOMINANCE).AsFloat ();
			string padDisplay = string.Format ("P: {0:0.00} A: {1:0.00} D: {2:0.00}", p, a, d);
			GUI.Box (new Rect (0, Screen.height - PanelHeight, PanelWidth, PanelHeight), padDisplay);
		}
		string occDisplay = string.Format ("Joy: {0:0.00} Distress: {1:0.00}", currentJoy, currentDistress);
		GUI.Box (new Rect (0, Screen.height - PanelHeight + linesHeight, PanelWidth, PanelHeight), occDisplay);
		
	}
	
}

