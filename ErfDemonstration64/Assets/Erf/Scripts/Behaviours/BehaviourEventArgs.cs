﻿using UnityEngine;
using System;
using System.Collections;

public class BehaviorEventArgs : EventArgs
{
	public GameObject Owner { get; set; }
	public AiBehavior Behavior { get; set; }
}
