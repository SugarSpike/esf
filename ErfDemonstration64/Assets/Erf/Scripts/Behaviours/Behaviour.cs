﻿using UnityEngine;
using System.Collections;

public abstract class AiBehavior
{
	public GameObject Owner { get; set; }
	public GameObject Target { get; set; }
	public abstract void Update();
	public abstract void Enter();
	public abstract void Exit();
	
}
