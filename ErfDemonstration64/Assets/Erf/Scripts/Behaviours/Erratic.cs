﻿using System;
using UnityEngine;
using System.Collections;

public class Erratic : CheerUp {
		

	#region Initialization
	
	public Erratic() : base() { 
	}
	
	public Erratic(float range)
		: base(range)
	{
	}
	
	public Erratic(GameObject target, float range)
		: base(target,range)
	{
	}
	
	#endregion

	protected override void Jump()
	{
		Owner.GetComponent<Rigidbody>().AddForce (Vector3.up * UnityEngine.Random.Range(129.0f,160.0f));
		
	}
}
