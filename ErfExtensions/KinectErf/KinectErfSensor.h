#pragma once
#include "CommonKinectErf.h"
#include "EmotionSensor.h"
#include <sstream>
#include <fstream> 
#include <iostream> 
#include <string>
#include "FaceTrackingHelper.h"

namespace erf
{

    class KINECTERF_API KinectErfSensor : public EmotionSensor
    {

	private:
		vector<EmotionSensorReading* > previousReadings;
		map<string,string> parameters;
		unsigned int maxNumberOfPreviousReadings;
		int currentSensorBufferIndex;
		string lastError;
		FaceTrackingHelper faceTrackingHelper;

		void InitializeDefaultParameters();

		NUI_IMAGE_TYPE              depthType;
		NUI_IMAGE_TYPE              colorType;
		NUI_IMAGE_RESOLUTION        depthRes;
		NUI_IMAGE_RESOLUTION        colorRes;
		int                         nearMode;
		int                         seatedSkeletonMode;
		
		NUI_IMAGE_TYPE ParseImageType(const std::string& value);
		NUI_IMAGE_RESOLUTION ParseImageResolution(const std::string& value);

		void SaveAuResult(float* data,unsigned int numberOfAu);

	protected:

		static void FaceTrackedCallingBack(LPVOID lpParam);

	public:
		KinectErfSensor(ExternalComponentConfiguration* pConfiguration);
		KinectErfSensor(const KinectErfSensor &other);

		virtual ~KinectErfSensor();

		virtual EmotionSensorReading* GetSensorReading();

		virtual void GetSensorReadings(vector<EmotionSensorReading*>& container, int numberOfReadings);

		virtual string GetComponentLastError();

		virtual void GetSupportedParameters(vector<string>& paramHolder);

		virtual void GetParameters(map<string,string>& paramHolder);

		virtual const string GetParameter(string parameterName);

		virtual bool SetParameter(string parameterName, string parameterValue);

		virtual void Initialize();

		virtual void Reinitialize();

		virtual void Deinitialize();

	};
}

