
#include "stdafx.h"
#include "KinectEmotionModel.h"
#include "ExternalComponentEnums.h"
#include "ExternalComponentConfiguration.h"
#include "EmotionEnums.h"
#include "CharacterData.h"
#include "EecEvent.h"
#include "Variant.h"
#include "EmotionVector.h"
#include "EmotionConstants.h"
#include "EmotionSensorReading.h"
#include "EmotionSensor.h"
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>

namespace erf {
	
	enum AU_MODEL_EMOTIONS { 
		ANGER = 0,
		DISTRESS,
		FEAR,
		JOY,
		DISLIKING,
		MAX_DIFFERENCE
	};

	float AU_MODEL[5][7] = {
		{0.6f,	0.1f,	-0.1f,	0.6f,	0.1f,	-0.4f,	7.9f}, //Anger
		{-0.5f,	0.0f,	-0.3f,	0.3f,	0.8f,	-0.7f,	8.6f}, //Distress/Sadness
		{0.0f,	0.5f,	0.0f,	-0.3f,	0.2f,	0.2f,	7.2f}, //Fear
		{0.3f,	0.1f,	0.6f,	0.0f,	-0.8f,	0.0f,	7.8f}, //Joy/Happines
		{0.2f,	0.1f,	-0.1f,	0.0f,	-0.1f,	-0.1f,	6.6f} //Disliking/Disgust
	};

	KinectEmotionModel::KinectEmotionModel(ExternalComponentConfiguration* pConfiguration): 
		BasicObject("KinectEmotionModel"),EmotionExpert(pConfiguration)
	{
		this->componentState = ComponentState::INITIALIZED;
	}

	KinectEmotionModel::KinectEmotionModel(const KinectEmotionModel &other): 
			EmotionExpert(other.configuration)
	{
		this->objectName="KinectEmotionModel";
		this->timeElapsedEventTreshold = other.timeElapsedEventTreshold;
		this->globalExpertParameters.insert(other.globalExpertParameters.begin(),other.globalExpertParameters.end());
	}

	KinectEmotionModel::~KinectEmotionModel() { 
	} ;


	string KinectEmotionModel::GetComponentLastError(){
		string error = this->lastError;
		this->lastError = "";
		return error;
	}


	void KinectEmotionModel::GetParameters( map<string,string>& paramHolder) {
		paramHolder.insert(this->globalExpertParameters.begin(), this->globalExpertParameters.end());
	}

	void KinectEmotionModel::GetSupportedParameters(vector<string>& paramHolder){
		for(auto it = globalExpertParameters.begin(); it != globalExpertParameters.end(); ++it) {
			paramHolder.push_back(it->first);
		}
	}

	const string KinectEmotionModel::GetParameter(string parameterName)
	{
		map<string,string>::iterator it = this->globalExpertParameters.find(parameterName);
		if(it != this->globalExpertParameters.end())
			return it->second;
		return null;
	}

	bool KinectEmotionModel::SetParameter(string parameterName, string parameterValue){
		map<string,string>::iterator it = this->globalExpertParameters.find(parameterName);
		if(it == this->globalExpertParameters.end())
			return false;
		it->second = parameterValue;
		return true;
	}


	void KinectEmotionModel::InitializeForCharacter(CharacterData& characterData) {
		//Initialize for a new Game Character
		KinectModelData* modelData = new KinectModelData();
		modelData->timeElapsedEventSinceStart = 0;
		characterData.AddValue("kinect_data",Variant::Create<KinectModelData>(modelData));
	}
			
	bool KinectEmotionModel::IsEventSupported(EecEvent& event) { 
		int type = event.GetEventType();
		if(type == GameEvent::STD_TIME_ELAPSED)
			return true;
		return false; 
	} 
	
	EmotionVector*  KinectEmotionModel::InitializeEmotionVector(){
		EmotionVector* vector = new EmotionVector(EmotionModelType::STD_OCC);
		vector->AddValue(OccEmotions::JOY, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::DISTRESS, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::PRIDE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::SHAME, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::ADMIRATION, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::REPROACH, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::LOVE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::HATE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::HAPPY_FOR, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::RESENTMENT, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::GLOATING, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::PITY, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::HOPE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::FEAR, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::SATISFACTION, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::FEARS_CONFIRMED, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::RELIEF, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::DISAPPOINTMENT, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::GRATIFICATION, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::REMORSE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::GRATITUDE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::ANGER, Variant::Create(0.0f));
		return vector;
	}

	float CalculateAuDelta(int emotion, EmotionSensorReading* reading) {
		float difference = 0.0f;
		for(int i = 0 ; i < reading->ValueCount() ; i++) {
			float auValue = reading->GetValueByIndex(i)->AsFloat();
			float auModelValue = AU_MODEL[emotion][i];
			difference += abs(auValue - auModelValue);
		}
		float maxDifference = AU_MODEL[emotion][reading->ValueCount()];
		return (maxDifference-difference)/maxDifference;
	}

	EmotionVector* KinectEmotionModel::HandleEvent(EecEvent& event, CharacterData& userData) { 
		Variant* kinectData = userData.GetValue("kinect_data");
		if(kinectData == null || !kinectData->Is<KinectModelData>())
		{
			return null;
		}

		KinectModelData* data = kinectData->As<KinectModelData>();
		EmotionSensor* kinectSensor = userData.GetSensorByName("KinectErfSensor");
		if(kinectSensor == null)
		{
			cout << "KinectEmotionModel::HandleEvent - KinectErfSensor sensor not found!" << endl;
			return null; 
		}
		EmotionSensorReading* reading = kinectSensor->GetSensorReading();
		if(reading == null)
		{
			cout << "KinectEmotionModel::HandleEvent - KinectErfSensor has no available readings!" << endl;
			return null; 
		}

		//EmotionVector* newEmotionVector = InitializeEmotionVector();
		EmotionVector* newEmotionVector = new EmotionVector(EmotionModelType::STD_OCC);
		float angerDelta = CalculateAuDelta(AU_MODEL_EMOTIONS::ANGER,reading);
		float distressDelta = CalculateAuDelta(AU_MODEL_EMOTIONS::DISTRESS,reading);
		float fearDelta = CalculateAuDelta(AU_MODEL_EMOTIONS::FEAR,reading);
		float joyDelta = CalculateAuDelta(AU_MODEL_EMOTIONS::JOY,reading);
		float dislikingDelta = CalculateAuDelta(AU_MODEL_EMOTIONS::DISLIKING,reading);

		newEmotionVector->AddValue(OccEmotions::ANGER, Variant::Create(angerDelta));
		newEmotionVector->AddValue(OccEmotions::DISTRESS, Variant::Create(distressDelta)); //sadness
		newEmotionVector->AddValue(OccEmotions::FEAR, Variant::Create(fearDelta));
		newEmotionVector->AddValue(OccEmotions::JOY, Variant::Create(joyDelta));
		newEmotionVector->AddValue(OccEmotions::DISLIKING, Variant::Create(dislikingDelta)); //Disgust

		return newEmotionVector; 
	}

}