#ifndef _DEBUG
#include "stdafx.h"
#endif

#ifdef KINECTERF_EXPORTS
	#define KINECTERF_API __declspec(dllexport)
#else
	#define KINECTERF_API __declspec(dllimport)
#endif