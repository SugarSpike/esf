#include "stdafx.h"
#include "KinectErfSensor.h"
#include "ExternalComponentConfiguration.h"
#include "ExternalComponentEnums.h"
#include "EmotionSensorReading.h"
#include "Variant.h"
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include "ErfUtils.h"
#include <ctime>
#include <windowsx.h>
#include <WinError.h>
#include "ErfParameterUtils.h"

namespace erf {

	KinectErfSensor::KinectErfSensor(ExternalComponentConfiguration* pConfiguration): 
		BasicObject("KinectErfSensor"),EmotionSensor(pConfiguration, SensorType::STD_AU)
	{
		this->currentSensorBufferIndex = 0;
		this->componentState = ComponentState::UNINITIALIZED;
		InitializeDefaultParameters();
	}

	KinectErfSensor::KinectErfSensor(const KinectErfSensor &other): 
		BasicObject(other.objectName),EmotionSensor(other.configuration, SensorType::STD_AU)
	{
		throw "Not supported";
	}

	void KinectErfSensor::InitializeDefaultParameters() {
		SetParameter("NEAR_MODE","TRUE");
		SetParameter("SEATED_SKELETON_MODE","TRUE");
		SetParameter("DEPTH_TYPE","NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX");
		SetParameter("DEPTH_RES","NUI_IMAGE_RESOLUTION_320x240");
		SetParameter("COLOR_TYPE","NUI_IMAGE_TYPE_COLOR");
		SetParameter("COLOR_RES","NUI_IMAGE_RESOLUTION_640x480");
	}

	KinectErfSensor::~KinectErfSensor() { 
		for(unsigned int i = 0 ; i < previousReadings.size() ; i++)
			delete previousReadings[i];
		if(this->componentState == ComponentState::REINITIALIZED
				|| this->componentState == ComponentState::INITIALIZED)
			Deinitialize();
	} 


	EmotionSensorReading* KinectErfSensor::GetSensorReading()
	{
		//cout << "KinectErfSensor::GetSensorReading called" << endl;

		if(this->componentState == ComponentState::COMPONENT_ERROR ||
			this->componentState == ComponentState::DEINITIALIZED ||
			this->componentState == ComponentState::UNINITIALIZED)
		{
			lastError = "Component was not initialized";
			cout << "KinectErfSensor::GetSensorReading error: "<< lastError << endl;
			return null;
		}
		if(previousReadings.size() > 0)
			return previousReadings[previousReadings.size()-1];

		return null;
	}

	void KinectErfSensor::GetSensorReadings(vector<EmotionSensorReading*>& container, int numberOfReadings) {
		int readReadings = 0 ;
		for(int i = this->previousReadings.size() -1 ; i >= 0 ; i--)
		{
			if(readReadings == numberOfReadings)
				break;
			container.push_back(this->previousReadings[i]);
			readReadings++;
		}
	}

	string KinectErfSensor::GetComponentLastError(){
		cout << "KinectErfSensor::GetComponentLastError called" << endl;
		string error = this->lastError;
		this->lastError = "";
		return error;
	}

	
	void KinectErfSensor::GetParameters(map<string,string>& paramHolder){
		cout << "KinectErfSensor::GetParameters called" << endl;
		paramHolder.insert(parameters.begin(),parameters.end());
	}

	void KinectErfSensor::GetSupportedParameters(vector<string>& paramHolder){
		cout << "KinectErfSensor::GetSupportedParameters called" << endl;
		for(map<string,string>::iterator it = parameters.begin(); it != parameters.end(); ++it) {
			paramHolder.push_back(it->first);
		}
	}

	const string KinectErfSensor::GetParameter(string parameterName)
	{
		cout << "KinectErfSensor::GetParameter called" << endl;
		map<string,string>::iterator it = parameters.find(parameterName);
		if(it != parameters.end())
			return it->second;

		return null;
	}

	bool KinectErfSensor::SetParameter(string parameterName, string parameterValue){
		cout << "KinectErfSensor::SetParameter called" << endl;
		parameters[parameterName] = parameterValue;
		if(parameterName == "NEAR_MODE") {
			this->nearMode = erfutils::ParseIntFromBool(parameterValue);
		} else if(parameterName == "SEATED_SKELETON_MODE") {
			this->seatedSkeletonMode = erfutils::ParseIntFromBool(parameterValue);
		} else if(parameterName =="DEPTH_TYPE") {
			this->depthType = ParseImageType(parameterValue);
		} else if(parameterName =="DEPTH_RES") {
			this->depthRes = ParseImageResolution(parameterValue);
		} else if(parameterName =="COLOR_TYPE") {
			this->colorType = ParseImageType(parameterValue);
		} else if(parameterName =="COLOR_RES") {
			this->colorRes = ParseImageResolution(parameterValue);
		}
		return true;
	}
	
	NUI_IMAGE_TYPE KinectErfSensor::ParseImageType(const std::string& value) {
		if(value == "NUI_IMAGE_TYPE_DEPTH") {
			return NUI_IMAGE_TYPE_DEPTH;
		} else if(value == "NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX") {
			return NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX;
		} else if(value == "NUI_IMAGE_TYPE_COLOR") {
			return NUI_IMAGE_TYPE_COLOR;
		} else if(value == "NUI_IMAGE_TYPE_COLOR_YUV") {
			return NUI_IMAGE_TYPE_COLOR_YUV;
		} else if(value == "NUI_IMAGE_TYPE_COLOR_RAW_YUV") {
			return NUI_IMAGE_TYPE_COLOR_RAW_YUV;
		} else if(value == "NUI_IMAGE_TYPE_COLOR_INFRARED") {
			return NUI_IMAGE_TYPE_COLOR_INFRARED;
		} else if(value == "NUI_IMAGE_TYPE_COLOR_RAW_BAYER") {
			return NUI_IMAGE_TYPE_COLOR_RAW_BAYER;
		}
		return NUI_IMAGE_TYPE_DEPTH;
	}

	NUI_IMAGE_RESOLUTION KinectErfSensor::ParseImageResolution(const std::string& value) {
		if(value == "NUI_IMAGE_RESOLUTION_80x60") {
			return NUI_IMAGE_RESOLUTION_80x60;
		} else if(value == "NUI_IMAGE_RESOLUTION_320x240") {
			return NUI_IMAGE_RESOLUTION_320x240;
		} else if(value == "NUI_IMAGE_RESOLUTION_640x480") {
			return NUI_IMAGE_RESOLUTION_640x480;
		} else if(value == "NUI_IMAGE_RESOLUTION_1280x960") {
			return NUI_IMAGE_RESOLUTION_1280x960;
		} 
		return NUI_IMAGE_RESOLUTION_INVALID;
	}

	void KinectErfSensor::Initialize() {
		if(this->componentState != ComponentState::UNINITIALIZED &&
			this->componentState != ComponentState::DEINITIALIZED &&
			this->componentState != ComponentState::COMPONENT_ERROR)
		{
			lastError = "Only unitialized components can be initialized";
			return;
		}


		if(!SUCCEEDED(faceTrackingHelper.Init(null,
			FaceTrackedCallingBack,
			this,
			this->depthType,
			this->depthRes,
			this->nearMode,
			1, // if near mode doesn't work, fall back to default mode
			this->colorType,
			this->colorRes,
			this->seatedSkeletonMode))) {
				lastError ="Could not create FaceTracking instance";
				this->componentState = ComponentState::COMPONENT_ERROR;
				return;
		}

		this->componentState = ComponentState::INITIALIZED;
	}

	void KinectErfSensor::Reinitialize() {
		if(this->componentState != ComponentState::INITIALIZED &&
			this->componentState != ComponentState::DEINITIALIZED &&
			this->componentState != ComponentState::COMPONENT_ERROR&&
			this->componentState != ComponentState::REINITIALIZED)
		{
			lastError = "Only initialized/deinitialized components can be reinitialized";
			return;
		}
		Deinitialize();
		Initialize();
		this->componentState = ComponentState::REINITIALIZED;
	}

	void KinectErfSensor::Deinitialize(){
		if(this->componentState != ComponentState::INITIALIZED &&
			this->componentState != ComponentState::REINITIALIZED &&
			this->componentState != ComponentState::COMPONENT_ERROR)
		{
			lastError = "Only initialized/reinitialized components can be deinitialized";
			return;
		}
		this->faceTrackingHelper.Stop();
		this->componentState = ComponentState::DEINITIALIZED;
	}
	
	void KinectErfSensor::SaveAuResult(float* data,unsigned int numberOfAu) {
		
		EmotionSensorReading* reading = new EmotionSensorReading();
		char numstr[3];
		std::string baseName = "AU";
		for(unsigned int i = 0 ; i < numberOfAu ; i++) {
			sprintf(numstr, "%d", i+1);
			reading->AddValue(baseName+numstr,Variant::Create(data[i]));
		}
		
		previousReadings.push_back(reading);
		while(this->previousReadings.size() > 
				maxNumberOfPreviousReadings)
		{
			EmotionSensorReading* oldReading = previousReadings[0];
			previousReadings.erase(previousReadings.begin());
			delete oldReading;
		}
	}

	void KinectErfSensor::FaceTrackedCallingBack(PVOID pVoid)
	{
		KinectErfSensor* pSensor = reinterpret_cast<KinectErfSensor*>(pVoid);
		if (pSensor)
		{
			IFTResult* pResult = pSensor->faceTrackingHelper.GetResult();
			if (pResult && SUCCEEDED(pResult->GetStatus()))
			{
				FLOAT* pAU = NULL;
				UINT numAU;
				pResult->GetAUCoefficients(&pAU, &numAU);
				pSensor->SaveAuResult(pAU,numAU);

			}
		}
	}


}