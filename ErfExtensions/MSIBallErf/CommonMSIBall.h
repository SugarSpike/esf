#ifndef _DEBUG
#include "stdafx.h"
#endif

#ifdef MSIBALLERF_EXPORTS
	#define MSIBALLERF_API __declspec(dllexport)
#else
	#define MSIBALLERF_API __declspec(dllimport)
#endif