#pragma once
#include "CommonMSIBall.h"
#include "EmotionExpert.h"
#include "GameEnums.h"

namespace erf
{
	//This is an example of extending Game Events in order to
	//add new types
	//This enum must also be created on C# side, either throu SWIG library (lots of work)
	//or created by hand in a .cs file. I have created an example file MSIBallErfEnums.cs
	//and added it to this project (the .cs file needs to be added on the Unity side by hand)
	enum MSIBallEventType {
		MSIBall_OTHER_EVENT = GameEvent::EXT_GAME_EVENT,
		MSIBall_YET_ANOTHER_EVENT
	};

	//Just a simple struct to hold all the character-specific data
	//that is used by the expert
	struct MSIBALLERF_API MSIBallModelData {
		unsigned int timeElapsedEventSinceStart;
	};

	// Class representing the Emotion Model for MSIBall, the current version is a mock
	// Extending EmotionExpert enable us to load the expert dynamicly from a .dll and
	// also to register it as an Emoton Model in Game Characters
    class MSIBALLERF_API MSIBallEmotionModel : public EmotionExpert
    {
	private:
		//Parameters that are shared by all Characters using this expert
		//Character specific data should be stored in the CharacterData object
		//Currently there is only one global param: 
		//  TIME_ELAPSED_EVENT_TRESHOLD
		map<string,string> globalExpertParameters;
		//The global parameter
		unsigned int timeElapsedEventTreshold;
		//Last error message buffer
		string lastError;
		//Just initialize the first emotion vector with values
		EmotionVector* InitializeEmotionVector();
	public:
		MSIBallEmotionModel(ExternalComponentConfiguration* pConfiguration);

		// The copy constructor, can be ignored for now (default impl will suffice)
		MSIBallEmotionModel(const MSIBallEmotionModel &other);

		virtual ~MSIBallEmotionModel();

		//Expert methods

		//The expert ranking - an arbitrary value between 0..1 that should represent the expert rank
		virtual float GetModelRank() { return 1.0f; }

		//Should return last error in case of Error - no need to worry about that in a mock
		virtual string GetComponentLastError();

		//Return a global expert parameter value for given parameter name - Read only
		virtual const string GetParameter(string parameterName);

		//Returns the names of all supported global parameters
		virtual void GetSupportedParameters(vector<string>& paramHolder);
		
		//Returns a name:value pairs of all global parameters - Read only
		virtual void GetParameters(map<string,string>& paramHolder);

		//Sets the value of a global parameter
		virtual bool SetParameter(string parameterName, string parameterValue);

		// No need to initialize/reinitialize/deinitialize the mock
		virtual void Initialize() {};

		virtual void Reinitialize() {};

		virtual void Deinitialize() {};

		//Emotion model methods
		//Expert initialization for given Game Character
		virtual void InitializeForCharacter(CharacterData& characterData);
			
		//Return whenever the given event is supported by this expert
		//A general overhaul is comming for this part of application and
		//this function may dissapear altogether
		virtual bool IsEventSupported(EecEvent& event);

		//Handles an event, generating an emotion vector in response
		virtual EmotionVector* HandleEvent(EecEvent& event, CharacterData& userData);

	};
}

