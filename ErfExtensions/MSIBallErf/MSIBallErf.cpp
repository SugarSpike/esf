// ExternalLibraryMock.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "CommonMSIBall.h"
#include "ExternalComponentLibrary.h"
#include "ExternalComponentEnums.h"
#include "MSIBallEmotionModel.h"
using namespace erf;


extern "C" MSIBALLERF_API void RegisterComponents(ExternalComponentLibrary &library) {
	library.SetName("MSIBallErf");
	new MSIBallEmotionModel(&(library.GetConfiguration()));
}

//Returns what component types does the library support (Sensors and/or Experts)
extern "C" MSIBALLERF_API bool SupportsComponentType(ComponentType type)
{
	if(type == ComponentType::EXPERT)
		return true;
	return false;
}

//No global .dll parameters available, so just return false
extern "C" MSIBALLERF_API bool SetParameter(string parameterName, string parameterValue)
{
	return false;
}

//No global .dll parameters available, so just return null
extern "C" MSIBALLERF_API const std::string GetParameter(string parameterName)
{
	return null;
}

//No global .dll parameters available, so just return an empty map
//May be refactored (generates warning as map is not on heap)
extern "C" MSIBALLERF_API void GetParameters(map<string,string>& paramHolder)
{
}

//No global .dll parameters available, so just return an empty vector
//May be refactored (generates warning as vector is not on heap)
extern "C" MSIBALLERF_API void GetSupportedParameters(vector<string>& paramHolder){
}