
#include "stdafx.h"
#include "MSIBallEmotionModel.h"
#include "ExternalComponentEnums.h"
#include "ExternalComponentConfiguration.h"
#include "EmotionEnums.h"
#include "CharacterData.h"
#include "EecEvent.h"
#include "Variant.h"
#include "EmotionVector.h"
#include "EmotionConstants.h"
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>

namespace erf {

	MSIBallEmotionModel::MSIBallEmotionModel(ExternalComponentConfiguration* pConfiguration): 
		BasicObject("MSIBallEmotionModel"),EmotionExpert(pConfiguration)
	{
		this->timeElapsedEventTreshold = 50;
		this->componentState = ComponentState::INITIALIZED;
		//Not very clean, but for a mock will do
		this->globalExpertParameters["TIME_ELAPSED_EVENT_TRESHOLD"] = "50";
	}

	MSIBallEmotionModel::MSIBallEmotionModel(const MSIBallEmotionModel &other): 
			EmotionExpert(other.configuration)
	{
		this->objectName="MSIBallEmotionModel";
		this->timeElapsedEventTreshold = other.timeElapsedEventTreshold;
		this->globalExpertParameters.insert(other.globalExpertParameters.begin(),other.globalExpertParameters.end());
	}

	MSIBallEmotionModel::~MSIBallEmotionModel() { 
	} ;


	string MSIBallEmotionModel::GetComponentLastError(){
		string error = this->lastError;
		this->lastError = "";
		return error;
	}


	void MSIBallEmotionModel::GetParameters( map<string,string>& paramHolder) {
		paramHolder.insert(this->globalExpertParameters.begin(), this->globalExpertParameters.end());
	}

	void MSIBallEmotionModel::GetSupportedParameters(vector<string>& paramHolder){
		for(auto it = globalExpertParameters.begin(); it != globalExpertParameters.end(); ++it) {
			paramHolder.push_back(it->first);
		}
	}

	const string MSIBallEmotionModel::GetParameter(string parameterName)
	{
		map<string,string>::iterator it = this->globalExpertParameters.find(parameterName);
		if(it != this->globalExpertParameters.end())
			return it->second;
		return null;
	}

	bool MSIBallEmotionModel::SetParameter(string parameterName, string parameterValue){
		map<string,string>::iterator it = this->globalExpertParameters.find(parameterName);
		if(it == this->globalExpertParameters.end())
			return false;

		if(parameterName == "TIME_ELAPSED_EVENT_TRESHOLD" )
		{
			try {
				std::stringstream ss(parameterValue);
				int i;
				if ((ss >> i).fail() || !(ss >> std::ws).eof())
					throw std::bad_cast();

				if(parameterName == "PULSE_MIN" )
					this->timeElapsedEventTreshold = i;

			}catch(...)
			{
				lastError = "Numeric value expected";
				return false;
			}

		}
		it->second = parameterValue;
		return true;
	}


	void MSIBallEmotionModel::InitializeForCharacter(CharacterData& characterData) {
		//Initialize for a new Game Character
		MSIBallModelData* modelData = new MSIBallModelData();
		modelData->timeElapsedEventSinceStart = 0;
		characterData.AddValue("MSIBall_data",Variant::Create<MSIBallModelData>(modelData));
	}
			
	bool MSIBallEmotionModel::IsEventSupported(EecEvent& event) { 
		int type = event.GetEventType();
		if( type == MSIBallEventType::MSIBall_OTHER_EVENT ||
			type == MSIBallEventType::MSIBall_YET_ANOTHER_EVENT ||
			type == GameEvent::STD_OBSTACLES_CLOSE ||
			type == GameEvent::STD_SKILL_USED ||
			type == GameEvent::STD_TIME_ELAPSED)
			return true;
		return false; 
	} 
	
	EmotionVector*  MSIBallEmotionModel::InitializeEmotionVector(){
		EmotionVector* vector = new EmotionVector(EmotionModelType::STD_OCC);
		vector->AddValue(OccEmotions::JOY, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::DISTRESS, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::PRIDE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::SHAME, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::ADMIRATION, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::REPROACH, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::LOVE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::HATE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::HAPPY_FOR, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::RESENTMENT, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::GLOATING, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::PITY, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::HOPE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::FEAR, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::SATISFACTION, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::FEARS_CONFIRMED, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::RELIEF, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::DISAPPOINTMENT, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::GRATIFICATION, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::REMORSE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::GRATITUDE, Variant::Create(0.0f));
		vector->AddValue(OccEmotions::ANGER, Variant::Create(0.0f));
		return vector;
	}


	EmotionVector* MSIBallEmotionModel::HandleEvent(EecEvent& event, CharacterData& userData) { 
		Variant* MSIBallData = userData.GetValue("MSIBall_data");
		if(MSIBallData == null || !MSIBallData->Is<MSIBallModelData>())
		{
			//No data found
			return null;
		}

		MSIBallModelData* data = MSIBallData->As<MSIBallModelData>();

		EmotionVector* currentVector = userData.GetCurrentEmotionVector();
		float currentFear = 0.0f;

		if(currentVector != null)
		{
			currentFear = currentVector->GetValue(OccEmotions::FEAR)->AsFloat();
		}

		double randRange = rand() / double(RAND_MAX);

		/*
		MSIBallObstacleIsClose - b�dzie przez nas wysy�any gdy w pobli�u gracza znajdzie si� przeszkoda
		z 50% prawdpopodobie�stwem ustawia aktualna emocje na Fear += 0.5
		z 10% na Fear -= 0.5
		z 40% na Fear += 0.0*/
		if(event.GetEventType() == GameEvent::STD_OBSTACLES_CLOSE)
		{
			if(randRange < 0.1f)
				currentFear -= 0.5f;
			else if(randRange < 0.5f)
				null;
			else
				currentFear += 0.5f;
		}
		/*	MSIBallUsedSkill - b�dzie przez nas wysy��ny przy u�yciu umiej�tno�ci
		z 50% prawpopodobienstwem ustawia aktualna emocje na Fear -= 0.5
		z 10% na fear += 0.5
		z 40% na fear += 0*/
		else if(event.GetEventType() == GameEvent::STD_SKILL_USED)
		{
			if(randRange < 0.1f)
				currentFear += 0.5f;
			else if(randRange < 0.5f)
				null;
			else
				currentFear -= 0.5f;
		}
		/* MSIBallTimeElapsed - b�dzie przez nas wysy�any co kilka sekund
		zwi�ksza szanse przej�cia w wy�szy stan Fear w zale�no�ci od ilo�ci wcze�niej otrzymanych event�w MSIBallTimeElapsed.
		pocz�tkowo:
		50% na fear -= 0.1
		50% na fear += 0.1
		w trakcie gry:
		50 - X % na fear -= 0.1
		50% na fear += 0
		X % na fear += 0.1
		i w ko�cowych fazach gry (po otrzymaniu 50 takich event�w):
		0 % na fear -= 0.1
		50 - X % na fear += 0
		50 + X % na fear += 0.1*/
		else if(event.GetEventType() == GameEvent::STD_TIME_ELAPSED)
		{
			if(data->timeElapsedEventSinceStart < timeElapsedEventTreshold)
			{
				float x = data->timeElapsedEventSinceStart * 0.01f;
				if(randRange < (0.5f-x)){
					null;
				}
				else {
					currentFear += 0.1f;
				}
				data->timeElapsedEventSinceStart += 1;
			}
			else
			{
				currentFear += 0.1f;
			}
		}

		currentFear = currentFear < 0.0f ? 0.0f : (currentFear > 1.0f ? 1.0f : currentFear);

		//I'm assuming that you are using the OCC model of emotion or one of its subset 
		//As the mock is evaluating a specific emotion (fear) 
		//described by one arbitrary value (propability of state or intensity of emotion?)
		EmotionVector* newEmotionVector = null;
		if(currentVector != null) {
			 newEmotionVector = currentVector->Clone(); 
		}
		else {
			newEmotionVector = InitializeEmotionVector();
		}
		newEmotionVector->AddValue(OccEmotions::FEAR,Variant::Create(currentFear));
		return newEmotionVector; 
	}

}