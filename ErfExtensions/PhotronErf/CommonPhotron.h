#ifndef _DEBUG
#include "stdafx.h"
#endif

#ifdef PHOTRONERF_EXPORTS
	#define PHOTRONERF_API __declspec(dllexport)
#else
	#define PHOTRONERF_API __declspec(dllimport)
#endif