#include "stdafx.h"
#include "PadAffectionExpert.h"
#include "EmotionEnums.h"
#include "EmotionConstants.h"
#include "EmotionVector.h"
#include "EmotionSensor.h"
#include "CharacterData.h"
#include "EmotionSensorReading.h"
#include <sstream>
#include <ctime>
#include "Variant.h"

namespace erf 
{

		float RandomFloat(float min, float max)
		{
			return min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max-min)));
		}

		float ClampFloat(float val, float min, float max)
		{
			return  val < min ? min : (val > max ? max : val);
		}

		PadAffectionExpert::PadAffectionExpert(ExternalComponentConfiguration* pConfiguration):
			BasicObject("PadAffectionExpert"),EmotionExpert(pConfiguration)
		{
			this->smallTreshold = 80;
			this->midTreshold = 120;
			this->modelRank = 0.5f;
			
			this->bvpSensorName = "BvpSensor";
			this->emgSensorName = "EmgSensor";
			this->scSensorName = "ScSensor";
			parameters["MODEL_RANK"] = "0.5";
			parameters["SMALL_TRESHOLD"] = "80";
			parameters["MID_TRESHOLD"] = "120";
			parameters["BVP_SENSOR_NAME"] = bvpSensorName;
			parameters["EMG_SENSOR_NAME"] = emgSensorName;
			parameters["SC_SENSOR_NAME"] = scSensorName;
			this->componentState = ComponentState::INITIALIZED;
		}

		PadAffectionExpert::PadAffectionExpert(const PadAffectionExpert &other):
			EmotionExpert(other.configuration)
		{ throw "not supported";}

		PadAffectionExpert::~PadAffectionExpert() { }

	   //Expert methods
		string PadAffectionExpert::GetComponentLastError(){
			cout << "PadAffectionExpert::GetComponentLastError called" << endl;
			string error = this->lastError;
			this->lastError = "";
			return error;
		}


		void PadAffectionExpert::GetParameters(map<string,string>& paramHolder){
			cout << "PadAffectionExpert::GetParameters called" << endl;
			paramHolder.insert(parameters.begin(),parameters.end());
		}

		void PadAffectionExpert::GetSupportedParameters(vector<string>& paramHolder){
			cout << "PadAffectionExpert::GetSupportedParameters called" << endl;
			for(map<string,string>::iterator it = parameters.begin(); it != parameters.end(); ++it) {
				paramHolder.push_back(it->first);
			}
		}

		const string PadAffectionExpert::GetParameter(string parameterName)
		{
			cout << "PadAffectionExpert::GetParameter called: " << parameterName << endl;
			map<string,string>::iterator it = parameters.find(parameterName);
			if(it != parameters.end())
				return it->second;

			return null;
		}

		bool PadAffectionExpert::SetParameter(string parameterName, string parameterValue){
			cout << "PadAffectionExpert::SetParameter called: " << parameterName << ":" << parameterValue<< endl;
			map<string,string>::iterator it = parameters.find(parameterName);
			if(it == parameters.end())
			{
				cout << "PadAffectionExpert::SetParameter param not supported: " << parameterName << endl;
				return false;
			}

			if(parameterName == "SMALL_TRESHOLD" 
				|| parameterName == "MID_TRESHOLD")
			{
				try {
					std::stringstream ss(parameterValue);
					int i;
					if ((ss >> i).fail() || !(ss >> std::ws).eof())
						throw std::bad_cast();

					if(parameterName == "SMALL_TRESHOLD" )
						this->smallTreshold = i;
					else
						this->midTreshold = i;
				}catch(...)
				{
					lastError = "Numeric value expected";
					return false;
				}

			}
			else if(parameterName=="MODEL_RANK") {
				try{
					double temp = ::atof(parameterValue.c_str());
					this->modelRank = ClampFloat(temp, 0.0f, 1.0f);  
				}catch(...)
				{
					lastError = "Numeric value expected";
					return false;
				}
			}
		
			if("BVP_SENSOR_NAME" == parameterName)
				this->bvpSensorName = parameterValue;
			else if("EMG_SENSOR_NAME" == parameterName)
				this->emgSensorName = parameterValue;
			else if("SC_SENSOR_NAME" == parameterName)
				this->scSensorName = parameterValue;

			it->second = parameterValue;
			return true;
		}

		//Affection model methods

		void PadAffectionExpert::Initialize() {
			srand (static_cast <unsigned> (time(0)));
		}


		void PadAffectionExpert::InitializeForCharacter(CharacterData& characterData) {
			EmotionSensor* biofeedbackSensor = characterData.GetSensorByName("SensorDataFileMock");
			if(biofeedbackSensor == null)
			{
				cout << "PadAffectionExpert::HandleEvent - SensorDataFileMock mock sensor not found!" << endl;
				return;
			}
			biofeedbackSensor->GetSensorReading();
		}
			
		bool PadAffectionExpert::IsEventSupported(EecEvent& event) { 
			return true;
		}

		EmotionVector* PadAffectionExpert::HandleEvent(EecEvent& event, CharacterData& userData) { 

			EmotionVector* vector = new EmotionVector(EmotionModelType::STD_PAD);
			vector->SetUniformType(true);

			EmotionSensor* bvpSensor = userData.GetSensorByName(bvpSensorName);
			if(bvpSensor == null)
			{
				cout << "PadAffectionExpert::HandleEvent - BvpSensor sensor with name '"<< bvpSensorName <<"'not found!" << endl;
				delete vector;
				return null; 
			}

			EmotionSensor* emgSensor = userData.GetSensorByName(emgSensorName);
			if(emgSensor == null)
			{
				cout << "PadAffectionExpert::HandleEvent - EmgSensor sensor with name '"<< emgSensorName <<"' not found!" << endl;
				delete vector;
				return null; 
			}

			EmotionSensor* scSensor = userData.GetSensorByName(scSensorName);
			if(scSensor == null)
			{
				cout << "PadAffectionExpert::HandleEvent - scSensor sensor with name '"<< scSensorName <<"'not found!" << endl;
				delete vector;
				return null; 
			}


			float arousal = 0.0f;
			float pleasure = 0.0f;
			float dominance = 0.0f;
			EmotionVector* previousVector = userData.GetCurrentEmotionVector();
			if(previousVector != null)
			{
				if(previousVector->IsOfEmotionModelType( EmotionModelType::STD_PAD))
				{
					arousal = previousVector->GetValue(PadEmotions::AROUSAL)->AsFloat();
					pleasure = previousVector->GetValue(PadEmotions::PLEASURE)->AsFloat();
					dominance = previousVector->GetValue(PadEmotions::DOMINANCE)->AsFloat();
				} else
				{
					previousVector = userData.GetCurrentUnfilteredEmotionVector();
					if(previousVector != null &&
						previousVector->IsOfEmotionModelType( EmotionModelType::STD_PAD))
					{
						arousal = previousVector->GetValue(PadEmotions::AROUSAL)->AsFloat();
						pleasure = previousVector->GetValue(PadEmotions::PLEASURE)->AsFloat();
						dominance = previousVector->GetValue(PadEmotions::DOMINANCE)->AsFloat();
					}
				}
			}
			

			EmotionSensorReading* currentBvp = bvpSensor->GetSensorReading();
			EmotionSensorReading* previousBvp = currentBvp;
			std::vector<EmotionSensorReading* >  previousBvpReadings;
			bvpSensor->GetSensorReadings(previousBvpReadings,2);
			if(previousBvpReadings.size() > 1)
				previousBvp = previousBvpReadings[1];

			EmotionSensorReading* currentEmg = emgSensor->GetSensorReading();
			EmotionSensorReading* previousEmg = currentEmg;
			std::vector<EmotionSensorReading* >  previousEmgReadings;
			emgSensor->GetSensorReadings(previousEmgReadings,2);
			if(previousEmgReadings.size() > 1)
				previousEmg = previousEmgReadings[1];

			EmotionSensorReading* currentSc = scSensor->GetSensorReading();
			EmotionSensorReading* previousSc = currentSc;
			std::vector<EmotionSensorReading* >  previousScReadings;
			scSensor->GetSensorReadings(previousScReadings,2);
			if(previousScReadings.size() > 1)
				previousSc = previousScReadings[1];
			

			//Bloov volume pressure
			float pBvp = previousBvp->GetValue("BVP")->AsFloat();
			float cBvp= currentBvp->GetValue("BVP")->AsFloat();
			//ElektroMioGrafia
			float pEmg =previousEmg->GetValue("EMG")->AsFloat();
			float cEmg =currentEmg->GetValue("EMG")->AsFloat();
			//Skin Conductance
			float pSc = previousSc->GetValue("SC")->AsFloat();
			float cSc =currentSc->GetValue("SC")->AsFloat();
			
			//printf("FS: %f : %f : %f\n",currentBvp,currentEmg,currentSc);

			float bvpDelta = cBvp - pBvp;
			float emgDelta = cEmg - pEmg;
			float scDelta = cSc - pSc;

			//cout << "PadAffectionExpert::HandleEvent - deltas: " << bvpDelta << "," << emgDelta << "," << scDelta;

			//Some fuzzy logic would be nice, but no time for that
			const float minBound = 0.1f;
			const float maxBound = 0.101f;
			float arousalChange = RandomFloat(minBound,maxBound);
			float dominanceChange = RandomFloat(minBound,maxBound);
			float pleasureChange = RandomFloat(minBound,maxBound);

			if(scDelta != 0)
				pleasure = scDelta < 0 ? pleasure - pleasureChange : pleasure + pleasureChange;
			if(bvpDelta != 0)
				arousal = bvpDelta < 0 ? arousal - arousalChange : arousal + arousalChange;
			if(emgDelta != 0)
				dominance = emgDelta < 0 ? dominance + dominanceChange : dominance - dominanceChange;
			


			//Clam and add values to new vector
			pleasure = ClampFloat(pleasure, -1.0f, 1.0f); 
			arousal = ClampFloat(arousal, -1.0f, 1.0f);  
			dominance = ClampFloat(dominance, -1.0f, 1.0f);  
			//printf("PAD: %f : %f : %f\n",pleasure,arousal,dominance);
			
			vector->AddValue(PadEmotions::PLEASURE,Variant::Create(pleasure));
			vector->AddValue(PadEmotions::AROUSAL,Variant::Create(arousal));
			vector->AddValue(PadEmotions::DOMINANCE,Variant::Create(dominance));
			vector->SetModelRank(this->modelRank);
			return vector; 
		}


}