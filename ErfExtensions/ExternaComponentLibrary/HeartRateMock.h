#pragma once
#include "CommonExternalLibrary.h"
#include "EmotionSensor.h"

namespace erf
{

    class EXTERNALCOMPONENTLIBRARY_API HeartRateMock : public EmotionSensor
    {

	private:
		vector<EmotionSensorReading* > previousReadings;
		map<string,string> parameters;
		string lastError;
		unsigned int maxNumberOfPreviousReadings;
		int pulseMin;
		int pulseRange;

	public:
		HeartRateMock(ExternalComponentConfiguration* pConfiguration);

		HeartRateMock::HeartRateMock(const HeartRateMock &other);

		virtual ~HeartRateMock();

		virtual EmotionSensorReading* GetSensorReading();

		virtual void GetSensorReadings(vector<EmotionSensorReading*>& container, int numberOfReadings);

		virtual string GetComponentLastError();

		virtual void GetSupportedParameters(vector<string>& paramHolder);

		virtual void GetParameters(map<string,string>& paramHolder);

		virtual const string GetParameter(string parameterName);

		virtual bool SetParameter(string parameterName, string parameterValue);

		virtual void Initialize() {};

		virtual void Reinitialize() {};

		virtual void Deinitialize() {};

	};
}

