#include "stdafx.h"
#include "SensorDataFileMock.h"
#include "ExternalComponentConfiguration.h"
#include "ExternalComponentEnums.h"
#include "EmotionSensorReading.h"
#include "Variant.h"
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include "ErfUtils.h"
namespace erf {

	SensorDataFileMock::SensorDataFileMock(ExternalComponentConfiguration* pConfiguration): 
		BasicObject("SensorDataFileMock"),EmotionSensor(pConfiguration, SensorType::STD_HEART_RATE)
	{

		inputFilePath = "";
		maxNumberOfPreviousReadings = 50;
		fileColumnIndex = 0;
		fileColumnName = "";
		parameters["FILE_PATH"] = inputFilePath;
		parameters["MAX_READING_BUF_SIZE"] = "50";
		parameters["FILE_COLUMN_NAME"] = fileColumnName;
		parameters["FILE_COLUMN_INDEX"] = "0";
		this->componentState = ComponentState::UNINITIALIZED;
	}

	SensorDataFileMock::SensorDataFileMock(const SensorDataFileMock &other): 
		BasicObject(other.objectName),EmotionSensor(other.configuration, SensorType::STD_HEART_RATE)
	{
		throw "Not supported";
	}

	SensorDataFileMock::~SensorDataFileMock() { 
		for(unsigned int i = 0 ; i < previousReadings.size() ; i++)
			delete previousReadings[i];
	} ;


	EmotionSensorReading* SensorDataFileMock::GetSensorReading()
	{
		//cout << "SensorDataFileMock::GetSensorReading called" << endl;

		if(this->componentState == ComponentState::COMPONENT_ERROR ||
			this->componentState == ComponentState::DEINITIALIZED ||
			this->componentState == ComponentState::UNINITIALIZED)
		{
			lastError = "Component was not initialized";
			return null;
		}

		std::string line;
		if(inputFile.good())
		{
			if(!getline(inputFile,line))
			{
				this->Reinitialize();
				if(!getline(inputFile,line))
					return null;
			}
		}
		if(line.empty())
		{
			this->Reinitialize();
			if(!getline(inputFile,line))
				return null;
		}

		
		istringstream instream(line);
		string dummy;
		float data;
		for(unsigned int i = 0 ; i <= this->fileColumnIndex ; i++)
		{
			if(i == this->fileColumnIndex)
			{
				instream >> data ;
				break;
			}
			instream >> dummy ;
		}
		
		//printf("%f : %f : %f\n",bvp,emg,sc);
		EmotionSensorReading* reading = new EmotionSensorReading();

		reading->AddValue(this->fileColumnName,Variant::Create(data));
		
		previousReadings.push_back(reading);
		while(this->previousReadings.size() > 
				maxNumberOfPreviousReadings)
		{
			EmotionSensorReading* oldReading = previousReadings[0];
			previousReadings.erase(previousReadings.begin());
			delete oldReading;
		}

		//cout << "SensorDataFileMock::GetSensorReading read address: " << reading << endl;
		return reading;
	}

	void SensorDataFileMock::GetSensorReadings(vector<EmotionSensorReading*>& container, int numberOfReadings) {
		int readReadings = 0 ;
		for(int i = this->previousReadings.size() -1 ; i >= 0 ; i--)
		{
			if(readReadings == numberOfReadings)
				break;
			container.push_back(this->previousReadings[i]);
			readReadings++;
		}
	}

	string SensorDataFileMock::GetComponentLastError(){
		cout << "SensorDataFileMock::GetComponentLastError called" << endl;
		string error = this->lastError;
		this->lastError = "";
		return error;
	}

	
	void SensorDataFileMock::GetParameters(map<string,string>& paramHolder){
		cout << "SensorDataFileMock::GetParameters called" << endl;
		paramHolder.insert(parameters.begin(),parameters.end());
	}

	void SensorDataFileMock::GetSupportedParameters(vector<string>& paramHolder){
		cout << "SensorDataFileMock::GetSupportedParameters called" << endl;
		for(map<string,string>::iterator it = parameters.begin(); it != parameters.end(); ++it) {
			paramHolder.push_back(it->first);
		}
	}

	const string SensorDataFileMock::GetParameter(string parameterName)
	{
		cout << "SensorDataFileMock::GetParameter called" << endl;
		map<string,string>::iterator it = parameters.find(parameterName);
		if(it != parameters.end())
			return it->second;

		return null;
	}

	bool SensorDataFileMock::SetParameter(string parameterName, string parameterValue){
		cout << "SensorDataFileMock::SetParameter called" << endl;
		map<string,string>::iterator it = parameters.find(parameterName);
		if(it == parameters.end())
			return false;

		if( parameterName == "MAX_READING_BUF_SIZE" || parameterName == "FILE_COLUMN_INDEX")
		{
			try {
				std::stringstream ss(parameterValue);
				int i;
				if ((ss >> i).fail() || !(ss >> std::ws).eof())
					throw std::bad_cast();

				if(parameterName == "MAX_READING_BUF_SIZE")
				{
					this->maxNumberOfPreviousReadings = i;
					while(this->previousReadings.size() > 
							maxNumberOfPreviousReadings)
					{
						EmotionSensorReading* oldReading = previousReadings[0];
						previousReadings.erase(previousReadings.begin());
						delete oldReading;
					}
				}
				else if(parameterName == "FILE_COLUMN_INDEX")
				{
					this->fileColumnIndex = i;
				}
			}catch(...)
			{
				lastError = "Numeric value expected";
				return false;
			}

		}

		it->second = parameterValue;
		if(parameterName == "FILE_COLUMN_NAME")
		{
			this->fileColumnName = parameterValue;
		}

		if(parameterName == "FILE_PATH")
		{
			this->inputFilePath = parameterValue;
		}

		return true;
	}

	string GetCurrentExecutingDirectory()
	{
		char path[MAX_PATH];
		HMODULE hm = NULL;

		if (!GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | 
				GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
				(LPCSTR) &GetCurrentExecutingDirectory, 
				&hm))
		{
			return "???";
		}
		GetModuleFileNameA(hm, path, sizeof(path));
	
		return path;
	}

	void SensorDataFileMock::Initialize() {
		if(this->componentState != ComponentState::UNINITIALIZED &&
			this->componentState != ComponentState::COMPONENT_ERROR)
		{
			lastError = "Only unitialized components can be initialized";
			return;
		}
		this->inputFile.open(this->inputFilePath, std::ifstream::in);
		if(!this->inputFile.is_open())
		{
			
			lastError = "Error opening file: " + this->inputFilePath;
			cerr << "Error: " << strerror(errno) << " in " << 
			GetCurrentExecutingDirectory();
			this->componentState = ComponentState::COMPONENT_ERROR;
			return;
		}
		this->componentState = ComponentState::INITIALIZED;
	}

	void SensorDataFileMock::Reinitialize() {
		if(this->componentState != ComponentState::INITIALIZED &&
			this->componentState != ComponentState::DEINITIALIZED &&
			this->componentState != ComponentState::COMPONENT_ERROR&&
			this->componentState != ComponentState::REINITIALIZED)
		{
			lastError = "Only initialized/deinitialized components can be reinitialized";
			return;
		}
		if(this->inputFile.is_open())
		{
			this->inputFile.close();
		}

		this->inputFile.open(this->inputFilePath, std::ifstream::in);
		if(!this->inputFile.is_open())
		{
			
			lastError = "Error opening file: " + this->inputFilePath;
			this->componentState = ComponentState::COMPONENT_ERROR;
			return;
		}
		this->componentState = ComponentState::REINITIALIZED;
	}

	void SensorDataFileMock::Deinitialize(){
		if(this->componentState != ComponentState::INITIALIZED &&
			this->componentState != ComponentState::REINITIALIZED &&
			this->componentState != ComponentState::COMPONENT_ERROR)
		{
			lastError = "Only initialized/reinitialized components can be deinitialized";
			return;
		}
		if(this->inputFile.is_open())
		{
			this->inputFile.close();
		}
		this->componentState = ComponentState::DEINITIALIZED;
	}
	
	EmotionSensor* SensorDataFileMock::CloneSensor() {
		SensorDataFileMock* clone = new SensorDataFileMock(this->configuration);
		clone->SetParameter("FILE_PATH",this->GetParameter("FILE_PATH"));
		clone->SetParameter("MAX_READING_BUF_SIZE",this->GetParameter("MAX_READING_BUF_SIZE"));
		clone->SetParameter("FILE_COLUMN_NAME",this->GetParameter("FILE_COLUMN_NAME"));
		clone->SetParameter("FILE_COLUMN_INDEX",this->GetParameter("FILE_COLUMN_INDEX"));
		return clone;
	}

}