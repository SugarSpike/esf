#include "stdafx.h"
#include "HeartRateMock.h"
#include "ExternalComponentConfiguration.h"
#include "ExternalComponentEnums.h"
#include "EmotionSensorReading.h"
#include "Variant.h"
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>

namespace erf {

	HeartRateMock::HeartRateMock(ExternalComponentConfiguration* pConfiguration): 
		BasicObject("HearRateMock"),EmotionSensor(pConfiguration, SensorType::STD_HEART_RATE)
	{
		pulseMin = 90;
		pulseRange = 120;
		maxNumberOfPreviousReadings = 50;
		parameters["PULSE_MIN"] = "90";
		parameters["PULSE_RANGE"] = "120";
		parameters["MAX_READING_BUF_SIZE"] = "50";
		parameters["MOCK_PARAM"] = "MOCK_VALUE";
		this->componentState = ComponentState::INITIALIZED;
	}

	HeartRateMock::HeartRateMock(const HeartRateMock &other): 
		BasicObject(other.objectName),EmotionSensor(other.configuration, SensorType::STD_HEART_RATE)
	{
		throw "Not supported";
	}

	HeartRateMock::~HeartRateMock() { 
		for(unsigned int i = 0 ; i < previousReadings.size() ; i++)
			delete previousReadings[i];
	} ;


	EmotionSensorReading* HeartRateMock::GetSensorReading()
	{
		cout << "HeartRateMock::GetSensorReading called" << endl;
		EmotionSensorReading* reading = new EmotionSensorReading();
		int pulseRate = (rand() % pulseRange)+pulseMin;
		reading->AddValue("pulse",Variant::Create(pulseRate));
		previousReadings.push_back(reading);
		while(this->previousReadings.size() > 
				maxNumberOfPreviousReadings)
		{
			EmotionSensorReading* oldReading = previousReadings[0];
			previousReadings.erase(previousReadings.begin());
			delete oldReading;
		}
		cout << "HeartRateMock::GetSensorReading read address: " << reading << endl;
		return reading;
	}

	void HeartRateMock::GetSensorReadings(vector<EmotionSensorReading*>& container, int numberOfReadings) {
		int readReadings = 0 ;
		for(int i = this->previousReadings.size() -1 ; i >= 0 ; i--)
		{
			if(readReadings == numberOfReadings)
				break;
			container.push_back(this->previousReadings[i]);
			readReadings++;
		}
	}

	string HeartRateMock::GetComponentLastError(){
		cout << "HeartRateMock::GetComponentLastError called" << endl;
		string error = this->lastError;
		this->lastError = "";
		return error;
	}

	
	void HeartRateMock::GetParameters(map<string,string>& paramHolder){
		cout << "HeartRateMock::GetParameters called" << endl;
		paramHolder.insert(parameters.begin(),parameters.end());
	}

	void HeartRateMock::GetSupportedParameters(vector<string>& paramHolder){
		cout << "HeartRateMock::GetSupportedParameters called" << endl;
		for(map<string,string>::iterator it = parameters.begin(); it != parameters.end(); ++it) {
			paramHolder.push_back(it->first);
		}
	}

	const string HeartRateMock::GetParameter(string parameterName)
	{
		cout << "HeartRateMock::GetParameter called" << endl;
		map<string,string>::iterator it = parameters.find(parameterName);
		if(it != parameters.end())
			return it->second;

		return null;
	}

	bool HeartRateMock::SetParameter(string parameterName, string parameterValue){
		cout << "HeartRateMock::SetParameter called" << endl;
		map<string,string>::iterator it = parameters.find(parameterName);
		if(it == parameters.end())
			return false;

		if(parameterName == "PULSE_MIN" 
			|| parameterName == "PULSE_RANGE"
			|| parameterName == "MAX_READING_BUF_SIZE")
		{
			try {
				std::stringstream ss(parameterValue);
				int i;
				if ((ss >> i).fail() || !(ss >> std::ws).eof())
					throw std::bad_cast();

				if(parameterName == "PULSE_MIN" )
					this->pulseMin = i;
				else if(parameterName == "MAX_READING_BUF_SIZE")
				{
					this->maxNumberOfPreviousReadings = i;
					while(this->previousReadings.size() > 
							maxNumberOfPreviousReadings)
					{
						EmotionSensorReading* oldReading = previousReadings[0];
						previousReadings.erase(previousReadings.begin());
						delete oldReading;
					}
				}
				else
					this->pulseRange = i;


			}catch(...)
			{
				lastError = "Numeric value expected";
				return false;
			}

		}

		it->second = parameterValue;
		return true;
	}

}