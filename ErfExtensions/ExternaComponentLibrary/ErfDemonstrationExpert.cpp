#include "stdafx.h"
#include "ErfDemonstrationExpert.h"
#include "EmotionEnums.h"
#include "EmotionConstants.h"
#include "EmotionVector.h"
#include "EmotionSensor.h"
#include "CharacterData.h"
#include "EmotionSensorReading.h"
#include "EecEvent.h"
#include <sstream>
#include <ctime>
#include "Variant.h"

namespace erf 
{
		const string ErfDemonstrationExpert::MISSED_SHOTS = "ErfMissedShots";
		const string ErfDemonstrationExpert::DROPPED_SHOTS = "ErfDroppedShots";
		const string ErfDemonstrationExpert::SUCCESSFUL_SHOTS = "ErfSuccessfulShots";

		float RandomErfFloat(float min, float max)
		{
			return min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max-min)));
		}

		float ClampErfFloat(float val, float min, float max)
		{
			return  val < min ? min : (val > max ? max : val);
		}

		ErfDemonstrationExpert::ErfDemonstrationExpert(ExternalComponentConfiguration* pConfiguration):
			BasicObject("ErfDemonstrationExpert"),EmotionExpert(pConfiguration)
		{
			cout << "ErfDemonstrationExpert::ErfDemonstrationExpert - Initialization" << endl;
			this->modelRank = 0.8f;
			parameters["MODEL_RANK"] = "0.8";
			this->componentState = ComponentState::INITIALIZED;
		}

		ErfDemonstrationExpert::ErfDemonstrationExpert(const ErfDemonstrationExpert &other):
			EmotionExpert(other.configuration)
		{ throw "not supported";}

		ErfDemonstrationExpert::~ErfDemonstrationExpert() { }

	   //Expert methods
		string ErfDemonstrationExpert::GetComponentLastError(){
			cout << "ErfDemonstrationExpert::GetComponentLastError called: " << this->lastError << endl;
			string error = this->lastError;
			this->lastError = "";
			return error;
		}


		void ErfDemonstrationExpert::GetParameters(map<string,string>& paramHolder){
			cout << "ErfDemonstrationExpert::GetParameters called" << endl;
			paramHolder.insert(parameters.begin(),parameters.end());
		}

		void ErfDemonstrationExpert::GetSupportedParameters(vector<string>& paramHolder){
			cout << "ErfDemonstrationExpert::GetSupportedParameters called" << endl;
			for(map<string,string>::iterator it = parameters.begin(); it != parameters.end(); ++it) {
				paramHolder.push_back(it->first);
			}
		}

		const string ErfDemonstrationExpert::GetParameter(string parameterName)
		{
			cout << "ErfDemonstrationExpert::GetParameter called: " << parameterName << endl;
			map<string,string>::iterator it = parameters.find(parameterName);
			if(it != parameters.end())
				return it->second;

			return null;
		}

		bool ErfDemonstrationExpert::SetParameter(string parameterName, string parameterValue){
			cout << "ErfDemonstrationExpert::SetParameter called: " << parameterName << ":" << parameterValue<< endl;
			map<string,string>::iterator it = parameters.find(parameterName);
			if(it == parameters.end())
			{
				cout << "ErfDemonstrationExpert::SetParameter param not supported: " << parameterName << endl;
				return false;
			}
			it->second = parameterValue;
			return true;
		}

		//Affection model methods

		void ErfDemonstrationExpert::Initialize() {
			srand (static_cast <unsigned> (time(0)));
		}

		
		void ErfDemonstrationExpert::InitializeForCharacter(CharacterData& characterData) {
			EmotionSensor* mouseSensor = characterData.GetSensorByName("MouseSensor");
			if(mouseSensor == null)
			{
				cout << "ErfDemonstrationExpert::HandleEvent - MouseSensor sensor not found!" << endl;
				return;
			}
			characterData.AddValue(ErfDemonstrationExpert::MISSED_SHOTS,Variant::Create((int)0));
			characterData.AddValue(ErfDemonstrationExpert::DROPPED_SHOTS,Variant::Create((int)0));
			characterData.AddValue(ErfDemonstrationExpert::SUCCESSFUL_SHOTS,Variant::Create((int)0));
		}
			
		bool ErfDemonstrationExpert::IsEventSupported(EecEvent& event) { 
			cout << "ErfDemonstrationExpert::IsEventSupported - test for: " + event.GetEventType() << endl;
			return true;
		}

		EmotionVector* ErfDemonstrationExpert::HandleEvent(EecEvent& event, CharacterData& userData) { 
			
			//cout << "ErfDemonstrationExpert::HandleEvent - Handle event "<< endl;
			//printf("ErfDemonstrationExpert::HandleEvent - Handle event: ");
			EmotionVector* vector = new EmotionVector(EmotionModelType::STD_OCC);
			vector->SetUniformType(true);

			EmotionSensor* mouseSensor = userData.GetSensorByName("MouseSensor");
			if(mouseSensor == null)
			{
				cout << "ErfDemonstrationExpert::HandleEvent - MouseSensor sensor not found!" << endl;
				delete vector;
				return null; 
			}

			float distress = 0.0f;
			EmotionVector* previousVector = userData.GetCurrentEmotionVector();
			if(previousVector != null)
			{
				if(previousVector->IsOfEmotionModelType( EmotionModelType::STD_OCC))
				{
					distress = previousVector->GetValue(OccEmotions::DISTRESS)->AsFloat();
				} else
				{
					previousVector = userData.GetCurrentUnfilteredEmotionVector();
					if(previousVector != null &&
						previousVector->IsOfEmotionModelType( EmotionModelType::STD_OCC))
					{
						distress = previousVector->GetValue(OccEmotions::DISTRESS)->AsFloat();
					}
				}
			}
			int currentClicks = 0;
			int previousClicks= 0;

			//Mouse distance
			int currentDistance = 0;
			int previousDistance = 0;
			int previousPreviousDistance =0; 

			EmotionSensorReading* currentMouseReading = mouseSensor->GetSensorReading();
			if(currentMouseReading != null)
			{
				EmotionSensorReading* previousMouseReading = currentMouseReading;
				EmotionSensorReading* previousPreviousMouseReading = currentMouseReading;
				std::vector<EmotionSensorReading* >  previousMouseReadings;
				mouseSensor->GetSensorReadings(previousMouseReadings,3);
				if(previousMouseReadings.size() > 1)
				{
					previousMouseReading = previousMouseReadings[1];
					previousPreviousMouseReading = previousMouseReading;
				}
				if(previousMouseReadings.size() > 2)
					previousPreviousMouseReading = previousMouseReadings[2];
			

				Variant* v = currentMouseReading->GetValue("MouseClicks");
				//Mouse clicks
				currentClicks = currentMouseReading->GetValue("MouseClicks")->AsInteger();
				previousClicks= previousMouseReading->GetValue("MouseClicks")->AsInteger();
				//Mouse distance
				currentDistance =currentMouseReading->GetValue("MouseDistance")->AsInteger();
				previousDistance =previousMouseReading->GetValue("MouseDistance")->AsInteger();
				previousPreviousDistance =previousPreviousMouseReading->GetValue("MouseDistance")->AsInteger();
			}
			//Get data from eecEvent
			int droppedBalls = 0;
			int missedBalls = 0;
			int scoredBalls = 0;
			
			if(event.GetValue(ErfDemonstrationExpert::MISSED_SHOTS) != NULL)
			{
				droppedBalls = event.GetValue(ErfDemonstrationExpert::DROPPED_SHOTS)->AsInteger();
				missedBalls = event.GetValue(ErfDemonstrationExpert::MISSED_SHOTS)->AsInteger();
				scoredBalls = event.GetValue(ErfDemonstrationExpert::SUCCESSFUL_SHOTS)->AsInteger();
			}
			
			int pDroppedBalls = 0;
			int pMissedBalls = 0;
			int pScoredBalls = 0;
			if(userData.GetValue(ErfDemonstrationExpert::MISSED_SHOTS)!= NULL)
			{
				pDroppedBalls = userData.GetValue(ErfDemonstrationExpert::DROPPED_SHOTS)->AsInteger();
				pMissedBalls = userData.GetValue(ErfDemonstrationExpert::MISSED_SHOTS)->AsInteger();
				pScoredBalls = userData.GetValue(ErfDemonstrationExpert::SUCCESSFUL_SHOTS)->AsInteger();
			}

			userData.AddValue(ErfDemonstrationExpert::MISSED_SHOTS,Variant::Create(missedBalls));
			userData.AddValue(ErfDemonstrationExpert::DROPPED_SHOTS,Variant::Create(droppedBalls));
			userData.AddValue(ErfDemonstrationExpert::SUCCESSFUL_SHOTS,Variant::Create(scoredBalls));

			//printf("EDE Data: MS: %i : %i -> %i : %i GAME: %i, %i, %i -> %i, %i, %i\n",
			//	previousClicks,previousDistance,currentClicks,currentDistance,pDroppedBalls,pMissedBalls,pScoredBalls,
			//	droppedBalls,missedBalls,scoredBalls);

			float deltaDistressChange = 0.0f;

			int dMissedBalls = missedBalls - pMissedBalls;
			int dScoredBalls = scoredBalls - pScoredBalls;
			int dDroppedBalls = droppedBalls - pDroppedBalls;

			if( dMissedBalls > 0)
			{
				deltaDistressChange += RandomErfFloat(0.01f,0.02f);
			}

			if(dScoredBalls > 0)
			{
				deltaDistressChange -= RandomErfFloat(0.05f,0.1f);
			}

			if( dMissedBalls > 0
				&& dMissedBalls > dScoredBalls)
			{
				deltaDistressChange += RandomErfFloat(0.01f,0.05f);
			}

			float ballsShotClicks = (dMissedBalls + dScoredBalls + dDroppedBalls) * 2.0f;

			float dClicksWithMargin = (currentClicks - previousClicks) * 1.5f;

			if(ballsShotClicks > dClicksWithMargin)
			{
				deltaDistressChange += RandomErfFloat(0.03f,0.05f);
			}
			else
			{
				deltaDistressChange -= RandomErfFloat(0.005f,0.02f);
			}

			int dPreviousDistance = previousDistance - previousPreviousDistance;
			int dDistance = currentDistance - previousDistance;

			if(dDistance > dPreviousDistance)
			{
				deltaDistressChange += RandomErfFloat(0.02f,0.04f);
			}
			else
			{
				deltaDistressChange -= RandomErfFloat(0.01f,0.02f);
			}
			
			distress += deltaDistressChange;
			distress = ClampErfFloat(distress, -1.0f, 1.0f);
			//printf("EDE Distress: d%f -> %f\n",deltaDistressChange,distress);

			vector->AddValue(OccEmotions::DISTRESS,Variant::Create(distress));
			vector->AddValue(OccEmotions::JOY,Variant::Create(-distress));

			return vector; 
		}


}