#include "stdafx.h"
#include "MoodModelMockExpert.h"
#include "EmotionEnums.h"
#include "EmotionConstants.h"
#include "EmotionVector.h"
#include "EmotionSensor.h"
#include "MoodVector.h"
#include "CharacterData.h"
#include "EmotionSensorReading.h"
#include <sstream>
#include <ctime>
#include "Variant.h"

namespace erf 
{

		MoodModelMockExpert::MoodModelMockExpert(ExternalComponentConfiguration* pConfiguration):
			BasicObject("MoodModelMockExpert"),MoodExpert(pConfiguration)
		{
			modelRank = 1.0f;
			parameters["MODEL_RANK"] = "1.0";
			this->componentState = ComponentState::INITIALIZED;
		}

		MoodModelMockExpert::MoodModelMockExpert(const MoodModelMockExpert &other):
			MoodExpert(other.configuration)
		{ throw "not supported";}

		MoodModelMockExpert::~MoodModelMockExpert() { }

	   //Expert methods
		string MoodModelMockExpert::GetComponentLastError(){
			cout << "MoodModelMockExpert::GetComponentLastError called" << endl;
			string error = this->lastError;
			this->lastError = "";
			return error;
		}


		void MoodModelMockExpert::GetParameters(map<string,string>& paramHolder){
			cout << "MoodModelMockExpert::GetParameters called" << endl;
			paramHolder.insert(parameters.begin(),parameters.end());
		}

		void MoodModelMockExpert::GetSupportedParameters(vector<string>& paramHolder){
			cout << "MoodModelMockExpert::GetSupportedParameters called" << endl;
			for(map<string,string>::iterator it = parameters.begin(); it != parameters.end(); ++it) {
				paramHolder.push_back(it->first);
			}
		}

		const string MoodModelMockExpert::GetParameter(string parameterName)
		{
			cout << "MoodModelMockExpert::GetParameter called: " << parameterName << endl;
			map<string,string>::iterator it = parameters.find(parameterName);
			if(it != parameters.end())
				return it->second;

			return null;
		}

		bool MoodModelMockExpert::SetParameter(string parameterName, string parameterValue){
			cout << "PadAffectionExpert::SetParameter called: " << parameterName << ":" << parameterValue<< endl;
			map<string,string>::iterator it = parameters.find(parameterName);
			if(it == parameters.end())
			{
				cout << "PadAffectionExpert::SetParameter param not supported: " << parameterName << endl;
				return false;
			}

			if(parameterName=="MODEL_RANK") {
				try{
					double temp = ::atof(parameterValue.c_str());
					this->modelRank = temp; 
				}catch(...)
				{
					lastError = "Numeric value expected";
					return false;
				}
			}
		
			it->second = parameterValue;
			return true;
		}

		void MoodModelMockExpert::Initialize() {
		}


		void MoodModelMockExpert::InitializeForCharacter(CharacterData& characterData) {
		}
			
		bool MoodModelMockExpert::IsEmotionVectorSupported(EmotionVector& emotionVector) { 
			if(emotionVector.IsOfEmotionModelType(EmotionModelType::STD_OCC))
				return true;
			return false;
		}

		MoodVector* MoodModelMockExpert::HandleEmotionVector(EecEvent& event, CharacterData& characterData) { 

			MoodVector* vector = new MoodVector(EmotionModelType::STD_OCC);
			vector->SetUniformType(true);

			EmotionVector* current = characterData.GetCurrentEmotionVector();
			
			map<string,Variant*>& emotions =  current->GetValues();
			for (auto eIterator = emotions.begin() ; 
				eIterator != emotions.end(); ++eIterator)
			{
				float intensity = eIterator->second->AsFloat();
				if(intensity == 0.0f)
					continue;
				vector->AddValue(eIterator->first,Variant::Create(intensity / 2));
			}
		
			return vector; 
		}


}