#pragma once
#include "CommonExternalLibrary.h"
#include "ExternalFilter.h"
#include "EmotionConstants.h"

namespace erf
{
    class EXTERNALCOMPONENTLIBRARY_API PadToOccVectorFilter : public ExternalFilter
    {
		private:

			static map<int,string> CreateMap()
			{
			    map<int,string> m;
			    m[0] =OccEmotions::JOY;
				m[1] =OccEmotions::DISTRESS;
				m[2] =OccEmotions::PRIDE;
				m[3] =OccEmotions::SHAME;
				m[4] =OccEmotions::ADMIRATION;
				m[5] =OccEmotions::REPROACH;
				m[6] =OccEmotions::LOVE;
				m[7] =OccEmotions::HATE;
				m[8] =OccEmotions::HAPPY_FOR;
				m[9] =OccEmotions::RESENTMENT;
				m[10] =OccEmotions::GLOATING;
				m[11] =OccEmotions::PITY;
				m[12] =OccEmotions::HOPE;
				m[13] =OccEmotions::FEAR;
				m[14] =OccEmotions::SATISFACTION;
				m[15] =OccEmotions::FEARS_CONFIRMED;
				m[16] =OccEmotions::RELIEF;
				m[17] =OccEmotions::DISAPPOINTMENT;
				m[18] =OccEmotions::GRATIFICATION;
				m[19] =OccEmotions::REMORSE;
				m[20] =OccEmotions::GRATITUDE;
				m[21] =OccEmotions::ANGER;
				m[22] =OccEmotions::LIKING;
				m[23] =OccEmotions::DISLIKING;
			  return m;
			}
			static const map<int,string> ArrayIndexToOccName;
			
			static float const PadToOccMapping[24][3];

			string lastError;
		public:
			PadToOccVectorFilter(ExternalComponentConfiguration* pConfiguration);

			PadToOccVectorFilter(const PadToOccVectorFilter &other);
			virtual ~PadToOccVectorFilter();

			virtual string GetComponentLastError();

			virtual const string GetParameter(string parameterName);
		
			virtual void GetParameters(map<string,string>& paramHolder);

			virtual void GetSupportedParameters(vector<string>& paramHolder) ;

			virtual bool SetParameter(string parameterName, string parameterValue);

			virtual void Initialize();

			virtual void Reinitialize() {};

			virtual void Deinitialize() {};
			
			virtual bool IsEmotionVectorSupported(EmotionVector& vector);

			virtual void DoFilter(EmotionVector& emotionVector, CharacterData& characterData);

	};
}

