#pragma once
#include "CommonExternalLibrary.h"
#include "MoodExpert.h"

namespace erf
{

    class EXTERNALCOMPONENTLIBRARY_API MoodModelMockExpert : public MoodExpert
    {
		private:
			float modelRank;
			map<string,string> parameters;
			string lastError;
		public:
			MoodModelMockExpert(ExternalComponentConfiguration* pConfiguration);

			MoodModelMockExpert(const MoodModelMockExpert &other);
			virtual ~MoodModelMockExpert();

			//Expert methods
			virtual float GetModelRank() { return modelRank; }

			virtual string GetComponentLastError();

			virtual const string GetParameter(string parameterName);
		
			virtual void GetParameters(map<string,string>& paramHolder);

			virtual void GetSupportedParameters(vector<string>& paramHolder) ;

			virtual bool SetParameter(string parameterName, string parameterValue);

			virtual void Initialize();

			virtual void Reinitialize() {};

			virtual void Deinitialize() {};

			//Mood model methods
			virtual void InitializeForCharacter(CharacterData& characterData) ;
			
			virtual bool IsEmotionVectorSupported(EmotionVector& emotionVector) ;

			virtual MoodVector* HandleEmotionVector(EecEvent& event, CharacterData& characterData);

	};
}

