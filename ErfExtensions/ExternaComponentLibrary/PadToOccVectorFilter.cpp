#include "stdafx.h"
#include "PadToOccVectorFilter.h"
#include "EmotionVector.h"
#include "EmotionConstants.h"
#include "ErfUtils.h"
#include "EmotionEnums.h"
#include "Variant.h"

namespace erf 
{
		const int Pleasure  = 0;
		const int Arousal = 1;
		const int Dominance = 2;
		const float MinPrecision = -0.4;
		const float MaxPrecision = 0.4;
		const map<int,string> PadToOccVectorFilter::ArrayIndexToOccName =  PadToOccVectorFilter::CreateMap();

		float const PadToOccVectorFilter::PadToOccMapping[24][3] =
		{
			{0.4f,0.2f,0.1f}, ///Joy
			{-0.4f,-0.2f,-0.5f}, ///Distress
			{0.4f,0.3f,0.3f}, ///Pride
			{-0.3f,0.1f,-0.6f}, ///Shame
			{0.5f,0.3f,-0.2f}, ///Admiration
			{-0.3f,-0.1f,0.4f}, ///Reproach
			{0.3f,0.1f,0.2f}, ///Love
			{-0.6f,0.6f,0.3f}, ///Hate
			{0.4f,0.2f,0.2f}, ///Happy-for
			{-0.2f,-0.3f,-0.2f}, ///Resentment
			{0.3f,-0.3f,-0.1f}, ///Gloating
			{-0.4f,-0.2f,0.5f}, ///Pity
			{0.2f,0.2f,-0.1f}, ///Hope
			{-0.64f,0.6f,-0.43f}, ///Fear
			{0.3f,-0.2f,0.4f}, ///Satisfaction
			{-0.5f,-0.3f,-0.7f}, ///Feras-Confirmed
			{0.2f,-0.3f,0.4f}, ///Relief
			{-0.3f,0.1f,0.4f}, ///Disappointment
			{0.6f,0.5f,0.4f}, ///Gratification
			{-0.3f,0.1f,-0.6f}, ///Remorse
			{0.4f,0.2f,-0.3f}, ///Gratitude
			{-0.51f,0.59f,0.25f}, ///Anger
			{0.4f,0.16f,-0.24f}, ///Liking
			{-0.4f,0.2f,0.1f} ///Disliking
		};

		PadToOccVectorFilter::PadToOccVectorFilter(ExternalComponentConfiguration* pConfiguration):
			BasicObject("PadToOccVectorFilter"),ExternalFilter(pConfiguration)
		{
			this->componentState = ComponentState::INITIALIZED;
		}

		PadToOccVectorFilter::PadToOccVectorFilter(const PadToOccVectorFilter &other):
			ExternalFilter(other.configuration)
		{ throw "not supported";}

		PadToOccVectorFilter::~PadToOccVectorFilter() { }

	   //Expert methods
		string PadToOccVectorFilter::GetComponentLastError(){
			cout << "PadToOccVectorFilter::GetComponentLastError called" << endl;
			string error = this->lastError;
			this->lastError = "";
			return error;
		}


		void PadToOccVectorFilter::GetParameters(map<string,string>& paramHolder){
			cout << "PadToOccVectorFilter::GetParameters called" << endl;
		}

		void PadToOccVectorFilter::GetSupportedParameters(vector<string>& paramHolder){
			cout << "PadAffectionExpert::GetSupportedParameters called" << endl;
		}

		const string PadToOccVectorFilter::GetParameter(string parameterName)
		{
			return null;
		}

		bool PadToOccVectorFilter::SetParameter(string parameterName, string parameterValue){
			cout << "PadToOccVectorFilter::SetParameter called" << endl;
			return false;
		}

		//Affection model methods
		void PadToOccVectorFilter::Initialize() {
		}

		bool PadToOccVectorFilter::IsEmotionVectorSupported(EmotionVector& vector) {
			if(erfutils::Contains(vector.GetEmotionModelTypes(),(int)EmotionModelType::STD_PAD)
				|| erfutils::Contains(vector.GetEmotionModelTypes(),(int)EmotionModelType::STD_OCC))
				return true;

			return false;
		}

		void PadToOccVectorFilter::DoFilter(EmotionVector& emotionVector, CharacterData& characterData) {
			if(erfutils::Contains(emotionVector.GetEmotionModelTypes(),(int)EmotionModelType::STD_OCC))
				return;
			
			float pleasure = emotionVector.GetValue(PadEmotions::PLEASURE)->AsFloat();
			float arousal = emotionVector.GetValue(PadEmotions::AROUSAL)->AsFloat();
			float dominance = emotionVector.GetValue(PadEmotions::DOMINANCE)->AsFloat();
			emotionVector.Clear();
			emotionVector.GetEmotionModelTypes().clear();
			emotionVector.GetEmotionModelTypes().push_back(EmotionModelType::STD_OCC);
			for(int i = 0 ; i < 24 ; i++)
			{
				float patternPleasure = PadToOccMapping[i][Pleasure];
				float minPleasure = patternPleasure + MinPrecision;
				float maxPleasure = patternPleasure + MaxPrecision;
				if(pleasure < minPleasure || pleasure > maxPleasure)
				{
					emotionVector.AddValue(PadToOccVectorFilter::ArrayIndexToOccName.at(i),Variant::Create(0.0f));
					continue;
				}
				
				float patternArousal = PadToOccMapping[i][Arousal];
				float minArousal = patternArousal + MinPrecision;
				float maxArousal = patternArousal + MaxPrecision;
				if(arousal < minArousal || arousal > maxArousal)
				{
					emotionVector.AddValue(PadToOccVectorFilter::ArrayIndexToOccName.at(i),Variant::Create(0.0f));
					continue;
				}

				float patternDominance = PadToOccMapping[i][Dominance];
				float minDominance = patternDominance + MinPrecision;
				float maxDominance = patternDominance + MaxPrecision;
				if(dominance < minDominance || dominance > maxDominance)
				{
					emotionVector.AddValue(PadToOccVectorFilter::ArrayIndexToOccName.at(i),Variant::Create(0.0f));
					continue;
				}

				float totalDifference = abs(patternDominance-dominance) + abs (patternArousal - arousal) + abs(patternPleasure - pleasure);
				//Good old y=mx+b , with the assumption that MinPrecision = MaxPrecision, then the maximum totalDifference = 3*MaxPrecision
				float intensity = (-1.0f/(3.0f*MaxPrecision))*totalDifference + 1;
				emotionVector.AddValue(PadToOccVectorFilter::ArrayIndexToOccName.at(i),Variant::Create(intensity));
			}
		}

}