#pragma once
#include "CommonExternalLibrary.h"
#include "EmotionExpert.h"

namespace erf
{

    class EXTERNALCOMPONENTLIBRARY_API ErfDemonstrationExpert : public EmotionExpert
    {
		private:

		public:
			static const string MISSED_SHOTS;
			static const string DROPPED_SHOTS;
			static const string SUCCESSFUL_SHOTS;


		private:
			float modelRank;
			map<string,string> parameters;
			string lastError;
		public:
			ErfDemonstrationExpert(ExternalComponentConfiguration* pConfiguration);

			ErfDemonstrationExpert(const ErfDemonstrationExpert &other);
			virtual ~ErfDemonstrationExpert();

			//Expert methods
			virtual float GetModelRank() { return modelRank; }

			virtual string GetComponentLastError();

			virtual const string GetParameter(string parameterName);
		
			virtual void GetParameters(map<string,string>& paramHolder);

			virtual void GetSupportedParameters(vector<string>& paramHolder) ;

			virtual bool SetParameter(string parameterName, string parameterValue);

			virtual void Initialize();

			virtual void Reinitialize() {};

			virtual void Deinitialize() {};

			//Affection model methods
			virtual void InitializeForCharacter(CharacterData& characterData) ;
			
			virtual bool IsEventSupported(EecEvent& event);

			virtual EmotionVector* HandleEvent(EecEvent& event, CharacterData& userData);

	};
}

