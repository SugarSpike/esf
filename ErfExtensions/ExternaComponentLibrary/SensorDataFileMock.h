#pragma once
#include "CommonExternalLibrary.h"
#include "EmotionSensor.h"
#include <sstream>
#include <fstream> 
#include <iostream> 
#include <string>

namespace erf
{

    class EXTERNALCOMPONENTLIBRARY_API SensorDataFileMock : public EmotionSensor
    {

	private:
		vector<EmotionSensorReading* > previousReadings;
		map<string,string> parameters;
		string lastError;
		string inputFilePath;
		string fileColumnName;
		unsigned int maxNumberOfPreviousReadings;
		unsigned int fileColumnIndex;
		std::ifstream inputFile;

	public:

		SensorDataFileMock(ExternalComponentConfiguration* pConfiguration);

		SensorDataFileMock(const SensorDataFileMock &other);

		virtual ~SensorDataFileMock();

		virtual EmotionSensorReading* GetSensorReading();

		virtual void GetSensorReadings(vector<EmotionSensorReading*>& container, int numberOfReadings);

		virtual string GetComponentLastError();

		virtual void GetSupportedParameters(vector<string>& paramHolder);

		virtual void GetParameters(map<string,string>& paramHolder);

		virtual const string GetParameter(string parameterName);

		virtual EmotionSensor* CloneSensor();

		virtual bool SetParameter(string parameterName, string parameterValue);

		virtual void Initialize();

		virtual void Reinitialize();

		virtual void Deinitialize();

	};
}

