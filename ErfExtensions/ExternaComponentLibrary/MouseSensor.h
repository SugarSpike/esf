#pragma once
#include "CommonExternalLibrary.h"
#include "EmotionSensor.h"
#include <sstream>
#include <fstream> 
#include <iostream> 
#include <string>

namespace erf
{

    class EXTERNALCOMPONENTLIBRARY_API MouseSensor : public EmotionSensor
    {

	private:
		vector<EmotionSensorReading* > previousReadings;
		map<string,string> parameters;
		unsigned int maxNumberOfPreviousReadings;
		unsigned int fileColumnIndex;
		int mouseClicks;
		int currentSensorBufferIndex;
		string lastError;

		static MouseSensor* sensorInstance;

		MouseSensor(ExternalComponentConfiguration* pConfiguration);

		MouseSensor(const MouseSensor &other);

	public:

		static MouseSensor* GetInstance() { return sensorInstance; }

		static MouseSensor* CreateInstance(ExternalComponentConfiguration* pConfiguration);

		virtual ~MouseSensor();

		virtual EmotionSensorReading* GetSensorReading();

		virtual void GetSensorReadings(vector<EmotionSensorReading*>& container, int numberOfReadings);

		virtual string GetComponentLastError();

		virtual void GetSupportedParameters(vector<string>& paramHolder);

		virtual void GetParameters(map<string,string>& paramHolder);

		virtual const string GetParameter(string parameterName);

		virtual bool SetParameter(string parameterName, string parameterValue);

		virtual void Initialize();

		virtual void Reinitialize();

		virtual void Deinitialize();

	};
}

