// ExternalLibraryMock.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "CommonExternalLibrary.h"
#include "ExternalComponentLibrary.h"
#include "PadToOccVectorFilter.h"
#include "ExternalComponentEnums.h"
#include "ErfDemonstrationExpert.h"
#include "PadAffectionExpert.h"
#include "HeartRateMock.h"
#include "SensorDataFileMock.h"
#include <map>
#include <algorithm>
#include <string> 
#include "MouseSensor.h"
#include "ErfException.h"
#include "MoodModelMockExpert.h"
using namespace erf;

bool initialized = false;
std::map<string,string> globalParams;

static int componentsCount = 4;

void initialize()
{
	if(initialized)
		return;

	globalParams["GLOBAL_PARAM1"] = "TEST1";
	globalParams["GLOBAL_PARAM2"] = "TEST2";
	initialized = true;
}
	
extern "C" EXTERNALCOMPONENTLIBRARY_API void RegisterComponents(ExternalComponentLibrary &library) {
	initialize();
	library.SetName("ExternalLibraryMock");
	new HeartRateMock(&(library.GetConfiguration()));

	EmotionSensor* bvpSensor = new SensorDataFileMock(&(library.GetConfiguration()));;
	bvpSensor->SetName("BvpSensor");
	bvpSensor->SetParameter("FILE_COLUMN_INDEX","0");
	bvpSensor->SetParameter("FILE_COLUMN_NAME","BVP");

	EmotionSensor* emgSensor = new SensorDataFileMock(&(library.GetConfiguration()));;
	emgSensor->SetName("EmgSensor");
	emgSensor->SetParameter("FILE_COLUMN_INDEX","1");
	emgSensor->SetParameter("FILE_COLUMN_NAME","EMG");

	EmotionSensor* scSensor = new SensorDataFileMock(&(library.GetConfiguration()));;
	scSensor->SetName("ScSensor");
	scSensor->SetParameter("FILE_COLUMN_INDEX","2");
	scSensor->SetParameter("FILE_COLUMN_NAME","SC");

	new PadToOccVectorFilter(&(library.GetConfiguration()));
	new PadAffectionExpert(&(library.GetConfiguration()));
	PadAffectionExpert* other = new PadAffectionExpert(&(library.GetConfiguration()));
	other->SetName("PadAffectionExpert2");
	other->SetParameter("MODEL_RANK","0.25");
	other = new PadAffectionExpert(&(library.GetConfiguration()));
	other->SetName("PadAffectionExpert3");
	other->SetParameter("MODEL_RANK","0.75");
	other->SetParameter("MODEL_RANK","0.80");

	MouseSensor::CreateInstance(&(library.GetConfiguration()));
	new ErfDemonstrationExpert(&(library.GetConfiguration()));
	new MoodModelMockExpert(&(library.GetConfiguration()));
}

extern "C" EXTERNALCOMPONENTLIBRARY_API bool SupportsComponentType(ComponentType type)
{
	cout << "Global::SupportsComponentType called" << endl;
	initialize();
	return true;
}

extern "C" EXTERNALCOMPONENTLIBRARY_API ExternalExpert* CreateExpertForName(ExternalComponentLibrary &library, string expertName)
{
	initialize();
	std::string data = expertName; 
	std::transform(data.begin(), data.end(), data.begin(), ::tolower);
	cout << "Global::CreateExpertForName called: " << data << endl;
	PadAffectionExpert* newExpert = null;
	if(data.find("padaffectionexpert") == 0)
	{
		
		newExpert = new PadAffectionExpert(&(library.GetConfiguration()));
		std::ostringstream oss;
		oss << "PadAffectionExpert" << componentsCount++;
		newExpert->SetName(oss.str());
	}
	else
	{
		throw ErfException("Unknow expert name: " + expertName);
	}
	return newExpert;
}

extern "C" EXTERNALCOMPONENTLIBRARY_API bool SetParameter(string parameterName, string parameterValue)
{
	cout << "Global::SetParameter called" << endl;
	initialize();
	map<string,string>::iterator it = globalParams.find(parameterName);
	if(it == globalParams.end())
		return false;

	it->second = parameterValue;
	return true;
}

extern "C" EXTERNALCOMPONENTLIBRARY_API const std::string GetParameter(string parameterName)
{
	cout << "Global::GetParameter called" << endl;
	initialize();
	map<string,string>::iterator it = globalParams.find(parameterName);
	if(it != globalParams.end())
		return it->second;

	return null;
}

extern "C" EXTERNALCOMPONENTLIBRARY_API void GetParameters( map<string,string>& paramHolder)
{
	cout << "Global::GetParameters called" << endl;
	initialize();
	paramHolder.insert(globalParams.begin(), globalParams.end());
}

extern "C" EXTERNALCOMPONENTLIBRARY_API void GetSupportedParameters(vector<string>& paramHolder){
	cout << "Global::GetSupportedParameters called" << endl;
	initialize();
	for(map<string,string>::iterator it = globalParams.begin(); it != globalParams.end(); ++it) {
		paramHolder.push_back(it->first);
	}
}