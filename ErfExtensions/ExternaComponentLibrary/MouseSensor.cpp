#include "stdafx.h"
#include "MouseSensor.h"
#include "ExternalComponentConfiguration.h"
#include "ExternalComponentEnums.h"
#include "EmotionSensorReading.h"
#include "Variant.h"
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include "ErfUtils.h"
#include <ctime>
#include <windowsx.h>

#define MEASURMENT_SPAN 5.0
#define BUFFER_SIZE 12

#pragma data_seg(".shared")
HHOOK mouseSensorHook = NULL;
time_t lastCalculationTime = time(0);
int currentBufferIndex= 0;
int clickCount[BUFFER_SIZE] = {};
int overallDistance[BUFFER_SIZE]= {};
int prevMouseX = 0;
int prevMouseY = 0;
#pragma data_seg()
#pragma comment(linker, "/SECTION:.shared,RWS")


LRESULT CALLBACK MouseSensorMsgProc(UINT nCode, WPARAM wParam, LPARAM lParam)
{ 
	if(nCode < 0)
	{ 
		CallNextHookEx(mouseSensorHook, nCode, wParam, lParam);
		return 0;
	}
	LPMSG msg = (LPMSG)lParam;
	switch( msg->message  )
	{
		case WM_LBUTTONDBLCLK:
			clickCount[currentBufferIndex]++;
		break;
		case WM_MOUSEMOVE:
			{
			int currentX = GET_X_LPARAM(lParam);
			int currentY = GET_Y_LPARAM(lParam);
			int deltaX = currentX - prevMouseX;

			int temp = deltaX >> 31;     // make a mask of the sign bit
			deltaX ^= temp;     
		    deltaX -= temp;  
			int deltaY = currentY - prevMouseY;
			temp = deltaY >> 31;     // make a mask of the sign bit
			deltaY ^= temp;     
		    deltaX -= temp;  

			overallDistance[currentBufferIndex] += deltaX + deltaY;
			prevMouseX = currentX;
			prevMouseY = currentY;
			}
		break;
		case WM_NCMOUSEMOVE:
		break;
		case WM_LBUTTONDOWN:
		//cout << "wmdown" << endl;
			clickCount[currentBufferIndex]++;
		break;
		case WM_NCLBUTTONDOWN:
		break;
		case WM_LBUTTONUP:
		break;
		case WM_NCLBUTTONUP:
		break;
		case WM_RBUTTONDOWN:
		//cout << "wmdown2" << endl;
			clickCount[currentBufferIndex]++;
		break;
		case WM_NCRBUTTONDOWN:
		break;
		case WM_RBUTTONUP:
		break;
		case WM_NCRBUTTONUP:
		break;
        
	default:
		break;
	}

	time_t current = time(0);
	double dif = difftime(current,lastCalculationTime);
	//cout << "Dif " << dif << ">=" << MEASURMENT_SPAN << " " << current << " " << lastCalculationTime << endl;
	if(dif >= MEASURMENT_SPAN)
	{
		//cout << "Dif time, count new index" << endl;
		lastCalculationTime = current;
		currentBufferIndex++;
		currentBufferIndex %= BUFFER_SIZE;
		clickCount[currentBufferIndex] = 0;
		overallDistance[currentBufferIndex] = 0;
	}
	return CallNextHookEx(mouseSensorHook, nCode, wParam, lParam);
	//return erf::MouseSensor::GetInstance()->InternalMouseSensorMsgProc(code, wParam, lParam); 
}

namespace erf {
	
	MouseSensor* MouseSensor::sensorInstance = NULL;
	
	MouseSensor* MouseSensor::CreateInstance(ExternalComponentConfiguration* pConfiguration)
	{
		if(MouseSensor::sensorInstance != NULL)
		{
			cerr << "Instance already created!";
			return sensorInstance;
		}
		sensorInstance = new MouseSensor(pConfiguration);
		return sensorInstance;
	}

	MouseSensor::MouseSensor(ExternalComponentConfiguration* pConfiguration): 
		BasicObject("MouseSensor"),EmotionSensor(pConfiguration, SensorType::STD_HEART_RATE)
	{
		this->currentSensorBufferIndex = 0;
		this->mouseClicks = 0;
		this->componentState = ComponentState::UNINITIALIZED;
	}

	MouseSensor::MouseSensor(const MouseSensor &other): 
		BasicObject(other.objectName),EmotionSensor(other.configuration, SensorType::STD_HEART_RATE)
	{
		throw "Not supported";
	}

	MouseSensor::~MouseSensor() { 
		for(unsigned int i = 0 ; i < previousReadings.size() ; i++)
			delete previousReadings[i];
		if(this->componentState != ComponentState::UNINITIALIZED
				&& this->componentState != ComponentState::DEINITIALIZED)
			Deinitialize();
	} 


	EmotionSensorReading* MouseSensor::GetSensorReading()
	{
		//cout << "MouseSensor::GetSensorReading called" << endl;

		if(this->componentState == ComponentState::COMPONENT_ERROR ||
			this->componentState == ComponentState::DEINITIALIZED ||
			this->componentState == ComponentState::UNINITIALIZED)
		{
			lastError = "Component was not initialized";
			cout << "MouseSensor::GetSensorReading error: "<< lastError << endl;
			return null;
		}

		if(this->currentSensorBufferIndex == ::currentBufferIndex)
		{
			if(previousReadings.size() > 0)
				return previousReadings[previousReadings.size()-1];
			
			cout << "MouseSensor::GetSensorReading error: index misalign: " << this->currentSensorBufferIndex <<
				" : " << ::currentBufferIndex  << " : " << previousReadings.size() << endl;
			return null;
		}
		
		this->currentSensorBufferIndex = ::currentBufferIndex;
		int prevIndex = this->currentSensorBufferIndex -1;
		prevIndex = prevIndex == -1 ? BUFFER_SIZE-1 : prevIndex;

 		EmotionSensorReading* reading = new EmotionSensorReading();
		reading->AddValue("LBufIndex",Variant::Create(prevIndex ));
		reading->AddValue("MouseClicks",Variant::Create(::clickCount[prevIndex]));
		reading->AddValue("MouseDistance",Variant::Create(::overallDistance[prevIndex]));
		previousReadings.push_back(reading);
		while(this->previousReadings.size() > 
				maxNumberOfPreviousReadings)
		{
			EmotionSensorReading* oldReading = previousReadings[0];
			previousReadings.erase(previousReadings.begin());
			delete oldReading;
		}
		
		//cout << "MouseSensor::GetSensorReading return: " << (int)reading<< endl;
		//cout << "SensorDataFileMock::GetSensorReading read address: " << reading << endl;
		return reading;
	}

	void MouseSensor::GetSensorReadings(vector<EmotionSensorReading*>& container, int numberOfReadings) {
		int readReadings = 0 ;
		for(int i = this->previousReadings.size() -1 ; i >= 0 ; i--)
		{
			if(readReadings == numberOfReadings)
				break;
			container.push_back(this->previousReadings[i]);
			readReadings++;
		}
	}

	string MouseSensor::GetComponentLastError(){
		cout << "MouseSensor::GetComponentLastError called" << endl;
		string error = this->lastError;
		this->lastError = "";
		return error;
	}

	
	void MouseSensor::GetParameters(map<string,string>& paramHolder){
		cout << "MouseSensor::GetParameters called" << endl;
		paramHolder.insert(parameters.begin(),parameters.end());
	}

	void MouseSensor::GetSupportedParameters(vector<string>& paramHolder){
		cout << "MouseSensor::GetSupportedParameters called" << endl;
		for(map<string,string>::iterator it = parameters.begin(); it != parameters.end(); ++it) {
			paramHolder.push_back(it->first);
		}
	}

	const string MouseSensor::GetParameter(string parameterName)
	{
		cout << "MouseSensor::GetParameter called" << endl;
		map<string,string>::iterator it = parameters.find(parameterName);
		if(it != parameters.end())
			return it->second;

		return null;
	}

	bool MouseSensor::SetParameter(string parameterName, string parameterValue){
		cout << "MouseSensor::SetParameter called" << endl;
		map<string,string>::iterator it = parameters.find(parameterName);
		if(it == parameters.end())
			return false;

		return true;
	}

	void MouseSensor::Initialize() {
		if(this->componentState != ComponentState::UNINITIALIZED &&
			this->componentState != ComponentState::DEINITIALIZED &&
			this->componentState != ComponentState::COMPONENT_ERROR)
		{
			lastError = "Only unitialized components can be initialized";
			return;
		}

		if(mouseSensorHook != NULL)
		{
			lastError ="Hook already found. Only one MouseSensor can be active at any given time!";
			this->componentState = ComponentState::COMPONENT_ERROR;
			return;
		}

		mouseSensorHook = SetWindowsHookEx(WH_GETMESSAGE,(HOOKPROC)MouseSensorMsgProc,dllInstance,0);

		if(mouseSensorHook == NULL)
		{
			
			lastError ="SetWindowsHookEx call failed. Hook could not be created.";
			this->componentState = ComponentState::COMPONENT_ERROR;
			return;
		}

		this->componentState = ComponentState::INITIALIZED;
	}

	void MouseSensor::Reinitialize() {
		if(this->componentState != ComponentState::INITIALIZED &&
			this->componentState != ComponentState::DEINITIALIZED &&
			this->componentState != ComponentState::COMPONENT_ERROR&&
			this->componentState != ComponentState::REINITIALIZED)
		{
			lastError = "Only initialized/deinitialized components can be reinitialized";
			return;
		}
		//Clear all data
		this->componentState = ComponentState::REINITIALIZED;
	}

	void MouseSensor::Deinitialize(){
		if(this->componentState != ComponentState::INITIALIZED &&
			this->componentState != ComponentState::REINITIALIZED &&
			this->componentState != ComponentState::COMPONENT_ERROR)
		{
			lastError = "Only initialized/reinitialized components can be deinitialized";
			return;
		}

		if(mouseSensorHook != null)
		{
			UnhookWindowsHookEx(mouseSensorHook);
			mouseSensorHook = NULL;
		}

		this->componentState = ComponentState::DEINITIALIZED;
	}

}