#pragma once
#include "CommonExternalLibrary.h"
#include "EmotionExpert.h"

namespace erf
{

    class EXTERNALCOMPONENTLIBRARY_API PadAffectionExpert : public EmotionExpert
    {
		private:
			float modelRank;
			int smallTreshold;
			int midTreshold;
			map<string,string> parameters;
			string lastError;
			string bvpSensorName;
			string emgSensorName;
			string scSensorName;
		public:
			PadAffectionExpert(ExternalComponentConfiguration* pConfiguration);

			PadAffectionExpert(const PadAffectionExpert &other);
			virtual ~PadAffectionExpert();

			//Expert methods
			virtual float GetModelRank() { return modelRank; }

			virtual string GetComponentLastError();

			virtual const string GetParameter(string parameterName);
		
			virtual void GetParameters(map<string,string>& paramHolder);

			virtual void GetSupportedParameters(vector<string>& paramHolder) ;

			virtual bool SetParameter(string parameterName, string parameterValue);

			virtual void Initialize();

			virtual void Reinitialize() {};

			virtual void Deinitialize() {};

			//Affection model methods
			virtual void InitializeForCharacter(CharacterData& characterData) ;
			
			virtual bool IsEventSupported(EecEvent& event);

			virtual EmotionVector* HandleEvent(EecEvent& event, CharacterData& userData);

	};
}

