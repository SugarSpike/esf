﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Erf
{
    public static class OccEmotions
    {
        public const string JOY = "JOY";
        public const string DISTRESS = "DISTRESS";
        public const string PRIDE = "PRIDE";
        public const string SHAME = "SHAME";
        public const string ADMIRATION = "ADMIRATION";
        public const string REPROACH = "REPROACH";
        public const string LOVE = "LOVE";
        public const string HATE = "HATE";
        public const string HAPPY_FOR = "HAPPY_FOR";
        public const string RESENTMENT = "RESENTMENT";
        public const string GLOATING = "GLOATING";
        public const string PITY = "PITY";
        public const string HOPE = "HOPE";
        public const string FEAR = "FEAR";
        public const string SATISFACTION = "SATISFACTION";
        public const string FEARS_CONFIRMED = "FEARS_CONFIRMED";
        public const string RELIEF = "RELIEF";
        public const string DISAPPOINTMENT = "DISAPPOINTMENT";
        public const string GRATIFICATION = "GRATIFICATION";
        public const string REMORSE = "REMORSE";
        public const string GRATITUDE = "GRATITUDE";
        public const string ANGER = "ANGER";
    }

    public static class PadEmotions
    {
        public const string PLEASURE = "PLEASURE";
        public const string AROUSAL = "AROUSAL";
        public const string DOMINANCE = "DOMINANCE";
    }
}
