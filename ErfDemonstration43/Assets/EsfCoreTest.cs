﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Erf;

public class EsfCoreTest : MonoBehaviour {

	private CharacterModel model;

	// Use this for initialization
	void Start () {
		System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();

		//Wczytuje biblioteke do pamieci, istnieje zabezpieczenie przed ponownym wczytniem tej biblioteki
		ErfContext.GetInstance ().LoadComponentLibrary ("PhotronErf.dll");

		model = new CharacterModel ();
		GameConfiguration configuration = new GameConfiguration("Test_game_name", GameType.STD_RPG);
		configuration.AddGameCharacteristic(GameCharateristics.STD_CASUAL);
		configuration.AddGameCharacteristic(GameCharateristics.STD_ADULT);
		configuration.AddGameCharacteristic(GameCharateristics.STD_ACTION);
		
		configuration.AddGameChallenge(GameChallenges.STD_DONT_DIE);
		configuration.AddGameChallenge(GameChallenges.STD_BOSS_FIGHT);
		
		model.SetGameConfiguration(configuration);

		ExternalExpert photronEmotionModel = ErfContext.GetInstance ().FindExpert ("PhotronEmotionModel");

		model.RegisterExpert (photronEmotionModel);
		Debug.Log(string.Format("Initialize time: {0}", watch.ElapsedMilliseconds));
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void OnLeftClick()
	{
		EecEvent eecEvent = new EecEvent ((int)GameEvent.STD_TIME_ELAPSED);
		System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();
		bool handled = model.HandleEvent(eecEvent);
		watch.Stop();
		Debug.Log(string.Format("Event handled, time {0}", watch.ElapsedMilliseconds));
		if (handled)
		{ 
			watch.Reset();
			watch.Start();
			Debug.Log("Event handled");

			//EmotionModelType.STD_OCC
			EmotionVector emotionVector = model.GetEmotionVector();
			if(emotionVector.IsOfEmotionModelType((int)EmotionModelType.STD_OCC))
			{
				foreach(KeyValuePair<string,Variant> singleVectorVal in emotionVector.GetValues())
				{
					Debug.Log(string.Format("Emotion: {0} = {1} [{2}]",singleVectorVal.Key,singleVectorVal.Value.AsFloat(),
					          emotionVector.GetCertainty(singleVectorVal.Key)));
				}

			}
			float fearIntensity = emotionVector.GetValue(OccEmotions.FEAR).AsFloat();
			float fearCertainty = emotionVector.GetCertainty(OccEmotions.FEAR);
			Debug.Log(string.Format("Emotion FEAR test: {0} = {1} [{2}]",
			          OccEmotions.FEAR,
			          fearIntensity,
			          fearCertainty));

			Variant data =  emotionVector.GetValue("NON_EXISTANT");
			if(data == null)
				Debug.Log("NON_EXISTANT key not found");

			watch.Stop();
			Debug.Log(string.Format("HandleEnd time {0}", watch.ElapsedMilliseconds));
		}
	}

}
