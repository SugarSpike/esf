﻿using UnityEngine;
using System.Collections;
using Erf;

public class ErfInitialization : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	void Awake() {
		Debug.Log ("Initializing ERF for ErfDemo");
		ErfLogger.SetLoggingEnabled (false);
		ErfLogger.SetAllStreamsToLoggingFile (@"d://debug.txt");
		ExternalComponentLibrary library = ErfContext.GetInstance ().LoadComponentLibrary ("ExternalComponentLibrary.dll");
		
		if (library == null) {
			Debug.LogError("Could not load ExternalComponentLibrary library");
			return;
		}
		Debug.Log ("Library successfully loaded");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
