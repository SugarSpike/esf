using UnityEngine;
using System.Collections;
using Erf;

public class PlayerOccMouseCharacterModel : PlayerCharacterModel
{
	private EmotionSensor mouseSensor;

	public PlayerOccMouseCharacterModel(){
		this.Model = BuildOccMouseModel ();
	}

	private ExternalExpert InitializeOccMouseExpert(CharacterModel model, ErfContext context, string expertName)
	{
		ExternalExpert expert = context.FindExpert(expertName);
		if (expert == null) {
			
			Debug.LogError("Could not find expert:" + expertName);
			return null;
		}
		mouseSensor = FindSensorByName(context,"MouseSensor");
		model.RegisterEmotionSensor (mouseSensor);
		expert.Initialize ();
		if (expert.GetComponentState () == ComponentState.COMPONENT_ERROR) {
			Debug.LogError (expert.GetComponentLastError ());
			Debug.Log (expert.GetComponentLastError ());
			return null;
		}
		return expert;
	}
	
	private EmotionSensor FindSensorByName(ErfContext context,string sensorName)
	{
		EmotionSensor sensor = context.FindSensor(sensorName);
		if (sensor == null) {
			return null;
		}
		ComponentState currentState = sensor.GetComponentState ();
		if (currentState == ComponentState.UNINITIALIZED
		    ||	currentState == ComponentState.DEINITIALIZED) {
			Debug.Log ("Initializing sensor:" + sensorName);
			sensor.Initialize ();
		} else {
			Debug.Log ("Reinitializing sensor:" + sensorName);
			sensor.Reinitialize();
		}
		if(sensor.GetComponentState() == ComponentState.COMPONENT_ERROR)
		{
			string message = sensor.GetComponentLastError();
			Debug.Log(message);
			//Debug.LogError(fileReadingSensor.GetComponentLastError());
			return null;
		}
		EmotionSensorReading reading =  sensor.GetSensorReading ();
		if (reading != null) {
			Variant test = reading.GetValue("MouseClicks");
			Debug.Log("T: " + test.IsInteger());
			if(test.IsInteger())
				Debug.Log("C: " + test.AsInteger());
		}
		return sensor;
	}
	
	private CharacterModel BuildOccMouseModel()
	{
		CharacterModel model = new CharacterModel ();
		ErfContext context = ErfContext.GetInstance();
		ExternalExpert expert = InitializeOccMouseExpert (model, context, "ErfDemonstrationExpert");
		model.RegisterExpert (expert);
		return model;
	}

	public override bool UpdateModel(EecEvent eecEvent)
	{
		eecEvent.Clear ();
		GameStats currentStats = GameManager.Instance.GameStats [0];
		eecEvent.AddValue ("ErfDroppedShots", Variant.Create(currentStats.DroppedBalls));
		eecEvent.AddValue ("ErfMissedShots", Variant.Create(currentStats.MissedBalls));
		eecEvent.AddValue ("ErfSuccessfulShots", Variant.Create(currentStats.ScoredBalls));
		return this.Model.HandleEvent (eecEvent);
	}

	public override EmotionVector GetOccVector() {
		return Model.GetEmotionVector ();
	}
	
	public override EmotionVector GetPadVector(){
		return null;
	}

	
	
	public override EmotionVector GetPreviousOccVector(){
		return Model.GetCharacterData ().GetPreviousEmotionVector ();
	}
	
	public override EmotionVector GetPreviousPadVector(){
		return null;
	}

	public override void DeinitializeModel ()
	{
		if (mouseSensor != null)
			mouseSensor.Deinitialize ();
		base.DeinitializeModel ();
	}

	int i = 1;
	public override void PrettyPrintVectors ()
	{
		float joy = this.Model.GetEmotionVector().GetValue(OccEmotions.JOY).AsFloat();
		float distress = this.Model.GetEmotionVector().GetValue(OccEmotions.DISTRESS).AsFloat();
		string message = string.Format("{0}\tj:{4:0.00}\td:{5:0.00}",i++,joy,distress);
		Debug.Log (message);
	}

}

