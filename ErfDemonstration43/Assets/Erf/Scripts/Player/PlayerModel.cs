﻿using UnityEngine;
using System.Collections;
using Erf;

public class PlayerModel : Singleton<PlayerModel> {

	public float EvaluationInterval = 1.0f;
	private EecEvent timeElapsedEvent;
	private bool isReady = false;
	public bool DistressDominates {
		get;
		set;
	}

	private CharacterModel Model { get { return this.EmotionalModel.Model; } }

	private object emotionalModelLock = new object ();
	private PlayerCharacterModel EmotionalModel;

	private EmotionVector currentVector;
	public EmotionVector CurrentVector {
		get {
			lock (emotionalModelLock) {
				return currentVector;
			}
		}
	}

	private EmotionVector currentUnfilteredVector;
	public EmotionVector CurrentUnfilteredVector {
		get {
			lock (emotionalModelLock) {
				return currentUnfilteredVector;
			}
		}
	}

	private EmotionVector previousVector;
	public EmotionVector PreviousVector {
		get {
			lock (emotionalModelLock) {
				return previousVector;
			}
		}
	}

	public bool IsReady()
	{
		return isReady;
	}
	




	// Use this for initialization
	void Start () {
		Time.timeScale = 0.0f;
		this.EmotionalModel = PlayerCharacterModelFactory.BuildModel (GameConsts.PlayerCharacterType);
		timeElapsedEvent = new EecEvent ((int)GameEvent.STD_TIME_ELAPSED);
		this.EmotionalModel.UpdateModel(timeElapsedEvent); 

		InvokeRepeating ("UpdateEmotionalModel", 2.0f, EvaluationInterval);
		DistressDominates = false;
		isReady = true;
	}

	public void ChangeCharacterModel(PlayerCharacterModelType type)
	{
		if (GameConsts.PlayerCharacterType == type)
			return;
		GameConsts.PlayerCharacterType = type;
		if (this.EmotionalModel != null)
			this.EmotionalModel.DeinitializeModel ();

		this.EmotionalModel = PlayerCharacterModelFactory.BuildModel (GameConsts.PlayerCharacterType);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void SendEventToBallsManager(){
		EecEvent eecEvent = new EecEvent ((int)GameEvent.STD_PLAYER_EMOTIONAL_STATE_CHANGE);
		EmotionVector currentVector = EmotionalModel.GetOccVector ();
		EmotionVector previousVector = EmotionalModel.GetPreviousOccVector ();
		eecEvent.AddValue ("PlayerEmotionVector", 
		                   Variant.CreateEmotionVector (currentVector));
		eecEvent.AddValue ("PreviousPlayerEmotionVector", 
		                   Variant.CreateEmotionVector (previousVector));
		FeelBallsManager.Instance.SendEecEventToFeelBalls (eecEvent);

	}

	public void UpdateEmotionalModel()
	{
		if (this.EmotionalModel == null)
			return;

		lock (emotionalModelLock) {
			GameProfiler.Instance.PlayerPassStart();
			bool result = this.EmotionalModel.UpdateModel (timeElapsedEvent);
			GameProfiler.Instance.PlayerPassStop();
			if (result) {
				currentVector = EmotionalModel.GetOccVector();
				previousVector =  EmotionalModel.GetPreviousOccVector();
				currentUnfilteredVector =  EmotionalModel.GetPadVector();

				float currentJoy = currentVector.GetValue (OccEmotions.JOY).AsFloat ();
				float currentDistress = currentVector.GetValue (OccEmotions.DISTRESS).AsFloat ();
				DistressDominates = currentJoy < currentDistress ? true : false;

				if (GameConsts.EnableErfLog)
					EmotionalModel.PrettyPrintVectors ();
				SendEventToBallsManager();
			}
		}
	}

	public void OnApplicationQuit()
	{
		if (EmotionalModel != null)
			EmotionalModel.DeinitializeModel ();
	}
}
