﻿using System;
using UnityEngine;
using System.Collections;

public class CheerUp : AiBehavior {

	#region Properties & Fields
	
	public float Range { get; set; }
	
	#endregion
	
	#region Events & Delegates
	public delegate void EnterEventHandler(object sender, EventArgs e);
	public event EnterEventHandler OnEnter;
	
	public delegate void UpdateEventHandler(object sender, EventArgs e);
	public event UpdateEventHandler OnUpdate;
	
	public delegate void ExitEventHandler(object sender, EventArgs e);
	public event ExitEventHandler OnExit;

	public delegate void TargetOutOfRangeHandler(object sender, EventArgs e);
	public event TargetOutOfRangeHandler OnTargetOutOfRange;
	#endregion
	
	#region Initialization
	
	public CheerUp() : base() { 
		Target = GameObject.Find (GameConsts.PlayerTag);
		this.Range = GameConsts.PlayerCheerRadius;
	}
	
	public CheerUp(float range)
		: this()
	{
		this.Range = range;
	}
	
	public CheerUp(GameObject target, float range)
		: this()
	{
		this.Target = target;
		this.Range = range;
	}
	
	#endregion
	
	
	public override void Enter()
	{
		if (OnEnter != null)
			OnEnter(this, new BehaviorEventArgs() { Owner = Owner });
	}
	
	
	public override void Update()
	{
		if (OnUpdate != null)
			OnUpdate(this, new BehaviorEventArgs() { Owner = Owner });
		
		if (GameUtils.IsGrounded (Owner)) {
			if (!TargetOutOfRange ()) {
				Jump ();
			} else {
				if (OnTargetOutOfRange != null)
					OnTargetOutOfRange (this, new BehaviorEventArgs () { Owner = Owner });
				OnTargetOutOfRange = null;
			}
		}
	}

	protected bool TargetOutOfRange()
	{
		float distance;
		Vector3 playerPos = Owner.transform.position;
		Vector3 actorPos = Target.transform.position;
		//Ignore Y
		playerPos.y = actorPos.y = 0;
		distance = Vector3.Distance(playerPos,actorPos);
		
		if (distance >= Range)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	
	public override void Exit()
	{
		if (OnExit != null)
			OnExit(this, new BehaviorEventArgs() { Owner = Owner });
		OnExit = null;
	}

	
	protected virtual void Jump()
	{
		Owner.rigidbody.AddForce (Vector3.up * UnityEngine.Random.Range(129.0f,160.0f));
		
	}
}
