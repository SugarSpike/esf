﻿using System;
using UnityEngine;
using System.Collections;

public class HeldByPlayer : AiBehavior {

	#region Properties & Fields
	
	public float Range { get; set; }
	
	#endregion
	
	#region Events & Delegates
	public delegate void EnterEventHandler(object sender, EventArgs e);
	public event EnterEventHandler OnEnter;
	
	public delegate void UpdateEventHandler(object sender, EventArgs e);
	public event UpdateEventHandler OnUpdate;
	
	public delegate void ExitEventHandler(object sender, EventArgs e);
	public event ExitEventHandler OnExit;
	
	#endregion
	
	#region Initialization
	
	public HeldByPlayer() : base() { 
		Target = GameObject.Find (GameConsts.PlayerTag);
		this.Range = GameConsts.PlayerCheerRadius;
	}
	
	public HeldByPlayer(float range)
		: this()
	{
		this.Range = range;
	}
	
	public HeldByPlayer(GameObject target, float range)
		: this()
	{
		this.Target = target;
		this.Range = range;
	}
	
	#endregion
	
	
	public override void Enter()
	{
		if (OnEnter != null)
			OnEnter(this, new BehaviorEventArgs() { Owner = Owner });
	}
	
	
	public override void Update()
	{
		if (OnUpdate != null)
			OnUpdate(this, new BehaviorEventArgs() { Owner = Owner });


		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Owner.transform.position = ray.GetPoint (4.0f);

	}
	
	public override void Exit()
	{
		if (OnExit != null)
			OnExit(this, new BehaviorEventArgs() { Owner = Owner });
		OnExit = null;
	}

	
	protected virtual void Jump()
	{
	}
}
