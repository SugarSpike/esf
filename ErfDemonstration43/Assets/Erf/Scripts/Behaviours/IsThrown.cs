﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IsThrown : AiBehavior {
		
	#region Properties & Fields
	
	public FeelBall BallOwner { get; set; }

	private static List<GameObject> hoops;
	#endregion

	#region Events & Delegates
	public delegate void EnterEventHandler(object sender, EventArgs e);
	public event EnterEventHandler OnEnter;
	
	public delegate void UpdateEventHandler(object sender, EventArgs e);
	public event UpdateEventHandler OnUpdate;
	
	public delegate void ExitEventHandler(object sender, EventArgs e);
	public event ExitEventHandler OnExit;

	#endregion
	
	#region Initialization

	static IsThrown() {
		hoops = new List<GameObject> (GameUtils.GetHoops());
	}

	public IsThrown(FeelBall owner) : base() { 
		Target = GameObject.Find (GameConsts.PlayerTag);
		BallOwner = owner;
	}
	
	#endregion
	
	
	public override void Enter()
	{
		if (OnEnter != null)
			OnEnter(this, new BehaviorEventArgs() { Owner = Owner });
	}

	public override void Update()
	{
		if (OnUpdate != null)
			OnUpdate(this, new BehaviorEventArgs() { Owner = Owner });

		if (GameUtils.IsGrounded (Owner))
			return;
		bool distressDominate = PlayerModel.Instance.DistressDominates;
		if (BallOwner.IsFriendlyToPlayer && distressDominate)
			AutoAim ();
		else if(!BallOwner.IsFriendlyToPlayer && !distressDominate)
			AntiAim ();

	}

	private void AutoAim() {
		float minDistance = GameConsts.AutoAimRadius;
		Vector3 closestHoop = Vector3.zero;
		bool foundHoop = false;
		foreach (GameObject hoop in hoops) {
			Vector3 direction = hoop.transform.position - Owner.transform.position;
			float currentDistance = direction.sqrMagnitude;
			if(currentDistance < minDistance)
			{
				minDistance = currentDistance;
				closestHoop = direction;
				foundHoop = true;
				
			}
		}
		if (!foundHoop)
			return;
		
		Owner.rigidbody.AddForce (closestHoop.normalized * 5.0f);
		Debug.DrawRay(Owner.transform.position, closestHoop.normalized * 20.0f, Color.green,2f,false);
	}

	private void AntiAim(){
		float minDistance = GameConsts.AutoAimRadius;
		Vector3 closestHoop = Vector3.zero;
		bool foundHoop = false;
		foreach (GameObject hoop in hoops) {
			Vector3 direction = hoop.transform.position - Owner.transform.position;
			float currentDistance = direction.sqrMagnitude;
			if(currentDistance < minDistance)
			{
				minDistance = currentDistance;
				closestHoop = direction;
				foundHoop = true;
				
			}
		}
		if (!foundHoop)
			return;
		
		Owner.rigidbody.AddForce (closestHoop.normalized*-5.0f);
		Debug.DrawRay(Owner.transform.position, closestHoop.normalized*-5.0f, Color.red,2f,false);
	}
	
	public override void Exit()
	{
		if (OnExit != null)
			OnExit(this, new BehaviorEventArgs() { Owner = Owner });
		OnExit = null;
	}

	
	protected virtual void Jump()
	{
		
	}
}
