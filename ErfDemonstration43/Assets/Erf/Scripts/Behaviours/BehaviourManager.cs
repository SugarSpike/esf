﻿using System;
using UnityEngine;
using System.Collections;


public class BehaviorManager
{
	
	public delegate void BehaviorChangedHandler(object sender, EventArgs e);
	public event BehaviorChangedHandler OnBehaviorChanged;
	
	private GameObject owner;
	public GameObject Owner
	{
		get { return owner; }
		set
		{
			owner = value;
		}
	}
	
	private AiBehavior globalBehavior;
	public AiBehavior GlobalBehavior
	{
		get { return globalBehavior; }
		set { globalBehavior = value; if(globalBehavior!=null)globalBehavior.Owner = owner; }
	}
	
	private AiBehavior currentBehavior;
	public AiBehavior CurrentBehavior
	{
		get { return currentBehavior; }
		set { currentBehavior = value; if (currentBehavior != null)currentBehavior.Owner = owner; }
	}
	
	private AiBehavior previousBehavior;
	public AiBehavior PreviousBehavior
	{
		get { return previousBehavior; }
		set { previousBehavior = value; if (previousBehavior != null) previousBehavior.Owner = owner; }
	}
	
	public BehaviorManager() {
	}

	public void ChangeBehavior(AiBehavior newBehavior)
	{
		//Debug.Log ("FBall state change: " + newBehavior);

		if (OnBehaviorChanged != null)
			OnBehaviorChanged(this, new BehaviorEventArgs() { Owner = Owner, Behavior = (newBehavior == null ? GlobalBehavior : newBehavior) });
		
		previousBehavior = currentBehavior;
		if (CurrentBehavior != null) CurrentBehavior.Exit();
		CurrentBehavior = newBehavior;
		if (CurrentBehavior != null) CurrentBehavior.Enter ();
	}
	
	public void RevertPreviousBehavior()
	{
		if (CurrentBehavior != null) CurrentBehavior.Exit();
		AiBehavior temp = CurrentBehavior;
		CurrentBehavior = PreviousBehavior;
		PreviousBehavior = temp;
		if (CurrentBehavior != null) CurrentBehavior.Enter();
	}
	
	public void RevertBehaviorHandler(object sender, EventArgs e)
	{
		RevertPreviousBehavior();
	}
	
	public void Update()
	{
		if (CurrentBehavior != null) CurrentBehavior.Update();
		else if(GlobalBehavior!=null) GlobalBehavior.Update();
	}
	
}