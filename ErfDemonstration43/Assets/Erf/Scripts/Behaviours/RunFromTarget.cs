﻿using System;
using UnityEngine;
using System.Collections;

public class RunFromTarget : AiBehavior
{
	#region Properties & Fields
	
	private const int UPDATES_PER_SEC = 100;
	private const float TARGET_FAULT_RADIUS = 0.5f;
	private const float ROTATION_SPEED = 10.0f;
	private const float ROTATION_ANGLE_FAULT = 0.12f;
	
	public float Range { get; set; }
	
	#endregion
	
	#region Events & Delegates
	public delegate void EnterEventHandler(object sender, EventArgs e);
	public event EnterEventHandler OnEnter;
	
	public delegate void UpdateEventHandler(object sender, EventArgs e);
	public event UpdateEventHandler OnUpdate;
	
	public delegate void ExitEventHandler(object sender, EventArgs e);
	public event ExitEventHandler OnExit;
	
	public delegate void TargetOutOfRangeHandler(object sender, EventArgs e);
	public event TargetOutOfRangeHandler OnTargetOutOfRange;
	#endregion
	
	#region Initialization
	
	public RunFromTarget() : base() { 
		Target = GameObject.Find (GameConsts.PlayerTag);
		this.Range = 15.0f;
	}
	
	public RunFromTarget(float range)
		: this()
	{
		this.Range = range;
	}
	
	public RunFromTarget(GameObject target, float range)
		: this()
	{
		this.Target = target;
		this.Range = range;
	}
	
	#endregion
	
	
	public override void Enter()
	{
		if (OnEnter != null)
			OnEnter(this, new BehaviorEventArgs() { Owner = Owner });
	}
	
	
	public override void Update()
	{
		if (OnUpdate != null)
			OnUpdate(this, new BehaviorEventArgs() { Owner = Owner });
		
		if (GameUtils.IsGrounded (Owner)) {
			if (!TargetOutOfRange ()) {
				Move ();
			} else {
				//target is in range
				if (OnTargetOutOfRange != null)
					OnTargetOutOfRange (this, new BehaviorEventArgs () { Owner = Owner });
				OnTargetOutOfRange = null;
			}
		}
	}
	
	private bool TargetOutOfRange()
	{
		float distance;
		Vector3 playerPos = Owner.transform.position;
		Vector3 actorPos = Target.transform.position;
		//Ignore Y
		playerPos.y = actorPos.y = 0;
		distance = Vector3.Distance(playerPos,actorPos);
		
		if (distance >= Range)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	public override void Exit()
	{
		if (OnExit != null)
			OnExit(this, new BehaviorEventArgs() { Owner = Owner });
		OnExit = null;
		// Target.Animations.Play();
	}
	
	private void Move()
	{
		Vector3 actorXYZ = new Vector3(Owner.transform.position.x, 0, Owner.transform.position.z);
		Vector3 moveVector = new Vector3(Target.transform.position.x, 0, Target.transform.position.z) - actorXYZ;
		moveVector.Normalize();
		
		var rot = Quaternion.Euler (0, 180,0);
		moveVector = rot * moveVector;
		
		Owner.rigidbody.AddForce (moveVector * UnityEngine.Random.Range(20.0f,25.0f));
		Owner.rigidbody.AddForce (Vector3.up * UnityEngine.Random.Range(80.0f,100.0f));
		
	}
	
	
	
	
}
