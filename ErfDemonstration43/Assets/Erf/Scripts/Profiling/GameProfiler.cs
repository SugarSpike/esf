using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

public class GameProfilerEntry
{
	public GameProfilerEntry() {
		DateOfEntry = DateTime.Now;
	}
	
	public DateTime DateOfEntry {
		get;
		set;
	}
	
	public double ElapsedTime {
		get;
		set;
	}
	
	public int NoOfObjects { get; set; }
}

public class GameProfiler : IDisposable
{
	private static GameProfiler instance;

	public static GameProfiler Instance {
		get {
			if(instance == null)
				instance = new GameProfiler();
			return instance;
		}
	}

	List<GameProfilerEntry> playerModelMeasurments;
	List<GameProfilerEntry> ballFirstPassMeasurments;
	List<GameProfilerEntry> ballSecondPassMeasurments;
	
	public bool SaveMeasurmentOnGameEnd { get; set; }
	public bool ProfilingEnabled { get; set; }
	private bool initialized;
	private StreamWriter profileLogFile;
	
	private GameProfilerMeasurer measurer;
	
	private GameProfilerMeasurerType measurerType;
	public GameProfilerMeasurerType MeasurerType {
		get { return measurerType; }
		set {
			if(measurerType == value)
				return;
			measurerType = value;
			measurer = GameProfilerMeasurerFactory.Build(measurerType);
		}
	}
	
	public GameProfiler(){
		initialized = false;
		playerModelMeasurments = new List<GameProfilerEntry>();
		ballFirstPassMeasurments = new List<GameProfilerEntry>();
		ballSecondPassMeasurments = new List<GameProfilerEntry>();
		SaveMeasurmentOnGameEnd = true;
		ProfilingEnabled = true;
		MeasurerType = GameProfilerMeasurerType.DateTime;
	}
	
	public void PlayerPassStart(){ 
		measurer.PlayerPassStart ();
	}
	
	public void PlayerPassStop(){ 
		GameProfilerEntry entry = measurer.PlayerPassStop ();
		playerModelMeasurments.Add (entry);
		
	}
	
	public void BallFirstPassStart(){ 
		measurer.BallFirstPassStart ();
		
	}
	
	public void BallFirstPassStop(int noOfBalls){ 
		GameProfilerEntry entry = measurer.BallFirstPassStop (noOfBalls);
		ballFirstPassMeasurments.Add (entry);
	}
	
	public void BallSecondPassStart(){ 
		measurer.BallSecondPassStart ();
	}
	
	public void BallSecondPassStop(int noOfBalls){ 
		GameProfilerEntry entry = measurer.BallSecondPassStop (noOfBalls);
		ballSecondPassMeasurments.Add (entry);
	}
	
	private void SaveMeasurmentsToFile(string title, List<GameProfilerEntry> measurments)
	{
		GameProfilerEntry firstEntry = measurments [0];
		GameProfilerEntry lastEntry = measurments [measurments.Count - 1];
		profileLogFile.WriteLine ("===="+title+"====");
		profileLogFile.WriteLine ("ModelType: " + GameConsts.PlayerCharacterType);
		profileLogFile.WriteLine ("Start: {0}",firstEntry.DateOfEntry.ToString("yyyy-MM-dd HH:mm:ss:fff"));
		profileLogFile.WriteLine ("End: {0}",lastEntry.DateOfEntry.ToString("yyyy-MM-dd HH:mm:ss:fff"));
		string allMeasurments = "";
		double totalMiliseconds = 0;
		int totalObjects = 0;
		foreach (GameProfilerEntry entry in measurments) {
			totalMiliseconds += entry.ElapsedTime;
			totalObjects += entry.NoOfObjects;
			allMeasurments += entry.ElapsedTime.ToString("0.00000") + ":" + entry.NoOfObjects + ",";
		}
		
		
		profileLogFile.WriteLine ("TotalObjects: {0}, TotalMiliseconds: {1}, AverageMiliseconds: {2}",
		                          totalObjects, totalMiliseconds,totalMiliseconds/totalObjects );
		profileLogFile.WriteLine ("Measurment: " + allMeasurments);
		profileLogFile.WriteLine ("========");
	}
	
	private void SavePlayerMeasurmentsToFile(){
		
		int noOfMeasurments = playerModelMeasurments.Count;
		if (noOfMeasurments == 0)
			return;
		
		List<GameProfilerEntry> playerMeasurments = new List<GameProfilerEntry> (this.playerModelMeasurments);
		playerModelMeasurments.Clear ();
		SaveMeasurmentsToFile ("PLAYER MEASURMENT", playerMeasurments);
		
	}

	private void SaveBallFirstPassMeasurmentsToFile(){
		int noOfMeasurments = ballFirstPassMeasurments.Count;
		if (noOfMeasurments == 0)
			return;
		
		List<GameProfilerEntry> ballMeasurments = new List<GameProfilerEntry> (this.ballFirstPassMeasurments);
		ballFirstPassMeasurments.Clear ();
		SaveMeasurmentsToFile ("BALL FIRST PASS", ballMeasurments);
	}
	
	private void SaveBallSecondPassMeasurmentsToFile(){
		int noOfMeasurments = ballSecondPassMeasurments.Count;
		if (noOfMeasurments == 0)
			return;
		
		List<GameProfilerEntry> ballMeasurments = new List<GameProfilerEntry> (this.ballSecondPassMeasurments);
		ballSecondPassMeasurments.Clear ();
		SaveMeasurmentsToFile ("BALL SECOND PASS", ballMeasurments);
	}
	
	public void SaveMeasurmentsToFile(){
		if (!ProfilingEnabled)
			return;

		OpenFileStream ();
		SavePlayerMeasurmentsToFile ();
		SaveBallFirstPassMeasurmentsToFile ();
		SaveBallSecondPassMeasurmentsToFile ();
		CloseFileStream ();
		
	}

	public void SaveGameScoreToFile(int score, int ballsThruHoop, Dictionary<string,int> hoops)
	{
		OpenFileStream ();
		profileLogFile.WriteLine ("====GAME SCORE====");
		profileLogFile.WriteLine ("Time of end: {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff"));
		profileLogFile.WriteLine ("Score: {0}",score);
		profileLogFile.WriteLine ("Balls: {0}",ballsThruHoop);
		profileLogFile.WriteLine ("Hoops: {0}",hoops.Count);
		foreach (KeyValuePair<string,int> hoopStat in hoops) {
			profileLogFile.WriteLine ("{0}: {1}",hoopStat.Key,hoopStat.Value);
		}
		string gameStats = "";
		foreach(GameStats stats in GameManager.Instance.GameStats)
		{
			gameStats += string.Format("M: {0}, D: {1},S: {2}\n", stats.MissedBalls,stats.DroppedBalls,stats.ScoredBalls);
		}
		profileLogFile.WriteLine ("Stats\n: {0}",gameStats);
		profileLogFile.WriteLine ("========");
		CloseFileStream ();
	}

	public void TestPlayerModel(int numberOfPasses)
	{
		PlayerModel instance = PlayerModel.Instance;
		for (int i = 0; i < numberOfPasses; i++)
			instance.UpdateEmotionalModel ();

	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	private void OpenFileStream()
	{
		if (profileLogFile == null) {
			try {
				string path = GameUtils.GetApplicationPath() + GameConsts.ProfileFilePrefix + 
					DateTime.Now.ToString("yyyyMMddHH")+".txt";
				profileLogFile = new StreamWriter (path,true);
				initialized = true;
			} catch(Exception e)
			{
				Debug.LogError("ERROR WHILE TRYING TO CREATE PROFILE FILE:");
				Debug.LogError("PATH:: "+GameUtils.GetApplicationPath() + GameConsts.ProfileFilePrefix + 
				               DateTime.Now.ToString("yyyyMMddHH")+".txt");
				Debug.LogError(e.Message);
			}
		}
	}

	private void CloseFileStream()
	{
		if (profileLogFile != null) {
			try { 
				profileLogFile.Close();
			} catch(Exception e)
			{
				Debug.Log(e.Message);
			}
			finally {
				profileLogFile = null;
			}
		}
	}

	public void Dispose()
	{
		CloseFileStream ();
	}
}

