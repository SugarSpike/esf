using UnityEngine;
using System.Collections;
using System;
public class GameProfilerDateTime : GameProfilerMeasurer
{

	private DateTime  playerStart;
	private DateTime  ballStart;

	public override void PlayerPassStart(){ 
		playerStart = DateTime.UtcNow;
	}
	
	public override GameProfilerEntry PlayerPassStop(){ 
		TimeSpan elapsed = (DateTime.UtcNow - playerStart);
		GameProfilerEntry entry = new GameProfilerEntry();
		entry.ElapsedTime = elapsed.TotalMilliseconds;
		entry.NoOfObjects = 1;
		return entry;
	}
	
	public override void BallFirstPassStart(){ 
		ballStart = DateTime.UtcNow;
	}
	
	public override GameProfilerEntry BallFirstPassStop(int noOfBalls){ 
		TimeSpan elapsed = (DateTime.UtcNow - ballStart);
		GameProfilerEntry entry = new GameProfilerEntry();
		entry.ElapsedTime = elapsed.TotalMilliseconds;
		entry.NoOfObjects = noOfBalls;
		return entry;
	}
	
	public override void BallSecondPassStart(){ 
		ballStart = DateTime.UtcNow;
	}
	
	public override GameProfilerEntry BallSecondPassStop(int noOfBalls){ 
		TimeSpan elapsed = (DateTime.UtcNow - ballStart);
		GameProfilerEntry entry = new GameProfilerEntry();
		entry.ElapsedTime = elapsed.TotalMilliseconds;
		entry.NoOfObjects = noOfBalls;
		return entry;
	}
}

