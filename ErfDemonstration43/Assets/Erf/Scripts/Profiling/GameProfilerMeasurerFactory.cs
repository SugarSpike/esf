using UnityEngine;
using System.Collections;

public enum GameProfilerMeasurerType {
	StopWatch,
	DateTime
}

public static class GameProfilerMeasurerFactory 
{
	public static GameProfilerMeasurer Build(GameProfilerMeasurerType type)
	{
		GameProfilerMeasurer measurer = null;
		switch (type) {
		case GameProfilerMeasurerType.DateTime:
			measurer = new GameProfilerDateTime();
			break;
		case GameProfilerMeasurerType.StopWatch:
			measurer = new GameProfilerStopWatch();
			break;
		default:
			throw new UnityException("GameProfilerMeasurerFactory::Build type not supported: " + type);
		}
		return measurer;
	}
}

