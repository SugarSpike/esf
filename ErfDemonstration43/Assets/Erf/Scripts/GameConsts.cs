using UnityEngine;
using System.Collections;


public static class GameConsts
{
	public static bool ShowPlayerEmotion = true;
	public static bool ShowBallEmotion = true;
	public static bool ShowTestsPanel = true;

	public static PlayerCharacterModelType PlayerCharacterType = PlayerCharacterModelType.PadMock_Weighted;
	public static bool IsAffectiveMode = true;

	public static bool IsFirstRun = true;

	public static bool GameIsRunning = false;

	public const bool EnableErfLog = false;

	public static string PlayerTag = "Main Camera";

	public const float PlayerCheerRadius = 8.0f;

	public const float AutoAimRadius = 15.0f;

	public static string OccClipsRulesFilePath = @"occ.clp";

	public static string ProfileFilePrefix = "Profiling/profile_result_";

	public static string SensorDataFilePath = @"SensorMockData.txt";

	public const float FeelBallColorChange = 0.05f;

	public const float FeelBallPickTimeout = 4f;

	public const float TimeOfSingleGame = 900; // 900

	public const float Epsilon = 0.001f;

	public const int GameStatsIntervalsNo = 1;
	public const double GameStatsInterval = 0.0f;
}



