﻿using UnityEngine;
using System.Collections;

public class HoopTriggerScript : MonoBehaviour {

	private Vector3 startPoint;
	private Vector3? currentTarget;
	public float HoopPointsModifier = 1.0f;
	public Transform HoopObject;
	public Vector3 TranslationSpace = Vector3.zero;
	public float TranslationSpeed = 1.0f;
	// Use this for initialization
	void Start () { 
		startPoint = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (currentTarget == null)
			return;

		transform.position = Vector3.Slerp(transform.position, currentTarget.Value, Time.deltaTime * TranslationSpeed);
		HoopObject.position = Vector3.Slerp(HoopObject.position, currentTarget.Value, Time.deltaTime * TranslationSpeed);

		Vector3 distance = currentTarget.Value - transform.position;
		if (distance.magnitude < GameConsts.Epsilon) 
			currentTarget = null;
	}

	void OnTriggerEnter(Collider other) {
		FeelBall ball = FeelBallsManager.Instance.GetFeelBall (other.gameObject.name);
		if (ball.PickTimeout > 0.0f) {
			ball.WentThroughHoop (HoopPointsModifier);
			GameManager.Instance.BallsThroughtHoop ++;
			GameManager.Instance.BallsThroughByHoop[this.gameObject.name]++;
		}
	}
	 
	void OnTriggerExit(Collider other) {
		if (TranslationSpace == Vector3.zero)
			return;

		if(GameConsts.IsAffectiveMode && PlayerModel.Instance.DistressDominates)
			currentTarget = GameUtils.GetRandomPoint (TranslationSpace/2, startPoint);
		else
			currentTarget = GameUtils.GetRandomPoint (TranslationSpace/2, startPoint);
	}
}
