using UnityEngine;
using System.Collections;
using Erf;



public enum CurrentPage
{
	MainPage,
	PlayerModelPage,
	ProfilingPage,
	TestsPage,
	StatsPage,
	UnknownPage
}

public partial class GuiHolderScript : MonoBehaviour {

	private CurrentPage currentPage;

	int ProcessInput()
	{
		int currentPageIndex = -1;
		int num;
		if (Event.current.type == EventType.KeyDown) {
			// Convert to numeric value for convenience :)
			num = Event.current.keyCode - KeyCode.Alpha1 + 1;
			
			// Avoid having multiple number keys pressed!
			if (currentPageIndex == -1 && num >= 0 && num <= 9) {
				currentPageIndex = num;
				Event.current.Use();
			}
		}
		else if (Event.current.type == EventType.KeyUp) {
			num = Event.current.keyCode - KeyCode.Alpha1 + 1;
			if (currentPageIndex == num) {
				currentPageIndex = 0;
				Event.current.Use();
			}
		}
		return currentPageIndex;
	}

	public void ChangePage(int option)
	{
		if (option < 0)
			return;

		CurrentPage page = CurrentPage.UnknownPage;

		if (currentPage == CurrentPage.MainPage) {
			page = 
				option == 1 ? CurrentPage.PlayerModelPage : 
				option == 2 ? CurrentPage.ProfilingPage : 
				option == 3 ? CurrentPage.TestsPage : 
				option == 4 ? CurrentPage.StatsPage : 
				CurrentPage.UnknownPage;
		}
		else if (currentPage == CurrentPage.PlayerModelPage) {
			if(option == 0)
			{
				page = CurrentPage.MainPage;
			}
			else if(option ==1)
			{
				PlayerModel.Instance.ChangeCharacterModel(PlayerCharacterModelType.PadMock);
			}
			else if(option ==2)
			{
				PlayerModel.Instance.ChangeCharacterModel(PlayerCharacterModelType.MouseOcc);
			}
			else if(option ==3)
			{
				PlayerModel.Instance.ChangeCharacterModel(PlayerCharacterModelType.PadMock_Weighted);
			}
		}
		else if (currentPage == CurrentPage.ProfilingPage) {
			if(option == 0)
			{
				page = CurrentPage.MainPage;
			}
			else if(option ==1)
			{
				GameProfiler.Instance.SaveMeasurmentOnGameEnd = 
					!GameProfiler.Instance.SaveMeasurmentOnGameEnd;
			}
			else if(option ==2)
			{
				GameProfiler.Instance.ProfilingEnabled = 
					!GameProfiler.Instance.ProfilingEnabled;
			}
			else if(option ==3);
			{
				if(GameProfiler.Instance.ProfilingEnabled)
					GameProfiler.Instance.SaveMeasurmentsToFile();
			}
		}
		else if (currentPage == CurrentPage.TestsPage) {
			if(option == 0)
			{
				page = CurrentPage.MainPage;
			}
			else if(option ==1) 
			{
				GameProfiler.Instance.TestPlayerModel(10);
			}
			else if(option ==2) 
			{
				GameProfiler.Instance.TestPlayerModel(100);
			}
			else if(option ==3) 
			{
				GameProfiler.Instance.TestPlayerModel(1000);
			}
		}
		else if (currentPage == CurrentPage.StatsPage) {
			if(option == 0)
			{
				page = CurrentPage.MainPage;
			}
		}

		if ( page.Equals(CurrentPage.UnknownPage))
			return;

		this.currentPage = page;
	}


	private void RenderTestPanel()
	{
		switch (currentPage) {
		case CurrentPage.PlayerModelPage:
			GUI.Box (
				new Rect (0, 15, 250, 100), 
				string.Format( "Player Model: {0}\n" +
			              "0) Return\n" +
			              "1) Change to PAD Mock\n" +
			              "2) Change to OCC Mouse\n" + 
			              "3) Change to PAD Mock Weighted\n",
			              GameConsts.PlayerCharacterType));
			break;
		case CurrentPage.ProfilingPage:
			string enableDisableSaveOnExit = GameProfiler.Instance.SaveMeasurmentOnGameEnd?
				"1) Disable save on game end\n" : "1) Enable save on game end\n" ;
			if(GameProfiler.Instance.ProfilingEnabled)
			{
				GUI.Box (
					new Rect (0, 15, 500, 100), 
					string.Format( "Profiling (enabled):\n" +
				              "0) Return\n" +
				              enableDisableSaveOnExit +
				              "2) Disable data gathering\n" +
				              "3) Save current data to file\n"+
				              GameUtils.GetApplicationPath()));
			}
			else
			{
				GUI.Box (
					new Rect (0, 15, 200, 100), 
					string.Format( "Profiling (disabled):\n" +
				              "0) Return\n" +
				              enableDisableSaveOnExit +
				              "2) Enable data gathering\n" 
				              ));
			}
			break;
		case CurrentPage.TestsPage:
			GUI.Box (
				new Rect (0, 15, 150, 100), 
				string.Format( "Tests:\n" +
			              "0) Return\n" +
			              "1) 10\n" +
			              "2) 100\n" +
			              "3) 1000"));
			break;
		case CurrentPage.StatsPage:
			string gameStats = "";
			foreach(GameStats stats in GameManager.Instance.GameStats)
			{
				gameStats += string.Format("M: {0}, D: {1},S: {2}\n", stats.MissedBalls,stats.DroppedBalls,stats.ScoredBalls);
			}

			GUI.Box (
				new Rect (0, 15, 200, 200), 
				string.Format( "Stats:\n" +
			              gameStats +
			              "0) Return\n"));
			break;
		default:
			GUI.Box (
				new Rect (0, 15, 150, 100), 
				string.Format( "Configuration:\n" +
			              "1) Change Player Model\n" +
			              "2) Profiling\n" +
			              "3) Tests Suite\n" +
			              "4) Stats"));
			break;
		}
	}
	
	
}

