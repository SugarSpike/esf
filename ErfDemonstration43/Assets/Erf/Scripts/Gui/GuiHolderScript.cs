﻿using UnityEngine;
using System.Collections;
using Erf;

public partial class GuiHolderScript : MonoBehaviour {
	

	float PanelWidth = 200;
	float HeaderHeight = 40;
	float PanelHeight = 110;
	float linesHeight = 25;

	float EndGamePanelWidth = 180;
	float EndGamePanelHeight = 80;
	float FirstGamePanelWidth = 420;
	float FirstGamePanelHeight = 230;

	int counter = 0;

	// Use this for initialization
	void Start () {

	}

	void OnGUI()
	{
		PlayerModel playerModel = PlayerModel.Instance;
		if (!playerModel.IsReady ())
			return;


		EmotionVector current = playerModel.CurrentVector;
		if (current != null && GameConsts.ShowPlayerEmotion) {
			RenderPlayerInformation(playerModel, current);
		}
		if (FeelBallsManager.Instance.SelectedFeelBall != null) {
			FeelBall selectedBall = FeelBallsManager.Instance.SelectedFeelBall;
			if(GameConsts.ShowBallEmotion)
				RenderBallInformation(selectedBall);
			RenderSliders(100.0f - selectedBall.ThrowPower,
			              100.0f - ((selectedBall.HoldTimeLeft / selectedBall.StartingHoldTime)*100.0f));
		}

		//Draw current time left
		if (GameConsts.GameIsRunning) {
			GUI.Box (new Rect (Screen.width / 2 - 25, 0, 50, 25), GameManager.Instance.MinutesLeft + ":" + GameManager.Instance.SecondsLeft);
			GUI.Box (new Rect (Screen.width / 2 - 25, 25, 50, 25), GameManager.Instance.CurrentScore.ToString ());
		
			if (GameConsts.ShowTestsPanel && GameConsts.IsAffectiveMode){
				ChangePage(ProcessInput());
				RenderTestPanel();
			}
		
		} else {

			if(!GameConsts.IsFirstRun)
			{
				GUI.Box (new Rect (Screen.width / 2 - (EndGamePanelWidth/2), 10, EndGamePanelWidth, EndGamePanelHeight), 
				         "");
				GUI.Box (new Rect (Screen.width / 2 - (EndGamePanelWidth/2), 10, EndGamePanelWidth, EndGamePanelHeight), 
				         "");
				GUI.Box (new Rect (Screen.width / 2 - (EndGamePanelWidth/2), 10, EndGamePanelWidth, EndGamePanelHeight), 
				         string.Format("The game has ended!\nFinal score: {0}\nBalls through hoop: {1}\nCongratulations!",GameManager.Instance.CurrentScore,
				              GameManager.Instance.BallsThroughtHoop));
			}

			GUI.Box (new Rect (Screen.width / 2 - (FirstGamePanelWidth/2), Screen.height / 2 - (FirstGamePanelHeight/2), FirstGamePanelWidth, FirstGamePanelHeight),"");
			GUI.Box (new Rect (Screen.width / 2 - (FirstGamePanelWidth/2), Screen.height / 2 - (FirstGamePanelHeight/2), FirstGamePanelWidth, FirstGamePanelHeight), 
			         string.Format(
				"Throw as many balls through a hoop as you can within the time limit!\n\n" +
				"Controls:\n" +
				"Left mouse button: pick up a ball\n" +
				"Right mouse button: throw the ball\n" +
				"Holding middle mouse button: Rotate the camera\n" +
				"Escape: Drop the ball\n\n" +
				"Time holding the ball is limited (indicated by a bar on the right)\n" +
				"The strength of the throw is indicated by a powerslider on the right\n\n" +
		         	"Choose game mode:"));

			if (GUI.Button (new Rect ((Screen.width - FirstGamePanelWidth)/2, 
			                          Screen.height / 2 + (FirstGamePanelHeight/4)+20, 
			                          FirstGamePanelWidth/2, 30), 
			                "Affective"
			                ))
			{
				GameManager.Instance.ResetGame();
				GameConsts.IsFirstRun = false;
				GameConsts.IsAffectiveMode = true;
			}
			if (GUI.Button (new Rect ((Screen.width - FirstGamePanelWidth + FirstGamePanelWidth)/2, 
			                          Screen.height / 2 + (FirstGamePanelHeight/4)+20, 
			                          FirstGamePanelWidth/2, 30), 
			                "Non-Affective"
			                ))
			{
				GameManager.Instance.ResetGame();
				GameConsts.IsFirstRun = false;
				GameConsts.IsAffectiveMode = false;
			}
		}
	}

	


	// Update is called once per frame
	void Update () {
	
	}
}
