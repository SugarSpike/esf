﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class GameUtils {

	private static System.Random rand = new System.Random ();

	public static string GetApplicationPath()
	{
		string path = Application.dataPath;
		if (Application.platform == RuntimePlatform.OSXPlayer) {
			path += "/../../";
		} else if (Application.platform == RuntimePlatform.WindowsPlayer) {
			path += "/../";
		} else {
			path += "/";
		}
		if (!path.EndsWith ("/"))
			path += "/";

		return path;
	}

	public static bool IsGrounded(GameObject actor) {
		float distToGround = actor.collider.bounds.extents.y;
		return Physics.Raycast(actor.transform.position, -Vector3.up, distToGround + 0.1f);
	}

	public static float Clamp(float toClamp, float min, float max)
	{
		return toClamp < min ? min : toClamp > max ? max : toClamp;
	}

	public static float GetRandomNumber(double minimum, double maximum)
	{
		return (float)(rand.NextDouble() * (maximum - minimum) + minimum);
	}
	
	public static float GetRandomNumber(float minimum, float maximum)
	{
		if (minimum == maximum)
			return minimum;

		return (float)(rand.NextDouble() * (maximum - minimum) + minimum);
	}

	public static Vector3 GetRandomPoint(Vector3 space, Vector3 offset)
	{
		Vector3 randomPoint = new Vector3 (
			GetRandomNumber (-space.x, space.x),
			GetRandomNumber (-space.y, space.y),
			GetRandomNumber (-space.z, space.z));
		return randomPoint + offset;
	}

	public static List<GameObject> GetHoops()
	{
		List<GameObject> hoops = new List<GameObject> ();
		object[] obj = GameObject.FindObjectsOfType(typeof (GameObject));
		foreach (object o in obj)
		{
			GameObject go = (GameObject) o;
			foreach(Component comp in go.GetComponents<Component>())
			{
				if(comp is HoopTriggerScript)
				{
					hoops.Add(go);
					break;
				}
			}
			
		}
		return hoops;
	}
}
