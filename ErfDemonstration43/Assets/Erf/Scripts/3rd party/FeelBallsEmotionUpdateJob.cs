using System;
using System.Collections.Generic;
using Erf;

public class FeelBallsEmotionUpdateJob : ThreadedJob
{
	public List<FeelBall> FeelBallsToUpdate;
	private Dictionary<FeelBall,bool> UpdateResults;
	public EecEvent RaisedEecEvent;

	public FeelBallsEmotionUpdateJob ()
	{
		FeelBallsToUpdate = new List<FeelBall>();
		UpdateResults = new Dictionary<FeelBall,bool> ();
	}

	protected override void ThreadFunction()
	{
		
		GameProfiler.Instance.BallFirstPassStart ();
		foreach (FeelBall ball in FeelBallsToUpdate) {
			UpdateResults[ball] = ball.HandleEecEvent(RaisedEecEvent);
		}
		GameProfiler.Instance.BallFirstPassStop(FeelBallsToUpdate.Count);
	}
	protected override void OnFinished()
	{
		
		GameProfiler.Instance.BallSecondPassStart ();
		foreach(KeyValuePair<FeelBall, bool> ballUpdate in UpdateResults)
		{
			ballUpdate.Key.AfterEmotionUpdate(ballUpdate.Value);
		}
		GameProfiler.Instance.BallSecondPassStop (FeelBallsToUpdate.Count);
	}
}

