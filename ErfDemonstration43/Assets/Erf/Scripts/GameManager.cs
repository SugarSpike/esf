using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameStats {
	public GameStats() {
		MissedBalls = 0;
		DroppedBalls = 0;
		ScoredBalls = 0;
	}

	public int MissedBalls {
		get;
		set;
	}
	public int DroppedBalls {
		get;
		set;
	}
	public int ScoredBalls {
		get;
		set;
	}
}

public class GameManager : Singleton<GameManager> {

	private float lastIntervalChanged;
	private int currentIntervalIndex;
	private List<GameStats> gameStats;

	public List<GameStats> GameStats {
		get { return gameStats; }
	}

	public float TimeLeft {
		get;
		set;
	}

	public string MinutesLeft {
		get;
		set;
	}

	public string SecondsLeft {
		get;
		set;
	}

	public int CurrentScore {
		get;
		set;
	}
	
	public int BallsThroughtHoop {
		get;
		set;
	}

	public Dictionary<string,int> BallsThroughByHoop;

	public GameManager() {
		currentIntervalIndex = 0;
		lastIntervalChanged = 0.0f;
		gameStats = new List<GameStats> ();
		BallsThroughByHoop = new Dictionary<string, int> ();
		for (int i = 0; i < GameConsts.GameStatsIntervalsNo; i++)
			gameStats.Add (new GameStats ());
	}

	public void AddDroppedBallStat()
	{
		gameStats [currentIntervalIndex].DroppedBalls++;
	}

	public void AddMissedBallStat()
	{
		gameStats [currentIntervalIndex].MissedBalls++;
	}

	public void AddScoredBallStat()
	{
		gameStats [currentIntervalIndex].ScoredBalls++;
	}

	private void SetGameStatsIndex()
	{
		int indexIncrement = (int)(lastIntervalChanged / GameConsts.GameStatsInterval + 0.5);
		for (int i = 1; i <= indexIncrement; i++) {
			currentIntervalIndex = (currentIntervalIndex+i)%GameConsts.GameStatsIntervalsNo;
			gameStats[currentIntervalIndex] = new GameStats();
		}
		lastIntervalChanged = 0.0f;
	}

	public void ResetGame()
	{
		TimeLeft = GameConsts.TimeOfSingleGame;
		CurrentScore = 0;
		BallsThroughtHoop = 0;
		GameConsts.GameIsRunning = true;
		Time.timeScale = 1.0f;
		foreach (FeelBall ball in FeelBallsManager.Instance.FeelBalls)
			ball.RespawnBall ();
		BallsThroughByHoop.Clear ();
		foreach (GameObject hoop in GameUtils.GetHoops())
			BallsThroughByHoop.Add (hoop.name, 0);
		gameStats.Clear ();
		for (int i = 0; i < GameConsts.GameStatsIntervalsNo; i++)
			gameStats.Add (new GameStats ());
	}

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!GameConsts.GameIsRunning)
			return;

		TimeLeft -= Time.deltaTime;
		if (TimeLeft <= 0.0f) {
			EndGame ();
			return;
		}
		float minutes = Mathf.Floor (TimeLeft / 60);
		int seconds = Mathf.RoundToInt (TimeLeft % 60);
		if (seconds == 60) {
			seconds = 0;
			minutes++;
		}

		MinutesLeft = minutes.ToString ();
		SecondsLeft = seconds.ToString ();
		if (minutes < 10) {
			MinutesLeft = "0" + MinutesLeft;
		}
		if (seconds < 10) {
			SecondsLeft = "0" + SecondsLeft;
		}
		//lastIntervalChanged += Time.deltaTime;
		//SetGameStatsIndex ();
	}

	public void EndGame()
	{
		TimeLeft = 0.0f;
		Time.timeScale = 0.0f;
		FeelBallsManager.Instance.SelectedFeelBall = null;
		GameConsts.GameIsRunning = false;
		if (GameProfiler.Instance.SaveMeasurmentOnGameEnd)
			GameProfiler.Instance.SaveMeasurmentsToFile ();
		GameProfiler.Instance.SaveGameScoreToFile (CurrentScore, BallsThroughtHoop, BallsThroughByHoop);
	}
}

