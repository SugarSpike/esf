package pl.gda.pg.isi.erf;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;

import pl.gda.pg.isi.erf.EnumConstants.OccEmotions;

public class ErfCoreTestMain {

	private static final String LIB_LOCATION = System.getProperty("user.dir",
			"/Studia/MGR/SourceCode/Libraries/Erf/");
	
	static {
		try {
			System.err.println(System.getProperty("java.library.path"));
			addPath(LIB_LOCATION);
			System.loadLibrary("ErfCore");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	public static void addPath(String s) throws Exception {
	    File f = new File(s);
	    URI u = f.toURI();
	    URLClassLoader urlClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
	    Class<URLClassLoader> urlClass = URLClassLoader.class;
	    Method method = urlClass.getDeclaredMethod("addURL", new Class[]{URL.class});
	    method.setAccessible(true);
	    method.invoke(urlClassLoader, new Object[]{u.toURL()});
	}
	

	public static void main(String[] args) {
		try {
			System.out.println("Working Directory = "
					+ System.getProperty("user.dir"));
			ErfContext context = ErfContext.GetInstance();

			System.out.println("Initializing ERF for java demo");
			ErfLogger.SetLoggingEnabled(true);
			ErfLogger.SetAllStreamsToLoggingFile("d://debug.txt");
			ExternalComponentLibrary library = context
					.LoadComponentLibrary(LIB_LOCATION
							+ "ExternalComponentLibrary.dll");
			if (library == null) {
				System.err
						.println("Could not load ExternalComponentLibrary library");
				return;
			}

			library = ErfContext.GetInstance().LoadComponentLibrary(
					LIB_LOCATION + "MSIBallErf.dll");
			if (library == null) {
				System.err.println("Could not load MSIBallErf library");
				return;
			}

			ExternalExpert msiBallEmotionModel = ErfContext.GetInstance()
					.FindExpert("MSIBallEmotionModel");
			CharacterModel playerModel = new CharacterModel();
			playerModel.RegisterExpert(msiBallEmotionModel);

			EecEvent timeElapsedEvent = new EecEvent(
					GameEvent.STD_TIME_ELAPSED.swigValue());
			timeElapsedEvent.AddValue("NO_OF_COLLISIONS", Variant.Create(2));
			timeElapsedEvent.AddValue("CURRENT_SPEED", Variant.Create(7.0f));

			playerModel.HandleEvent(timeElapsedEvent);

			float fear = playerModel.GetEmotionVector()
					.GetValue(OccEmotions.FEAR).AsFloat();

			if (fear > 0.0f)
				System.out.println("FIRST I WAS AFRAID, I WAS PETRIFIED!");
			else
				System.out.println("I AM FEARLESS! I AM DEATH! I AM FIRE!");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
